% An example of usage of FWM_VLFTRANSMITTER
% Usage: script_instance=1; fwm_vlftransmitter_example
global backup_interval output_interval
backup_interval=3600; % 1 hour
output_interval=300; % 5 minutes
vlftransmitters;
switch script_instance
    case 1
        standardargs={NML,'iri',[-1280:5:1280],[150 700],'NML/Geomagnetic/',...
            'year',2008,'geomagnetic_coordinates',1};
        %% Do the stuff without D-region/Coulomb collisions first
        for wintertime=1:1
            for daytime=0:1
                commonargs={standardargs{:},'daytime',daytime,'month',6*(wintertime+1)};
                fwm_vlftransmitter(commonargs{:});
            end
        end
        %% With the extras
        for wintertime=0:1
            for daytime=0:1
                commonargs={standardargs{:},'daytime',daytime,'month',6*(wintertime+1)};
                fwm_vlftransmitter(commonargs{:},'D-region',1);
            end
        end
    case 2
        standardargs={NML,'iri',[-1280:5:1280],[150 700],'NML/Geographic/','year',2008};
        %% The new version, in geographic coordinates
        for wintertime=0:0
            for daytime=0:0
                commonargs={standardargs{:},'daytime',daytime,'month',6*(wintertime+1)};
                fwm_vlftransmitter(commonargs{:});
            end
        end
        %% With the extras
%         for wintertime=0:1
%             for daytime=0:1
%                 commonargs={standardargs{:},'daytime',daytime,'month',6*(wintertime+1)};
%                 fwm_vlftransmitter(commonargs{:},'Coulomb_collisions',1);
%             end
%         end
end

%% NAA run
% dir is MATLAB/fwm
fwm_vlftransmitter(NAA,'iri',[-1280:5:1280],[150 660],'NAA/','year',2008,'daytime',0,'month',6);

%% Modified NAA, to take a different frequency
NAA17=NAA;
NAA17.name='NAA17';
NAA17.f=17800;
fwm_vlftransmitter(NAA17,'iri',[-1280:5:1280],[150 660],'NAA/','year',2008,'daytime',0,'month',6);
