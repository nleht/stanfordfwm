% Example of the fwm_nonaxisymmetric usage
global clight eps0 mu0 impedance0
if isempty(clight)
    loadconstants
end
h=[0 0.001 50:120].'; % altitudes in km
% We place the source at 1 m altitude, so that the field on the ground is
% calculated more accurately
f=5000; % frequency in Hz
w=2*pi*f; k0=w/clight;
%Bgeo=[0 0 -5e-5]; % The NONVERTICAL geomagnetic field
Bgeo=[0 1e-5 -4e-5]
Ne=getNe(h,'Stanford_eprof1'); % Electron density in 1/m^3
Ne(1:3)=0; % The ionosphere starts at 51 km, at 50 km it is still zero.
perm=get_perm(h,w,'Ne',Ne,'Bgeo',Bgeo);
sground=30e-3; % conductivity of the ground in S/m
%eground=1+i*sground/(eps0*w);
eground='E=0';

% Specify the current I in nperp=(nx,ny) space
% For a point source, the current is a constant in (nx,ny) space
np0=[]; % The points at which the current is given, =[] for point source
I0=[0;0;1;0;0;0]; % vertical current of moment = 1 A*m
ksa=[2]; % it is placed at h(ksa);
m=0; % the circular harmonic, Ir,Iphi,Iz(nx,ny)~exp(i*m*th)

% Output points
hi=[0;max(h)].'; % output altitudes in km
%rkm=[0:2000]; % output distances in km
xkmi=[-500:5:500];
ykmi=xkmi;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Main program call:
global output_interval backup_interval
output_interval=60 % To observe the progress every 60 sec
backup_interval=600 % Default = 3600 sec
bkp_file='antennaground';
% The arguments are:
%rperp1=xkmi; rperp2=ykmi;
%coornperp=1; nperp1=[]; nperp2=[]; coorrperp=1;
retol=1e-4; drkm=[]; rmaxkm=[];
[EH,calculation_plan,npe_struct,waves_struct]=fwm_nonaxisymmetric(f,h,eground,perm,...
    ksa,1,[],[],I0,...
    1,xkmi,ykmi,hi,...
    rmaxkm,drkm,retol,bkp_file);

%%
tmp=load([bkp_file '_FWM_ASSEMBLE_XY.mat']);
EH=tmp.EH;
%[EH,np,EHn,npb,plan]=fwm_axisymmetric(f,h,eground,perm,ksa,np0,m,I0,rkm,hi);
% We could also call the wraper, fwm_axisymmetric_antenna.
% The result is in the same units as I0. To convert to V/m, multiply by
% Z0:
E=impedance0*EH(1:3,:,:,:);
% The magnetic field is in the same units as E. Convert to SI units:
H=EH(4:6,:,:,:);
B=mu0*H;

%% Plotting
Nyc=(length(xkmi)+1)/2
for ki=1:2
    %ki=1; % on the ground
    %ki=2; % at 120 km
    figure;
    subplot(2,1,1);
    semilogy(xkmi,squeeze(abs(E(:,ki,:,Nyc)))); legend('E_x','E_y','E_z')
    title(['E, V/m at ' num2str(hi(ki)) ' km']);
    xlabel('x, km')
    subplot(2,1,2);
    semilogy(xkmi,squeeze(abs(B(:,ki,:,Nyc)))); legend('B_x','B_y','B_z')
    title(['B, T at ' num2str(hi(ki)) ' km']);
    xlabel('x, km')
end

%% The power flux at the upper boundary
S=0.5*real(cross(conj(E(:,2,:,:)),H(:,2,:,:)));
Sz=squeeze(S(3,:,:,:));
figure;
imagesc(xkmi,ykmi,log10(Sz.'+abs(max(Sz(:)))*1e-8));
set(gca,'ydir','normal','fontsize',14); axis equal tight;
c=colorbar;
set(c,'fontsize',14);
set(get(c,'title'),'string','log_{10}S_z, W/m^2','fontsize',14);
title(['Upward power flux at h=' num2str(max(h)) ' km for I=1 A-m'])
xlabel('x, km (geomag. W-E)')
ylabel('y, km (geomag. S-N)')
%% Check the field phase
figure;
imagesc(xkmi,ykmi,angle(squeeze(E(1,2,:,:)).'));
set(gca,'ydir','normal','fontsize',14); axis equal tight;
c=colorbar;
set(c,'fontsize',14);
set(get(c,'title'),'string','V/m','fontsize',14);
title(['Phase of Ex at h=' num2str(max(h)) ' km for I=1 A-m'])
xlabel('x, km (geomag. W-E)')
ylabel('y, km (geomag. S-N)')

%% Power flux in k-vector domain
tmp=load([bkp_file '_FWM_WAVES.mat']);
Sf=0.5*impedance0*real(cross(conj(tmp.EHn(1:3,2,:)),tmp.EHn(4:6,2,:)));
Sfz=squeeze(Sf(3,:,:)); Sfz=Sfz(:).'; % coerce to the same size as tmp.da
figure;
pointillism(tmp.nx,tmp.ny,log10(Sfz+abs(max(Sfz(:)))*1e-8));

%% A figure which is better for printing
% Interpolate in (k,chi) space
Np=length(tmp.np);
Nchi0=tmp.Nphi(Np);
chi0=[0:Nchi0-1]/Nchi0*2*pi;
i2=cumsum(tmp.Nphi); i1=[1 i2(1:Np-1)+1];
Sfz0=zeros(Np,Nchi0);
for kp=1:Np
    Nchi=tmp.Nphi(kp);
    chi=[0:Nchi]*2*pi/Nchi;
    Sfz0(kp,:)=interp1(chi,[Sfz(i1(kp):i2(kp)) Sfz(i1(kp))],chi0);
end
% Integrate over k
dnp=diff(tmp.npb);
Sk=cumsum(Sfz0.*repmat(dnp,[1 Nchi0]),1);
%
dnx=0.002;
dny=0.002;
nmax=1.5;
nx=[-nmax:dnx:nmax]; Nnx=length(nx);
ny=[-nmax:dny:nmax]; Nny=length(ny);
[nxm,nym]=ndgrid(nx,ny);
chim=mod(atan2(nym,nxm),2*pi);
nm=sqrt(nxm.^2+nym.^2);
Skp=interp2(tmp.np,[chi0 2*pi],[Sk Sk(:,1)].',nm+dnx/2,chim);
Skm=interp2(tmp.np,[chi0 2*pi],[Sk Sk(:,1)].',nm-dnx/2,chim);
Ska=(Skp-Skm)/dnx;
Ska(isnan(Ska))=0;
%
figure;
imagesc(nx,ny,log10(Ska+max(Ska(:))*1e-8).');
set(gca,'ydir','normal','fontsize',14); axis equal tight
xlabel('n_x=k_x/k_0 (k_0=w/c)'); ylabel('n_y=k_y/k_0')
title(['Upward power flux at h=' num2str(max(h)) ' km in k_\perp-space for I=1 A-m'])
c=colorbar;
set(c,'fontsize',14);
set(get(c,'title'),'string','log_{10}S_z, W/(unit n^2)','fontsize',14);

