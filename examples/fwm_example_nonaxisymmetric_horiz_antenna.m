% Example of the fwm_nonaxisymmetric usage, with a horizontal long dipole antenna
%
% To start with let's run an example for a location at HE Holt (NWC).  The
% antenna will be a half-wave dipole with a sinusoidal current distribution
% located at an elevation of 50 meters above ground.  Use one amp input
% current.  The ground conductivity should be 1e-3 S/m with a relative
% dielectric constant of 4.  Use the operational frequency at NWC (19.8 kHz)
% so we can compare the results with Morris's calculations.
%
% The important result is the ratio of power flowing into the magnetosphere to
% the total radiated power.  For the horizontal antenna the total radiated
% power has to be calculated by integrating the Poynting vector.
% 
% How about doing the calculation for a magnetically N-S antenna and an E-W
% antenna.

%% Options
do_total_energy=false; % we are interested only in the field in the ionosphere
%do_total_energy=true; % longer calculation for total radiated energy
do_ionosphere=true; % false for no ionosphere
do_supercond=1; % see lines 74--83
kpol=1; % kpol==1 is x, 2 is y, 3 is z (vertical). [Hidden feature: kpol==4,5,6 are magnetic dipole antennas]
do_point_source=false; % false for half-wavelength antenna

%% Options which are usually fixed
make_I_profile_plot=false; % false, unless you want to plot it
Ne_profile='HAARPsummernight';
hionstart=80; % where the ionosphere starts, radiation before transmission
hmax=135; % at this altitude we calculate the transmitted radiation
eground_real=4;
sground=1e-3;

%% Check the options
if ~do_point_source
    if kpol==1
        phAnt=0;
    elseif kpol==2
        phAnt=pi/2;
    else
        error('cannot model z-distributed source')
    end
end
if do_total_energy && (kpol==3 || kpol==6)
    disp('Sorry, the total input energy cannot be calculated for vertical antenna')
    error('a different algorithm needed, cannot use E*J');
end

%% Parameters
directions='xyzuvw'; % the directions 4-6 are magnetic currents
global clight eps0 mu0 impedance0
if isempty(clight)
    loadconstants
end
% Load NWC parameters
vlftransmitters
f=NWC.f % frequency in Hz
P0=NWC.P0 % Radiated power in W
w=2*pi*f; k0=w/clight;
Bgeo=NWC.Bgeo % The NONVERTICAL geomagnetic field, in (y,z) plane
hsource=10; % was: 0.05; % in km
if do_ionosphere
    h=[0 hsource hionstart:hmax].'; % altitudes in km, antenna is at 50 m above ground
    % Ionosphere (could also use IRI)
    Ne=getNe(h,Ne_profile); % Electron density in 1/m^3
    Ne(1:2)=0;
    % Permittivity tensor
    perm=get_perm(h,w,'Ne',Ne,'Bgeo',Bgeo);
    perm(:,:,3)=(1+1e-6i)*eye(3); % non-ionosphere, but need to differ from vacuum for technical reasons
else
    h=[0 hsource hmax].';
    perm=repmat(eye(3),[1 1 3]);
    perm(:,:,3)=(1+1e-6i)*eye(3); % non-ionosphere, but need to differ from vacuum for technical reasons
end
if do_supercond==1
    eground='E=0'; % ideally conducting ground
elseif do_supercond==-1 % ideally magnetically conducting "ground"
    eground='H=0';
elseif do_supercond==0
    eground=eground_real+1i*sground/(eps0*w); % The specified ground parameters
    %eground=4+1e-8i;
elseif do_supercond==2 % no ground
    eground='free';
end
eground
if do_supercond==0
    disp(['Expected max n=' num2str(sqrt(abs(eground)))]);
end

%% Specify the current I in nperp=(nx,ny) space
if do_point_source
    bkp_file='pnt';
else
    bkp_file='hfw'; % halfwave
end
bkp_file=[bkp_file '_' directions(kpol)];
if do_supercond==1
    bkp_file=[bkp_file '_idl'];
elseif do_supercond==-1
    bkp_file=[bkp_file '_mir']; % "mirror", plasma with eps=0 or H=0
elseif do_supercond==0
    bkp_file=[bkp_file '_cnd'];
elseif do_supercond==2
    bkp_file=[bkp_file '_fre']; % free b.c.
end
if do_ionosphere
    bkp_file=[bkp_file '_ion'];
else
    bkp_file=[bkp_file '_vac'];
end
I00=zeros(6,1);
I00(kpol)=1;
if do_point_source
    drkm = 0.1;
    disp(['Horizontal size of the antenna is ' num2str(drkm*1e3) ' m'])
    if 1
        % For a point source, the current is a constant in (nx,ny) space
        nx0=[]; ny0=[]; % The points at which the current is given, =[] for point source
        I0=I00; % horizontal current of moment = 1 A*m
        Il=1;
    else
        nx0=[-200:1:200]; ny0=nx0;
%         if kpol==3
            I0prof=ones(length(nx0),length(ny0));
%         else
%             % For a horizontal antenna, we have a difficulty converging at
%             % large nperp
%             nlimit = 1/(drkm*1e3*k0);
%             [nxm,nym]=ndgrid(nx0,ny0);
%             I0prof = exp(-(nxm.^2+nym.^2)/(2*nlimit^2));
%         end
        %I0=repmat(I00,[1 1 length(nx0) length(ny0)]); % (6 x Ms x Nnx0 x Nny0)
        I0=bsxfun(@times,I00,shiftdim(I0prof,-2)); % (6 x Ms x Nnx0 x Nny0)
        Il=1;
    end
else
    drkm = 0;
    % Half-wavelength with amplitude k0/2 or total current moment I*l=1 A-m
    % Distributed current, will be interpolated in (nx,ny) space
    nx0=[-41:.02:41]; ny0=nx0;
    [nxm,nym]=ndgrid(nx0,ny0);
    % Directions parallel and perpendicular to the current
    npar=nxm*cos(phAnt)+nym*sin(phAnt);
    nper=-nxm*sin(phAnt)+nym*cos(phAnt);
    % Half-wavelength dipole with cosine distribution, FT along axis
    %I0prof=(1/2)*(sin((npar-1)*pi/2)./(npar-1)+sin((npar+1)*pi/2)./(npar+1));
    I0prof=(pi/4)*(sinc((npar-1)/2)+sinc((npar+1)/2));
    I0prof=dedenormal(I0prof,1);
    %I0prof=I0prof.*exp(-(nxm.^2+nym.^2)/(2*30^2));
    % - Only needed for ground calculations
    %% Double-check
    figure;
    %subplot(1,2,1);
    imagesc(nx0,ny0,real(I0prof).'); axis xy equal tight; colorbar;
    set(gca,'fontsize',14);
    title(['I_' directions(kpol) '(nx,ny)']); xlabel('n_x'); ylabel('n_y');
    if make_I_profile_plot
        echo_print({'EPS','PNG'},['pictures/I_hfw_' directions(kpol)]);
    end
    %subplot(1,2,2); imagesc(nx0,ny0,imag(I0prof).'); axis xy equal tight; colorbar;
    %%
    I0=bsxfun(@times,I00,shiftdim(I0prof,-2)); % (6 x Ms x Nnx0 x Nny0)
    Il=1; % Total current moment in lambda/2 dipole (for amplitude = k0/2)
end
Pvert=impedance0*(k0*Il)^2/6/pi % Equivalent power from vertical dipole over superconducting surface
ksa=[2]; % it is placed at h(ksa);

%% Output points
if do_total_energy
    hi=[0 hsource hionstart hmax].'; % output altitudes in km
    % Since we are going to higher nperp, then compensate the calculation
    % time by increasing dn by decreasing rmax, go to smaller distances
    if do_point_source
        xkmi=[-20:2:20];
    else
        xkmi=[-200:2:200];
    end
else
    hi=[0 hionstart hmax].';
    % Go to higher distances
    xkmi=[-500:5:500];
end;
ykmi=xkmi;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Main program call:
global output_interval backup_interval
output_interval=60 % To observe the progress every 60 sec
backup_interval=1200 % Default = 3600 sec
% The arguments are:
coornperp=1; nperp1=nx0; nperp2=ny0;
coorrperp=1; rperp1=xkmi; rperp2=ykmi;
retol=1e-2; %retol=1e-4;
% drkm=0; % Is given above % was: 0
rmaxkm=[];
bkp_file=[bkp_file '_err' num2str(-log10(retol))];
% Used as defaults, here for debugging into fwm_nonaxisymmetric
chi0=0; new_last_step=''; do_exact_if_possible=false;
if do_total_energy
    no_axisymmetric_extension=true;
    if do_point_source
        np0maxgiven=100;
    else
        np0maxgiven=40;
    end
    bkp_file=[bkp_file '_npmax' num2str(np0maxgiven)];
else
    no_axisymmetric_extension=true;
    np0maxgiven=[];
end
%%
[EH,extras_struct,npe_struct,waves_struct]=fwm_nonaxisymmetric(...
    f, h, eground, perm,...
    ksa, coornperp, nperp1, nperp2, I0, ...
    coorrperp, rperp1, rperp2, hi, ...
    rmaxkm, drkm, retol, bkp_file, ...
    chi0, new_last_step, do_exact_if_possible, no_axisymmetric_extension, np0maxgiven);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Presentation of the results
tmp=load([bkp_file '_FWM_ASSEMBLE_XY.mat']);
EH=tmp.EH;
% The result is in the same units as I0, i.e., A/m (in rperp-domain) or A*m
% (in kperp-domain). To convert to V/m, multiply by Z0:
E=impedance0*EH(1:3,:,:,:); % in V/m
% The magnetic field is in the same units as E. Convert to SI units:
H=EH(4:6,:,:,:); % in A/m
B=mu0*H; % in T

%% Plotting
Nyc=(length(xkmi)+1)/2
kihmax=find(abs(hi-hmax)<1e-6); % at the top
kihionstart=find(abs(hi-hionstart)<1e-6); % just below the ionosphere
for ki=1:length(hi)
    %ki=1; % on the ground
    %ki=2; % at 120 km
    figure;
    subplot(2,1,1);
    semilogy(xkmi,squeeze(abs(E(:,ki,:,Nyc)))); legend('E_x','E_y','E_z')
    title(['E, V/m at ' num2str(hi(ki)) ' km']);
    xlabel('x, km')
    subplot(2,1,2);
    semilogy(xkmi,squeeze(abs(B(:,ki,:,Nyc)))); legend('B_x','B_y','B_z')
    title(['B, T at ' num2str(hi(ki)) ' km']);
    xlabel('x, km')
end

%% The power flux at the upper boundary
ki=kihmax;
S=0.5*real(cross(conj(E(:,ki,:,:)),H(:,ki,:,:))); % W/m^2
Sz=squeeze(S(3,:,:,:));
figure;
plot(xkmi,Sz(:,Nyc)); grid on;
figure;
imagesc(xkmi,ykmi,log10(Sz.'+abs(max(Sz(:)))*1e-3));
set(gca,'ydir','normal','fontsize',14); axis equal tight;
c=colorbar;
set(c,'fontsize',14);
set(get(c,'title'),'string','log_{10}S_z, W/m^2','fontsize',14);
title(['S_z, h=' num2str(max(h)) ' km, I*l=1 A-m'])
xlabel('x, km (geomag. W-E)')
ylabel('y, km (geomag. S-N)')
%if ~do_total_energy
    echo_print({'EPS','PNG'},['pictures/' bkp_file '_Sion']);
%end
% Calculate the total power
Pionr=sum(Sz(:))*mean(diff(xkmi))*mean(diff(ykmi))*1e6 % convert from km to m


%% Check the field phase
figure;
imagesc(xkmi,ykmi,angle(squeeze(E(1,kihmax,:,:)).'));
set(gca,'ydir','normal','fontsize',14); axis equal tight;
c=colorbar;
set(c,'fontsize',14);
set(get(c,'title'),'string','V/m','fontsize',14);
title(['Phase of Ex at h=' num2str(max(h)) ' km for I=1 A-m'])
xlabel('x, km (geomag. W-E)')
ylabel('y, km (geomag. S-N)')

%% Power flux in k-vector domain
% Note that EHf is in A*m
tmp=load([bkp_file '_FWM_WAVES.mat']);
% 1. In the ionosphere
Sionfvec=0.5*impedance0*real(cross(conj(tmp.EHf(1:3,kihmax,:)),tmp.EHf(4:6,kihmax,:))); % W*m^2
Sionf=squeeze(Sionfvec(3,:,:)); Sionf=Sionf(:).'; % coerce to the same size as tmp.da
Pionf=sum(Sionf.*tmp.da)*(k0/(2*pi))^2 % W, integrated in kperp-plane
disp(['Underestimation of Pionr/Pionf=' num2str(Pionr/Pionf)]);
figure;
pointillism(tmp.nx,tmp.ny,log10(Sionf+abs(max(Sionf(:)))*1e-8)); axis equal;
% 2. Below the ionosphere
if ~isempty(kihionstart)
    Sbelowfvec=0.5*impedance0*real(cross(conj(tmp.EHf(1:3,kihionstart,:)),tmp.EHf(4:6,kihionstart,:))); % W*m^2
    Sbelowf=squeeze(Sbelowfvec(3,:,:)); Sbelowf=Sbelowf(:).'; % coerce to the same size as tmp.da
    Pbelowf=sum(Sbelowf.*tmp.da)*(k0/(2*pi))^2 % W, integrated in kperp-plane
else
    Pbelowf=nan;
    Sbelowf=[];
end
% 3. Into the ground
kignd=find(abs(hi)<1e-6)
if ~isempty(kignd)
    Sgndfvec=0.5*impedance0*real(cross(conj(tmp.EHf(1:3,kignd,:)),tmp.EHf(4:6,kignd,:))); % W*m^2
    Sgndf=-squeeze(Sgndfvec(3,:,:)); Sgndf=Sgndf(:).'; % coerce to the same size as tmp.da
    % Integrate in kperp-plane
    Pgndf=sum(Sgndf.*tmp.da)*(k0/(2*pi))^2 % W
    figure;
    pointillism(tmp.nx,tmp.ny,log10(Sgndf+abs(max(Sgndf(:)))*1e-8)); axis equal;
    title('Lower boundary')
else
    Pgndf=nan;
    Sgndf=[];
end
% 4. Power input at the source location
kisrc=find(abs(hi-hsource)<1e-6)
if ~isempty(kisrc)
    Ipar=interp2(ny0,nx0,I0prof,tmp.ny,tmp.nx);
    Epar=squeeze(tmp.EHf(kpol,kisrc,:)).';
    Sinpf=-0.5*impedance0*real(conj(Epar).*Ipar); % W*m^2
    Sinpf=Sinpf(:).'; % coerce to the same size as tmp.da
    % Integrate in kperp-plane
    Pinpf=sum(Sinpf.*tmp.da)*(k0/(2*pi))^2 % W
    figure;
    pointillism(tmp.nx,tmp.ny,Sinpf); axis equal;
    title('Power input')
else
    Pinpf=nan;
    Sinpf=[];
end
disp(['Fraction lost in ionosphere = (Pbelow-Pion)/Pbelow = ' num2str((Pbelowf-Pionf)/Pbelowf)]);

%% A figure which is better for printing
% Interpolate in (k,chi) space
np0max=max(tmp.npb)
dn=np0max/200; nmax=np0max;
P2=zeros(3,1);
for ki=1:3
    %%
    switch(ki)
        case 1
            Splot=Sgndf;
            ts='downward';
            hplot=0;
        case 2
            Splot=Sinpf;
            ts='input';
            hplot=hsource;
        case 3
            Splot=Sionf;
            ts='upward';
            hplot=max(hi);
    end
    if isempty(Splot)
        continue;
    end
    [n,Ska]=interp_k_chi(tmp.np,tmp.npb,tmp.Nphi,Splot,dn,nmax);
    figure;
    %imagesc(n,n,log10(Ska+max(Ska(:))*1e-4).');
    imagesc(n,n,Ska.');
    set(gca,'ydir','normal','fontsize',14); axis equal tight
    xlabel('n_x=k_x/k_0 (k_0=w/c)'); ylabel('n_y=k_y/k_0')
    title(['k_\perp ' ts ' power, h=' num2str(hplot) ' km, I*l=1 A-m'])
    c=colorbar;
    set(c,'fontsize',14);
    set(get(c,'title'),'string','W-m^2','fontsize',14);
    P2(ki)=sum(Ska(:))*(k0/(2*pi))^2*dn^2; % W
    echo_print({'EPS','PNG'},['pictures/' bkp_file '_kperp_' ts]);
end
P2
P1=[Pgndf;Pinpf;Pionf]

%% Repeat the main result again
diary_fname=[bkp_file '_result_summary.txt'];
if exist(diary_fname,'file')
    delete(diary_fname);
end
diary(diary_fname)
diary on
do_total_energy
do_point_source
do_ionosphere
kpol
do_supercond

Pvert
disp(['Transmitted Pion/Pvert=' num2str(Pionf/Pvert)]);
%disp(['Error Pionr/Pionf=' num2str(Pionr/Pionf)]);
disp(['Ground absorption Pgnd/Pvert=' num2str(Pgndf/Pvert)]);
disp(['Increase in impedance Pinp/Pvert=' num2str(Pinpf/Pvert)]);
disp(['Underestimation of Pionr/Pionf=' num2str(Pionr/Pionf)]);
disp(['Fraction transmitted thru ionosphere = Pion/Pbelow = ' num2str(Pionf/Pbelowf)]);
diary off

%% On the axis (estimate the error in the total radiated energy)
if do_total_energy
    if (kpol==1 && ~do_point_source) || (kpol==2 && do_point_source)
        ii=find(abs(tmp.nx)<1e-6 & tmp.ny>=0);
        nn=tmp.ny(ii);
        ist=find(tmp.np==nn(1),1); % the first few are probably missing
        dn=diff(tmp.npb(ist:end)); dn=dn(:).';
        SS=Sinpf(ii);
    elseif (kpol==2 && ~do_point_source) || (kpol==1 && do_point_source)
        % Do along the other axis
        ii=find(abs(tmp.ny)<1e-6 & tmp.nx>=0);
        nn=tmp.nx(ii); % must be tmp.np(1:end).'
        dn=diff(tmp.npb); dn=dn(:).';
        SS=Sinpf(ii);
    else
        error('no axis');
    end
    figure;
    tail=(nn>max(nn)-1);
    p=polyfit(nn(tail),log(SS(tail)),1);
    % It looks like the energy input decreases exponentially with n
    semilogy(nn,[SS; exp(p(1)*nn + p(2))],'x-')
    grid on
    % The missing energy as the fraction of a whole is
    err=(-SS(end)/p(1))/sum(SS.*dn)
    disp(['Error in Pinpf/Pvert=' num2str(err*Pinpf/Pvert)]);
end
