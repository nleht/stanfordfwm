% Reflection height
loadconstants
global clight impedance0

prof='HAARPwinternight';

%% The grid
% Recommended:
fmin=1000; df=10; fmax=5000; hmin=50; dhlow=0.1; hmed=100; dhhigh=0.1; hmax=250; thid=0;
% User-specified:
fmin=1000; df=25; fmax=5000; hmin=50; dhlow=2; hmed=100; dhhigh=2; hmax=150; thid=45;

%% Initial setup
hlow=(hmin:dhlow:hmed).';
daytime=0;
% Avoid a jump in active species at 73 km (extrapolate below 73 km):
Nac=getSpecies('O',hlow);
istop=find(Nac>0, 1);
hlow(istop)
izero=find(hlow<hlow(istop));
coef=log(Nac(istop+1)/Nac(istop))/(hlow(istop+1)-hlow(istop));
Nac(izero)=Nac(istop)*exp(coef*(hlow(izero)-hlow(istop)));
% Don't forget to convert to cm^{-3}
[Nspec0,S0,specnames]=ionochem_6spec(prof,daytime,hlow,'Nac_profile',Nac/1e6);
Nelow=Nspec0(:,1)*1e6;

hhigh=(hmed+dhhigh:dhhigh:hmax).';
Nehigh=getNe(hhigh,prof); % in m^{-3}

h=[0 ; hlow ; hhigh];
M=length(h)

Ne=[0 ; Nelow ; Nehigh];
% Ne(1)==0 is important for separating TE and TM waves !!!

farr=(fmin:df:fmax);
nf=length(farr);
fc=(farr(1:nf-1)+farr(2:nf))/2;
Ei=zeros(2,2,nf);
R=zeros(2,nf);

% thid=0; % in degrees
% Matrix to rotate to x',y'=y,z', z'|| incidence
nx0=sind(thid);
nz0=cosd(thid);

%% The long cycle
for kf=1:nf
    f=farr(kf)
    w=2*pi*f;
    k0=w/clight;
    perm=get_perm(h,w,'Ne',Ne,'Babs',5e-5,'thB',0,'phB',0);
    nz=zeros(4,M); Fext=zeros(6,4,M);
    for k=1:M
        [nz(:,k),Fext(:,:,k)]=fwm_booker_bianisotropic(perm(:,:,k),nx0,0);
    end
    F=Fext([1:2 4:5],:,:);
    Rground = fwm_Rground('E=0', nx0, 0);
    [Pu,Pd,Ux,Dx,Ruh,Rdl,Rul,Rdh] = fwm_radiation(h*1e3*k0,nz,F,Rground);
    % - the ground boundary condition is not really needed for Ru{l|h}

    % Reflection coefficient matrix (from the sky) at the ground
    [vv,dd]=eig(Rul(:,:,1)); % extract eigen-modes
    % Provide the continuity
    Rtmp=diag(dd);
    if kf==1
        ii=[1 2];
    else
        % Which one is closer to the previous value?
        Rprev = R(:,kf-1);
        x1=sum(abs(Rtmp-Rprev));
        x2=sum(abs(Rtmp(2:-1:1)-Rprev));
        if x1<x2
            ii=[1 2];
        else
            ii=[2 1];
        end
    end
    % Reflection coefficient (scalar, for 2 eigen-modes)
    R(:,kf)=Rtmp(ii);
    % The field structure in each eigen-mode
    EHtmp=Fext(:,:,1)*[vv(:,ii);zeros(2,2)];
    % Rotate to the coordinate system connected to the incident ray
    Ei(1,:,kf)=nz0*EHtmp(1,:)-nx0*EHtmp(3,:); % Ex'
    Ei(2,:,kf)=EHtmp(2,:); % Ey'
end

%% Results
% Ei1 has Ex' with zero phase
Eangle=angle(Ei(1,:,:));
Ei1=Ei.*exp(-1i*repmat(Eangle,[2 1 1]));

phase=unwrap(angle(R),[],2); %+2*n*pi, where n is unknown
k0=2*pi*farr/clight;
hr=(phase+pi)./(2*repmat(k0,[2 1])*1e3);
hramb=2*pi./(2*repmat(k0,[2 1])*1e3); % ambiguity

%% Figures
figure(1); plot(farr/1e3,hr);
%hold on; plot(farr/1e3,hr-hramb); plot(farr/1e3,hr+hramb); hold off
xlabel('f, kHz'); ylabel('Reflection height, km')

figure(2); plot(farr/1e3,abs(R));
xlabel('f, kHz'); ylabel('|R|');

% Resonances
figure(3); plot(farr/1e3,1./abs(R+1));
% If the Earth were 'H=0': hold on; plot(farr/1e3,1./abs(R-1)); hold off
xlabel('f, kHz'); ylabel('1/|R+1|');

figure(4); plot(farr/1e3,abs(squeeze(Ei1(1,:,:))));
hold on; plot(farr/1e3,abs(squeeze(Ei1(2,:,:))),'--'); hold off;
Eyphase=squeeze(angle(Ei1(2,:,:)));

figure(5); plot(farr/1e3,unwrap(Eyphase,[],2));
