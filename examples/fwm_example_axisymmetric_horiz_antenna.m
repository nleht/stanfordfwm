% Demo of fwm_axisymmetric
% Radiation by a horizontal dipole
global impedance0 clight mu0 eps0
if isempty(impedance0)
    loadconstants
end
h=[0 10 80:120].';
%h=[0 81:120].';
M=length(h);

f=10e3;
w=2*pi*f;
k0=w/clight;
perm=get_perm(h,w,'Bgeo',[0 0 5e-5]);
%perm=repmat(eye(3),[1 1 M]);

% Set up the an arbitrary dipole current. It is always zeroth axial
% harmonic (m=0) in cartesian coordinates.
ksa=[2]; % location of the current at h(ksa)
Ms=length(ksa);
I = [0;1;0;0;0;0];
m=[-1;0;1] % The harmonic number for the horizontal CCW dipole
nharm=length(m);
% For I=[1;0;0;0;0;0], we would have
% Ih(:,1)=[1/2;-1i/2;0;0;0;0]; % projection onto \hat{n_+}, times \hat{n_+}
% Ih(:,3)=[1/2;1i/2;0;0;0;0];
% Ir,Iphi~exp(i*m*phi)
% The way described in documentation to FWM_AXISYMMETRIC
Ih=zeros(6,nharm);
% Ik+i*Ichi= (Ix+i*Iy)*exp(-i*chi)
% Ik-i*Ichi= (Ix-i*Iy)*exp(+i*chi)
Icarp = I([1 4])+1i*I([2 5]); % Ix+i*Iy
Icarm = I([1 4])-1i*I([2 5]); % Ix-i*Iy
Icylp = zeros(2,1,nharm); % Ik+i*Ichi
Icylm = Icylp; % Ik-i*Ichi
Icylp(:,1) = Icarp; % shift harmonic by -1
Icylm(:,3) = Icarm; % shift harmonic by +1
Ih([3 6],2) = I([3 6]); % just copy z-component zeroth harmonic
Ih([1 4],:) = (Icylp+Icylm)/2;  % Ik
Ih([2 5],:) = (Icylp-Icylm)/2i; % Ichi
disp('The current harmonics are:')
Ih
I0=zeros(6,Ms,nharm);
I0(:,1,:)=Ih;
% We could have achieved this by
[I01, np0, m1]=fwm_harmonics([],[],I,3);
% Check if the result is the same
max(abs(I0-I01),[],'all')
assert(all(m==m1))

% Output
hi=[0 max(h)].';
Mi=length(hi);
xkm=0:.1:500;
Nx=length(xkm);

EH0=zeros(6,Mi,Nx,nharm);
retol=1e-3;
sground = 0.01; % Typical ground conductivity is 3--30 mS/m
eground=1+1i*sground/(w*eps0);
ground_bc=eground; %'free';

%% A relatively long calculation
for kharm=1:nharm
    disp(['******* Harmonic m=' num2str(m(kharm)) ' *******']);
    if max(abs(I0(:,:,kharm)),[],'all')/max(abs(I0),[],'all')<1e-10
        disp('Skipping ...')
        EH0(:,:,:,kharm)=0;
    else
        [EH0(:,:,:,kharm),np,EHn,npb,kie,npt,EHnt,nptb]=...
            fwm_axisymmetric(f,h,ground_bc,perm,ksa,[],m(kharm),I0(:,:,kharm),xkm,hi,[],[],retol);
        ms=num2str(kharm);
        eval(['EHn' ms '=EHn; np' ms '=np; EHnt' ms '=EHnt; npt' ms '=npt;']);
    end
end

%% The field in 2D
% E_{m,+|-|z}(rvec)~exp(i*(m+n)*phi_r), n={1|-1|0}
% E_{m,r|phi_r|z}(rvec)~exp(i*m*phi_r)

% Now, we introduce phi dependence
phi=[0:360].'*pi/180;
dphi=phi(2)-phi(1)
nphi=length(phi);
EHcyl=zeros(6,Mi,Nx,nphi); % Er, Ephi, Ez
for iphi=1:nphi
    for km=1:nharm
        EHcyl(:,:,:,iphi)=EHcyl(:,:,:,iphi)+EH0(:,:,:,km)*exp(1i*phi(iphi)*m(km));
    end
end

% To cartesian -- rotate EHcyl
EHcar = EHcyl; % also copy z-components
EHr = EHcyl([1 4],:,:,:);
EHphi = EHcyl([2 5],:,:,:);
phim = repmat(shiftdim(phi,-3), [2 Mi Nx 1]);
EHcar([1 4],:,:,:)=EHr.*cos(phim)-EHphi.*sin(phim);
EHcar([2 5],:,:,:)=EHr.*sin(phim)+EHphi.*cos(phim);

%% The way described in documentation to FWM_AXISYMMETRIC
% Ex+i*Ey = (Er(phi==0)+i*Ephi(phi==0))*exp(i*(m+1)*phi)
% Ex-i*Ey = (Er(phi==0)-i*Ephi(phi==0))*exp(i*(m-1)*phi)
EHcylp = EH0([1 4],:,:,:)+1i*EH0([2 5],:,:,:); % size==[2 Mi Nx nharm]
EHcylm = EH0([1 4],:,:,:)-1i*EH0([2 5],:,:,:);
mnew=[-2;-1;0;-1;2]; % We have monopole and quadrupole, no dipole!
nharmnew = length(mnew);
EHcarp = zeros(2,Mi,Nx,nharmnew);
EHcarm = EHcarp;
EHcarp(:,:,:,3:5) = EHcylp; % *exp(1i*(m+1)*phi)
EHcarm(:,:,:,1:3) = EHcylm; % *exp(1i*(m-1)*phi)
EHcarh = zeros(6,Mi,Nx,nharmnew);
EHcarh([1 4],:,:,:) = (EHcarp+EHcarm)/2;
EHcarh([2 5],:,:,:) = (EHcarp-EHcarm)/(2i);
EHcar1 = zeros(6,Mi,Nx,nphi); % Ex, Ey, Ez
for iphi=1:nphi
    for km=1:length(mnew)
        EHcar1(:,:,:,iphi) = EHcar1(:,:,:,iphi) + EHcarh(:,:,:,km)*exp(1i*phi(iphi)*mnew(km));
    end
end
EHcar1([3 6],:,:,:)=EHcyl([3 6],:,:,:); % same!
max(abs(EHcar-EHcar1),[],'all') % checks out
    
%% Power flux
S=0.5*real(cross(conj(EHcyl(1:3,:,:,:)),EHcyl(4:6,:,:,:)))*impedance0;
Sr=squeeze(S(1,Mi,:,:));
Sth=squeeze(S(2,Mi,:,:));
Sz=squeeze(S(3,Mi,:,:));

%% Plotting
[rm,thm]=ndgrid(xkm,phi);
xm=rm.*cos(thm);
ym=rm.*sin(thm);
c=3; ki=1; do_cyl=0;
% Cylindrical coordinate system components
cnames={'E_r','E_\phi','E_z','B_r','B_\phi','B_z'};
% Cartesian coordinate system components
dnames={'E_x','E_y','E_z','B_x','B_y','B_z'};
ccoef=[[1 1 1]*impedance0 [1 1 1]*mu0];
if do_cyl
    name=cnames{c};
    EorB=squeeze(EHcyl(c,ki,:,:))*ccoef(c);
else
    name=dnames{c};
    EorB=squeeze(EHcar(c,ki,:,:))*ccoef(c);
end
figure;
hh=pcolor(xm,ym,real((EorB))); set(hh,'edgecolor','none'); axis equal;
xlabel('x, km'); ylabel('y, km');
title([name ' at ' num2str(hi(ki)) ' km'])
colorbar;

%%
Babs=mu0*squeeze(sqrt(sum(abs(EHcyl(4:6,ki,:,:)).^2,1)));
figure;
hh=pcolor(xm,ym,log10(Babs)); set(hh,'edgecolor','none'); axis equal;
xlabel('x, km'); ylabel('y, km');
title(['log10(Babs) at ' num2str(hi(ki)) ' km'])
colorbar;

figure;
Szplot = Sz.*(Sz>0)+1e-16;
hh=pcolor(xm,ym,log10(Szplot)); set(hh,'edgecolor','none'); axis equal;
xlabel('x, km'); ylabel('y, km');
title('log10(S_z) at 120 km')
colorbar

figure;
hh=pcolor(xm,ym,Sr); set(hh,'edgecolor','none'); axis equal;
xlabel('x, km'); ylabel('y, km');
title('S_r at 120 km')
colorbar

figure;
hh=pcolor(xm,ym,Sth); set(hh,'edgecolor','none'); axis equal;
xlabel('x, km'); ylabel('y, km');
title('S_\theta at 120 km')
colorbar
