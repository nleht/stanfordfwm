%% Comparison of axisymmetric and nonaxisymmetric resuts:
% This is the axisymmetric part

% First, load the configuration
fwm_example_horizcompare_common

% Output horizontal coordinates
rkmi=0:1:600;
phi=[0:360].'*pi/180;

%% A relatively long calculation
mkdir horizcompare
if ~exist('horizcompare/axisym.mat','file')
    % The interface is very similar to fwm_nonaxisymmetric
    EH = fwm_axisymmetric_any_source(f,h,ground_bc,perm,...
        ksa,1,[],[],In,...   % '1' means In is in cartesian coordinates
        2,rkmi,phi,hi,...    % '2' means polar coordinates
        rmaxkm,drkm,retol);
    save('horizcompare/axisym.mat','EH');
else
    load('horizcompare/axisym.mat');
end

%% Plot an arbitrary component
c=3; ki=1; % component and height number
figure(1);
clf;
[rm,thm]=ndgrid(rkmi,phi);
xm=rm.*cos(thm);
ym=rm.*sin(thm);
% Cartesian coordinate system components
names={'E_x','E_y','E_z','B_x','B_y','B_z'};
ccoef=[[1 1 1] [1 1 1]/clight];
hh=pcolor(xm,ym,real(squeeze(EH(c,ki,:,:))*ccoef(c)));
set(hh,'edgecolor','none'); axis equal tight;
xlabel('x, km'); ylabel('y, km');
title([names{c} ' at ' num2str(hi(ki)) ' km' Icaption])
colorbar;

%% Magnetic field on the ground
figure(2);
clf;
ki=1;
Babs=squeeze(sqrt(sum(abs(EH(4:6,ki,:,:)).^2,1)))/clight;
hh=pcolor(xm,ym,log10(Babs)); set(hh,'edgecolor','none'); axis equal tight;
xlabel('x, km'); ylabel('y, km');
title(['log10(Babs) at ' num2str(hi(ki)) ' km' Icaption])
colorbar;

%% Power flux on the upper boundary
figure(3);
clf;
S=0.5*real(cross(conj(EH(1:3,Mi,:,:)),EH(4:6,Mi,:,:)))/impedance0;
Sz=squeeze(S(3,:,:,:));
hh=pcolor(xm,ym,log10(Sz));
set(hh,'edgecolor','none'); axis equal tight;
xlabel('x, km'); ylabel('y, km');
title(['S_z at ' num2str(hi(Mi)) ' km' Icaption])
colorbar;
echo_print({'PNG'},'horizcompare/Sz_ax')

%% Plot the fields on the ground
% Field on the x-axis
E = EH(1:3,:,:,1);
B = EH(4:6,:,:,1)/clight;
ki=1;
figure(4);
clf;
subplot(2,1,1);
semilogy(rkmi,squeeze(abs(E(:,ki,:)))); legend('E_x','E_y','E_z')
title(['E, V/m at ' num2str(hi(ki)) ' km']);
xlabel('x, km')
subplot(2,1,2);
semilogy(rkmi,squeeze(abs(B(:,ki,:)))); legend('B_x','B_y','B_z')
title(['B, T at ' num2str(hi(ki)) ' km']);
xlabel('x, km')
