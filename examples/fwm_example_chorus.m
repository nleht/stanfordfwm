% Chorus/whistler propagation to the ground
% The coordinates are geomagnetic, i.e. x-axis is geomagnetic west-east and
% y-axis is geomagnetic south-north.

%% Load constants
close all
clear all
global eps0 clight impedance0
if isempty(clight)
    loadconstants
end

dirbase = './chorus';
figdirbase = './figures';
plot_formats = {'PNG','FIG'};

% Inline functions
get_S = @(EH_) 0.5*real(cross(conj(EH_(1:3,:,:)), EH_(4:6,:,:), 1))/impedance0;
% For plotting, more convenient is the different order of indices
get_Splot = @(EH_) 0.5*real(cross(conj(EH_(:,:,1:3)), EH_(:,:,4:6), 3))/impedance0;
get_zcomp = @(S_) squeeze(S_(3,:,:));
get_Sz = @(EH_) get_zcomp(get_S(EH_));
iperp = [1:2 4:5]; % the horizontal (continuous) components

%% Configuration
% Source characteristics
r_duct = 1e3; % duct radius, determining the typical wave packet width in meters
hs = 1000; % source altitude in kilometers
% There are several possible observation locations: set location=1..3
location = 2;
f = 1.8e3; % Frequency [Hz]
% The main goal of calculation?
configuration = 'propagation';
if strcmp(configuration, 'propagation')
    ground = 'conducting'; % try also 'free'
    % Output coordinates
    Dxkm=500; Dykm=1000; % smaller distance to save calculation time -- may depend on location
    dxkm=5; dykm=5;
    nmax = 80; % the nperp range, must be large for propagation
    dn = 0.5;
    use_displace = 0;
    hpropag = 0:10:1000;
    accurate_calculation = 0; % Recommended: = 0; (will have aliases, but reasonable memory usage)
    rmaxkm = []; drkm = []; retol = 1e-3; % defaults
    % Set drkm to a value lower than dxkm to get sharper fields.
    % No reason to set it lower than r_duct/1e3 -- but it will overload
    % memory anyway.
    runmarker = '_500x1000km'; % to distinguish different runs if parameters are tweaked
    include_evanescent_eiwg_excitation = 1; % since we calculated all of the evanescent waves anyway
    if 0
        % Unfortunately, the following settings (excpected to get rid of
        % aliases) give memory overload:
        accurate_calculation = 1;
        retol = 4e-2; % no matter how bad I tried the resolution
        runmarker = '_retol4e-2';
        % Even tried memory tweaking:
        % Memory usage in FWM_ASSEMBLE_XY
        % Something strange going on, needs careful analysis of that function
        global memory_use_coef
        memory_use_coef = 0.001;
    end
elseif strcmp(configuration, 'detection') % Field on the ground
    % This calculation is faster, so can have better resolution etc.
    ground = 'conducting';
    Dxkm=600; Dykm=600; % Up to which horizontal distance to calculate
    dxkm=3; dykm=3;
    nmax = 1.7; % limit to nmax >~ 1 to save calculation time if only ground field is needed
    dn = 0.0025; % should be small for use_displace==1
    % A faster calculation when the result is far from (x=0,y=0) point
    use_displace = 1;
    % The displacements are given in each problem
    hpropag = []; % if only ground field is needed
    runmarker = '';
    % Supposedly, setting this to one will give more accurate field at h>0, but
    % with longer calculations.
    % NOTE: Plotting the power flux entering the EIWG from above did not show
    % more accuracy
    accurate_calculation = 0;
    rmaxkm = []; drkm = []; retol = 1e-3; % defaults
    include_evanescent_eiwg_excitation = 0; % We don't have all evanescent waves
end

% Geomagnetic field is a function of altitude?
use_var_B = 1;

%% Debugging options (default for most should be = 0)

% Optional refractive index surface plot
plot_n = 0;

% Initial Poynting vector
plot_initial_Sz = 0;

% The source shape in configuration space
plot_source_shape = 0;

% When plotting the field at the source, include the evanescent component?
include_source_evanescent_wave = 0;

% Plot energy entering and leaving EIWG in nperp-space
plot_Sz_nperp_EIWG = 0;

%% Locations
if location==1
    % Inputs for IRI
    year = 2014;
    DOY = 1; % Day may also be given either as -DOY or as +mmdd
    hour = 12; % hour<24 if local; is 24+hour if time is UTC
    latitude = 46.90;
    longitude = 17.88;
    hion = 65; % for waves entering ionosphere
    displaceykm = iff(use_displace, 250, 0);
elseif location==2
    % Inputs for IRI
    year = 2015;
    DOY = 90; % Day may also be given either as -DOY or as +mmdd
    hour = 23.3167; % hour<24 if local; is 24+hour if time is UTC
    latitude = -73.5973;
    longitude = -30.2785;
    hion = 80; % for waves entering ionosphere
    displaceykm = iff(use_displace, -200, 0);
elseif location==3
    % Inputs for IRI
    year = 2015;
    DOY = 90; % Day may also be given either as -DOY or as +mmdd
    hour = 23.3167; % hour<24 if local; is 24+hour if time is UTC
    latitude = -30.2785;
    longitude = -73.5973;
    hion = 80; % for waves entering ionosphere
    displaceykm = iff(use_displace, -800, 0);
else
    error('unknown location number')
end
displacexkm = 0;

%% Choose output altitudes
if accurate_calculation==2
    hi_core = [0 hion hpropag];
    hi_extra = [];
elseif accurate_calculation==1
    hi_core = [0 hion];
    hi_extra = hpropag;
elseif accurate_calculation==0
    hi_core = 0;
    hi_extra = [hion hpropag];
end

%% Electron density from IRI
%rtdir = fullfile(pwd,'external/IRI2023/IRI2023/data');
IRI = IRIClass;

%Define electron density profile
is_geomag = 0;
hstart = 50;
hstop = 1000;
hstep = 1;
[H, ionosphere] = IRI.sub(is_geomag, latitude, longitude, year, -DOY, hour, hstart, hstop, hstep);
ne = ionosphere(1,:);
ne(ne==-1) = 0;
% Extend to ground
H = [0 H];
ne = [0 ne];

%% Define optimized layers
ii = slowchange(ne, 0.05);
h = H(ii);
M = length(h);
Ne = interp1(H,ne,h); % Electron density in 1/m^3
ksa = fwm_get_layers(h, hs); % in which layer is the source?

% Define magnetic field
% In principle, Bgeo in FWM may be variable with height
if use_var_B
    MB = M;
    Bgeo0 = zeros(M, 3);
    for k=1:M
        Bgeo0(k,:) = IRI.igrf(latitude,longitude,year,h(k));
    end
    Bgeos0 = Bgeo0(ksa,:);
else
    MB = 1;
    Bgeo0 = IRI.igrf(latitude,longitude,year,h(ksa));
    Bgeos0 = Bgeo0;
end
% Declination of the field (azimuth) at the source height
phB = atan2(Bgeos0(1),Bgeos0(2));
% Rotate the field to align y-axis with geomagnetic north at source height
rotmat = [cos(phB) -sin(phB); sin(phB) cos(phB)];
Bgeo = Bgeo0;
for k=1:MB
    Bgeo(k,1:2) = rotmat*Bgeo0(k,1:2).';
end

%% Gaussian packet of the source
w=2*pi*f;
k0=w/clight;
if use_var_B
    Bgeos = Bgeo(ksa,:);
else
    Bgeos = Bgeo;
end
permvert = get_perm(h(ksa), w, 'Ne', Ne(ksa), 'Bgeo', [0 0 sqrt(sum(Bgeos.^2))]);
nzvert = fwm_booker_bianisotropic(permvert, 0, 0);
n_source = -real(nzvert(3)); % could also take +real(nzvert(2))
thB = atan2(Bgeos(2), Bgeos(3));
nyc = -sign(Bgeos(3))*sin(thB)*n_source; % for downward wave
sigma_x = r_duct;
sigma_y = abs(r_duct/cos(thB));
sigma_nx = 1/sigma_x/k0;
sigma_ny = 1/sigma_y/k0;
shape_fun = @(nx_, ny_) exp(-nx_.^2/(2*sigma_nx^2) - (ny_-nyc).^2/(2*sigma_ny^2))/(2*pi*sigma_nx*sigma_ny);
% The current components
In0 = [0;1;0;0;0;0]*impedance0; % Normalization is arbitrary

%% Electric properties of the medium, at given frequency 
% Ground BC
if strcmp(ground,'conducting')
    sground = 0.01; % Typical ground conductivity is 3--30 mS/m
    eground = 1+1i*sground/(w*eps0);
    ground_bc = eground; % can be also, e.g., 'E=0' or 'free';
else
    ground_bc = ground;
end
% Dielectric permittivity
perm = get_perm(h, w, 'Ne', Ne, 'Bgeo', Bgeo);

%% (nx,ny) needed for FWM to calculate the field on the ground (only!)
% We neglect the evanescent waves with |nperp|>1
dnx = dn; % 0.0025; % Small dn is needed to resolve rapid oscillations due to displacement
% -- We may build the displacement into fwm_nonaxisymmetric in the future
nxmax = nmax;
Ntmp = 2*round(nxmax/dnx)+1;
nx0 = (-Ntmp/2:Ntmp/2).'*dnx;
%nx1=(-1.5+dnx/2:dnx:1.5-dnx/2).';
ny0 = nx0;
dny = dnx;
Nnx0 = length(nx0);
Nny0 = length(ny0);
[nx, ny] = ndgrid(nx0, ny0);

%% The source current (also only for field calculation on the ground)
% current along y, size = 6 x Ms x Nnx0 x Nny0
shape = shape_fun(nx, ny); % Nnx0 x Nny0
% check the sum
fprintf('The shape integral in the given (nx,ny) area = %f (it is ok if <<1) \n', sum(shape*dnx*dny,'all'))
In = repmat(In0, [1 1 Nnx0 Nny0]).*permute(shape, [3 4 1 2]);

%% Output coordinates
dirext = ['_' configuration num2str(location)];
if strcmp(ground,'conducting')
    dirext = [dirext 'c'];
elseif strcmp(ground,'free')
    dirext = [dirext 'f'];
end
dirext = [dirext iff(use_displace,'d','') 'e' num2str(accurate_calculation) ...
    runmarker];
hi = [hi_core hi_extra];
Mi = length(hi);
% Mi = 2, in either case
xkmi = displacexkm + (-Dxkm:dxkm:Dxkm);
ykmi = displaceykm + (-Dykm:dykm:Dykm);
Nx = length(xkmi);
Ny = length(ykmi);
if use_displace
    displace = permute(exp(1i*k0*(nx*displacexkm + ny*displaceykm)*1e3), [3 4 1 2]);
else
    % Discard the value of displace*km (they should have been set to zero
    % anyway)
    displace = 1;
    displacexkm = 0; displaceykm = 0;
end

%% Main program call:
global output_interval backup_interval
output_interval=60; % To observe the progress every 60 sec
backup_interval=1800; % Default = 3600 sec
thedir = [dirbase dirext];
if ~exist(thedir,'dir')
    mkdir(thedir)
end
bkp_file=fullfile(thedir,'nonaxisym');
coornperp = 1; coorrperp = 1; % Cartesian coordinates both in (nx,ny) and (x,y)
% EH has size 6 x Mi x Nx x Ny, where Mi = 2 (0 and hion)
EH = fwm_nonaxisymmetric(f, h, ground_bc, perm, ...
    ksa, coornperp, nx0, ny0, In.*displace, ...
    coorrperp, xkmi-displacexkm, ykmi-displaceykm, hi_core, ...
    rmaxkm, drkm, retol, bkp_file, ...
    'hi_extra',hi_extra); % NEW! extra hi for which the n-grid is not optimized
EH_ground = permute(EH(:,1,:,:),[3 4 1 2]);
S_ground = get_Splot(EH_ground); % Nx x Ny x 3
% Create the directory figures -- wait until the calculations are finished!
figdir = [figdirbase dirext];
if ~exist(figdir, 'dir')
    mkdir(figdir)
end

%% PART 2: The total downward energy flux from the source

%% 1. The new (nx, ny) for the source, to capture the waves which do not reach ground
Nnxs = 200;
if location==1
    nxmaxs=50;
elseif location==2
    nxmaxs = 100;
end
dnxs = 2*nxmaxs/Nnxs;
dnys = dnxs;
Nnys = Nnxs;
nxs0 = (-Nnxs/2:Nnxs/2-1).'*dnxs;
nys0 = (-Nnys/2:Nnys/2-1).'*dnys;
[nxs, nys] = ndgrid(nxs0, nys0);
Np = Nnxs*Nnys;
shapes = shape_fun(nxs, nys); % Nnx0s x Nny0s
% check the sum
fprintf('The shape integral in the given (nx,ny) area = %f (must be close to 1 for accurate fraction)\n', ...
    sum(shapes*dnxs*dnys,'all'))
Ins = repmat(In0, [1 1 Nnxs Nnys]).*permute(shapes, [3 4 1 2]);
eiz=permute(perm(:,3,ksa),[1 3 2]);
DEHs = squeeze(fwm_deh(Ins, eiz, nxs(:), nys(:))); % size = 4 x Np

%% 2. Booker equation
fprintf('Solving Booker equation ... ')
[nzs, Fexts] = fwm_booker_bianisotropic(perm(:,:,ksa), nxs, nys);
fprintf('done!\n')

%% Plot the bell-shaped refractive index surface (optional)
if plot_n
    figure;
    nzsplot = reshape(nzs(3,:), [Nnxs Nnys]);
    surf(nxs0, nys0, real(nzsplot).');
    axis equal
end

%% 3. The Poynting vector and the integrated power flux
F = Fexts(iperp,:,:);
% The last index of EHn indicates the 4 modes:
% 1. up evanescent, 2. up whistler, 3. down whistler, and 4. down
% evanescent
EHns = zeros(6,Np,4); % The 4 modes: up and down 
for ip=1:Np
    Dud = F(:,:,ip)\DEHs(:,ip); % really, [u;-d]
    ud = diag([Dud(1:2);-Dud(3:4)]);
    EHns(:,ip,:) = Fexts(:,:,ip)*ud;
end
% Energy flux above and below the source
%Snzs = squeeze(0.5*real(conj(EHns(1,:,:)).*EHns(4,:,:)-conj(EHns(2,:,:)).*EHns(3,:,:)))/impedance0;
Snzs = get_Sz(EHns);
%Snzs = squeeze(Sns)
Ps = sum(Snzs*dnxs*dnys,1)*k0^2/(2*pi)^2;
P0 = -Ps(3); % Total downward power
% scale_factor = sqrt(P0); % for 1 W power input, divide the result of FWM by this number
%fprintf('The total initial downward power is %g\n', P0)
fprintf('Evanescent mode carries fraction %g of the downward power flux, must be zero\n', Ps(4)/Ps(3))

%% Plot the flux as a function of (nx, ny)
if plot_initial_Sz
    figure;
    % Upward flux
    surf(nxs0, nys0, reshape(Snzs(:,1)+Snzs(:,2),[Nnxs Nnys]).')
    hold on
    % Downward flux (negative)
    surf(nxs0, nys0, reshape(Snzs(:,3)+Snzs(:,4),[Nnxs Nnys]).')
end

%% The field at the source in configuration space
indx=[Nnxs/2+1:Nnxs 1:Nnxs/2];
Lx = 2*pi/(dnxs*k0);
dx = Lx/Nnxs; % = lambda/(2*nxmaxs) -- depends on frequency
Ly = Lx;
dy = dx;
x = ((-Nnxs/2):Nnxs/2-1).'*dx;
y = x;
% For plotting, choose selected x
irarx = find(x>-5e3 & x<5e3);
if location==1
    irary = irarx;
elseif location==2
    irary = irarx;
elseif location==3
    irary = find(y>-10e3 & y<10e3);
end
xrarkm = x(irarx)/1e3;
yrarkm = y(irary)/1e3;
% The current -- shape, mostly for debugging
shaper = zeros(Nnxs, Nnys);
shaper(indx,:) = ifft(shapes(indx,:),[],1)/dx;
shaper(:,indx) = ifft(shaper(:,indx),[],2)/dy;
if plot_source_shape
    figure;
    imagesc(xrarkm, yrarkm, abs(shaper(irarx,irary)).');
    set(gca,'ydir','normal'); axis equal tight;
    xlabel('x, km'); ylabel('y, km');
    title(sprintf('The shape of the source at %d km', hs))
end

%% Plot downward-radiated E, H at the source
if include_source_evanescent_wave
    EHnds = EHns(:,:,3)+EHns(:,:,4);
    is_ev = 'whistler+evanescent';
else
    EHnds = EHns(:,:,3);
    is_ev = 'only whistler';
end
EHnds = reshape(EHnds, [6 Nnxs Nnys]);
EHs = zeros(6, Nnxs, Nnys);
EHs(:,indx,:) = ifft(EHnds(:,indx,:),[],2)/dx;
EHs(:,:,indx) = ifft(EHs(:,:,indx),[],3)/dy;

hh = figure;
maxE = max(sqrt(sum(abs(EHs(1:2,:,:)).^2, 1)),[],'all');
maxH = max(sqrt(sum(abs(EHs(4:5,:,:)).^2, 1)),[],'all');
n = maxH/maxE;
EHsrar = permute(EHs(:,irarx,irary),[3 2 1])/maxH;
Ncycles = 2;
Nt = 20; % number of time points per cycle
for it=1:Nt*Ncycles
    phase = it/Nt; % the phase
    EHt = real(exp(-2i*pi*phase)*EHsrar);
    set(0, 'CurrentFigure', hh); % so that we do not plot in a window we clicked on accidentally
    quiver(xrarkm, yrarkm, EHt(:,:,1)*n, EHt(:,:,2)*n,0,'b');
    hold on;
    quiver(xrarkm, yrarkm, EHt(:,:,4), EHt(:,:,5),0,'r');
    axis equal tight;
    title(sprintf('Field radiated downward at %.1f km\n%s\nw*t/(2*pi)=%.3f', hs, is_ev, phase))
    legend(sprintf('E x %.1f',n),'H');
    xlabel('x, km'); ylabel('y, km');
    shg
    pause(.2)
    hold off
end

%% PART 3: Plot the scaled FWM results (for chorus input of 1 Watt)

%% Plot the field on the ground
figure;
% EH_ground is 6 x Nx x Ny
imagesc(xkmi, ykmi, abs(EH_ground(:,:,3)).'/sqrt(P0));
set(gca,'ydir','normal'); axis equal tight;
xlabel('x, km'); ylabel('y, km');
title('E_z on the ground for 1 Watt chorus')
colorbar
echo_print(plot_formats, fullfile(figdir, 'Ezground'));

%% Horizontal energy flux on the ground (vertical is almost zero)
% Rarefy for plotting
irarxkm = 1:10:Nx;
irarykm = 1:10:Ny;
figure;
quiver(xkmi(irarxkm), ykmi(irarykm), ...
    S_ground(irarxkm, irarykm,1).'/P0, S_ground(irarxkm, irarykm,2).'/P0)
axis equal tight;
title('Horizontal power flux on the ground');
xlabel('x, km'); ylabel('y, km');
echo_print(plot_formats, fullfile(figdir, 'S_ground'));

%% PART 4: Calculate energy entering EIWG from ionosphere (only downward!)
% EIWG == Earth-Ionosphere WaveGuide

%% Extract separately the upward and downward modes at h=hion
fprintf('Separating the waves into modes ... ');
waves = load([bkp_file '_FWM_WAVES.mat']);
fprintf('load ... ');
N = length(waves.nx); % number of Fourier components
% Fextvac is 6 x 4 x N
[~, Fextvac] = fwm_booker_bianisotropic(perm(:,:,1), waves.nx, waves.ny);
fprintf('Booker ... ');
Fvac = Fextvac(iperp,:,:); % 4 x 4 x N
udvac = zeros(4,N);
EHnvac_sep = zeros(6,N,2); % EHnvac_sep(:,:,1) is up, (:,:,2) is down
for ip=1:N
    % Separate EHn into modes
    ud = Fvac(:,:,ip)\waves.EHn(iperp,2,ip); % amplitudes, at h=hion
    udmat = [ud(1) 0; ud(2) 0; 0 ud(3); 0 ud(4)]; % up and down
    EHnvac_sep(:,ip,:) = Fextvac(:,:,ip)*udmat;  % separate up and down
end
fprintf('done!\n');
Snzvac = get_Sz(EHnvac_sep); % N x 2
% Integrate over (kx, ky)
Pvac = sum(Snzvac.*repmat(waves.da(:), [1 2]),1)*k0^2/(2*pi)^2;
fprintf('Fraction of power entering EIWG is %g\n', -Pvac(2)/P0)
fprintf('Fraction of it lost (eventually, to the ground): %g\n', sum(Pvac)/Pvac(2))

%% Sz in nperp-space
if plot_Sz_nperp_EIWG
    % Sz entering EIWG from above: plot it in nperp-space
    Ntmp = 200;
    nxplot = nxmax*(-Ntmp:Ntmp)/Ntmp;
    nyplot = nxplot;
    Sxy = interp_polar2xy(waves.Nphi, waves.npb, -Snzvac(:,2), nxplot, nyplot);
    Sxymin = max(Sxy(:))*1e-8;
    Sxy(Sxy<=Sxymin) = Sxymin;
    figure;
    imagesc(nxplot,nyplot,log10(Sxy)');
    set(gca,'ydir','normal'); axis equal tight;
    title('log10(S_z) entering EIWG from above');
    xlabel('n_x'); ylabel('n_y');
    colorbar
    
    % Same for waves leaving EIWG (should be about the same)
    %Sxy = interp_polar2xy(waves.Nphi, waves.npb, Snzvac(:,1), nxplot, nyplot);
    %Sxymin = max(Sxy(:))*1e-3;
    %Sxy(Sxy<=Sxymin) = Sxymin;
    %figure;
    %imagesc(nxplot,nyplot,log10(Sxy)');
    %set(gca,'ydir','normal'); axis equal tight;
    %title('S_z leaving EIWG');
    %xlabel('n_x'); ylabel('n_y');
    %colorbar
end

%% Waves entering EIWG in configuration space
EHneiwg = zeros(6, 1, N);
EHneiwg(:,1,:) = EHnvac_sep(:,:,2); % The downward wave
% Do the inverse Fourier transform
% EH has size 6 x 1 x Nx x Ny
fname = [bkp_file '_EIWG'];
disp('Calculating EHeiwg ... ')
% Inverse Fourier transform (nx,ny)->(x,y)
if include_evanescent_eiwg_excitation
    % The result may be a bit different from EH_ion for "free" ground_bc.
    % The reason for it is that in fwm_nonaxisymmetric, the field at hight
    % nperp is automatically damped with a gaussian if dxkm, dykm are
    % large, i.e., is multiplied by exp(-(nperp*drdampd).^2/2),
    % where drdampd=min([dxkm dykm])*1e3*k0.
    % This gives smoother horizontal distribution of E,H
    % The discrepancy may be considered the calculational error.
    EHeiwg = fwm_assemble(k0, waves.nx, waves.ny, waves.da, EHneiwg, ...
        coorrperp, xkmi-displacexkm, ykmi-displaceykm, [fname '_EVANESCENT']);
else
    ii = find(waves.nx.^2+waves.ny.^2<1); % only non-evanescent waves
    EHeiwg = fwm_assemble(k0, waves.nx(ii), waves.ny(ii), waves.da(ii), EHneiwg(:,:,ii), ...
        coorrperp, xkmi-displacexkm, ykmi-displaceykm, fname);
end
EHeiwg = squeeze(EHeiwg); % 6 x Nx x Ny
Szeiwg = get_Sz(EHeiwg);

% Plot the power flux entering the EIWG from above
figure;
Snorm = -Szeiwg/P0;
minS = max(Snorm(:))*1e-4;
if include_evanescent_eiwg_excitation
    imagesc(xkmi, ykmi, Snorm.');
else
    Snorm(Snorm<minS)=minS;
    imagesc(xkmi, ykmi, log10(Snorm).');
end
set(gca,'ydir','normal'); axis equal tight;
% 'iff' is a custom function -- conditional assignment in C or Python
vv = iff(include_evanescent_eiwg_excitation,'Flux','log10(flux)');
title(sprintf('%s (W/m^2) down to EIWG at %.1f km for P_0=1 W', vv, hion));
xlabel('x, km'); ylabel('y, km');
colorbar
echo_print(plot_formats, fullfile(figdir, ['SzEIWG' iff(include_evanescent_eiwg_excitation,'_EVANESCENT','')]));

%% Total field at hion
figure;
EH_ion = permute(EH(:,2,:,:),[3 4 1 2]); % Nx x Ny x 6
S_ion = get_Splot(EH_ion);
imagesc(xkmi, ykmi, S_ion(:,:,3).'/P0)
set(gca,'ydir','normal'); axis equal tight;
title(sprintf('Total S_z (W/m^2) at %.1f km for P_0=1 W', hion));
xlabel('x, km'); ylabel('y, km');
colorbar
echo_print(plot_formats, fullfile(figdir,'Sz_hion'));

%% Propagation - (y,z) plane at x=0
if strcmp(configuration,'propagation')
    if isempty(hpropag)
        error('no propagation was calculated!')
    end
    %% The field
    figure;
    [~,Nxc]=min(abs(xkmi));
    EHyz = permute(EH(:,3:Mi,Nxc,:),[2 4 1 3]); % Nz x Ny x 6
    fplot = sqrt(sum(abs(EHyz(:,:,4:5)).^2,3)/P0);
    fmin = max(fplot(:))*1e-3;
    fplot(fplot<fmin)=fmin;
    imagesc(ykmi, hpropag, log10(fplot));
    set(gca,'ydir','normal'); axis equal tight;
    title('log10(|B_\perp|) for P_0=1 W at x=0');
    xlabel('y, km'); ylabel('h, km');
    colorbar
    echo_print(plot_formats, fullfile(figdir,'propagation'));
    %% Also plot the power flux (demo)
    figure;
    S = get_Splot(EHyz);
    irarykm = find(ykmi>-500 & ykmi<0);
    irarzkm = find(hpropag<200);
    %irarykm = 1:length(ykmi);
    %irarzkm = 1:length(hpropag);
    Syz = S(irarzkm,irarykm,2:3);
    Syza = sqrt(sum(Syz.^2,3));
    maxSyz = max(Syza(:));
    Syzn1 = Syz./repmat(Syza,[1 1 2]);
    Syzn2 = Syz/maxSyz;
    rdemo = 0.3;
    Syzn = rdemo*Syzn1 + (1-rdemo)*Syzn2; % for the demo, boost the small values
    quiver(ykmi(irarykm), hpropag(irarzkm), Syzn(:,:,1), Syzn(:,:,2));
    axis equal tight
    title('Poynting vector demo at x=0');
    xlabel('y, km'); ylabel('h, km');
    echo_print(plot_formats, fullfile(figdir,'flow'));
end
    

