% Radiation by a horizontal dipole: Compare axisymmetric and
% nonaxisymmetric results
global impedance0 clight mu0 eps0
if isempty(impedance0)
    loadconstants
end
h=[0 10 50:120].'; % altitudes in km
M=length(h);

f=10e3;
w=2*pi*f;
k0=w/clight;
Ne=getNe(h,'Stanford_eprof3'); % Electron density in 1/m^3
Ne(1:3) = 0; % The ionosphere starts at 51 km, at 50 km it is still zero.
perm = get_perm(h,w,'Ne',Ne,'Bgeo',[0 0 5e-5]);

% Set up the an arbitrary dipole current
ksa=[2]; % location of the current at h(ksa)
Ms=length(ksa);
% 1 A*m current moment at altitude h(ksa), converted to Budden units V*m:
% It is also the value in (nx,ny) space
In = [0;1;0;0;0;0]*impedance0;
% See Note 2 to FWM_FIELD on how to use units
Icaption=' for I_y=1 A-m at 10 km, vertical B_{geo}';

% Output altitudes
hi=[0 max(h)].';
Mi=length(hi);

% Ground boundary condition, given as the Earth dielectric permittivity
sground = 0.01; % Typical ground conductivity is 3--30 mS/m
eground=1+1i*sground/(w*eps0);
ground_bc=eground; % can be also, e.g., 'E=0' or 'free';

% Settings for FWM
retol=1e-3;
drkm=1;
rmaxkm=1000;
