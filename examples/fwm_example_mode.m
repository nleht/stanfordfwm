% Example of usage of FWM_MODEFINDER and FWM_MODESTRUCT

%% Initialize
global clight
if isempty(clight)
    loadconstants;
end
%h=[0 50:69 70:.25:95 95.5:.5:200].';
h=[0 79 80:.25:95 95.5:.5:110].';
M=length(h);
%w=2000*2*pi;
w=21400*2*pi;
k0=w/clight;
%Ne=getNe(h,'Stanford_nighttime');
Ne=getNe(h,'HAARPwinternight');
nue=get_nu_electron_neutral_swamy92(h);
% Get rid of collisions at high altitudes
if 0
    h0=170; H=4;
    cutfun=1./(1+exp((h-h0)/H)).^2;
    cutfun(end)=0;
    nue=nue.*cutfun;
end
% Anisotropic medium with vertical B field
Babs=5e-5;
[perm,S,P,D]=get_perm_with_ions(h,w,'Ne',Ne,'nue',nue,'Babs',Babs,'need_ions',0);
% Isotropic medium
permi=get_perm_with_ions(h,w,'Ne',Ne,'nue',nue,'Babs',0,'need_ions',0);

%% Find all modes
% Avoid C=0 point. There is a cut at Re(C)=0, so avoid it.
%Crmin=1e-5; Crmax=4; Cimin=-.6; Cimax=.2;
%Crmin=1e-2; Crmax=1.01; Cimin=-.05; Cimax=.05;
Crmin=0.01; Crmax=1.06; Cimin=-.1; Cimax=.05;
debugflag=1;
doplot=0;
disp('=====Modes in anisotropic medium=====');
[C,Cmul,alf,permrot,Cbox,Fbox]=fwm_modefinder(...
    k0,h,'E=0',perm,[],Crmin,Crmax,Cimin,Cimax,[],[],[],debugflag,doplot);
disp('=====Modes in isotropic medium=====');
[Ci,Cmuli,alfi,permroti,Cbox,Fboxi]=fwm_modefinder(...
    k0,h,'E=0',permi,[],Crmin,Crmax,Cimin,Cimax,[],[],[],debugflag,doplot);

%% Plot the roots
% Cbox is the same for both cases
[Nr,Ni]=size(Cbox);
Cre=real(Cbox(:,1));
Cim=imag(Cbox(1,:));
subplot(2,1,1);
imagesc(Cre,Cim,log10(abs(Fbox.'))); axis equal; set(gca,'ydir','normal')
hold on;
nC=length(C);
for k=1:nC
    text(real(C(k)),imag(C(k)),num2str(k));
end
grid on
title('ANISOTROPIC')
hold off
subplot(2,1,2);
imagesc(Cre,Cim,log10(abs(Fboxi.'))); axis equal; set(gca,'ydir','normal')
hold on;
nCi=length(Ci);
for k=1:nCi
    text(real(Ci(k)),imag(Ci(k)),num2str(k));
end
grid on
title('ISOTROPIC')
hold off

%% This box has 9 modes (both anisotropic and isotropic cases), as shown
% from the phase change when going around the box
figure;
Fboxb=[Fbox(2:end,1).' Fbox(end,1:end) Fbox(end:-1:2,end).' Fbox(2,end:-1:1)];
phase=unwrap(angle(Fboxb)-angle(Fboxb(1)))/(2*pi);
subplot(2,1,1); plot(phase,'x-');
title(['ANISOTROPIC: Total phase change=' num2str(phase(end))]);
Fboxbi=[Fboxi(2:end,1).' Fboxi(end,1:end) Fboxi(end:-1:2,end).' Fboxi(2,end:-1:1)];
phasei=unwrap(angle(Fboxbi)-angle(Fboxbi(1)))/(2*pi);
subplot(2,1,2); plot(phasei,'x-');
title(['ISOTROPIC: Total phase change=' num2str(phasei(end))]);

%% The mode structures
hi=[0:.1:110];
Mi=length(hi);
np=sqrt(1-C.^2);
EH=zeros(6,Mi,nC);
for k=1:nC
    k
    [EH(:,:,k),nz,Fext]=fwm_modestruct(C(k),k0,h,'E=0',permrot,hi);
    nzM=nz(:,M);
    SFM=real(cross(conj(Fext(1:3,:,M)),Fext(4:6,:,M)))/2;
    nvec=[repmat(np(k),[1 4]) ; zeros(1,4) ; nzM.'];
    SFa=sqrt(sum(SFM.^2));
    % Projection of vector n onto the Poynting vector, for four modes
    sum(nvec.*SFM)./SFa
end
figure; semilogy(hi, squeeze(abs(EH(3,:,:)))); grid on;
legend(num2str([1:nC]'));
title('ANISOTROPIC: Ez')
% - Some solutions blow up at higher altitudes!
EHi=zeros(6,Mi,nCi);
for k=1:nCi
    EHi(:,:,k)=fwm_modestruct(Ci(k),k0,h,'E=0',permroti,hi);
end    
figure;
subplot(2,1,1);
semilogy(hi, squeeze(abs(EHi(3,:,:))));
set(gca,'ylim',[1e-3 10]); grid on;
legend(num2str([1:nCi]'));
title('ISOTROPIC: Ez')
subplot(2,1,2);
semilogy(hi, squeeze(abs(EHi(6,:,:))));
set(gca,'ylim',[1e-3 10]); grid on;
legend(num2str([1:nCi]'));
title('ISOTROPIC: Hz')
% - isotropic solutions are not blowing up!
