% Demonstration of low-frequency calculations
%
% The peculiarity of low frequencies is that resolution "Δn_perp" in the
% horizontal component of the refractive index n_perp=k_perp/k0, which is
% by default assumed to be 2π/(rmax*k0) = clight/(rmax*f) was too large.
% For rmax=1000km and f=20Hz, for example, we get Δn_perp=15. But most of
% the waves are contained in the region |n_perp|<1.5 or so (which may be
% demonstrated by plotting results from file *_FWM_WAVES.mat) (see the
% code snippet at the end).
%
% The resolution in radial direction (along n_perp) is automatically
% calculated to be good, using the "bestgrid.m" function. However, the
% angular (azimuthal angle in n-space) resolution was insufficient for the
% reason stated above. The problem was fixed by manually increasing the
% number of angular points (an automatic resolution might be not as
% efficient). It is suggested that the default (8 angles) should be enough
% for most problems. In particular, it resolves angular anisotropy in
% radiation up to octupole moment.

%% Global setup
close all
clear all
global ech me uAtomic eps0 clight impedance0
if isempty(clight)
    loadconstants
end

rootdir = 'low_frequencies';

frequencies = [120 20]; % frequencies in Hz

% The frequencies of interest are below ion gyrofrequencies, so ions MUST
% be included! However, we provide an option not to include them (a
% hypothetical ionosphere with infinitely heavy ions).
use_ions = 1; % must use ions at the frequencies of interest!

% The minimum resolution in azimuthal angle in n-space is 2^log2Nphimin
% points
log2Nphimin = []; % Nphimin = 2^log2Nphimin
% -- default value is 3, used if [] is given here
% I tried setting log2Nphimin=6, didn't see any difference in the result.
% However, the calculation time does not change significantly, either.

% Feedback, backup and memory
global output_interval backup_interval
output_interval=60 % To observe the progress every 60 sec
backup_interval=1800 % Default = 3600 sec

plot_formats = {'PNG'};

% Create the root directories
if ~exist(rootdir,'dir')
    mkdir(rootdir)
end
figdir = fullfile(rootdir,'figures');
if ~exist(figdir,'dir')
    mkdir(figdir)
end

%% IRI profile at the given conditions
% On how to make IRI work with MATLAB, see
% https://gitlab.com/nleht/small-projects/-/tree/main/IRI2023_instructions
IRI = IRIClass;
% 18.50000, -67.10000
lat = 18.5; % north
lon = -67.1; % east
is_geomag = 0;
[h0, ionosphere] = IRI.sub(is_geomag,lat,lon,2022,905,25+6+42/60,0,600,1);
ionosphere(ionosphere==-1) = 0;
N_e = ionosphere(1,:);
N_Op = ionosphere(5,:).*N_e/100;
N_Hp = ionosphere(6,:).*N_e/100;
N_Hep = ionosphere(7,:).*N_e/100;
N_O2p = ionosphere(8,:).*N_e/100;
N_NOp = ionosphere(9,:).*N_e/100;
N_Np = ionosphere(11,:).*N_e/100;

figion = figure;
semilogx(N_e, h0);
set(gca,'xlim',[1e5, 1e12])
hold on;
semilogy(N_Op, h0);
semilogy(N_Np, h0);
semilogy(N_Hp, h0);
semilogy(N_Hep, h0);
semilogy(N_O2p, h0);
semilogy(N_NOp, h0);
grid on;
legend({'Ne', 'O+', 'N+', 'H+', 'He+', 'O2+', 'NO+'})
hold off;
xlabel('m^{-3}'); ylabel('h, km')
echo_print(plot_formats, fullfile(figdir, 'ionosphere'))

% Rarefy the altitudes for computational efficiency
itmp = slowchange(N_e, 0.05);
ind = [1 6 80 itmp(2:end)];
h = h0(ind);
M = length(h)

% Magnetic field
% We turn it a bit to be in (y,z) plane, so that it is easier to see the
% effects of its tilt. Thus, x-axis corresponds to west->east direction,
% and y-axis to south->north.
Bgeo = IRI.igrf(lat, lon, 2022, 100);
Bhoriz = sqrt(Bgeo(1)^2 + Bgeo(2)^2);
Bgeo = [0 Bhoriz Bgeo(3)]

% Ions and electrons at selected heights
ions={'O+', 'N+', 'H+', 'He+', 'O2+', 'NO+'};
Zi = [1 1 1 1 1 1];
Massi = [16 14 1 4 32 30];
Ni = [N_Op(ind); N_Np(ind); N_Hp(ind); N_Hep(ind); N_O2p(ind); N_NOp(ind)].';
Ne = N_e(ind);

%% Source and ground bc
In0 = [0;0;1;0;0;0]*1e8*impedance0;
ksa = 2; Ms = length(ksa);
ground_bc = 'E=0';

%% FWM directory, with name reflecting FWM options
if use_ions
    subdir = 'ions';
else
    subdir = 'electrons';
end
if ~isempty(log2Nphimin)
    subdir = [subdir '_Nphi' num2str(2^log2Nphimin)];
end
rmaxkm = [];
%rmaxkm = 3000;
drkm = []; retol = 1e-3;
if ~isempty(rmaxkm)
    subdir = [subdir '_rmaxkm' num2str(rmaxkm)];
end

%% Experiment with limiting n in order to accelerate calculation
% Spoiler: it was not very successful
limited = 0;
if limited
    % May give inaccurate results
    limited_nmax = 50;
    nx0 = [-limited_nmax limited_nmax]; ny0=nx0;
    Nnx = length(nx0); Nny = length(ny0);
    In = repmat(In0, [1 Ms Nnx Nny]);
    dirext = ['_nmax' num2str(limited_nmax)];
else
    nx0 = []; ny0 = [];
    In = In0;
    dirext = '';
end
subdir = [subdir dirext];

%% Create the directory
thedir = fullfile(rootdir, subdir);
if ~exist(thedir,'dir')
    mkdir(thedir);
end

%% Other options for FWM: output points
hi = (100:2:600).';
xkmi = (-1000:5:1000);
Nx = length(xkmi)
ykmi = (-1000:5:1000);
Ny = length(ykmi)
Nxc = find(abs(xkmi)<1e-3)
Nyc = find(abs(ykmi)<1e-3)

%% Run for f=120 Hz and f=20 Hz
tstart = datetime('now');

for kfreq=1:length(frequencies)

    f = frequencies(kfreq);

    %% Run for the chosen frequency
    w = 2*pi*f;
    k0=w/clight;
    bkp_file = fullfile(thedir,['nonaxisym_' num2str(f)]);
    if use_ions
        perm = get_perm_with_ions(h, w, 'Ne', Ne, 'ui', Massi, 'Zi', Zi, 'Ni', Ni, 'Bgeo', Bgeo);
    else
        perm = get_perm(h, w, 'Ne', Ne, 'Bgeo', Bgeo);
    end

    %% n-surface
    do_nsurf = 0;
    if do_nsurf
        %% n-surface calculation
        fprintf('Calculating n-surface ... ')
        N2 = 4000;
        dnp = .5;
        np = ((-N2-1:N2)+.5)*dnp;
        Nnp = length(np);
        nz=zeros(4,Nnp,M,2);
        no = zeros(Nnp,1);
        parfor k=1:M
            nz(:,:,k,1)=fwm_booker_bianisotropic(perm(:,:,k),np,no);
        end
        parfor k=1:M
            nz(:,:,k,2)=fwm_booker_bianisotropic(perm(:,:,k),no,np);
        end
        fprintf('done!\n')
        %% Plot it
        c = 'gbrc';
        for k=1:M
            for s=1:2
                subplot(1,2,s)
                for mode=1:4
                    nzp = squeeze(nz(mode,:,k,s));
                    plot(np, real(nzp),['-' c(mode)]);
                    axis equal
                    hold on
                    plot(np, imag(nzp),['--' c(mode)]);
                end
                grid on
                hold off
                title(sprintf('h=%.1f',h(k)));
            end
            pause;
        end
    end

    %% Run FWM
    fname_slices = [bkp_file '_EH_slices.mat'];
    if ~exist(fname_slices, 'file')
%         fname = [bkp_file '_EH.mat']; % maybe not needed, duplicates FWM_ASSEMBLE_XY.mat
%         if ~exist(fname,'file')
            disp('Running FWM ...')
            EH = fwm_nonaxisymmetric(f, h, ground_bc, perm, ... % Enviromnent
                ksa, 1, nx0, ny0, In, ...                       % Source
                1, xkmi, ykmi, hi, ...                          % Output points
                rmaxkm, drkm, retol, bkp_file,...               % Options
                'log2Nphimin',log2Nphimin);                     % More options
            % EH has size 6 x Mi x Nx x Ny
%             fprintf('Saving FWM results ... ')
%             save(fname, 'EH','-v7.3')
%             fprintf('done!\n')
%         else
%             fprintf('Loading FWM results ... ')
%             load(fname)
%             fprintf('done!\n')
%         end
        fprintf('Saving variables for plotting ... ')
        % Extract the (x,z) and (y,z) slices
        EHxz = EH(:,:,:,Nyc); % 6 x Mi x Nx
        EHyz = squeeze(EH(:,:,Nxc,:)); % 6 x Mi x Ny
        save(fname_slices,'EHxz','EHyz','-v7.3')
        fprintf('done!\n')
    else
        % Load EHxz, EHyz
        fprintf('Loading variables for plotting ... ')
        load(fname_slices)
        fprintf('done!\n')
    end

    %% Plot it!
    fig = figure;
    freqtitle = [num2str(f) 'Hz: '];
    fieldcoef = {1 1/clight};
    indexfield = {(1:3) (4:6)};
    fieldname = {'E' 'H'};
    fieldunits = {'[V/m]' '[T]'};
    for kfield=1:2
        % E in V/m, B in T
        ii = indexfield{kfield};
        Fxz = squeeze(sqrt(sum(abs(EHxz(ii,:,:)).^2,1)))*fieldcoef{kfield}; % Mi x Nx
        Fyz = squeeze(sqrt(sum(abs(EHyz(ii,:,:)).^2,1)))*fieldcoef{kfield}; % Mi x Ny
        Fsmall = max([Fxz(:);Fyz(:)])*1e-3;
        
        subplot(2,2,2*kfield-1);
        imagesc(ykmi, hi, log10(Fyz+Fsmall));
        set(gca,'ydir','normal'); axis tight; cb = colorbar;
        title([freqtitle 'log_{10}|' fieldname{kfield} '|, x=0'])
        title(cb,fieldunits{kfield})
        xlabel('y, km'); ylabel('z, km')
        
        subplot(2,2,2*kfield);
        imagesc(xkmi, hi, log10(Fxz+Fsmall));
        set(gca,'ydir','normal'); axis tight; cb = colorbar;
        title([freqtitle 'log_{10}|' fieldname{kfield} '|, y=0'])
        title(cb,fieldunits{kfield})
        xlabel('x, km'); ylabel('z, km')
    end
    set(fig, 'Units','centimeters');
    pos = get(fig, 'Position');
    pos(3)=40; pos(4)=20;
    set(fig,'Position',pos);
    figfname = fullfile(figdir, ['f' num2str(f) 'Hz_' subdir]);
    drawnow;
    pause(3); % wait a bit? Some evil concurrency going on!
    echo_print(plot_formats,figfname);
end

dur = datetime('now')-tstart;
fprintf('Calculations lasted for %s\n', char(dur))

%% Demonstrate at which n_perp the most of the waves are
if 0
    % After we have done some calculations
    tmp = load(fullfile(thedir,'nonaxisym_20_FWM_WAVES.mat'))
    ii = find(abs(tmp.ny)<1e-5);
    [~,isor] = sort(tmp.nx(ii));
    semilogy(tmp.nx(ii(isor)), squeeze(abs(tmp.EHn(:,100,ii(isor)))))
    xlim([-10 10])
    grid on
    xlabel('n_\perp, dimensionless')
    ylabel(['Field component amplitude at h=' num2str(hi(100)) 'km'])
    echo_print(plot_formats, fullfile(figdir,'nspace'))
end
