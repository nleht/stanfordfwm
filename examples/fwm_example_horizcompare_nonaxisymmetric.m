%% Comparison of axisymmetric and nonaxisymmetric resuts:
% This is the non-axisymmetric part

% First, load the configuration
fwm_example_horizcompare_common

% Output horizontal coordinates
xkmi=[-600:3:600];
ykmi=xkmi; % distance in y direction

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main program call:
global output_interval backup_interval
output_interval=60 % To observe the progress every 60 sec
backup_interval=600 % Default = 3600 sec
mkdir horizcompare
bkp_file='horizcompare/nonaxisym';
rperp1=xkmi; rperp2=ykmi;
coornperp=1; nperp1=[]; nperp2=[]; coorrperp=1;
EH=fwm_nonaxisymmetric(f, h, ground_bc, perm, ...
    ksa, coornperp, nperp1, nperp2, In, ...
    coorrperp, xkmi, ykmi, hi, ...
    rmaxkm,drkm,retol,bkp_file);

%% Check the field amplitude
c=3; ki=1; % component and height number
figure(5);
clf;
names = {'E_x','E_y','E_z','B_x','B_y','B_z'};
coef = [[1 1 1] [1 1 1]/clight];
imagesc(xkmi,ykmi,real(squeeze(coef(c)*EH(c,ki,:,:))).');
set(gca,'ydir','normal'); axis equal tight;
cbar=colorbar;
set(get(cbar,'title'),'string','V/m');
title([names{c} ' at h=' num2str(h(ki)) ' km' Icaption])
xlabel('x, km (geomag. W-E)')
ylabel('y, km (geomag. S-N)')

%% Magnetic field on the ground
figure(6);
clf;
ki=1;
Babs=squeeze(sqrt(sum(abs(EH(4:6,ki,:,:)).^2,1)))/clight;
imagesc(xkmi,ykmi,log10(Babs).');
set(gca,'ydir','normal'); axis equal tight;
cbar=colorbar;
set(get(cbar,'title'),'string','V/m','fontsize',12);
title(['log10(Babs) at h=' num2str(hi(ki)) ' km' Icaption])
xlabel('x, km (geomag. W-E)')
ylabel('y, km (geomag. S-N)')

%% Power flux on the upper boundary
figure(7);
clf;
S=0.5*real(cross(conj(EH(1:3,Mi,:,:)),EH(4:6,Mi,:,:)))/impedance0;
Sz=squeeze(S(3,:,:,:));
imagesc(xkmi,ykmi,log10(Sz).');
set(gca,'ydir','normal'); axis equal tight;
xlabel('x, km'); ylabel('y, km');
title(['S_z at ' num2str(hi(Mi)) ' km' Icaption])
colorbar;
echo_print({'PNG'},'horizcompare/Sz_nonax')

%%  Plot the fields on the ground
ki=1; % on the ground
%ki=2; % at 120 km
E = EH(1:3,:,:,:);
B = EH(4:6,:,:,:)/clight;
Nyc=(length(ykmi)+1)/2;
Nxc=(length(xkmi)+1)/2;
figure(8);
clf;
subplot(2,1,1);
semilogy(xkmi(Nxc:end),squeeze(abs(E(:,ki,Nxc:end,Nyc)))); legend('E_x','E_y','E_z')
title(['E, V/m at ' num2str(hi(ki)) ' km']);
xlabel('x, km')
subplot(2,1,2);
semilogy(xkmi(Nxc:end),squeeze(abs(B(:,ki,Nxc:end,Nyc)))); legend('B_x','B_y','B_z')
title(['B, T at ' num2str(hi(ki)) ' km']);
xlabel('x, km')
