%% Reflection coefficient from ionosphere
% The simplified version of fwm_example.m

loadconstants
global clight impedance0

% Altitudes of boundaries between layers in km
h=[0 50:120].';
M=length(h);
Ne=getNe(h,'Stanford_eprof1'); % in m^{-3}
Ne(1)=0; % Important for separating TE and TM waves !!!
w=2*pi*1000000
k0=w/clight
% The dielectric permittivity tensor "perm"
% x direction is to the East
% y direction is to the North
Bh = 5e-5;
phi = -pi/4
th = pi/3
perm = get_perm(h,w,'Ne',Ne,'Bgeo',[Bh*sin(phi) Bh*cos(phi) 0]);
%perm=get_perm(h,w,'Ne',Ne,'Bgeo',[0 0 -5e-5]);
%perm=get_perm(h,w,'Ne',Ne,'Bgeo',[0 1e-5 -5e-5]);
nz=zeros(4,M); Fext=zeros(6,4,M);
nx = sin(th)
ny = 0;

%% Calculate the mode structure
for k=1:M
    [nz(:,k),Fext(:,:,k)]=fwm_booker_bianisotropic(perm(:,:,k),nx, ny);
end
F=Fext([1:2 4:5],:,:);

% Note: the boundary condition 'E=0' is not used for the calculation of
% Ru{l,h}
Rground = fwm_Rground('E=0', nx, ny);
zdim=h(:).'*1e3*k0; % row, dimensionless
dz=diff(zdim);
% Reflection coefficient matrix at the ground
% components are [TE;TM]
R0=fwm_Ru0(dz,nz(:,1:M-1),F);

%%
% TE up (Eabs=1)
%u=[1;0];
% TM:
u=[0;1];

EHu=Fext(:,:,1)*[u;0;0];
EHu = EHu/exp(1i*angle(EHu(3)))

% Reflected wave (if TE is original)
d=R0*u;

EHd=Fext(:,:,1)*[Rground*d;d];

% Get rid of the phase
%EHd = EHd/EHd(5)
EHd = EHd/exp(1i*angle(EHd(5)))

% Poynting vector
S=real(cross(conj(EHd(1:3)),EHd(4:6)))/2/impedance0
