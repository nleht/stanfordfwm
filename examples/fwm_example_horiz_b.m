%HORIZ_B Example of usage of FWM_NONAXISYMMETRIC
% The horizontal magnetic field - what is the emission?
% The timing (on NLPC) for hi=[0]:
% FWM is divided into
%   FWM_BESTGRID ~ 2 min
%   FWM_WAVES    ~ 13 min
%   FWM_ASSEMBLE ~ 3 min

%% Global variables
% We need "do_vertical" as input to this file
global mu0 impedance0 eps0 clight
if isempty(mu0)
    loadconstants
end
resultsdir='.';
picturesdir='.';

%% Set up current density as a function of altitude and B in y-direction
f=1875; % Hz
w=2*pi*f;
k0=w/clight;
h=[0 55:120].'; % h in km
M=length(h);
Bgeo=[0 3e-5 0]; % Geomagnetic field in T - along y
% The current density on the vertical z-axis, in A/m^2
hmaxJ=79;
directionJ=1;
J0=exp(-(h-hmaxJ).^2/(2*3^2))*2.5e-9;

%% Limit the source in altitudes - to save calculation time
ksa=find(J0>max(J0)*1e-6); % Index of layers with the source
Ms=length(ksa)

%% Collapse current density into surface currents
dz=1e3*diff(h);
dz1=[dz(1)/2 ; (dz(1:M-2)+dz(2:M-1))/2 ; dz(M-1)/2];
I0=J0(ksa).*dz1(ksa); % to include currents both below and above the layer boundaries


%% 3D current
% Assume axially symmetric currents, with gaussian horizontal distribution
Iwidth=2.3e4;
dx0=10e3;
Nx0=128;
x0=[-Nx0/2:Nx0/2-1]*dx0;
% y-grid for current values - choose the same as x-grid
Ny0=Nx0;
dy0=dx0;
y0=[-Ny0/2:Ny0/2-1]*dy0;
[x0m,y0m]=ndgrid(x0,y0);
prof=exp(-(x0m.^2+y0m.^2)/(2*Iwidth^2));
% Electric surface current in x-direction
I=zeros(6,Ms,Nx0,Ny0);
I(directionJ,:,:,:)=repmat(I0(:),[1 Nx0 Ny0]).*repmat(shiftdim(prof,-1),[Ms 1 1]);

%% Take Fourier transform over horizontal coordinates
% I.e., convert to (nx,ny) space
dnx0=2*pi/(k0*dx0*Nx0)
nx0=[-Nx0/2:Nx0/2-1]*dnx0;
ny0=nx0;
Nnx0=length(nx0); Nny0=length(ny0); % same as Nx0, Ny0

% 1. Analytical
I0f=2*pi*Iwidth^2*I0;
nwidth=1/(k0*Iwidth) % in n-space
[nx0m,ny0m]=ndgrid(nx0,ny0);
proff=exp(-(nx0m.^2+ny0m.^2)/(2*nwidth^2));
If_an=zeros(6,Ms,Nnx0,Nny0);
If_an(directionJ,:,:,:)=repmat(I0f(:),[1 Nnx0 Nny0]).*repmat(shiftdim(proff,-1),[Ms 1 1]);

% 2. Take FFT (works for arbitrary currents)
% NOTE: we must have x0(Nx0/2+1)=nx0(Nnx0/2+1)=0
tmp=fftshift(fftshift(I,3),4);
tmp=fft(fft(tmp,[],3),[],4)*dx0*dy0; % Note the normalization!
If=fftshift(fftshift(tmp,3),4);

% 3. Compare them
tmp=max(abs(If_an(:)-If(:)))/max(abs(If(:)));
disp(['Fourier transform error=' num2str(tmp)]);


%% Set up dielectric permittivity tensor and other arguments
Ne=getNe(h,'Stanford_eprof3');
perm=get_perm(h,w,'Ne',Ne,'Bgeo',Bgeo); % ..., 'nue',nue
eground='E=0'; % = 1 + i*sground/(w*eps0);
xkm=[-500:5:500]; % Horizontal output coordinates in km
Nx=length(xkm);
ykm=xkm; Ny=length(ykm);
%rmaxkm=3000; % Optional
rmaxkm=[];
%hi=[0:5:70 72:2:120].'; % Output altitudes in km
hi=[0:15:75].';
Mi=length(hi);
drkm=[]; % Optional
%retol=1e-4; % error tolerance, optional
retol=[];
global output_interval backup_interval
output_interval=60 % To observe the progress every 60 sec
backup_interval=600 % Default = 3600 sec
xyzstr='xyz'; JMstr='JM';
% Backup file name
dirJstr=[JMstr(floor((directionJ-1)/3)+1) xyzstr(mod(directionJ-1,3)+1)]
fname=['horizb_f' num2str(f) '_h' num2str(hmaxJ) '_' dirJstr];
bkp_file=[resultsdir '/' fname];

%chi0=pi/4; % auxiliary - optional;
chi0=[];

%% Call the FWM
% Optional arguments can be skipped or replaced with []
coornperp=1; % cartesian
nperp1=nx0; nperp2=ny0;
coorrperp=1; % cartesian
rperp1=xkm; rperp2=ykm;
[EH,EHf,nx,ny]=fwm_nonaxisymmetric(f,h,eground,perm,...
    ksa,coornperp,nx0,ny0,impedance0*If,...
    coorrperp,xkm,ykm,hi,...
    rmaxkm,drkm,retol,bkp_file,chi0);

%% Fields and Poynting vector
E=EH(1:3,:,:,:);
B=EH(4:6,:,:,:)/clight;
Bp=shiftdim(sqrt(sum(abs(B(1:2,:,:,:)).^2,1)),1);
% The Poynting vector
S=permute(real(cross(conj(E),B))/2/mu0,[3 4 2 1]); % Nx x Ny x Mi x 3

%% Plotting of the field on the ground
ki=1;
figure;
subplot(1,2,1)
imagesc(xkm,ykm,real(squeeze(E(3,ki,:,:)).')/1e-3);
set(gca,'ydir','normal'); axis equal tight; colorbar;
title('E_z at the ground, mV/m'); xlabel('x (E-W), km'); ylabel('y (S-N), km');
subplot(1,2,2)
imagesc(xkm,ykm,squeeze(Bp(ki,:,:)).'/1e-12);
set(gca,'ydir','normal'); axis equal tight; colorbar;
title('B_\perp at the ground, pT'); xlabel('x (E-W), km');

picbase=[picturesdir '/' fname];
echo_print({'EPS','PNG'},[picbase '_EBg']);

%% The radial Poynting vector integrated over altitudes
[xm,ym]=ndgrid(xkm,ykm);
phim=repmat(atan2(ym,xm),[1 1 Mi]);
Sr=S(:,:,:,1).*cos(phim)+S(:,:,:,2).*sin(phim); % Nx x Ny x Mi
Sphi=-S(:,:,:,1).*sin(phim)+S(:,:,:,2).*cos(phim);
phi=[0:5:360]*pi/180; dphi=phi(2)-phi(1); Nphi=length(phi);
r=[0:5:500]; Nr=length(r);
[rg,phig]=ndgrid(r,phi);
xg=rg.*cos(phig);
yg=rg.*sin(phig);
Src=(Sr(:,:,1:Mi-1)+Sr(:,:,2:Mi))/2;
tmp=repmat(shiftdim(diff(hi(:))*1e3,-2),[Nx Ny 1]); % dz in m
Srint=sum(Src.*tmp,3); % integrate over height, Nx x Ny
Srphir=interp2(ykm,xkm,Srint,yg,xg); % Nr x Nphi
Srr=sum(Srphir(:,1:Nphi-1)*2*pi.*rg(:,1:Nphi-1)*1e3*dphi,2); % as a function of r

%% Radiation diagram
figure;
rchosen=[75 100 200 300];
nchosen=length(rchosen);
sx=zeros(length(phi),nchosen); sy=sx;
for k=1:nchosen
    ir=find(r==rchosen(k));
    s=Srphir(ir,:);
    tmp=max(s);
    sx(:,k)=s.*cos(phi);
    sy(:,k)=s.*sin(phi);
end
tmp=max(max(sx))
plot(sx,sy,'linewidth',1); axis equal; hold on;
set(gca,'fontsize',14);
plot([-tmp tmp],[0 0],'k--','linewidth',1);
plot([0 0],[-tmp tmp],'k--','linewidth',1);
axis tight; hold off
legend([repmat('r=',[nchosen 1]) num2str(rchosen(:)) repmat(' km',[nchosen 1])])
xlabel('\int S_x dz, W/m'); ylabel('\int S_y dz, W/m')
echo_print({'EPS','PNG'},[picbase '_rd']);
