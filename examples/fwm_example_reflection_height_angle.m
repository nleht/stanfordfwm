%% Reflection height
% This is a modified file examples/fwm_example_reflection_height.m
loadconstants
global clight me ech eps0


%% The grid
prof='HAARPwinternight'; hminplot=80;
f=100000; hmin=70; dh=.1; hmax=135; thetadmax=89.75; dthd=0.025; hmaxplot=100;
% f=50000; hmin=60; dh=.2; hmax=135; thetadmax=89.75; dthd=0.05; hmaxplot=110;
% f=20000; hmin=50; dh=.5; hmax=150; thetadmax=89.75; dthd=0.05; hmaxplot=150;
% f=10000; hmin=0; dh=.5; hmax=150; thetadmax=89.7; dthd=0.1; hmaxplot=150;
%Bgeo=[2e-5 2e-5 0];
disp('**************************************************')

%% Initial setup
h = [0 hmin:dh:hmax].';
M = length(h)
Ne = getNe(h, prof);
% Ne(1)==0 is important for separating TE and TM waves !!!

thetasd = -thetadmax:dthd:thetadmax;
N = length(thetasd)
thetadplot = -85:10:85;
kthplot=find(thetasd==thetadplot(1)):round(10/dthd):N;

%% The long cycle
f
w=2*pi*f;
k0=w/clight;
zd=h*1e3*k0; % dimensionless height
%zdi=zd; % Output altitudes are the same
%[kia,dzdl,dzdh]=fwm_get_layers(zd,zdi);
perm=get_perm(h,w,'Ne',Ne,'Bgeo',Bgeo);
nz=zeros(4,M,N); Fext=zeros(6,4,M,N);
thetitle=[prof ' B=' num2str(Bgeo(1)) ',' num2str(Bgeo(2)) ',' num2str(Bgeo(3)) 'T']
fname=[prof '_' num2str(Bgeo(1)) '_' num2str(Bgeo(2)) '_' num2str(Bgeo(3))];
disp('Part 1')
drawnow('update')
for k=1:M
    [nz(:,k,:),Fext(:,:,k,:)]=fwm_booker_bianisotropic(perm(:,:,k),sind(thetasd),0);
end
F=Fext([1:2 4:5],:,:,:);
Rorig=zeros(2,N);
Eorig=zeros(2,M,N);
disp('Part 2')
drawnow('update')
parfor knp=1:N
    thid = thetasd(knp);
    nx0=sind(thid);
    nz0=cosd(thid);
    Rground = fwm_Rground('E=0', nx0, 0);
    [Pu,Pd,Ux,Dx,Ruh,Rdl,Rul,Rdh] = fwm_radiation(zd,nz(:,:,knp),F(:,:,:,knp),Rground);
    % - the ground boundary condition is not really needed for Ru{l|h} but
    % we have to give it for other outputs
    % Reflection coefficient matrix (from the sky) at the ground
    [vv,dd]=eig(Rul(:,:,1)); % extract eigen-modes
    Rorig(:,knp)=diag(dd);
    ul=zeros(2,2,M); dl=zeros(2,2,M);
    % For propagation up:
    ul(:,:,1)=vv; % [1;0] for TE wave; [0;1] for TM wave
    for k=1:M-1
        uh=Pu(:,:,k)*ul(:,:,k);
        ul(:,:,k+1)=Ux(:,:,k)*uh;
        dl(:,:,k)=Pd(:,:,k)*Ruh(:,:,k)*uh;
    end
    ud=permute(cat(1,ul,dl),[1 3 2]);
    %EH=Fext*ud; where first two dims of Fext and first dim of ud is used
    EH=permute(sum(repmat(Fext(:,:,:,knp),[1 1 1 2]).*repmat(shiftdim(ud,-1),[6 1 1 1]),2),[1 4 3 2]);
    Eorig(:,:,knp) = squeeze(sqrt(sum(abs(EH(1:3,:,:)).^2)));
end
%%%%%%%%%%%%%%%%%%%%
% Switch modes to provide continuity
disp('Part 3')
drawnow('update')
%Ei=zeros(2,2,N);
R=Rorig;
%iswitch=zeros(2,N);
E=Eorig;
for knp=1:N
    % Provide the continuity
    Rtmp = Rorig(:,knp);
    if knp==1
        %iswitch(:,knp)=[1 2];
        ii=[1 2];
    else
        % Which one is closer to the previous value?
        Rprev = R(:,knp-1);
        x1=sum(abs(Rtmp-Rprev));
        x2=sum(abs(Rtmp(2:-1:1)-Rprev));
        if x1<x2
            ii=[1 2];
        else
            ii=[2 1];
        end
    end
    % Reflection coefficient (scalar, for 2 eigen-modes)
    R(:,knp)=Rtmp(ii);
    E(:,:,knp)=Eorig(ii,:,knp);
    % The field structure in each eigen-mode
    %EHtmp=Fext(:,:,1)*[vv(:,ii);zeros(2,2)];
    % Rotate to the coordinate system connected to the incident ray
    %Ei(1,:,knp)=nz0*EHtmp(1,:)-nx0*EHtmp(3,:); % Ex'
    %Ei(2,:,knp)=EHtmp(2,:); % Ey'
end

%% Results
% Ei1 has Ex' with zero phase
%Eangle=angle(Ei(1,:,:));
%Ei1=Ei.*exp(-1i*repmat(Eangle,[2 1 1]));

% Phase unwrapping starts at the grazing incidence!
%phase=unwrap(angle(R(:,N:-1:1)),[],2); %+2*n*pi, where n is unknown
%phase=phase(:,N:-1:1);
% Since we start at negative angles, not anymore:
phase=unwrap(angle(R),[],2); %+2*n*pi, where n is unknown
kz = k0*cosd(thetasd);
zr=(phase+pi)./2./repmat(kz,[2 1]);
zramb=2*pi./2./repmat(kz,[2 1]); % ambiguity

%% Figures
if plot_atmosphere
    figure(1);
    semilogx(Ne, h)
    xlabel('N_e')
    ylabel('h, km')
    title(prof)
    echo_print({'PNG','FIG'},prof)

    figure(2);
    semilogx(sqrt(ech^2*Ne/(eps0*me))/(2*pi), h)
    hold on
    semilogx(get_nu_electron_neutral_swamy92(h), h)
    hold off
    ylim([hminplot hmax])
    xlabel('Frequency, Hz')
    ylabel('h, km')
    legend('f_e','\nu_e')
    grid on
    title(prof)
    echo_print({'PNG','FIG'},[prof '_wp'])
end

%%
figure(3); plot(thetasd,zr/1e3,'LineWidth',2);
plot_ambiguity_1 = 1;
plot_ambiguity_2 = 1;
if plot_ambiguity_1
    hold on;
    set(gca,'ColorOrderIndex',1)
    plot(thetasd,(zr-zramb)/1e3,'--');
    set(gca,'ColorOrderIndex',1)
    plot(thetasd,(zr+zramb)/1e3,'--');
    hold off
end
if plot_ambiguity_2
    hold on;
    set(gca,'ColorOrderIndex',1)
    plot(thetasd,(zr-2*zramb)/1e3,'--');
    set(gca,'ColorOrderIndex',1)
    plot(thetasd,(zr+2*zramb)/1e3,'--');
    hold off
end
hold on;
for knp=kthplot
    set(gca,'ColorOrderIndex',1)
    plot(E(:,2:M,knp)*10+thetasd(knp),h(2:M))
end
hold off
xlabel('Incidence angle, degrees'); ylabel('Reflection height, km')
xlim([-90 110])
ylim([hmin hmaxplot])
title(thetitle)
echo_print({'PNG','FIG'},[fname '_zr'])

%%
figure(4); plot(thetasd,abs(R));
xlim([-90 90])
ylim([0 1])
xlabel('\theta'); ylabel('|R|');
title(thetitle)
echo_print({'PNG','FIG'},[fname '_R'])

% %% Resonances
% figure(5); plot(thetasd,1./abs(R+1));
% % If the Earth were 'H=0': hold on; plot(farr/1e3,1./abs(R-1)); hold off
% xlabel('\theta'); ylabel('1/|R+1|');
% title('Resonances')

% %%
% figure(6); plot(thetasd,abs(squeeze(Ei1(1,:,:))));
% hold on; plot(thetasd,abs(squeeze(Ei1(2,:,:))),'--'); hold off;
% 
% %%
% Eyphase=squeeze(angle(Ei1(2,:,:)));
% figure(7); plot(thetasd,unwrap(Eyphase,[],2));
