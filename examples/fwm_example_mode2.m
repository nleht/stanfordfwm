% Check the long distance propagation capabilities

%% Initialize
global clight
if isempty(clight)
    loadconstants;
end
%h=[0 50:69 70:.25:95 95.5:.5:200].';
h=[0 0.001 79 80:.25:95 95.5:.5:110].';
M=length(h);
%w=2000*2*pi;
f=21400;
w=2*pi*f;
k0=w/clight;
%Ne=getNe(h,'Stanford_nighttime');
Ne=getNe(h,'HAARPwinternight');
nue=get_nu_electron_neutral_swamy92(h);
% Get rid of collisions at high altitudes
if 0
    h0=170; H=4;
    cutfun=1./(1+exp((h-h0)/H)).^2;
    cutfun(end)=0;
    nue=nue.*cutfun;
end
% Anisotropic medium with vertical B field
Babs=5e-5;
perm=get_perm_with_ions(h,w,'Ne',Ne,'nue',nue,'Babs',Babs,'thB',0,'need_ions',0);
eground='E=0';

%% Output points
hi=[0:.1:110].'; % output altitudes in km
Mi=length(hi);
xkm=[0:2000]; % output distances in km
Nx=length(xkm);

%% Calculate mode eigenvalues
debugflag=1;
if exist('long_propagation_roots.mat')~=2
    [C,Cmul,alf,permrot,Cbox,Fbox]=fwm_modefinder(...
        k0,h,eground,perm,[],1e-5,2,-.2,.05,[],[],debugflag);
    % Sort by attenuation
    [tmp,ii]=sort(abs(alf));
    C=C(ii); alf=alf(ii); Cmul=Cmul(ii);
    save long_propagation_roots C Cmul alf Cbox Fbox
else
    % Skip lengthy calculation
    load long_propagation_roots
    permrot=perm;
end
nC=length(C);
nx=sqrt(1-C.^2);
ii=find(imag(nx)<0); nx(ii)=-nx(ii);

%% Adjoing permittivity
% In this case (vertical B) is the same
permadj=perm;
for k=1:M
    permadj(:,:,k)=transpose(perm(:,:,k));
    permadj(1,[2:3],k)=-permadj(1,[2:3],k);
    permadj([2:3],1,k)=-permadj([2:3],1,k);
end

%% Modes and adjoint modes
EHmode=zeros(6,Mi,nC);
for k=1:nC
    EHmode(:,:,k)=fwm_modestruct(C(k),k0,h,eground,perm,hi);
end
EHmodeadj=zeros(6,Mi,nC);
for k=1:nC
    EHmodeadj(:,:,k)=fwm_modestruct(C(k),k0,h,eground,permadj,hi);
end
% Ex, Ey, Hz are zero at the ground! (we get a small field due to numerical error)
EHmode([1 2 6],1,:)=0; EHmodeadj([1 2 6],1,:)=0;


%% The Pappert-Smith [1972] vector and conjugate of its adjoint
fvec=EHmode([2 3 5 6],:,:);
% Adjoint
gvecc=EHmode([6 5 3 2],:,:); gvecc([2 3],:,:)=-gvecc([2 3],:,:);
% Normalize gvecc
for k=1:nC
    % Integrate over hi , dhi=100 m;
    tmp=sum(sum(gvecc(:,:,k).*fvec(:,:,k)))*100;
    gvecc(:,:,k)=gvecc(:,:,k)/tmp;
end
ormat=zeros(nC,nC);
for k1=1:nC
    for k2=1:nC
        ormat(k1,k2)=sum(sum(gvecc(:,:,k1).*fvec(:,:,k2)))*100;
    end
end
% - ormat must be unit matrix
figure; imagesc([1:nC],[1:nC],abs(ormat)); colorbar;

%% Now, the amplitudes are obtained from
% Am=\int (-Kz*g1ccm+Ky*g2ccm+Iz*g3ccm-Iy*g4ccm) dz
% If we have I=I0z delta(z), with I0z=1 V*m (Budden units), then
% Am= g3ccm(z=0)
Am=squeeze(gvecc(3,1,:));
save long_propagation_modes f w k0 Babs C nC nx hi Mi EHmode Am


%% Calculate the fields using FWM
% Specify the current I in nperp=(nx,ny) space
% For a point source, the current is a constant in (nx,ny) space
np0=[]; % The points at which the current is given, =[] for point source
I0=[0;0;1;0;0;0]; % vertical current of moment = 1 V*m
ksa=[2]; % it is placed at h(ksa);
m=0; % the circular harmonic, Ir,Iphi,Iz(nx,ny)~exp(i*m*th)

% Small memory
hisp=[0:1:110];
Misp=length(hisp);

% Main program call:
% ONLY FOR thB=0!!!!!
if exist('long_propagation_fwm.mat')~=2
    [EH,EHf0,np,ki2,npt,EHft0]=fwm_axisymmetric(f,h,eground,perm,ksa,np0,m,I0,xkm,hi);
    save long_propagation_fwm EH
else
    load long_propagation_fwm
end
% EH (6 x Mi x Nx)

%% Compare them
%figure;
EHexc=zeros(6,Nx); % on the ground
%chosenmodes=[1 3 6 8]
chosenmodes=[1:nC];
for k=chosenmodes
    for mm=1:6
        % There is no good justification of the factor (k0*nx(k)/2) so far
        % Except that in 1D case G=i/(2*kp)*exp(i*kp*x)
        % and in 2D case G=(i/4)*besselh(0,1,kp*x)
        % so exp(i*kp*x) corresponds to kp/2*besselh(0,1,kp*x)
        EHexc(mm,:)=EHexc(mm,:)+EHmode(mm,1,k)*(k0*nx(k)/2)*besselh(0,1,k0*nx(k)*xkm*1e3)*Am(k);
    end
    %semilogy(xkm,abs(EHexc)); hold on; plot(xkm,squeeze(abs(EH(:,1,:))),'--'); hold off;
    %title(k)
    %pause
end

%%
figure; semilogy(xkm,abs(EHexc)); hold on;
plot(xkm,squeeze(abs(EH(:,1,:))),'--'); hold off;
grid on; legend('Ex','Ey','Ez','Hx','Hy','Hz','FWM results');
title(['f=' num2str(f/1000) ' kHz, B=' num2str(Babs) ' T, thB=0'])
xlabel('r, km'); ylabel('E,H (V/m) for I=1 V*m');
%echo_print({'FIG','EPS'},'long_propagation')

%%
if 0
figure; semilogy(xkm,real(EHexc)); hold on;
plot(xkm,squeeze(real(EH(:,1,:))),'--'); hold off;
grid on; legend('Ex','Ey','Ez','Hx','Hy','Hz','FWM results');
title(['f=' num2str(f/1000) ' kHz, B=' num2str(Babs) ' T, thB=0'])
xlabel('r, km'); ylabel('E,H (V/m) for I=1 V*m');
%echo_print({'FIG','EPS'},'long_propagation')
figure; semilogy(xkm,imag(EHexc)); hold on;
plot(xkm,squeeze(imag(EH(:,1,:))),'--'); hold off;
grid on; legend('Ex','Ey','Ez','Hx','Hy','Hz','FWM results');
title(['f=' num2str(f/1000) ' kHz, B=' num2str(Babs) ' T, thB=0'])
xlabel('r, km'); ylabel('E,H (V/m) for I=1 V*m');
end
% AGREEMENT IS AMAZING!

