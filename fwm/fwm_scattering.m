function dEH=fwm_scattering(k0,h,eground,perm,ksa,nx0,ny0,I0,...
    xkmi,ykmi,hi,rmax,retol,inxtyp,inytyp,bkp_file)

global output_interval backup_interval
if isempty(output_interval)
    output_interval=60;
end
if isempty(backup_interval)
    backup_interval=3600;
end
zd=h*k0*1e3;
Ms=length(ksa);
Mi=length(hi);

%% Find optimal grid - similar to FWM_WAVES
disp('***** Calculating the best grid *****');
% hi=[0].';
% rmax=600e3; retol=1e-3; % -- these determine the grid
%rmax=sqrt(max(abs(xkm))^2+max(abs(ykm))^2)*1e3;
% Mi=length(hi);
zdi=hi*k0*1e3;
[kia,dzl,dzh]=fwm_get_layers(zd,zdi);
%Nnx0=length(nx0); Nny0=length(ny0);
Ifc=I0(:,:,inxtyp,inytyp); % some typical current
fun=@(np_arg) permute(fwm_field(zd,eground,perm,ksa,np_arg,0,...
        repmat(Ifc,[1 1 length(np_arg)]),kia,dzl,dzh),[3 1 2]);
% The minimum dnperp, determined by max(x)
magic_scale=0.9; % 0.9 works for vacuum; probably =0.5 will work for anything.
% To resolve it, we need at least
dn0=magic_scale*2*pi/(k0*rmax)
npmax=sqrt(max(nx0)^2+max(ny0)^2)
npbi=fwm_initial_npb(npmax,dn0);
bkp_file_ext=[bkp_file '_grid.mat']; % Backup file name
if ~exist(bkp_file_ext,'file')
    [npb,np,EHf0,relerror]=bestgrid(fun,{},npbi,dn0,retol);
    eval(['save ' bkp_file_ext ' npb np EHf0 relerror']);
else
    eval(['load ' bkp_file_ext]);
    disp(['Loaded precalculated file ' bkp_file_ext]);
end

%% Create a polar grid using found "np" by adding chi-dependence
% i.e., calculate nx,ny,da
tmp=find(np>nx0(1) & np<nx0(end));
ip1=tmp(1); ip2=tmp(end);
npg=np(ip1:ip2); npbg=npb(ip1:ip2+1);
Nnp=length(npg);
chimax=zeros(1,Nnp);
Nchi=zeros(1,Nnp);
ny1=max(ny0);
nx1=min(nx0);
if nx1<0
    error('this method is not applicable');
end
for ip=1:Nnp
    np1=npg(ip);
    if np1>sqrt(ny1^2+nx1^2)
        chimax(ip)=atan2(ny1,sqrt(np1^2-ny1^2));
    elseif np1>nx1
        chimax(ip)=atan2(sqrt(np1^2-nx1^2),nx1);
    else
        error('should not get here')
    end
    Nchitmp=2*chimax(ip)*np1/dn0; % Nphi must be greater than this
    Nchi(ip)=2*max(1,ceil(Nchitmp/2))+1;
end
%Nchimax=max(Nchi);
ch2=cumsum(Nchi);
Ntot=ch2(Nnp);
ch1=[0 ch2(1:Nnp-1)];
disp(['Number of (nx,ny) modes = ' num2str(Ntot)]);
nx=zeros(1,Ntot); ny=zeros(1,Ntot); da=zeros(1,Ntot);
% Correction of a bug! Was:
%ringarea=pi*diff(npbg.^2);
sectorarea=diff(npbg.^2)/2; % per radian
for ip=1:Nnp
    ii=[ch1(ip)+1:ch2(ip)];
    dchi=2*chimax(ip)/Nchi(ip);
    tmp=(Nchi(ip)-1)/2;
    p=[-tmp:tmp]*dchi;
    nx(ii)=npg(ip)*cos(p);
    ny(ii)=npg(ip)*sin(p);
    % Correction of a bug! Was:
    %da(ii)=ringarea(ip)/Nchi(ip);
    da(ii)=dchi*sectorarea(ip);
end

%% Interpolate onto the found grid
disp('***** Interpolating the currents onto the best grid *****');
Ifp=permute(I0,[4 3 1 2]);
Ifi=zeros(6,Ms,Ntot);
for c=1:6
    %c
    for ks=1:Ms
        % I0p(:,:,c,ks) has size Nny0 x Nnx0
        Ifi(c,ks,:)=interp2(nx0,ny0,Ifp(:,:,c,ks),nx,ny);
    end
end

if any(isnan(Ifi(:)))
    error('Ifi is nan');
end

%% Cycle over the (nx,ny) mesh (the rest of FWM_WAVES)
disp('***** Calculate the field in (nx,ny)-space *****');
% The whole idea is just to calculate
%    EHf=fwm_field(zd,eground,perm,ksa,nx,ny,Ifi,kia,dzl,dzh);
% but it requires a lot of memory, so we split calculation in several chunks
chunk1=[0:1000:Ntot-1]; % chunk starts -1
chunk2=[chunk1(2:end) Ntot]; % chunk ends
Nchunks=length(chunk1)
% Auxiliary arguments
bkp_file_ext=[bkp_file '_EHf.mat']; % Backup file name
% Load backup if available
if ~exist(bkp_file_ext,'file')
    EHf=zeros(6,Mi,Ntot);
    ipstart=1;
else
    eval(['load ' bkp_file_ext]);
    ipstart=ip+1;
    if ipstart>Nchunks
        disp('EHF: loaded pre-calculated results');
        % Amend the backup file
        %eval(['save ' bkp_file_ext ' ip EHf nx ny da']);
    end
end
tstart=now*24*3600; toutput=tstart; timebkup=tstart;
for ip=ipstart:Nchunks
    ii=[chunk1(ip)+1:chunk2(ip)];
    EHf(:,:,ii)=fwm_field(zd,eground,perm,ksa,nx(ii),ny(ii),Ifi(:,:,ii),kia,dzl,dzh);
    timec=now*24*3600;
    ttot=timec-tstart;
    if timec-toutput>output_interval
        toutput=timec;
        disp(['EHF: Done=' num2str(chunk2(ip)/Ntot*100) '%; ' ...
            'Time=' hms(ttot) ...
            ', ETA=' hms(ttot/(chunk2(ip)-chunk1(ipstart))*(Ntot-chunk2(ip)))]);
    end
    if timec-timebkup>backup_interval || ip==Nchunks
        timebkup=timec;
        disp(['Backing up to ' bkp_file_ext ' ...']);
        eval(['save ' bkp_file_ext ' ip EHf nx ny da chimax Nchi npg npbg']);
        disp(' ... done');
    end
end

%% Inverse Fourier transform
% Does not take long for hi==[0], but takes longer for extended hi
disp('***** Sum the modes with appropriate weights (last step) *****');
% 6 x Mi x Nxi x Nyi
dEH=fwm_assemble_xy(k0,nx,ny,da,EHf,xkmi*1e3,ykmi*1e3,bkp_file);
