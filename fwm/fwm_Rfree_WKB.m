function Rfree=fwm_Rfree_WKB(direction,dzd,nz,Q,approach)
%FWM_RFREE_WKB Corrected reflection coefficient for radiative bc
%
% Usage: Rfree=fwm_Rfree_WKB(direction,dzd,nz,Q[,approach])
%
% Inputs:
% (note: we denote Q:=Qc here for brevity)
% Sizes: Mc=4 for cold medium
%    dzd (scalar) - dh*k0 (dimensionless)
%    direction   - +1 or -1, upper or lower boundary?
%    nz (Mc x 2), Q (Mc x Mc x 2) - last two values before upper/lower boundary
%
% Output:
%    Rfree (Mc/2 x Mc/2) - reflection coefficient (zero for a uniform medium).
%
% Implementation notes:
% See notebook #11, page 115.
% Diagonalize w=(u,d) evolution matrix Ls. Ls is strictly diagonal in
% uniform media, where it is equal to N=inv(Q)*L*Q (L is the
% Clemmow-Heading operator), the matrix with nz on the diagonal. We expect
% that it is almost diagonal in slowly changing media. We find
%   Ls=N-i*Gamma 
% where
%   Gamma=-inv(Q)*(dQ/d(k0*z)).
% The diagonalization is Ls=Qs*Ns*inv(Qs), where Ns is diagonal. The
% reflection coefficient is zero for the re-diagonalized amplitudes ws,
% defined from Qs*ws=w, so the reflection coefficent for w is expressed in
% terms of Qs.
%
% The challenge is that Q is not determined uniquely, but up to phases and the
% order of its columns. The first issue was resolved by minimizing the
% difference between two neighboring points (approach==1), or by using an
% expression which is invariant to phase change (approach==2), the second
% issue is not present if all nz are different enough not to cross into one
% another on interval dz (we assume they have been sorted by FWM_BOOKER).
%
% See also: FWM_RGROUND
% Author: Nikolai G. Lehtinen

%% Check inputs
if nargin<5
    approach=[];
end
if isempty(approach)
    approach=1;
end
if ~(direction==-1 || direction==1)
    error('Direction must be +-1')
end

Mc=size(nz,1);
if ~(size(nz,2)==2 && all(size(Q)==[Mc Mc 2]))
    error('wrong size')
end

%% Find QidQ == inv(Q)*dQ == -Gamma*dz
Q2=Q(:,:,2); nz2=nz(:,2);
Q1=Q(:,:,1); nz1=nz(:,1);
% The following two methods give same results (approximately)
switch approach
    case 1
        % Need to (1) permute the columns of F(M-1) so that they align with modes
        % at M given by F(M)
        % - skip this step for now
        % (2) make sure the phases are correct too
        % The phase is found by minimizing |Q2(:,k)-exp(i*ph(k))*Q1(:,k)|^2
        % This gives
        ph=diag(angle(Q1'*Q2)); % all 4 phases
        Q1new=Q1*diag(exp(1i*ph));
        QidQ=0.5*(Q2+Q1new)\(Q2-Q1new); % approximately
        %FidF=FM\(FM-Fp); % almost same result
    case 2
        % QidQ(i,j) == -(Q\dL*Q)_{ij}/(nz(i)-nz(j))
        % Another way: Use L to find Q1
        % This way, we don't need to worry to match modes and their phases.
        L1=Q1*diag(nz1)/Q1; % - does not depend on the mode phases/order
        nalmost=Q2\L1*Q2 % almost diagonal, note: Q2\L2*Q2 == n2
        dn=nalmost-diag(nz2) % small
        QidQ=dn./(repmat(nz2,[1 4])-repmat(nz2.',[4 1]))
        QidQ(~isfinite(QidQ))=0; % Rule out diag. elements and multi-roots
end

%% Re-diagonalize propagation operator (diagonalize Ls)
Ls=diag(nz2)-QidQ/(1i*dzd); % propagation operator for w=(u,d)

[Qs,tmp]=eig(Ls);
sortangle=pi/100; % if imaginary part is extremely small, we can sort by real part
% Must sort again, to find outward
nzs=diag(tmp);
[~,ii]=sort(-imag(nzs*exp(1i*sortangle)));
%nzs(ii)
Qs=Qs(:,ii);
if direction==+1
    iout=[1:Mc/2]; iin=[Mc/2+1:Mc];
else % direction==-1
    iin=[1:Mc/2]; iout=[Mc/2+1:Mc];
end
% In terms of ws=(us,ds), the reflection is zero, i.e., ds==0. Substituting
% this into Qs*ws=w, we get d=Rfree*u, where
Rfree=Qs(iin,iout)/Qs(iout,iout);
% This reflection coefficient turns outward waves into inward waves.
