% 2010-07-07
% Initial development and testing of CFWM (continuous full-wave method)
% ---------------------------------------------------------------------
% Both FWM and CFWM solve the Shrodinger equation
%   d/dz*(u,d)=1i*H*(u,d)
% where H=k0*diag(nz)+1i*m, with matrix m=F^{-1}dF/dz.
% In FWM, nz and F are piecewise-constant, in CFWM, they are continuous.
%
% In this script, we find the propagation matrix P(z2,z1), such that
%   (u,d)(z2)=P(z2,z1)*(u,d)(z1)
% The recursion formulas for the reflection coefficients Ru, Rd and
% propagation matrices U, D for u, d (separate calculation of which in FWM
% is necessary for the numerical stability) are derived from the elements
% of P:
%   Ru1=Ru(z1)= (P12du+P12dd*Ru2)*(P12uu+P12ud*Ru2)^{-1}
%             =-(P21dd-Ru2*P21ud)^{-1}*(P21du-Ru2*P21uu)
%   U=U(z2,z1)=P21uu+P21ud*Ru1=(P12uu+P12ud*Ru2)^{-1}
% and analogous formulas for Rd(z2) in terms of Rd(z1), and D=D(z1,z2).
% Here we shorten the notation by Ru2=Ru(z2), P12=P(z1,z2), P21=P(z2,z1).
% Puu, Pud etc are the submatrices corresponding to {u|d} portions.
% Note that these formulas look identical to ones used in FWM for
% transition between media, with Tu replaced by P(z2,z1) and Td replaced by
% P(z1,z2)=(P(z2,z1))^{-1}).
%
% Unlike FWM, CFWM solves the Schrodinger equation only approximately, but
% for a smooth medium (i.e., without sudden changes of the dielectric
% constant). However, the convergence for dz->0 can be made better because
% of the better model of the medium.
%
% To solve it, we devised several methods. The best one is to split H into
% a part that can be diagonalized by a linear transformation R (i.e.,
% H0=R*w*R^{-1}, w is a z-dependent diagonal matrix) and a perturbation
% part that becomes a non-diagonal matrix V (i.e., with zero on diagonals):
%   H=H0+R*V*R^{-1}=R*(w+V)*R^{-1}
% Both w and V are expanded in Taylor series around the center of each
% interval zc(k)=(z(k)+z(k+1))/2.
% Then the Shrodinger equation is solved approximately by expanding in
% powers of V, we use the fact that commutator
%   [H0(z1),H0(z2)]=R*[w(z1),w(z2)]*R^{-1}=0
% Thus, we may use the Dyson series expansion in powers of V. Only zeroth
% and first order terms are kept. The answer is in the form:
%   P21=R*Pp*Tcor*Pm*R^{-1};
% where
%   R^{-1} -- describes rotation of (u,d) to a convenient basis
%   Pm=diag(exp(1i*Integral[w,{z,0,dz/2})) -- describes transport
%                                             from 0 to dz/2
%   Tcor=eye(Ndim)+DWKB -- describes the corrections to transport
%   Pp=diag(exp(1i*Integral[w,{z,dz/2,dz})) -- describes transport
%                                              from dz/2 to dz
%   R -- describes rotation out of the convenient basis back to (u,d)
% We see that (u,d) is continuous and plays the old role of (Ep,Hp).
% The (u,d) in the rotated basis, i.e. w=(w1,w2)=R^{-1}*(u,d) plays
% the old role of (u,d) (then R is analogous to F).
% Thus, instead of tracking reflection coefs u->d and d->u, we can track
% the "reflection coefficients" that transform w1->w2 and w2->w1.
% The transport of w is given by
%   w(z2-0)=Pp*Tcor*Pm*w(z1+0)=PP2*Tcor*PP1*Trot*w(z1-0),
% with Trot=R^{-1}*Rprev being the transport through boundary z1
% (Rprev is the R for the layer below z1).
% This is completely analogous to old FWM, in which Tcor and Trot
% are the abrupt transitions (analogous to FWM's T=F2^{-1}*F1), and
% diagonal matrices Pm and Pp are the continuous
% propagation in the bulk (analogous to FWM's diag(Pu,(Pd)^{-1})).
% For the stability against swamping, we see that w has to be
% sorted by descending imaginary part.

do_rediag=1;

do_exp=0; % Choose n(z) profile
do_spline=1; % Evaluation of m - exact vs interpolation. In advanced applications, has to be 1.
do_TM=1; % Choose the mode
do_doublepoints=1; % Evaluate H and F at zc vs interpolation from values at z(k)

% Matrix size - total number of modes. We use 2 modes: 1 up, 1 down:
Ndim=2;

% Interval
z1=0;
z2=1;

% Choose the medium properties and the wave frequency
n1=1;
n2=20;
k0=2*pi*30;

% Horizontal k-vector
nx=0.5;

if do_exp
    alfa=log(n2/n1)/(z2-z1);
    get_n=@(z_arg) n1*exp(alfa*(z_arg-z1));
    get_nder=@(z_arg) alfa*n1*exp(alfa*(z_arg-z1));
else
    % Linear n
    get_n=@(z_arg) (n1*(z2-z_arg)+n2*(z_arg-z1))/(z2-z1);
    get_nder=@(z_arg) (n2-n1)/(z2-z1);
end
get_nz=@(n_arg) sqrt(n_arg.^2-nx^2);
if do_TM
    % TM wave: 
    % Field structure: F*(u,d)=(Ex,Hy)
    get_F=@(n_arg) [get_nz(n_arg)/n_arg get_nz(n_arg)/n_arg; n_arg -n_arg];
    % m==F^{-1}*F'
    get_m_exact=@(z_arg) get_nder(z_arg)/(2*get_n(z_arg))*([1 -1;-1 1]+...
        nx^2/(get_n(z_arg)^2-nx^2)*[1 1;1 1]);
    % Clemmow-Heading operator acting on f=(Ex,Hy): df/dz=i*k0*L*f
    get_L=@(n_arg) [0 get_nz(n_arg)^2/n_arg^2; n_arg^2 0];
else % TE
    % F*(u,d)=(Ey,Hx)
    get_F=@(n_arg) [1 1; -get_nz(n_arg) get_nz(n_arg)];
    get_m_exact=@(z_arg) get_n(z_arg)*get_nder(z_arg)/2/(get_n(z_arg)^2-nx^2)*[1 -1;-1 1];
    get_L=@(n_arg) -[0 1; get_nz(n_arg)^2 0];
end
% Transition n2->n1
get_T=@(n1_arg,n2_arg) get_F(n1_arg)\get_F(n2_arg);


%%
log2N=[0:10];
Nlog2N=length(log2N)
Pcfwm=zeros(2,2,Nlog2N);
Pcfwm1=Pcfwm; Pcfwm2=Pcfwm;
Pfwm1=Pcfwm;
Pfwm2=Pcfwm;
PcfwmWKB=Pcfwm; PcfwmWKB2=Pcfwm;
Pprim=Pcfwm;
%%
m_err=zeros(1,Nlog2N);
Acor=m_err; Bcor=m_err;
for klog2N=1:Nlog2N
    N=2^log2N(klog2N)
    z=z1+(z2-z1)/N*[0:N];
    zc=(z(1:N)+z(2:N+1))/2;
    dz=diff(z);
    nn=get_n(z);
    nz=get_nz(nn);
    dn=diff(nz);
    nc=get_n(zc);
    nzc=get_nz(nc);
    %% Spline approximation
    if do_doublepoints
        zfull=unique(sort([z zc]));
    else
        zfull=z;
    end
    Nfull=length(zfull);
    nfull=get_n(zfull);
    Ffull=zeros(2,2,Nfull); Fifull=Ffull; mfull=Ffull;
    for k=1:Nfull
        % Note: in advanced applications, F may contain an arbitrary phase
        % which will make the derivatives to be defined up to a term
        % proportional to F*diag(phase). One may remove this term by
        % (1) minimizing the norm of each column vector
        % F(:,k)-F(:,k-1)*phase(k), which determines
        % phase(k)=arg(F(:,k-1)'*F(:,k)); or
        % (2) using another expression for m=F^{-1}*dF/dz:
        % m_ij=-(F^{-1}(dL/dz)F)_ij/(n_i-n_j), i~=j; m_ii is unknown!!!!
        F=get_F(nfull(k));
        Ffull(:,:,k)=F;
        Fifull(:,:,k)=inv(F);
    end
    F_pp=spline(zfull,Ffull);
    Fder=ppval(derpp(F_pp),zfull);
    for k=1:Nfull
        mfull(:,:,k)=inv(Ffull(:,:,k))*Fder(:,:,k);
    end
    m_pp=spline(zfull,mfull);
    get_m_spline=@(z_arg) ppval(m_pp,z_arg);
    %% Check the spline approximation
    err=0;
    for k=1:Nfull
        m=get_m_exact(zfull(k));
        tmp=get_m_spline(zfull(k))-m;
        e=max(abs(tmp(:)))/max(abs(m(:)));
        if err<e
            err=e;
        end
    end
    err
    m_err(klog2N)=err;
    if do_spline
        get_m=get_m_spline;
    else
        get_m=get_m_exact;
    end
    if do_rediag
        % For better accuracy, must re-diagonalize H with correction
        % Outstanding problem: need a better way to calculate m
        get_H=@(z_arg) k0*get_nz(get_n(z_arg))*diag([1 -1])+1i*get_m(z_arg);
    else
        % Note that in this case, the transport matrix P is applied
        % directly to field vector (Ex,Hy) (for TM wave), which is
        % continuous across the layers, so we don't need to worry to
        % multiply by Tu or Td.
        get_H=@(z_arg) k0*get_L(get_n(z_arg));
    end
    %% CFWM
    P=eye(Ndim); P1=P; P2=P; P3=P;
    %%
    Acorrection=zeros(1,N);
    Bcorrection=zeros(1,N);
    for k=1:N
        t1=z(k);
        t2=z(k+1);
        tc=zc(k);
        Hc=get_H(tc);
        [R,dd]=eig(Hc); Ri=inv(R);
        w=diag(dd);
        % Note that we assume w=const, R=const
        % dw(i,j)==w(j)-w(i);
        dw=repmat(w(:).',[Ndim 1])-repmat(w(:),[1 Ndim]);
        tau=dz(k)/2;
        % Quadratic Correction
        % H=Hc+A*(t-tc)+B*(t-tc)^2;
        H1=get_H(t1);
        H2=get_H(t2);
        A=((t2-tc)*(H1-Hc)/(t1-tc)-(t1-tc)*(H2-Hc)/(t2-tc))/(t2-t1);
        Acorrection(k)=max(abs(A(:)))/max(abs(w))*tau;
        B=(-(H1-Hc)/(t1-tc)+(H2-Hc)/(t2-tc))/(t2-t1);
        Bcorrection(k)=max(abs(B(:)))/max(abs(w))*tau^2;
        x=dw*tau;
        % DA and DB are O(tau^3) when tau->0
        % When dw*tau is ~1, DA=O(tau^2) and DB=O(tau^3).
        % DA improves mostly non-diagonal elements of P
        Ar=Ri*A*R; Br=Ri*B*R;
        % Ri*H*R=diag(w)+Ar*(t-tc)+Br*(t-tc)^2=diag(w)+Vr
        % Dyson series expansion of the interaction-picture
        % evolution operator UI(-tau,tau)=1+(DA+DB)+DAA+...
        % First order Dyson series term
        DA=1i*(2*tau)^2*Ar.*sincn(1,x);
        % DB improves mostly diagonal elements of P
        DB=1i*(2*tau)^3*Br.*sincn(2,x);
        % Second order Dyson series term for (A,A)
        % DAA is O(tau^5) when tau->0
        % and DAA=O(tau^4) when dw*tau~1
        % It improves the result for off-diagonal elements of P
        % However, just a bit, and only in the limit dw*tau->0
        C=zeros(Ndim,Ndim,Ndim);
        % AA_ijk=Ar_ij*Ar_jk
        AA=repmat(Ar,[1 1 Ndim]).*repmat(shiftdim(Ar,-1),[Ndim 1 1]);
        % Ar*Ar==squeeze(sum(AA,2)) etc
        % Indeces are C(i,k,j)
        xkj=repmat(shiftdim(x,-1),[2 1 1]);
        xik=repmat(x,[1 1 2]);
        xij=repmat(permute(x,[1 3 2]),[1 2 1]);
        in=find(xkj~=0);
        i0=find(xkj==0);
        xkjn=xkj(in);
        xikn=xik(in);
        xijn=xij(in);
        C(in)=(sincn(1,xijn)-2i*xkjn.*sincn(2,xijn)-...
            (1+1i*xkjn).*exp(-1i*xkjn).*sincn(1,xikn))./xkjn.^2;
        xik0=xik(i0);
        C(i0)=2*sincn(3,xik0)-sincn(1,xik0)/2;
        DAA=-(2*tau)^4/4*squeeze(sum(AA.*C,2));
        max(abs(DAA(:)));
        P=R*diag(exp(2i*tau*w))*Ri*P;
        P1=R*diag(exp(1i*tau*w))*(eye(Ndim)+DA+DB)*diag(exp(1i*tau*w))*Ri*P1;
        P2=R*diag(exp(1i*tau*w))*(eye(Ndim)+DA+DB+DAA)*diag(exp(1i*tau*w))*Ri*P2;
        %% WKB expansion - put all diagonal elements into w
        % Use only up to first term in Dyson series
        % Now we assume w~=const but still R=const
        % The integrals of t^n exp(i*\int dw(t)dt) are done by
        % change of variable
        %  t -> x=[\int dw(t)dt]/dw0
        % The best so far!
        w1=diag(Ar); w2=diag(Br);
        V1r=Ar-diag(w1); V2r=Br-diag(w2);
        % w(t)=w+w1*(t-tc)+w2*(t-tc)^2;
        % Vr(t)=V1r*(t-tc)+V2r*(t-tc^2); Vr_{ii}==0
        dw1=repmat(w1(:).',[Ndim 1])-repmat(w1(:),[1 Ndim]);
        dw2=repmat(w2(:).',[Ndim 1])-repmat(w2(:),[1 Ndim]);
        % Non-diagonal only
        [im,jm]=ndgrid(1:Ndim,1:Ndim);
        ii=find(im~=jm);
        w0=dw(ii);
        w1d=dw1(ii)./w0; w2d=dw2(ii)./w0;
        dphi1=w0.*(-tau+w1d*tau^2/2-w2d*tau^3/3);
        dphi2=w0.*(tau+w1d*tau^2/2+w2d*tau^3/3);
        dS1=(sincn0(1,dphi2)-sincn0(1,dphi1))./w0.^2;
        dS2=(sincn0(2,dphi2)-sincn0(2,dphi1))./w0.^3;
        dS3=(sincn0(3,dphi2)-sincn0(3,dphi1))./w0.^4;
        dS4=(sincn0(4,dphi2)-sincn0(4,dphi1))./w0.^5;
        C1=zeros(Ndim,Ndim); C2=zeros(Ndim,Ndim);
        % We can add more terms here if we want more precision
        C1(ii)=dS1-dS2.*w1d*3/2+dS3.*((5/2)*w1d.^2-(4/3)*w2d);
        C2(ii)=dS2-2*dS3.*w1d+dS4.*(3.75*w1d.^2-(5/3)*w2d);
        phi1=w*tau+w1*tau^2/2+w2*tau^3/3;
        phi2=w*tau-w1*tau^2/2+w2*tau^3/3;
        DWKB1=1i*V1r.*C1;
        DWKB2=1i*V2r.*C2;
        P3=R*diag(exp(1i*phi1))*(eye(Ndim)+DWKB1+DWKB2)*diag(exp(1i*phi2))*Ri*P3;
    end
    Acor(klog2N)=max(Acorrection);
    Bcor(klog2N)=max(Bcorrection);
    Pcfwm(:,:,klog2N)=P;
    Pcfwm1(:,:,klog2N)=P1;
    Pcfwm2(:,:,klog2N)=P2; % Even more accurate CFWM
    PcfwmWKB(:,:,klog2N)=P3;
    %% WKB expansion to the third power, using the spline coefs
    %% 1. Prepare the spline
    maxorder=4; % cubic, constant
    order=3; % used to calculate coefficients of H expansion
    % order==3 is the same as the above WKB method
    H=zeros(Ndim,Ndim,N+1);
    for k=1:N+1
        H(:,:,k)=get_H(z(k));
    end
    H_pp=spline(z,H); % H''' is continuous at the first and last transitions
    %H_pp=csape(z,H,'second'); % the usual spline, with H''=0 at the ends
    switch order
        case 4
            % Extract the coefficients of a cubic expansion
            % Coefficients at zc=z+zs
            coefsc=newcoefspp(H_pp,zc,maxorder);
            % coefs has size Ndim^2*N x 4
            H_coefs=reshape(coefsc,[Ndim Ndim N maxorder]);
        case 3
            % Quadratic
            H_coefs=zeros(Ndim,Ndim,N,maxorder);
            for k=1:N
                if do_doublepoints
                    Hc=get_H(zc(k));
                else
                    Hc=ppval(H_pp,zc(k));
                end
                H1=H(:,:,k); H2=H(:,:,k+1);
                tc=zc(k); t1=z(k); t2=z(k+1);
                A=((t2-tc)*(H1-Hc)/(t1-tc)-(t1-tc)*(H2-Hc)/(t2-tc))/(t2-t1);
                B=(-(H1-Hc)/(t1-tc)+(H2-Hc)/(t2-tc))/(t2-t1);
                H_coefs(:,:,k,maxorder-2)=B; H_coefs(:,:,k,maxorder-1)=A;
                H_coefs(:,:,k,maxorder)=Hc;
            end
        otherwise
            error('not implemented')
    end
    [tmp1,tmp2]=ndgrid(1:Ndim,1:Ndim);
    nd=find(tmp1~=tmp2); % non-diagonal indeces
    V=zeros(Ndim,Ndim,maxorder-1); % Correction to rotated H, nondiagonal
    wcor=zeros(Ndim,maxorder-1); % Diagonal correction to rotated H (i.e., frequency corrections)
    %dwcor=zeros(Ndim,Ndim,maxorder-1); % matrix of differences of frequency corrections
    Nnd=Ndim*(Ndim-1); % # of elements not on diagonal;
    Wn=zeros(Nnd,maxorder-1); % non-zero (non-diagonal)
    % part of said differences, normalized to zero-order frequency
    % differences
    C1=zeros(Ndim,Ndim); C2=zeros(Ndim,Ndim);
    dS=zeros(Nnd,5);
    %% 2. The cycle
    P=eye(Ndim);
    for k=1:N
        tau=dz(k)/2;
        [R,dd]=eig(H_coefs(:,:,k,maxorder)); Ri=inv(R);
        w=diag(dd); % eigen-frequencies at the center of interval k
        dw=repmat(w(:).',[Ndim 1])-repmat(w(:),[1 Ndim]);
        W0=dw(nd); % non-zero frequency differences
        for j=1:maxorder-1
            tmp=Ri*H_coefs(:,:,k,4-j)*R;
            % Split diagonal and non-diagonal parts
            dd=diag(tmp);
            wcor(:,j)=dd; % diagonal
            V(:,:,j)=tmp-diag(dd); % non-diagonal
            dwcortmp=repmat(dd(:).',[Ndim 1])-repmat(dd(:),[1 Ndim]);
            %dwcor(:,:,j)=dwcortmp;
            Wn(:,j)=dwcortmp(nd)./W0;
        end
        %a=[ones(Nnd,1) Wn]./repmat([1:maxorder],[Nnd 1]);
        %b=ones(Nnd,maxorder);
        %b(:,2)=-a(:,2); b(:,3)=-a(:,3)
        b2=-Wn(:,1)/2;
        b3=-Wn(:,2)/3+Wn(:,1).^2/2;
        b4=-Wn(:,3)+5*Wn(:,1).*Wn(:,2)/6-5*Wn(:,1).^3/8;
        Phi1=W0.*(-tau+Wn(:,1)*tau^2/2-Wn(:,2)*tau^3/3+Wn(:,3)*tau^4/4);
        Phi2=W0.*(tau+Wn(:,1)*tau^2/2+Wn(:,2)*tau^3/3+Wn(:,3)*tau^4/4);
        for j=1:4
            % Integral[x^j*exp(1i*W0*x),{x,phi1,phi2}]
            dS(:,j)=(sincn0(j,Phi2)-sincn0(j,Phi1))./W0.^(j+1);
        end
        % Last terms in C1, C2 do not help!
        if do_spline
            C1(nd)=dS(:,1)+3*b2.*dS(:,2);%+(4*b3+2*b2.^2).*dS(:,3);
            C2(nd)=dS(:,2);%+4*b2.*dS(:,3);
        else
            C1(nd)=dS(:,1)+3*b2.*dS(:,2)+(4*b3+2*b2.^2).*dS(:,3);%+5*(b4+b2.*b3).*dS(:,4);
            C2(nd)=dS(:,2)+4*b2.*dS(:,3); %+5*(b3+b2.^2).*dS(:,4);
        end
        %C1(nd)=dS(:,1)-dS(:,2).*Wn(:,1)*3/2+dS(:,3).*((5/2)*Wn(:,1).^2-(4/3)*Wn(:,2));
        %C2(nd)=dS(:,2)-2*dS(:,3).*Wn(:,1)+dS(:,4).*(3.75*Wn(:,1).^2-(5/3)*Wn(:,2));
        % phase from 0 to +tau
        phi0ptau = w*tau+wcor(:,1)*tau^2/2+wcor(:,2)*tau^3/3+wcor(:,3)*tau^4/4;
        % phase from -tau to 0
        phimtau0 = w*tau-wcor(:,1)*tau^2/2+wcor(:,2)*tau^3/3-wcor(:,3)*tau^4/4;
        DWKB1=1i*V(:,:,1).*C1;
        DWKB2=1i*V(:,:,2).*C2;
        P=R*diag(exp(1i*phi0ptau))*(eye(Ndim)+DWKB1+DWKB2)*diag(exp(1i*phimtau0))*Ri*P;
    end
    PcfwmWKB2(:,:,klog2N)=P;
    %% FWM1 (traditional)
    P=get_T(nc(1),nn(1));
    for k=1:N
        P=diag(exp(1i*k0*dz(k)*nzc(k)*[1 -1]))*P;
        if k<N
            P=get_T(nc(k+1),nc(k))*P;
        end
    end
    P=get_T(nn(N+1),nc(N))*P;
    Pfwm1(:,:,klog2N)=P;
    %% FWM2 (centered)
    P=eye(2);
    for k=1:N
        P=diag(exp(1i*k0*dz(k)/2*nz(k+1)*[1 -1]))*get_T(nn(k+1),nn(k))*...
            diag(exp(1i*k0*dz(k)/2*nz(k)*[1 -1]))*P;
    end
    Pfwm2(:,:,klog2N)=P;
    %% Primitive CFWM - same as Pcfwm, so we don't plot it
    % This method does not give a better convergence for dz->0 than FWM.
    P=eye(Ndim);
    for k=1:N
        P=expm(1i*dz(k)*get_H(zc(k)))*P;
    end
    Pprim(:,:,klog2N)=P;
end
Pcfwm0=Pcfwm(:,:,Nlog2N);
dPcfwm=reshape(Pcfwm-repmat(Pcfwm0,[1 1 Nlog2N]),[4 Nlog2N]).';
Pcfwm10=Pcfwm1(:,:,Nlog2N);
dPcfwm1=reshape(Pcfwm1-repmat(Pcfwm10,[1 1 Nlog2N]),[4 Nlog2N]).';
Pcfwm20=Pcfwm2(:,:,Nlog2N);
dPcfwm2=reshape(Pcfwm2-repmat(Pcfwm20,[1 1 Nlog2N]),[4 Nlog2N]).';
Pfwm10=Pfwm1(:,:,Nlog2N);
dPfwm1=reshape(Pfwm1-repmat(Pfwm10,[1 1 Nlog2N]),[4 Nlog2N]).';
Pfwm20=Pfwm2(:,:,Nlog2N);
dPfwm2=reshape(Pfwm2-repmat(Pfwm20,[1 1 Nlog2N]),[4 Nlog2N]).';
PcfwmWKB0=PcfwmWKB(:,:,Nlog2N);
dPcfwmWKB=reshape(PcfwmWKB-repmat(PcfwmWKB0,[1 1 Nlog2N]),[4 Nlog2N]).';
PcfwmWKB20=PcfwmWKB2(:,:,Nlog2N);
dPcfwmWKB2=reshape(PcfwmWKB2-repmat(PcfwmWKB20,[1 1 Nlog2N]),[4 Nlog2N]).';

%% Energy flux
Ptot=Pfwm10;
tmp=get_F(get_n(z2))*Ptot
real(conj(tmp(1,:)).*tmp(2,:))

%%
figure;
logeN=log10(2)*log2N;
if do_doublepoints
    log2Nc=log2N+1;
else
    log2Nc=log2N;
end
logeNc=log10(2)*log2Nc;
% -- we added center points for CFWM, thereby doubling N
plot_log='binary';
switch plot_log
    case 'natural'
        xval=logeN; xvalc=logeNc;
    case 'binary'
        % Binary log
        xval=log2N; xvalc=log2Nc;
    case 'lambdas' % log2(dz/lambda)
        dz0=(z2-z1)./2.^log2N;
        dz0c=(z2-z1)./2.^log2Nc;
        xval=log2(dz0*k0/(2*pi));
        xvalc=log2(dz0c*k0/(2*pi));
end
plot_max=1
if plot_max
    semilogy(xval,max(abs(dPfwm1),[],2),'o-'); hold on; % non-continuous (old) -- STANDARD
    %semilogy(logeNc,abs(dPcfwm),'x-'); % High error - no advantage compared to old FWM
    semilogy(xvalc,max(abs(dPcfwm1),[],2),'v-'); % High initial error (exp), best(lin)
    semilogy(xvalc,max(abs(dPcfwm2),[],2),'s-'); % High initial error (exp), best(lin)
    semilogy(xvalc,max(abs(dPcfwmWKB),[],2),'*-'); % Best initial error (both lin and exp)
    semilogy(xvalc,max(abs(dPcfwmWKB2),[],2),'*--'); % High initial error (exp), best(lin)
    % - this case has high initial error even when n1\approx n2
    semilogy(xval,max(abs(dPfwm2),[],2),'o--'); % non-continuous (old), centered (no advantage)
else
    semilogy(xval,abs(dPfwm1),'o-'); hold on; % non-continuous (old) -- STANDARD
    %semilogy(logeNc,abs(dPcfwm),'x-'); % High error - no advantage compared to old FWM
    semilogy(xvalc,abs(dPcfwm1),'v-'); % High initial error (exp), best(lin)
    semilogy(xvalc,abs(dPcfwm2),'s-'); % High initial error (exp), best(lin)
    semilogy(xvalc,abs(dPcfwmWKB),'*-'); % Best initial error (both lin and exp)
    semilogy(xvalc,abs(dPcfwmWKB2),'*--'); % High initial error (exp), best(lin)
    % - this case has high initial error even when n1\approx n2
    semilogy(xval,m_err,'kx-');
    semilogy(xval,abs(dPfwm2),'o--'); % non-continuous (old), centered (no advantage)
end
semilogy(xval,m_err,'kx-');
semilogy(xval,Acor,'k');
semilogy(xval,Bcor,'k--');
hold off
% Observation: Error for old fwm stays flat down to dz=lambda/2 and starts
% dropping at smaller dz. Some flattening is also observed for cfwm
% algorightm dPcfwm1
grid on
switch plot_log
    case 'natural'
        xlabel('log_e(N)');
    case 'binary'
        xlabel('log_2(N)');
    case 'lambdas'
        xlabel('log_2(dz/\lambda)');
end
title(['n2=' num2str(n2) '; exp=' num2str(do_exp) '; 1/\lambda=' num2str(k0/(2*pi)) '; rediag=' num2str(do_rediag)]);
