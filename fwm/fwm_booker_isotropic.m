function [nz,Fext,L,Fzop]=fwm_booker_isotropic(epsilon,mu,nx,ny)
%FWM_BOOKER_ISOTROPIC Solve the Booker equation for isotropic medium
% Solve the Booker equation for isotropic medium.
% The analitycal solution is known, for TE and TM waves.
% Usage:
%   [nz,Fext]=fwm_booker_isotropic(epsilon,nx,ny);
% Inputs:
%   epsilon  - scalar dielectric permittivity;
%   nx,ny    - horizontal refraction coefficients nx=kx/k0, ny=ky/k0;
%              k0=w/c;
% nx and ny are 1D arrays of length N.
% Outputs:
%   nz   - 4 x N array of vertical refraction coefficients = kz/k0,
%          for 2 modes and 2 directions, sorted by decreasing imaginary
%          part, so that the first two values correspond to upgoing modes
%          (u1,u2) and the second two to downgoing modes (d1,d2), for each
%          value of nx and ny;
%   Fext - 6 x 4 x N matrix converting the mode variables
%          (u1,u2,d1,d2) into field (Ex,Ey,Ez,Z0*Hx,Z0*Hy,Z0*Hz) for each
%          value of nx and ny.
% NOTE: Z0=sqrt(mu0/eps0) is the impedance of free space.
% See also: FWM_BOOKER_BIANISOTROPIC, FWM_BOOKER_PAPPERTSMITH
% Author: Nikolai G. Lehtinen

do_Fext=(nargout>1);
do_L=(nargout>2);
% nx, ny must be 1D arrays
nx=nx(:);
if length(ny)==1
    ny=ny*ones(size(nx));
end
ny=ny(:);
N=length(nx);
np2=nx.^2+ny.^2;

% lame temporary fix (nz=0 is not handled well ...)
ii=find(np2==1);
if ~isempty(ii)
    disp('FWM_BOOKER: WARNING: Lame fix applied ...');
    nx(ii)=nx(ii)+1e-5;
    np2(ii)=nx(ii).^2+ny(ii).^2;
end

% Determine the special input: epsilon==Inf
if isinf(epsilon) || isinf(mu)
    % Infinite conductivity => perfect reflection
    nz=zeros(4,N);
    nz(1:2,:)=Inf; nz(3:4,:)=-Inf;
    np=sqrt(np2);
    inz=find(np~=0);
    cp=ones(size(np)); sp=zeros(size(np));
    cp(inz)=nx(inz)./np(inz); sp(inz)=ny(inz)./np(inz);
    if do_Fext
        Fext=zeros(6,4,N);
        Fext(4,1,:)=-cp; Fext(4,2,:)=-sp; Fext(4,3,:)=cp; Fext(4,4,:)=sp;
        Fext(5,1,:)=-sp; Fext(5,2,:)=cp; Fext(5,3,:)=sp; Fext(5,4,:)=-cp;
    end
    return;
end
if isinf(mu)
    % Infinite permeability => ?
end

nz0=sqrt(epsilon*mu-np2);
% Make sure imag(nz0)>=0
% This is only a problem for complex nx.
ii=find(imag(nz0)<0);
nz0(ii)=-nz0(ii);
nz=[nz0,nz0,-nz0,-nz0].';
if ~do_Fext
    return
end
E=zeros(3,4,N);
n0=sqrt(epsilon*mu);
np=sqrt(np2);
parfor knp=1:N
    % knp
    nx0=nx(knp); ny0=ny(knp);
    % nz0=nz(1,knp); % >0 (at least for real np0), ==
    % sqrt(perm(1,1)-np0^2);
    np0=np(knp); % >0
    if np0>0
        cp=nx0/np0; sp=ny0/np0;
    else
        cp=1; sp=0;
    end
    ct=nz0(knp)/n0; st=np0/n0; % cos(th), sin(th), 0<th<pi, st>0
    % Isotropic medium:
    % Upgoing waves:
    % TE
    % Fext(:,1,knx)= ...
    %   [-sp ; cp ; 0 ; -nz0*cp ; -nz0*sp ; np0];
    % TM
    % Fext(:,2,knx)= ...
    %   [cp*ct ; sp*ct ; -st ; -n0*sp ; n0*cp ; 0];
    % Downgoing waves
    % Fext(:,3,knx)= ...
    %   [-sp ; cp ; 0 ; nz0*cp ; nz0*sp ; np0];
    % Fext(:,4,knx)= ...
    %   [cp*ct ; sp*ct ; st ; n0*sp ; -n0*cp ; 0];
    E(:,:,knp)=[-sp cp*ct -sp cp*ct ; cp sp*ct cp sp*ct ; 0 -st 0 st];
end
nvec=zeros(3,4,N);
nvec(1,:,:)=repmat(permute(nx,[2 3 1]),[1 4 1]);
nvec(2,:,:)=repmat(permute(ny,[2 3 1]),[1 4 1]);
nvec(3,:,:)=permute(nz,[3 1 2]);
Fext=cat(1,E,cross(nvec,E));
if ~do_L
    return
end
parfor knp=1:N
    F=Fext([1:2 4:5],:,knp);
    L(:,:,knp)=F*diag(nz(:,knp))/F;
    Fzop(:,:,knp)=Fext([3 6],:,knp)/F;
end
