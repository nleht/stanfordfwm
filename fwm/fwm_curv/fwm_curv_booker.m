function [nz,Fext,L,Fzop]=fwm_curv_booker(K,nx,ny,alpha1d,alpha2d,zd_position,tolsort)
%FWM_BOOKER_BIANISOTROPIC_CURVILINEAR Solve the Booker equation in
%curvilinear coordinate system
%
% See FWM_BOOKER_BIANISOTROPIC for solving Booker equation in Cartesian
% coordinate system
%
% Solve the Booker equation for a general bianisotropic linear medium with
% an arbitrary constitutive tensor K:
%   ( D )   ( epsilon xi ) ( E )     ( E )
%   (   ) = (            ) (   ) = K (   )
%   ( B )   (   eta   mu ) ( H )     (   )
% We find nz as the eigenvalues of the Clemmow-Heading propagation operator
% acting on the horizontal fields (Ex,Ey,Z0*Hx,Z0*Hy) [Clemmow and Heading,
% 1954, doi:10.1017/S030500410002939X]. The operator was denoted L by
% Pappert and Smith [1972, doi:10.1029/RS007i002p00275, equation (1), with
% orthogonal coordinates x,y] and T by Clemmow and Heading [1954] and
% Budden [1961: Budden, K. G., "Radio Waves in Ionosphere", Cambridge
% University Press, 1961, p. 389, eq. (18.18)], who used permuted
% horisontal field components.
% The horizontal fields for different modes are the eigenvectors of L. The
% Booker equation in the form of a characteristic equation was derived by
% Clemmow and Heading [1954, eq. (22)] and Budden [1961, p. 398, eq.
% (18.19-18.20)].
%
% The relation between curvilinear coordinate system and Cartesian
% coordinate system.
% Let us consider a orthogonal curvilinear system (X,Y,Z), and a Cartesian
% coordinate system (s_x,s_y,s_z). The coordinate transformation can be
% defined as
% ds_x=hx*dX
% ds_y=hy*dY
% ds_z=hz*dZ
% where ds_x means the differential of s_x, and hx,hy,hz are the stretching
% factors tranforming the Cartesian coordinate to curvilinear coordinate.
% The stretching factors can be defined as r/R, where r is the distance
% from the point to the origin, and R is the radius of curvature of the
% inner shell in that certain direction. For example, for spherical
% coordinates, hx=r/R0,hy=r/R0,hz=r/R0, where R0 is the radius of inner
% spherical shell; for cylindrical coordinates, assuming the axis direction
% is x direction, hx=1,hy=r/R0,hz=r/R0, where R0 is the radius of inner
% cylindrical shell. hx=1 can be obtained by regarding the radius of
% curvature in the axis direction to be infinity. If z direction is always
% chosen as the radius direction, hz can be always chosen to one.
% 
% Notice: The inputs nx and ny, and output nz are all refractive index in
% the curvilinear coordinate system. Take x and y as horizontal directions,
% what are conserved in different z position are nx and ny in the 
% curvilinear system. The refractive index in the Cartesian coordinate,
% nx_Cartesian and ny_Cartesian, should be scaled to nx and ny in the
% curvilinear system when put in the input of this function. Moreover, the
% output nz should be re-scaled back to nz_Cartesian before using nz in the
% original length scale in other parts of fwm!
% The relation between (nx,ny,nz) and (nx_Cartesian,ny_Cartesian,
% nz_Cartesian) is
% nx = nx_Cartesian*hx
% ny = ny_Cartesian*hy
% nz = nz_Cartesian*hz
%
% Usage:
%   [nz,Fext,L,Fzop]=fwm_curv_booker(K,nx,ny,alpha1,alpha2,[tolsort]);
% Inputs:
%   K             - 6 x 6 matrix defined above, describing materials of the 
%                   medium
%   nx,ny         - horizontal refraction coefficients in curvilinear system,
%                   nx=kx/k0*hx, ny=ky/k0*hy; k0=w/c
%   alpha1,alpha2 - hx=1+alpha1*z_position,hy=1+alpha2*z_position;hx and hy
%                   are stretching factors from curvilinear system to Cartesian
%                   system in x and y directions, while hz has been chosen to 
%                   be one
%   z_position    - the altitude of the field point to the surface of the
%                   earth
%   k0            - the wavenumber in the free space
% Optional inputs:
%   tolsort - small number (default=3e2*eps). If nz is almost real (within
%      this error), nz is sorted by real part instead of imaginary part.
% Outputs:
%   nz   - 4 x N array of vertical refraction coefficients = kz/k0*hz,
%          for 2 modes and 2 directions, sorted by decreasing imaginary
%          part, so that the first two values correspond to upgoing modes
%          (u1,u2) and the second two to downgoing modes (d1,d2), for each
%          value of nx and ny;
%   Fext - 6 x 4 x N matrix converting the mode variables
%          (u1,u2,d1,d2) into field (Ex,Ey,Ez,Z0*Hx,Z0*Hy,Z0*Hz) for each
%          value of nx and ny.
% Optional outputs:
%   L    - 4 x 4 x N Clemmow-Heading propagation operator
%   Fzop - 2 x 4 x N Operator to get Ez, Hz from (Eperp,Hperp)
% NOTE: Z0=sqrt(mu0/eps0) is the impedance of free space.
% Author: Linhai Qiu, Nikolai G. Lehtinen, with matrix formalism help from
% Forrest Foust.
%
% See also: FWM_BOOKER_ISOTROPIC, FWM_BOOKER_BIANISOTROPIC 

%% Parse the arguments (input and output)
do_Fext=(nargout>1);
do_L=(nargout>2);
if ~all(size(K)==6)
    if all(size(K)==3)
        % Convert to 6x6 matrix
        perm=K;
        K=eye(6); K(1:3,1:3)=perm;
    else
        error('K is of wrong size');
    end
end
if nargin<8
    tolsort=[];
end
if isempty(tolsort)
    tolsort=3e2*eps;
end

%% nx, ny must be 1D arrays
nx=nx(:);
if length(ny)==1
    ny=ny*ones(size(nx));
end
ny=ny(:);
N=length(nx);
if ~(length(alpha1d)==1 && length(alpha2d)==1 && length(zd_position)==1)
    error('alpha1, alpha2 and z_position should be scalar in this function');
end
%% Setup
nz=zeros(4,N);
if do_Fext
    F=zeros(4,4,N);
end
if do_L
    L=zeros(4,4,N);
end
Fzop=zeros(2,4,N);
% U is not used for nonmagnetic media, but "parfor" still somehow needs it
U=zeros(6,6,3);
U(:,:,1)=kron([0 -1;1 0],[0 0 0;0 0 -1;0 1 0]);
U(:,:,2)=kron([0 -1;1 0],[0 0 1;0 0 0;-1 0 0]);
U(:,:,3)=kron([0 -1;1 0],[0 -1 0;1 0 0;0 0 0]);
ip=[1:2 4:5];
iz=[3 6];
resonance=(det(K(iz,iz))==0);
if resonance
    error('Resonance!');
end
B1=[0 0 0 0;0 0 -1 0;0 0 0 0;1 0 0 0];
B2=[0 0 0 1;0 0 0 0;0 -1 0 0;0 0 0 0];
hx=1+alpha1d*zd_position;
hy=1+alpha2d*zd_position;
%% Calculate anisotropic modes
% Equation to be solved is
%    (nx*U(:,:,1)+ny*U(:,:,2)+nz*U(:,:,3))*F=K*F
parfor knp=1:N
    % knp
    nx0=nx(knp); ny0=ny(knp);
    A=nx0*U(:,:,1)/hx+ny0*U(:,:,2)/hy-K;
    % Operator to convert (Ex,Ey,Hx,Hy) into (Ez,Hz)
    Fzop1=K(iz,iz)\A(iz,ip); % 2 x 4
    % We use the fact that inv(U(ip,ip,3))==U(ip,ip,3)
    L1=U(ip,ip,3)*(K(ip,ip)-A(ip,iz)*Fzop1-alpha1d/(1i*hx)*B1-alpha2d/(1i*hy)*B2);
    Fzop(:,:,knp)=Fzop1;
    if do_L
        L(:,:,knp)=L1;
    end
    if do_Fext
        [F(:,:,knp),dd]=eig(L1); % All calculations are done here
        nz(:,knp)=diag(dd);
    else
        nz(:,knp)=eig(L1);
    end
end
%% Find the sorting order, same as in FWM_BOOKER
% Sort in descending order according to real part
[dummy,ii1]=sort(real(nz),1,'descend');
nztmp=nz(ii1+repmat([0:N-1]*4,4,1));
% Sort in descending order according to imaginary part
% Note that if the imaginary part is equal to zero, no sorting should
% take place!
imagnztmp=imag(nztmp);
maxreal=max(abs(real(nztmp([1 4],:))));
smallimag=abs(imagnztmp-repmat(mean(imagnztmp,1),[4 1]))./repmat(maxreal,[4 1]);
imagnztmp(smallimag<tolsort)=0;
%nztmp=real(nztmp)+1i*imagnztmp;
[dummy,ii2]=sort(imagnztmp,1,'descend');
ii=ii1(ii2+repmat([0:N-1]*4,4,1));

%% Do the actual sorting
nz=nz(ii+repmat([0:N-1]*4,4,1));
% -- not the same as nz=nz(ii,:);
% Add Ez, Hz
if do_Fext
    % Matrix transforming F into Fext:
    stretch=zeros(6,4,N);
    stretch(1,1,:)=1; stretch(2,2,:)=1;stretch(4,3,:)=1;stretch(5,4,:)=1;
    stretch([3 6],:,:)=Fzop;
%     % Sort F using index ii
%     F=permute(F,[2 3 1]);
%     F2=reshape(F,[4*N 4]);
%     for c=1:4
%         F(:,:,c)=reshape(F2(ii+repmat([0:N-1]*4,4,1),c),[4 N]);
%     end
%     F=permute(F,[3 1 2]); % 4 x 4 x N
%     % Finally, do the "stretching"
%     Fext=permute(sum(repmat(permute(stretch,[1 2 4 3]),[1 1 4 1]).*repmat(permute(F,[4 1 2 3]),[6 1 1 1]),2),[1 3 4 2]);
    Fext=zeros(6,4,N);
    for knp=1:N
        Fext(:,:,knp)=stretch(:,:,knp)*F(:,ii(:,knp),knp);
    end
end

