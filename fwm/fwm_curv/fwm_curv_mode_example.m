% Example of usage of FWM_MODEFINDER and FWM_MODE_HEIGHTGAIN in the curved
% case
% What is the role of curvature in modification of the EIWG
% (Earth-Ionosphere waveguide) modes both in isotropic medium (B=0) and a medium 
% with a vertical geomagnetic field?

%% Setup
save_plots=0;
global clight REarth
if isempty(clight)
    loadconstants;
end
% Heights in km
%h=[0 50:69 70:.25:95 95.5:.5:200].';
h=[0:79 80:.25:95 95.5:.5:110].';
M=length(h);
% Frequency of 21.4 kHz
w=21400*2*pi;
k0=w/clight;
% Electron density and collision frequency
%Ne=getNe(h,'Stanford_nighttime');
Ne=getNe(h,'HAARPwinternight');
nue=get_nu_electron_neutral_swamy92(h);
% Anisotropic medium with vertical B field
Babs=5e-5;
[perm,S,P,D]=get_perm_with_ions(h,w,'Ne',Ne,'nue',nue,'Babs',Babs,'need_ions',0);
% Isotropic medium
permi=get_perm_with_ions(h,w,'Ne',Ne,'nue',nue,'Babs',0,'need_ions',0);
% Dimensionless curvature
zd=h*k0*1e3;
eground='E=0';
alphad=1/(k0*REarth*1e3);

%% Find all modes
debugflag=1;
doplot=0;
for do_isotropic=0:1
    %%
    if do_isotropic
        permuse=permi;
        fnameuse='curvmodes_isot'
        disp('=====Modes in isotropic medium=====');
        titleuse='isotropic';
    else
        permuse=perm;
        fnameuse='curvmodes_anis'
        disp('=====Modes in anisotropic medium=====');
        titleuse='anisotropic';
    end
    % Analyze cases:
    % 1. no curvature (kalpha1=0, kalpha2=0)
    % 2. cylindrical curvature in the propagation direction (kalpha1=1, kalpha2=0)
    % 3. cylindrical curvature in the perpendicular direction (kalpha1=0, kalpha2=1)
    % 4. spherical curvature (kalpha1=1, kalpha2=1)
    for kalpha2=0:1
        for kalpha1=0:1
            alpha1d=kalpha1*alphad;
            alpha2d=kalpha2*alphad;
             % Specify the box in the complex plane for the mode finder
            % (where to look for "C", see below)
            % There is a cut at Re(C)=0, so avoid it.
            for kbox=1:2
                % We do two "C" boxes, the second box captures some modes with
                % low attenuation but nx>1 (i.e., Imag(C)<0, Real(C)~0)
                switch kbox
                    case 1
                        Crmin=0.001; Crmax=1.06; Cimin=-.048; Cimax=.01;
                    case 2
                        Crmin=1e-6; Crmax=0.03; Cimin=-.15; Cimax=-.048;
                end
                fname=[fnameuse '_' num2str(kalpha1) num2str(kalpha2) '_box' num2str(kbox) '.mat'];
                if ~exist(fname,'file')
                    disp(['***** ' titleuse ', curvature=(' num2str(kalpha1) ', ' num2str(kalpha2) '), box=' num2str(kbox) ' *****']);
                    disp(['Calculating and saving into file ' fname ' ...'])
                    % The values calculated are:
                    %  C - horizontal refractive index (complex, roots of
                    %     characteristic equation)
                    %    (the name comes from C=cos(theta), the ionosphere incidence angle)
                    %  Cmul - root multiplicities (hopefully, all are = 1)
                    % Optional:
                    %  Cr, Ci, Cbox - the grid values of "C"
                    %  Fbox - values of the characteristic equation determinant
                    %  (==0 for the roots)
                    [C,Cmul,~,~,Cr,Ci,Cbox,Fbox]=fwm_curv_modefinder(...
                        k0,h,'E=0',permuse,alpha1d,alpha2d,[],Crmin,Crmax,Cimin,Cimax,[],[],[],debugflag,doplot);
                    % Attenuation, in dB/Mm (also may be calculated in
                    % "fwm_curv_modefinder")
                    % alf=20*k0*imag(sqrt(1-C.^2))/log(10)*1e6;
                    disp(['Saving file ' fname])
                    if isempty(C)
                        eval(['save ' fname ' C permuse']);
                    else
                        eval(['save ' fname ' C Cmul permuse Cr Ci Cbox Fbox']);
                    end
                else
                    disp(['File ' fname ' exists, skipping calculations']);
                end
            end
        end
    end
end

%% Sanity check: Plot the roots in the complex "C" plane
for do_isotropic=0:1
    %%
    if do_isotropic
        fnameuse='curvmodes_isot';
        titleuse='isotropic';
    else
        fnameuse='curvmodes_anis';
        titleuse='anisotropic';
    end
    figure(100+do_isotropic);
    set(gcf,'Name',titleuse);
    for kalpha1=0:1
        for kalpha2=0:1
            subplot(4,1,kalpha1*2+kalpha2+1);
            for kbox=1:2
                fname=[fnameuse '_' num2str(kalpha1) num2str(kalpha2) '_box' num2str(kbox) '.mat'];
                if exist(fname,'file')
                    load(fname);
                    if ~isempty(C)
                        imagesc(Cr,Ci,log10(abs(Fbox.'))); axis equal xy tight;
                        hold on;
                    end
                end
            end
            nCcum=0;
            for kbox=1:2
                fname=[fnameuse '_' num2str(kalpha1) num2str(kalpha2) '_box' num2str(kbox) '.mat'];
                if exist(fname,'file')
                    load(fname);
                    if ~isempty(C)
                        nC=length(C);
                        for k=1:nC
                            text(real(C(k)),imag(C(k)),num2str(k+nCcum));
                        end
                        nCcum=nCcum+nC;
                    end
                end
            end
            title(['curvature=(' num2str(kalpha1) ', ' num2str(kalpha2) ')'])
        end
    end
    if save_plots
        echo_print({'EPS','PNG'},['curvature_' titleuse '_roots']);
    end
end

%% Plot the root comparison
figure(102);
plotcolor='brck';
for do_isotropic=0:1
    %%
    if do_isotropic
        plotchar='o';
        fnameuse='curvmodes_isot';
        titleuse='isotropic';
    else
        plotchar='x';
        fnameuse='curvmodes_anis';
        titleuse='anisotropic';
    end
    for kalpha1=0:1
        for kalpha2=0:1
            % Collect all the roots from all solution boxes
            Cs=[];
            for kbox=1:2
                fname=[fnameuse '_' num2str(kalpha1) num2str(kalpha2) '_box' num2str(kbox) '.mat'];
                if exist(fname,'file')
                    load(fname);
                    Cs=[Cs C];
                else
                    error(['file ' fname ' does not exist'])
                end
            end
            nx=sqrt(1-Cs.^2);
            %Attenuation, in dB/Mm (also may be calculated in "fwm_curv_modefinder")
            alf=20*k0*imag(nx)/log(10)*1e6;
            plot(1./real(nx),alf,[plotcolor(kalpha1*2+kalpha2+1) plotchar],'linewidth',2); hold on;
        end
    end
end
hold off
title('o-isotropic, x-anisotropic');
legend('Location','SouthEast','flat','cyl in y','cyl in x','spherical');
grid on;
xlabel('vph'); ylabel('attenuation');
set(gca,'ylim',[0 20],'xlim',[0.95 1.2]);
if save_plots
    echo_print({'EPS','PNG'},'curvature_vph_atten');
end

%% Addition: Mode structure (height gains)
% See "fwm_curv_mode_example_complicated.m"
hi=[0:.1:110];
Mi=length(hi);
% Choose isotropicity and curvatures along y
do_isotropic=0;
kalpha2=0;
% Set the file name
if do_isotropic
    fnameuse='curvmodes_isot';
    titleuse='isotropic';
    permuse=permi;
else
    fnameuse='curvmodes_anis';
    titleuse='anisotropic';
    permuse=perm;
end
%% Cycle over curvature along x
for kalpha1=0:1
    %%
    % Collect all the roots from all solution boxes
    Cs=[];
    for kbox=1:2
        fname=[fnameuse '_' num2str(kalpha1) num2str(kalpha2) '_box' num2str(kbox) '.mat'];
        if exist(fname,'file')
            load(fname);
            Cs=[Cs C];
        else
            error(['file ' fname ' does not exist'])
        end
    end
    nC=length(Cs);
    nx=sqrt(1-Cs.^2);
    EH=zeros(6,Mi,nC);
    %%
    for k=1:nC
        %%
        k
        [EH(:,:,k),name,dF,udMi,nzMi,FextMi,nz,Fext]=fwm_curv_mode_heightgain(Cs(k),k0,h,'E=0',permuse,hi,kalpha1*alphad,kalpha2*alphad,1);
        if 0
            nzM=nz(:,M);
            SFM=real(cross(conj(Fext(1:3,:,M)),Fext(4:6,:,M)))/2;
            nvec=[repmat(nx(k),[1 4]) ; zeros(1,4) ; nzM.'];
            SFa=sqrt(sum(SFM.^2));
            % Projection of vector n onto the Poynting vector, for four modes
            sum(nvec.*SFM)./SFa
        end
    end
    figure(103+kalpha1); semilogx(squeeze(abs(EH(3,:,:))),hi); grid on;
    legend(num2str([1:nC]'));
    title('ANISOTROPIC: Ez')
end

%%
set(gca,'ylim',[1e-3 10]); grid on;
legend(num2str([1:nCi]'));
title('ISOTROPIC: Ez')
subplot(2,1,2);
semilogy(hi, squeeze(abs(EHi(6,:,:))));
set(gca,'ylim',[1e-3 10]); grid on;
legend(num2str([1:nCi]'));
title('ISOTROPIC: Hz')
% - isotropic solutions are not blowing up!
