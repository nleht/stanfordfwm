function [EHf,name,dF,udMi,nzMi,FextMi,nz,Fext]=fwm_curv_mode_heightgain(C0,k0,h,eground,permrot,hi,alpha1d,alpha2d,debugflag)
%FWM_MODE_HEIGHTGAIN Find the mode height gain for given C0
% Usage:
%  [EH,name,dF]=fwm_curv_mode_heightgain(C0,k0,h,eground,permrot,hi,debugflag);
% Inputs:
%  C0                   == nz == kz/k0
%  k0                   == w/c
%  h        (M x 1)     -- altitudes in km for permrot
%  eground              -- ground eps or 'E=0'
%  permrot  (3 x 3 x M) -- permittivity tensors at h, in coordinate system
%                         in which nperp=nx
%  hi       (Mi x 1)    -- altitudes in km at which the field is to be
%                         calculated
% Outputs:
%  EH (Mi x 6) -- fields (Ex,Ey,Ez,Z0*Hx,Z0*Hy,Z0*Hz) at altitudes hi
%  name - the name of the mode, e.g., 'QTE' or 'TM'
% NOTE: Only scalar C0 can be used
% Previous version was called "fwm_modestruct" (now defunct)
% See also: FWM_MODE_EXAMPLE, FWM_MODEFINDER, FWM_BOOKER, FWM_RADIATION,
%    FWM_FIELD, FWM_MODE_ALLHEIGHTGAINS

%% Default arguments
if nargin<9
    debugflag=0;
end

%% Setup
M=length(h);
Mi=length(hi);
np=sqrt(1-C0.^2); % horizontal refractive index
zdim=h(:).'*1e3*k0; % row, dimensionless
nz=zeros(4,M);
Fext=zeros(6,4,M);
for k=1:M
    [nz(:,k),Fext(:,:,k)]=...
        fwm_curv_booker(permrot(:,:,k),np,0,alpha1d,alpha2d,zdim(k));
end
% Make sure the WHISTLER is going up
PM=real(cross(conj(Fext(1:3,:,M)),Fext(4:6,:,M)))/2;
[PMsorted,ii]=sort(PM(3,:).',1,'descend');
nz(:,M)=nz(ii,M);
Fext(:,:,M)=Fext(:,ii,M);
if debugflag>0 && imag(nz(2,M))<0
    disp('WARNING: diverging up-wave');
    disp('nz='); disp(nz(:,M))
    disp('P='); disp(PMsorted)
end

%% Calculate the 2 x 2 reflection coef
dz=diff(zdim);
[Pu,Pd]=fwm_PuPd(dz,nz(:,1:M-1));
U=zeros(2,2,M-1);
RuhPu=zeros(2,2,M-1);
for k=M-1:-1:1
    % Down Td(k)
    Td=Fext([1:2 4:5],:,k)\Fext([1:2 4:5],:,k+1);
    %if k==M-1
    %    disp(Td)
    %end
    Tu=Fext([1:2 4:5],:,k+1)\Fext([1:2 4:5],:,k);
    if any(isnan(Td(:))) || any(isnan(Tu(:)))
        error(['k=' num2str(k)]);
    end    
    % Rul(:,:,k+1)
    if k+1<M
        Rul=Pd(:,:,k+1)*RuhPu(:,:,k+1);
    else
        Rul=zeros(2,2); % no reflection at M
    end
    % Ruh(:,:,k)*Pu(:,:,k)
    RuhPu(:,:,k)=(Td(3:4,1:2)+Td(3:4,3:4)*Rul)/...
        (Td(1:2,1:2)+Td(1:2,3:4)*Rul)*Pu(:,:,k);
    %Rul(:,:,k)=Pd(:,:,k)*Ruh(:,:,k)*Pu(:,:,k);
    U(:,:,k)=Tu(1:2,1:2)*Pu(:,:,k)+Tu(1:2,3:4)*RuhPu(:,:,k);    
    if any(isnan(U(:,:,k)))
        error(['k=' num2str(k) ': U']);
    end
end

%% The mode content in each layer
%ud=zeros(4,M);
%udprime=zeros(4,M);
% Initial condition: since det(eye(2)+Ru(:,:,1))==0, find when
% (eye(2)+Ru(:,:,1))*ud(1:2,1)==0
% Find u(1:2,1) as an eigenvector corresponding to zero eigenvalue of
% matrix (eye(2)-Rd*Ru(:,:,1))
Rg=fwm_Rground(eground,np,0);
[v,dd]=eig(eye(2)-Rg*Pd(:,:,1)*RuhPu(:,:,1));
[tmp,ii]=min(abs(diag(dd)));
if debugflag>0
    disp(['Error in F = ' num2str(tmp)]);
end
dF=dd(ii,ii);
ul=zeros(2,M); dh=zeros(2,M-1);
u0=v(:,ii); % mode contents on the ground
% Normalize
u0=u0/sum(abs(u0).^2);
if all(abs(u0)>1e-6)
    name='Q';
else
    name='';
end
if abs(u0(1))>abs(u0(2))
    name=[name 'TE'];
else
    name=[name 'TM'];
end
ul(:,1)=u0;
for k=1:M-1
    ul(:,k+1)=U(:,:,k)*ul(:,k);
    dh(:,k)=RuhPu(:,:,k)*ul(:,k);
end
%disp('ul='); disp(ul(:,M))

%% Field at each altitude "hi", i.e. the height gain
if isempty(hi)
    % We only needed the name
    EHf=[];
    return
end
zdimi=hi(:).'*1e3*k0; % dimensionless
[kia,dzl,dzh]=fwm_get_layers(zdim,zdimi);
Mi=length(kia);
ud=fwm_intermediate(nz,ul,dh,kia,dzl,dzh);

EHf=zeros(6,M);
for ki=1:Mi
    k=kia(ki);
    EHf(:,ki)=Fext(:,:,k)*ud(:,ki);
end

%% The upper boundary mode contents
udMi=ud(:,Mi); nzMi=nz(:,kia(Mi)); FextMi=Fext(:,:,kia(Mi));
