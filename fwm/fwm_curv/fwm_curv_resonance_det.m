function [FT,PMz,P0z,Rg,Ruarr,nz,Fext]=fwm_curv_resonance_det(zd,eground,perm,nx,ny,alpha1d,alpha2d)
%FWM_RESONANCE_DET Calculate det(1-Ru*Rd)
% Usage: FT=fwm_resonance_det(zd,eground,perm,nx,ny)
% See also: FWM_MODEFINDER, FWM_MODESTRUCT
% Author: Nikolai G. Lehtinen

M=length(zd);
N=length(nx);
global output_interval
if isempty(output_interval)
    output_interval=20;
end

%% Same as in fwm_field
% Vertical reflactive index and mode structure
nz=zeros(4,M,N); Fext=zeros(6,4,M,N);
tstart=now*24*3600; toutput=tstart; first_output=1;
for k=1:M
    [nz(:,k,:),Fext(:,:,k,:)]=fwm_curv_booker(perm(:,:,k),nx,ny,alpha1d,alpha2d,zd(k));
    timec=now*24*3600; ttot=timec-tstart;
    if timec-toutput>output_interval
        toutput=timec;
        if first_output
            disp('FWM_RESONANCE_DET: 1. Refractive index and mode structure in each layer');
            first_output=0;
        end
        disp(['FWM_RESONANCE_DET: Done=' num2str(k/M*100) '%; Time=' ...
            hms(ttot) ', ETA=' hms(ttot/k*(M-k))]);
    end
end

%% Make sure the WHISTLER is going up
% This is never a problem for real np, but for complex, there can be 2
% waves attenuating upward.
% ONLY propagating waves. If the wave attenuates fast enough, keep it.

PM=real(cross(conj(Fext(1:3,:,M,:)),Fext(4:6,:,M,:)))/2;
PMz=permute(PM(3,:,:,:),[2 4 1 3]); % Get rid of singleton dims
[dummy,ii]=sort(PMz,1,'descend');
for ip=1:N
    nz(:,M,ip)=nz(ii(:,ip),M,ip);
    Fext(:,:,M,ip)=Fext(:,ii(:,ip),M,ip);
end

% Vertical Poynting flux at the ground level
P0=real(cross(conj(Fext(1:3,:,1,:)),Fext(4:6,:,1,:)))/2;
P0z=permute(P0(3,:,:,:),[2 4 1 3]);

%% Calculate the 2 x 2 reflection coef from upper boundary
dz=diff(zd); % dimensionless=k0*h
FT=zeros(N,1);
Rg=fwm_curv_Rground(eground,nx,ny,squeeze(Fext(:,:,1,:)),alpha1d,alpha2d);
if nargout>4
    Ruarr=zeros(2,2,N);
end
tstart=now*24*3600; toutput=tstart; first_output=1;
for ip=1:N
    %[Pu,Pd]=fwm_PuPd(dz,nz(:,:,ip));
    kh=nz(:,1:M-1,ip).*repmat(dz(:).',4,1);
    kh(3:4,:)=-kh(3:4,:); % so that imag(kh)>0
    Ep=exp(1i*kh);
    % Transporting up (|Pu|<=1)
    Pu=zeros(2,2,M-1); Pu(1,1,:)=Ep(1,:); Pu(2,2,:)=Ep(2,:);
    % Note that for evanescent waves, |Pu|<=1
    % Transporting down (|Pd|<=1)
    Pd=zeros(2,2,M-1); Pd(1,1,:)=Ep(3,:); Pd(2,2,:)=Ep(4,:);

    Ru=zeros(2,2); % Rul(M) - no reflection
    % There is no R{u,d}h(:,:,M) because layer M has no upper boundary.
    for k=M-1:-1:1
        Td=Fext([1:2 4:5],:,k,ip)\Fext([1:2 4:5],:,k+1,ip);
        if any(~isfinite(Td(:)))
            Fext([1:2 4:5],:,k,ip)
            Fext([1:2 4:5],:,k+1,ip)
            error('Td is infinite')
        end
        tmp=Td(1:2,1:2)+Td(1:2,3:4)*Ru;
        if det(tmp)==0
            k
            ip
            nz(:,k:k+1,ip)
            Fext(:,:,k:k+1,ip)
            Ptmp=real(cross(conj(Fext(1:3,:,k:k+1,ip)),Fext(4:6,:,k:k+1,ip)))/2;
            squeeze(Ptmp(3,:,:))
            nx(ip)
            Td
            Ru
            save fwm_resonance_det_dump k Fext nz nx ip Td Ru
            error('det(Td(1:2,1:2)+Td(1:2,3:4)*Ru)==0')
        end
        Ru=Pd(:,:,k)*((Td(3:4,1:2)+Td(3:4,3:4)*Ru)/ ...
            (Td(1:2,1:2)+Td(1:2,3:4)*Ru))*Pu(:,:,k); % Rul(k);
    end
    if nargout>4
        Ruarr(:,:,ip)=Ru;
    end
    if(P0z(1,ip)<0)
        % Not necessary but makes Ru, Rg continuous
        FT(ip)=det(eye(2,2)-inv(Rg(:,:,ip)*Ru));
    else    
        FT(ip)=det(eye(2,2)-Rg(:,:,ip)*Ru); % =0 for the mode
    end
    timec=now*24*3600; ttot=timec-tstart;
    if timec-toutput>output_interval
        toutput=timec;
        if first_output
            disp('FWM_RESONANCE_DET: 2. Resonance determinant');
            first_output=0;
        end
        disp(['FWM_RESONANCE_DET: Done=' num2str(ip/N*100) '%; Time=' ...
            hms(ttot) ', ETA=' hms(ttot/ip*(N-ip))]);
    end
end
