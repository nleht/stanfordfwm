function [C,Cmul,alf,permrot,Cr,Ci,C0box,Fbox]=fwm_curv_modefinder(k0,h,eground,perm,alpha1d,alpha2d,phi,...
    Crmin,Crmax,Cimin,Cimax,dCmax,dCtol,dftol,debugflag,doplot)
%FWM_MODEFINDER Find modes for a flat Earth, non-isotropic medium
% Usage:
%   [C,Cmul,alf,permrot]=fwm_modefinder(k0,h,eground,perm,phi,...
%      Crmin,Crmax,Cimin,Cimax[,debugflag,doplot]);
%   % More advanced: [C,Cmul,alf,permrot,Cbox,Fbox]=fwm_modefinder(...);
%   EH=fwm_modestruct(C(1),k0,h,eground,permrot,hi);
% Inputs:
%   k0               == w/c
%   h (M x 1)        -- altitudes (km)
%   eground          -- ground eps or 'E=0'
%   perm (3 x 3 x M) -- permittivity tensors at h
%   phi              -- angle of kperp with x-axis (clockwise! default==0)
%   Crmin,Crmax,Cimin,Cimax - the box in complex plane containing roots
% Optional inputs:
%   debugflag - default=0
%   doplot - default=0, plot the roots in the complex plane
% Outputs:
%   C,Cmul  == kz/k0 for all modes (we don't distinguish TM and TE), and
%      their multiplicities (normally, all Cmul==1)
%   alf     -- attenuation in dB/Mm
%   permrot -- "perm" rotated to the direction of kperp, to be used
%              later with FWM_MODESTRUCT
% Optional outputs:
%   Cbox, Fbox - grids of C in the complex plane and values of
%      F=det(1-Ru*Rd), calculated by FWM_RESONANCE_DET
%      on this grid (we must have F==0 for the modes)
% See also: FWM_MODE_EXAMPLE, FWM_RESONANCE_DET, FWM_MODESTRUCT,
%    FWM_BOOKER, FWM_RADIATION, FWM_FIELD
% Author: Nikolai G. Lehtinen

%% Initialize the parameters
if nargin<14
    doplot=0;
end
if nargin<13
    debugflag=[];
end
if nargin<12
    dftol=[];
end
if nargin<11
    dCtol=[];
end
if nargin<10
    dCmax=[];
end
if isempty(debugflag)
    debugflag=0;
end
M=length(h);
zd=h(:).'*1e3*k0; % row, dimensionless
if isempty(phi)
    phi=0;
end
if phi~=0
    % Rotate the permittivity by phi around z-axis
    sp=sin(phi); cp=cos(phi);
    % Active
    rotmxa=[cp -sp 0; sp cp 0; 0 0 1];
    % Passive (inverse of active)
    rotmxp=[cp sp 0; -sp cp 0; 0 0 1];
    permrot=zeros(3,3,M);
    for k=1:M
        permrot(:,:,k)=rotmxa*perm(:,:,k)*rotmxp;
    end
else
    permrot=perm;
end

%% Check if permrot is symmetrical for nx-> -nx (necessary ny -> -ny)
tmat=diag([-1 -1 1]);
issym=1;
for k=1:M
    tmp=tmat*permrot(:,:,k)*tmat-permrot(:,:,k);
    if max(abs(tmp(:)))>1e-4
        issym=0;
        break;
    end
end
if debugflag>0
    if issym
        disp('Symmetric medium (nx <-> -nx)')
    else
        disp('Asymmetric medium');
    end
end

%% Cut the region if necessary (not implemented)
% - to avoid branch cuts at C<1 (or C>1)
branch=[];
if ~issym
    if Cimin*Cimax<0
        if (Crmin-1)*(Crmax-1)<0
            error('Must cut the region into Re(C)<1 and Re(C)>1');
        end
        if Crmax<1
            branch=0;
        elseif Crmin>1
            branch=1;
        end
    end
end

%% Calculate the roots
[C,Cmul,Cr,Ci,C0box,Fbox]=fzero_holomorphic(...
    @fwm_curv_modefinder_flwpc,{zd,eground,permrot,alpha1d,alpha2d,branch,debugflag},Crmin,Crmax,Cimin,Cimax,...
    dCmax,dCtol,dftol,[],debugflag);
justgetbox=(ischar(C) && strcmp(C,'box'));
if justgetbox || doplot
    figure;
    imagesc(real(C0box(:,1)),imag(C0box(1,:)),log10(abs(Fbox.'))); set(gca,'ydir','normal');
    axis equal
    colorbar
    hold on
end
if doplot
    for k=1:length(C)
        text(real(C(k)),imag(C(k)),num2str(k));
    end
    hold off
end
if justgetbox
    alf=[];
    return
end

%% Attenuation, in dB/Mm
alf=20*k0*imag(sqrt(1-C.^2))/log(10)*1e6;


