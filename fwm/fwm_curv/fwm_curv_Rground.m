function Rground=fwm_curv_Rground(eground,nx,ny,F1,alpha1d,alpha2d)
%FWM_RGROUND Ground reflection coefficient matrix
% Usage:
%    eground=1+i*sground/(w*eps0);
%    Rground=fwm_Rground(eground,nx,ny);
% where nx, ny are of the same length (Np) to get 2 x 2 x Np array Rground.
% NOTES:
%    1. Since TE and TM modes separate, Rground(:,:,ip) is diagonal 2 x 2
%    2. eground can be Inf -- then Rground has -1 on diagonals
%    3. Special boundary conditions 'E=0','H=0' and 'free' are allowed.
%    4. We assume vacuum right above ground (perm===eye(3)).
% Author: Nikolai G. Lehtinen
% NOTE: Currently assumes that the first layer is vacuum. Must rewrite! So
% far fixed only 'free' case.

if length(ny)==1
    ny=ny*ones(size(nx));
end
N=length(nx);
if length(ny)~=N
    error('Nx~=Ny');
end
iup=[1:2]; ido=[3:4]; % indeces corresponding to up and down modes
iEp=[1:2]; iHp=[4:5]; % the way we arrange the fields
ic=[1:2 4:5]; % continuous components of the field
Rground=zeros(2,2,N);
% Handle special cases
if ischar(eground) || isinf(eground)
    if (ischar(eground) && strcmp(eground,'E=0')) || (~ischar(eground) && isinf(eground))
        % E=0 at the ground
        for knp=1:N
            Rground(:,:,knp)=-F1(iEp,iup,knp)\F1(iEp,ido,knp);
        end
    elseif ischar(eground) && strcmp(eground,'H=0') || (~ischar(eground) && eground==0)
        for knp=1:N
            Rground(:,:,knp)=-F1(iHp,iup,knp)\F1(iHp,ido,knp);
        end
    elseif (ischar(eground) && strcmp(eground,'free'))
        % stays zero
    else
        error('internal error')
    end
elseif nargin<=4
    error('need curvature paramenters')
else
    [dummy,Fext]=fwm_curv_booker(eye(3)*eground,nx,ny,alpha1d,alpha2d,0);
    % Assume they are correctly sorted
    for knp=1:N
        Tu=F1(ic,:,knp)\Fext(ic,:,knp);
        Rground(:,:,knp)=Tu(iup,ido)/Tu(ido,ido);
    end
end
