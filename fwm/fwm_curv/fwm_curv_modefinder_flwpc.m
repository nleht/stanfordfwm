function FT=fwm_curv_modefinder_flwpc(C0,zd,eground,perm,alpha1d,alpha2d,branch,debugflag)
%FWM_MODEFINDER_FLWPC Auxiliary, wrapper for FWM_RESONANCE_DET
% To be used with FZERO_HOLOMORPHIC
if nargin<6
    debugflag=0;
end
if nargin<5
    branch=0;
end
if isempty(branch)
    branch=0;
end
% C0 can be a matrix
[Nr,Ni]=size(C0);
if branch
    np=sqrt(1-C0.^2); % horizontal refractive index, Re(np)>0
else
    np=1i*sqrt(C0.^2-1); % horizontal refractive index, Im(np)>0
    % This precaution is to avoid cut at C0>1, corresponding to real(np)=0
end
if debugflag>1
    disp(['FWM_MODEFINDER_FLWPC: length(C0)=' num2str(length(C0))]);
end
FT=fwm_curv_resonance_det(zd,eground,perm,np,0,alpha1d,alpha2d);
