function [n,Ska]=interp_k_chi(np,npb,Nphi,Sfz,dn,nmax)
%INTERP_K_CHI Interpolate the power flux in (k,chi) space, for better printing
Np=length(Nphi);
Nchi0=Nphi(Np);
chi0=[0:Nchi0-1]/Nchi0*2*pi;
i2=cumsum(Nphi); i1=[1 i2(1:Np-1)+1];
Sfz0=zeros(Np,Nchi0);
for kp=1:Np
    Nchi=Nphi(kp);
    chi=[0:Nchi]*2*pi/Nchi;
    Sfz0(kp,:)=interp1(chi,[Sfz(i1(kp):i2(kp)) Sfz(i1(kp))],chi0);
end
% Integrate over k
dnp=diff(npb);
Sk=cumsum(Sfz0.*repmat(dnp,[1 Nchi0]),1);
n=[-nmax:dn:nmax]; Nn=length(n);
[nxm,nym]=ndgrid(n,n);
chim=mod(atan2(nym,nxm),2*pi);
nm=sqrt(nxm.^2+nym.^2);
Skp=interp2(np,[chi0 2*pi],[Sk Sk(:,1)].',nm+dn/2,chim);
Skm=interp2(np,[chi0 2*pi],[Sk Sk(:,1)].',nm-dn/2,chim);
Ska=(Skp-Skm)/dn;
Ska(isnan(Ska))=0;
