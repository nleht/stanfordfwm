function [nz,Fext,L,Fzop,B]=fwm_booker_bianisotropic(K,nx,ny,toliso,tolsort)
%FWM_BOOKER_BIANISOTROPIC Solve the Booker equation
%
% Replaces FWM_BOOKER
% See also FWM_BOOKER_WARM which is more general and may be used instead of
% this function.
%
% See FWM_BOOKER_ISOTROPIC for TE and TM modes in isotropic media.
%
% Solve the Booker equation for a general bianisotropic linear medium with
% an arbitrary constitutive tensor K:
%   ( D )   ( epsilon   xi ) ( E )     ( E )
%   (   ) = (              ) (   ) = K (   )
%   ( B )   (   eta     mu ) ( H )     ( H )
% We find nz as the eigenvalues of the Clemmow-Heading propagation operator
% acting on the horizontal fields (Ex,Ey,Z0*Hx,Z0*Hy) [Clemmow and Heading,
% 1954, doi:10.1017/S030500410002939X]. The operator was denoted L by
% Pappert and Smith [1972, doi:10.1029/RS007i002p00275, equation (1), with
% orthogonal coordinates x,y] and T by Clemmow and Heading [1954] and
% Budden [1961: Budden, K. G., "Radio Waves in Ionosphere", Cambridge
% University Press, 1961, p. 389, eq. (18.18)], who used permuted
% horisontal field components.
%
% The horizontal fields for different modes are the eigenvectors of L. The
% Booker equation in the form of a characteristic equation was derived by
% Clemmow and Heading [1954, eq. (22)] and Budden [1961, p. 398, eq.
% (18.19-18.20)].
% Usage:
%   [nz,Fext]=fwm_booker_bianisotropic(K,nx,ny[,toliso,tolsort]);
% Inputs:
%   perm     - 3 x 3 dielectric permittivity tensor;
%   nx,ny    - horizontal refraction coefficients nx=kx/k0, ny=ky/k0;
%              k0=w/c; 1D arrays of length N (if ny is scalar, it is
%              extended to be a 1D array filled with the same value).
% Optional inputs:
%   toliso  - small number (default=3e2*eps) used to test the isotropicity
%      of tensor K, so that FWM_BOOKER_ISOTROPIC can be called. Set to 0 or
%      any value <0 if you want to avoid calling FWM_BOOKER_ISOTROPIC
%   tolsort - small number (default=3e2*eps). If nz is almost real (within
%      this error), nz is sorted by real part instead of imaginary part.
% Outputs:
%   nz   - 4 x N array of vertical refraction coefficients = kz/k0,
%          for 2 modes and 2 directions, sorted by decreasing imaginary
%          part, so that the first two values correspond to upgoing modes
%          (u1,u2) and the second two to downgoing modes (d1,d2), for each
%          value of nx and ny;
%   Fext - 6 x 4 x N matrix converting the mode variables
%          (u1,u2,d1,d2) into field (Ex,Ey,Ez,Z0*Hx,Z0*Hy,Z0*Hz) for each
%          value of nx and ny.
% Optional outputs:
%   L    - 4 x 4 x N Clemmow-Heading propagation operator
%   Fzop - 2 x 4 x N Operator to get Ez, Hz from (Eperp,Hperp)
% NOTE: Z0=sqrt(mu0/eps0) is the impedance of free space.
% Author: Nikolai G. Lehtinen, with matrix formalism help from Forrest
%   Foust.
% See also: FWM_BOOKER_ISOTROPIC

%% Parse the arguments (input and output)
do_Fext=(nargout>1);
do_L=(nargout>2);
magnetic=~all(size(K)==3);
if nargin<5
    tolsort=[];
end
if nargin<4
    toliso=[];
end
if isempty(toliso)
    toliso=3e2*eps;
end
if isempty(tolsort)
    tolsort=3e2*eps;
end

%% First of all, check for isotropicity
if toliso<=0
    isotropic=0;
else
    epsilon=K(1,1);
    if magnetic
        mu=K(4,4);
        tmp=K-kron([epsilon 0;0 mu],eye(3));
    else
        mu=1;
        tmp=K-epsilon*eye(3);
    end
    isotropic=(max(abs(tmp(:)))<=toliso);
end
% Determine the special input: epsilon==Inf
if isotropic
    if any(isnan(K(:))) || any(isinf(K(:)))
        epsilon=Inf;
    end
    if do_L
        [nz,Fext,L,Fzop]=fwm_booker_isotropic(epsilon,mu,nx,ny);
    elseif do_Fext
        [nz,Fext]=fwm_booker_isotropic(epsilon,mu,nx,ny);
    else
        nz=fwm_booker_isotropic(epsilon,mu,nx,ny);
    end
    return;
end

%% nx, ny must be 1D arrays
nx=nx(:);
if length(ny)==1
    ny=ny*ones(size(nx));
end
ny=ny(:);
N=length(nx);

%% Setup
nz=zeros(4,N);
if do_Fext
    F=zeros(4,4,N);
end
if do_L
    L=zeros(4,4,N);
end
Fzop=zeros(2,4,N);
% U is not used for nonmagnetic media, but "parfor" still somehow needs it
U=zeros(6,6,3);
U(:,:,1)=kron([0 -1;1 0],[0 0 0;0 0 -1;0 1 0]);
U(:,:,2)=kron([0 -1;1 0],[0 0 1;0 0 0;-1 0 0]);
U(:,:,3)=kron([0 -1;1 0],[0 -1 0;1 0 0;0 0 0]);
ip=[1:2 4:5];
iz=[3 6];
if magnetic
    resonance=(det(K(iz,iz))==0);
else
    resonance=(K(3,3)==0);
end
if resonance
    error('Resonance!')
end

%% Calculate anisotropic modes
% Equation to be solved is
%    (nx*U(:,:,1)+ny*U(:,:,2)+nz*U(:,:,3))*F=K*F
parfor knp=1:N
    % knp
    nx0=nx(knp); ny0=ny(knp);
    if magnetic
        A=nx0*U(:,:,1)+ny0*U(:,:,2)-K;
        % Operator to convert (Ex,Ey,Hx,Hy) into (Ez,Hz)
        Fzop1=K(iz,iz)\A(iz,ip); % 2 x 4
        % We use the fact that inv(U(ip,ip,3))==U(ip,ip,3)
        L1=U(ip,ip,3)*(K(ip,ip)-A(ip,iz)*Fzop1);
    else
        % We use the fact that K(iz,iz) is just a number, so the explicit
        % implementation of the general algorithm (such as above in "if
        % magnetic ...") is rather simple.
        np=[nx0;ny0];
        nr=[-ny0 nx0]; % rotated by 90 degrees (phys. meaning unknown)
        Fzop1=[-[K(3,1:2) nr]/K(3,3);nr 0 0]; % Operator to get Ez, Hz from (Eperp,Hperp)
        % Pappert-Smith "forward" operator
        L1=[zeros(2,2) [0 1; -1 0] ; np*nr+[-K(2,1:2);K(1,1:2)] zeros(2,2)] + ...
            [np;-K(2,3);K(1,3)]*Fzop1(1,:);
    end
    Fzop(:,:,knp)=Fzop1;
    if do_L
        L(:,:,knp)=L1;
    end
    if do_Fext
        [F(:,:,knp),dd]=eig(L1); % All calculations are done here
        nz(:,knp)=diag(dd);
    else
        nz(:,knp)=eig(L1);
    end
end
%% Find the sorting order, same as in FWM_BOOKER
% Sort in descending order according to real part
[~,ii1]=sort(real(nz),1,'descend');
nztmp=nz(ii1+repmat((0:N-1)*4,4,1));
% Sort in descending order according to imaginary part
% Note that if the imaginary part is equal to zero, no sorting should
% take place!
imagnztmp=imag(nztmp);
maxreal=max(abs(real(nztmp([1 4],:))));
smallimag=abs(imagnztmp)./repmat(maxreal,[4 1]);
imagnztmp(smallimag<tolsort)=0;
%nztmp=real(nztmp)+1i*imagnztmp;
[~,ii2]=sort(imagnztmp,1,'descend');
ii=ii1(ii2+repmat((0:N-1)*4,4,1));

%% Do the actual sorting
nz=nz(ii+repmat((0:N-1)*4,4,1));
% -- not the same as nz=nz(ii,:);
% Add Ez, Hz
if do_Fext
    % Matrix transforming F into Fext:
    stretch=zeros(6,4,N);
    stretch(1,1,:)=1; stretch(2,2,:)=1;stretch(4,3,:)=1;stretch(5,4,:)=1;
    stretch([3 6],:,:)=Fzop;
%     % Sort F using index ii
%     F=permute(F,[2 3 1]);
%     F2=reshape(F,[4*N 4]);
%     for c=1:4
%         F(:,:,c)=reshape(F2(ii+repmat([0:N-1]*4,4,1),c),[4 N]);
%     end
%     F=permute(F,[3 1 2]); % 4 x 4 x N
%     % Finally, do the "stretching"
%     Fext=permute(sum(repmat(permute(stretch,[1 2 4 3]),[1 1 4 1]).*repmat(permute(F,[4 1 2 3]),[6 1 1 1]),2),[1 3 4 2]);
    Fext=zeros(6,4,N);
    for knp=1:N
        Fext(:,:,knp)=stretch(:,:,knp)*F(:,ii(:,knp),knp);
    end
end
B = -inv(U(ip,ip,3));

