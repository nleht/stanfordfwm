# `StanfordFWM` - Stanford Full-Wave Method code

## About using this code

Just a reminder: the files here are protected by copyright, even though there is no license attached. If you would like to use the models in your work, I would prefer if you contact the author (@nleht).

All work using code in this folder **must** cite the following articles:

* Lehtinen, N. G. and U. S. Inan (2008), Radiation of ELF/VLF waves by harmonically varying currents into a stratified ionosphere with application to radiation by a modulated electrojet, _J. Geophys. Res._, **113**, A06301, [`doi:10.1029/2007JA012911`](http://dx.doi.org/10.1029/2007JA012911).

* Lehtinen, N. G. and U. S. Inan (2009), Full-wave modeling of transionospheric propagation of VLF waves, _Geophys. Res. Lett._, **36**, L03104, [`doi:10.1029/2008GL036535`](http://dx.doi.org/10.1029/2009JA014776).

The attached [manual](../doc/Full_Wave_Method_for_Stratified_Media.pdf) is for reference only and may not be further distributed.

## Introduction

### The physics convention

`StanfordFWM` separates the Earth's atmosphere (and ionosphere) into horizontal translationally-symmetric layers, starting at the ground.

The solution is usually found at fixed frequency `f` (in Hz). You can perform a Fourier transform `f->t` yourself if you need the time curve of electromagnetic signals. The "physics" convention is used, all variables are harmonic $`A_\mathrm{real}(t)`$ and are obtained from their complex amplitudes $`A`$ as
```math
A_\mathrm{real}(t) = \Re A e^{-i\omega t}
```
The _angular_ frequency $`\omega`$ is denoted in the code as `w=2*pi*f`. From it, we derive the vacuum wave number `k0=w/clight` (i.e., $`k_0=\omega/c`$). In a uniform medium (such as present in each layer), the plane waves are described by a wave vector **k**, which may be normalized to give the refractive index vector **n** with components `nx=kx/k0`, `ny=ky/k0` and `nz=kz/k0`, i.e.
```math
A = A_0 e^{ik_0\mathbf{n}\cdot\mathbf{r}}
```

The fields have six components E<sub>x</sub>, E<sub>y</sub>, ..., H<sub>z</sub>. They are stored in arrays called `EH`. In the horizontal **n**-domain, the name is `EHn`, i.e., the name modifier is `n`. Analogously, the surface current (or vertical current moment per unit area, see below) has six components (three electric and three magnetic) and is denoted `I` in the horizontal **r**-domain and `In` in the horizontal **n**-domain. The six components run over the first dimension (axis) of the arrays.

### Units and Horizontal Fourier Transform (HFT) conventions

Magnetic field _H_ in `EH` is given in V/m. These are rationalized units introduced by Budden (1985). Thus, $`\mathbf{H}_\mathsf{SI} = \mathbf{H}/Z_0`$, where $`Z_0=\sqrt{\mu_0/\epsilon_0}`$ is the impedance of free space, denoted by `impedance0` in the code. The currents are also converted to rationalized units, i.e. electric currents are $`\mathbf{J}_e = Z_0\mathbf{J}_{e,\mathsf{SI}}`$ and magnetic currents $`\mathbf{J}_m = \mathbf{J}_{m,\mathsf{SI}}`$ (in Weber convention).

See also the [manual](doc/Full_Wave_Method_for_Stratified_Media.pdf) in "doc" folder.

The currents  in $`(x,y)`$ space are given as surface currents flowing between layers, so in SI their units are A/m. This unit also applies to vertical currents which are given as vertical current moment per unit area. After conversion to rationalized units, we get V/m. These $`(x,y)`$-space currents are denoted as `I` in the code. **NOTE: Some subroutines may still use `I` instead of `In` -- this is to be corrected!**. In $`(k_x, k_y)`$ or $`(n_x,n_y)`$ space, the currents (denoted as `In` in the code) are the horizontal Fourier transform (HFT) of `I`, so the SI unit of `In` is A-m, which must be converted to rationalized V-m, i.e., by multiplying by `impedance0` before using in the code. The HFT is defined as:
```math
\mathtt{In}(n_x,n_y) = \int \mathtt{I}(x,y) e^{-ik_0(n_xx+n_yy)}\,dx\,dy
```

The point sources are given as $`\mathtt{I}(x,y) = IL\delta(x,y)`$ where _IL_ is the current moment in A-m. After HFT, we get $`\mathtt{In}(n_x,n_y) = \mathit{const} = IL`$. The routines which take the currents as an input, usually take `In` (`fwm_axisymmetric`, `fwm_nonaxisymmetric` etc). Thus, it must be given in the rationalized units of V-m. The output of these routines is `EH` in $`(x,y)`$ space and is in the rationalized units of V/m. The conversion V-m $`\rightarrow`$ V/m is done by the inverse HFT (IHFT):
```math
\mathtt{EH}(x,y) = \left(\frac{k_0}{2\pi}\right)^2 \int \mathtt{EHn}(n_x,n_y) e^{ik_0(n_xx+n_yy)}\,dn_x\,dn_y
```

The IHFT is implemented in subroutines `fwm_hankel` and `fwm_assemble`. However, only `fwm_assemble` does the unit conversion inside the subroutine, as it takes `k0` as one of the arguments. In `fwm_hankel`, the inputs and outputs must be scaled with `k0` by the user.

### The full-wave method

The solution for electromagnetic field **E** and **H** is found as a function of altitude _z_ for each fixed horizontal **n**-vector (with components denoted in the code as `nx` and `ny`, or just `np` for axially-symmetric situations), i.e., in the _horizontal Fourier space_. We can do it because the horizontal **n** is conserved in the horizontally-stratified medium by Snell's law. The current sources **J** (if present) are converted to the horizonal Fourier space first and serve as the input for this procedure. Finally (if required), the field is re-assembled from horizontal **n**-components back to the **r**-space.

The "meat" of the method is in finding the field in the horizontal Fourier space. This is done by using the following **steps**:

 1. The modes (plane waves) are found in each layer. There are four modes in total, namely two modes going {up|down} which have (usually) distinct `nz{>0|<0}`. The electromagnetic field components in each mode are denoted as columns of 6x4 matrix `Fext`. The amplitudes of the {up|down} modes are represented by column `{u|d}` of length 2, and all four modes are denoted as a column `ud`. The vertical refractive indices `nz` and matrix `Fext` are found by solving the Booker equation in `fwm_booker_bianisotropic.m`.
 2. The mode amplitudes `u`, `d` are found recursively (see below), in `fwm_radiation.m`.
 3. The six components of electromagnetic field **E**, **H** are found as `EH=Fext*ud` at each output altitude.

## Conventions and notations used in `StanfordFWM` code

Some variables have very terse names. In order to understand their role in the code, here is a summary of the conventions by which they were named.

### Geometry

Variable names related to layers:

 * `h` - altitudes of boundaries between layers, in kilometers. This is a vertical array (column) of length `M`, and (usually) `h(1)==0` (ground).
 * `z` - same as `h`, but in meters, i.e. defined as `z=h*1e3`. SI units are usually used by default in this code, `h` is an exception. Usually denotes an array, which may create a dilemma on how do denote a single value. (In the current version of code, individual values are denoted, e.g., as `z0`, but this may change in future versions to be consistent with the array of indices like `ksa`, see below).
 * `dz` - layer thicknesses.
 * `M` - number of boundaries or number of layers. The last (highest) layer is infinite and is above `z(M)`
 * `k` - layer or boundary number. The layer between boundaries `z(k)` and `z(k+1)` has number `k`
 * `ka` - array of `k`s. This is usually used with a modifier, e.g. `ksa` or `kia` (see below).
 * Length values may have modifier `d`, e.g., `zd`, `dzd`. This stands for `dimensionless`, and the conversion is `zd=z*k0`

Modifiers applied to variable names:

 1. `s` - from the word "source". This denotes anything related to the current sources in the model, i.e.:
    * `Ms` - number of boundaries which have sources in them
    * `zs` - source altitude(s) (dimensionless version is `zds`).
    * `ks` - index of the boundary in which source is located.
    * `ksa` - array of indices of boundaries in which the sources are located. It has length `Ms`.

 2. `i`-from the word "intermediate". This denotes anything related to altitudes which do not necessarily coincide with the layer boundaries. Usually these altitudes are associated with output altitudes, at which we need the fields to be calculated. These variables are inputs and outputs of [`fwm_get_layers`](fwm_get_layers.m). E.g.:
    * `hi` - output heights in km. Related variables are, e.g., `zi` and `zdi`.
    * `dzdi{l|h}` - distance from a given `zi` to a layer boundary {below|above}.
    * `kia` - indices of layers in which `zi` are situated.

### Mode amplitudes and reflection coefficients

The mode amplitudes (see the second step above) are found in two steps:

 1. Reflection coefficients from {above|below} `R{u|d}` are calculated recursively by going {downwards|upwards}. The reflection coefficients are 2x2 matrices and convert the upward and downward mode amplitudes into each other, i.e., `d=Ru*u`, `u=Rd*d`.
 2. {Upward|downward} mode amplitudes `{u|d}` are calculated recursively by going {upwards|downwards}.

This is represented schematically in Figure 1 in the attached [manual](../doc/Full_Wave_Method_for_Stratified_Media.pdf):

![Mode amplitudes and reflection coefs at a boundary](fig/boundary_svg.svg)

The modifier `{l|h}` may be attached to these notations to denote the values at {lower|higher} boundaries of a given layer. For example, `ul(:,k)` denotes the upward mode (2x1 array) at the lower boundary of layer `k`. The combined mode amplitudes are, e.g., `udl = cat(1, ul, dl)`. Examples with reflection coefficients:

 * `Rdl(:,:,1)` (2x2 matrix) - reflection coefficient from the ground.
 * `Rul(:,:,1)` - reflection coefficient from ionosphere but calculated at ground level.

### Important note on the ground boundary condition
At `h(1)==0`, there must be vacuum, i.e., `perm(:,:,1)==eye(3)`, for correct
application of the boundary condition at $`h=0`$ (except `ground_bc=='free'`).



## Flowchart

Here is the flowchart of the sequence of operations in which the fields are calculated. Red is input, green is output:

```mermaid
flowchart TB
    subgraph "Current sources"
        direction TB
        I[Currents I in x,y space]
        style I fill:#f9f,stroke:#333,stroke-width:4px
        Ik[I in nx,ny space]
        BC[Boundary conditions ΔE, ΔH between layers]
        dud[Δu, Δd on boundaries]
        I --> |Fourier transform: x,y to nx,ny| Ik
        Ik --> BC
        BC --> dud
    end
    Booker --> dud
    subgraph "Incident wave sources"
        direction TB
        EHi[Incident waves E,H in x,y space]
        style EHi fill:#f9f,stroke:#333,stroke-width:4px
        EHik[Incident E,H in nx,ny space]
        udi[Incident wave amplitudes ui, di]
        EHi -->|Fourier transform: x,y to nx,ny| EHik
        EHik --> udi
    end
    subgraph "Calculations at fixed nx, ny"
        nxny[Choose nx,ny]
        Booker[nz, Fext]
        subgraph Mode_amplitudes
            Rud[Ru, Rd] --> ud[u, d]
        end
        EHk[E, H in nx,ny-space]
        Booker --> Mode_amplitudes
        ud --> EHk
        Booker --> EHk
        EHk --> |adjust to avoid loss of accuracy| nxny
    end
    Booker --> udi
    dud --> ud
    udi --> ud
    EHk --> |Inverse Fourier transform: nx,ny to x,y| EH[Electromagnetic field E, H in x,y-space]
    style EH fill:#9f9,stroke:#333,stroke-width:4px
    nxny --> Booker
    nxny --> Ik
    nxny --> EHik
```


