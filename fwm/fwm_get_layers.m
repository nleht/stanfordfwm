function [kia,dzdil,dzdih]=fwm_get_layers(zd,zdi)
%FWM_GET_LAYERS Prepare the indices of layers for FWM
% Usage:
%     [kia,dzdil,dzdih]=fwm_get_layers(zd,zdi);
% Inputs:
%     zd - boundaries between layers
%     zdi - the output points
% Outputs:
%     kia - numbers of layers of output points
%     dzdi{l,h} - distance to the nearest boundary {below|above}
%                 the output point
% 
% This function is usually used with dimensionless distances zd=z*k0, etc.
% where k0=w/clight.
% Author: Nikolai G. Lehtinen
Mi=length(zdi);
M=length(zd);
kia=zeros(Mi,1);
dzdil=nan(Mi,1); dzdih=dzdil;
for ki=1:Mi
    % The altitudes
    zdi0=zdi(ki);
    k=find(zd<=zdi0, 1, 'last' ); % Which layer are we in?
    kia(ki)=k;
    dzdil(ki)=zdi0-zd(k); % Distance to the boundary below
    if k<M
        dzdih(ki)=zd(k+1)-zdi0; % Distance to the boundary above
    end
end
