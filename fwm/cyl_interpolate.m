function EH=cyl_interpolate(r,ul,dh,ri,kappa,m,cyl_info)
%CYL_INTERPOLATE Calculate E,H at intermediate points in a cylindrical system
%
% Works with CYL_BOOKER_BIANISOTROPIC. Note that unlike FWM_INTERMEDIATE or
% FWM_INTERPOLATE (the analogous subroutine for planar case), the outputs
% are fields, not amplitudes.
%
% Usage:
%   [kappa,Flext,Fhext,Pu,Pd,cyl_info]=cyl_booker_bianisotropic(K,m,beta,r);
%   % ... Apply FWM (same algorith as in planar case) to get ul, dh
%   % (see FWM_RADIATION for guidance)
%   EH=cyl_interpolate(r,ul,dh,ri,kappa,m,cyl_info);
%
% Inputs (note the order of arguments, chosen to mimick 'interp1'):
%   (Mi is the number of output points)
%   ul, dh - 2 x M, upward/downward amplitudes at lower/upper boundaries of
%     each layer, calculated using FWM algorithm
%   ri - 1D array of length Mi, dimensionless output radii
%   kappa, r, m, cyl_info - inputs and outputs of CYL_BOOKER_BIANISOTROPIC
%     (see comments there).
%
% Author: Nikolai G. Lehtinen
% See also: cyl_booker_bianisotropic, fwm_intermediate, fwm_field

%% Find the layer numbers
M=length(r)-1;
Mi=length(ri);
[~,kia]=histc(ri,r);
% When the output altitudes are equal to grid altitudes, we choose the
% output altitude just above the grid altitude, except for the top point,
% which is chosen just below the top boundary if it coincides with it.
kia(kia>M)=M;
drl=ri-r(kia); drh=r(kia+1)-ri;

%% Scaled bessel solutions
small_z=cyl_info.small_z;
% At small z
besselzu_small_z=@(m_arg,z_arg) bessely(m_arg,z_arg).*(z_arg/2).^m/factorial(m);
besselzd_small_z=@(m_arg,z_arg) besselj(m_arg,z_arg)./(z_arg/2).^m*factorial(m);
% At large z
besselzu_large_z=@(m_arg,z_arg) besselh(m_arg,1,z_arg,1);
besselzd_large_z=@(m_arg,z_arg) besselh(m_arg,2,z_arg,1);
% Columns of length 2
besselzu=@(m_arg,z_arg,k_arg) besselzu_small_z(m_arg,z_arg).*small_z(:,k_arg)+...
    besselzu_large_z(m_arg,z_arg).*(~small_z(:,k_arg));
besselzd=@(m_arg,z_arg,k_arg) besselzd_small_z(m_arg,z_arg).*small_z(:,k_arg)+...
    besselzd_large_z(m_arg,z_arg).*(~small_z(:,k_arg));
besselzmat=@(m_arg,z_arg,k_arg) [diag(besselzu(m_arg,z_arg,k_arg)) diag(besselzd(m_arg,z_arg,k_arg))];

%% Field at each intermediate point ri
EH=zeros(6,Mi);
for ki=1:Mi
    k=kia(ki); % layer number
    V1=cyl_info.V(:,:,k); kappa1=kappa(:,k);
    GmVk1=cyl_info.GmVk(:,:,k); GpVk1=cyl_info.GpVk(:,:,k); 
    z=kappa1*ri(ki);
    %% Field-matrices
    Qz=V1*besselzmat(m,z,k);
    Qp=0.5*(GpVk1*besselzmat(m-1,z,k)+GmVk1*besselzmat(m+1,z,k));
    % Rearrange Fp,Fz to get Fext(phi,z,r)
    tmp=[Qp;Qz];
    Q1=tmp([2 5 1 4 6 3],:);
    %% Amplitudes
    Pui=diag((r(k)/ri(ki))^m*small_z(:,k)+exp(1i*kappa1*drl(ki)).*(~small_z(:,k)));
    ui=Pui*ul(:,k);
    Pdi=diag((ri(ki)/r(k+1))^m*small_z(:,k)+exp(1i*kappa1*drh(ki)).*(~small_z(:,k)));
    di=Pdi*dh(:,k);
    wi=[ui;di];
    %% Field
    EH(:,ki)=Q1*wi;
end
