function [Carr,nxarr]=fwm_modefinder_fullcircle(...
    k0,h,eground,perm0,ph,Crmin,Crmax,Cimin,Cimax,dCmax,dCtol,dFTtol,root0_file,bkp_file,debugflag)
% perm0 is at phB

if ph(1)~=0
    error('Must start with zero')
end
M=length(h);

%% Calculate mode eigenvalues for the zero angle
if ~exist(root0_file,'file')
    [C,Cmul,alf,permrot,Cbox,Fbox]=fwm_modefinder(...
        k0,h,eground,perm0,[],Crmin,Crmax,Cimin,Cimax,dCmax,dCtol,dFTtol,debugflag);
    % Sort by attenuation
    [tmp,ii]=sort(abs(alf));
    C=C(ii); alf=alf(ii); Cmul=Cmul(ii);
    nC=length(C);
    nx=sqrt(1-C.^2);
    ii=find(imag(nx)<0); nx(ii)=-nx(ii);
    disp(['Saving to ' root0_file ]);
    eval(['save ' root0_file ' C Cmul alf Cbox Fbox nC nx']);
else
    % Skip lengthy calculation
    disp(['Loading from ' root0_file]);
    eval(['load ' root0_file]);
end   
zd=k0*h*1e3;
FT=fwm_resonance_det(zd,eground,perm0,nx(:),0);
disp(['Error in C0=' num2str(max(abs(FT)))]);

%% Go over angles, use fzero_holomorphic_extrap
% According to the adjoint theorem, pi-ph gives the same C as ph
%ph=[0 0.1 .5 1:.5:9 9.05:0.05:9.5 10:.5:360]*pi/180; - for f=37.5kHz
%ph=[0 0.1 .5 1:.5:360]*pi/180;
%ph=[0 0.1 0.5 2.5:2.5:360]*pi/180; % for f=24kHz
nph=length(ph);
if ~exist(bkp_file,'file')
    Carr=zeros(nC,nph);
    Carr(:,1)=C;
    isdone=zeros(1,nph); isdone(1)=1;
else
    eval(['load ' bkp_file]);
end
for iph=find(~isdone)
    disp(['iph=' num2str(iph) ', ph=' num2str(ph(iph)*180/pi) ' deg']);
    % Determine if we already calculated at pi-ph
    ii=find(abs(pi-ph(iph)-ph)<1e-6 | abs(3*pi-ph(iph)-ph)<1e-6);
    if ~isempty(ii) && isdone(ii(1))
        % Copy over
        Carr(:,iph)=Carr(:,ii(1));
        disp(['  Copy from ph=' num2str(ph(ii(1))*180/pi) ' deg']);
        isdone(iph)=1;
        continue;
    end
    % Rotate the permittivity by phi around z-axis
    sp=sin(ph(iph)); cp=cos(ph(iph));
    % Active
    rotmxa=[cp -sp 0; sp cp 0; 0 0 1];
    % Passive (inverse of active)
    rotmxp=[cp sp 0; -sp cp 0; 0 0 1];
    perm=zeros(3,3,M);
    for k=1:M
        perm(:,:,k)=rotmxa*perm0(:,:,k)*rotmxp;
    end
    % Find the three closest calculated values
    Cp={}; php={};
    for diph=1:min(3,iph-1)
        Cp={Cp{:},Carr(:,iph-diph)}; php={php{:},ph(iph-diph)};
    end
    % Use extrapolation to find the starting value for new roots and
    % calculate them
    Carr(:,iph)=fzero_holomorphic_extrap(@fwm_modefinder_flwpc,{zd,eground,perm,0},...
        ph(iph),php,Cp,1e-10,1e-10,debugflag);
    isdone(iph)=1;
    % Backup
    eval(['save ' bkp_file ' ph isdone Carr']);
end
eval(['save ' bkp_file ' ph isdone Carr']);
nxarr=sqrt(1-Carr.^2);
