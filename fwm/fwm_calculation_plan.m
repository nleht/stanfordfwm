function calculation_plan=fwm_calculation_plan(zd,perm,zds,zdi,point_source,npmax_source,drdampd)
%FWM_NPMAX Determine the maximum nperp.
% Usage:
%     calculation_plan=...
%     fwm_calculation_plan(zd,perm,zds,zdi,point_source,npmax_source,drdampd)
% where calculation_plan is a structure with fields
%     npmax1,need_gauss1,npmax2,need_gauss2,extend,kion
%
% This is an auxiliary function whose goal is to limit calculations in
% nperp-domain but still to achieve reasonable precision. We neglect
% contributions of the order of exp(-evanescent_const) where
% evanescent_const=30 (hardcoded).
%
% This function is particularly useful when the sources and observations
% are in vacuum.
%
% It serves three goals:
%   1. Determine maximum nperp for which the calculations have to be
%     done. This allows to limit calculations for both axisymmetric and
%     nonaxisymmetric cases.
%   2. Determine maximum nperp for which the ionosphere reflections are
%     important. Neglecting the ionosphere allows to do calculations in
%     axisymmetric way, which is much faster.
%   3. Determine whether the source I(nperp) needs to be "moderated" by a
%     Gaussian so that nperp does not extend to infinity.
%
% The FIRST goal may be determined by the following factors:
%   1. npmax_source, the maximum nperp for which the sources are nonzero.
%   2. Size of the source. This is particularly important for point (or
%     small) source, and if some of the observation altitudes coincide with
%     altitudes of the sources. Then we must assume a finite size of the
%     source. This size is given by drdamp (passed to this function as
%     argument drdampd=drdamp*k0). The limiting nperp is then
%         npdamp=sqrt(2*evanescent_const)/(drdamp*k0)
%     which is obtained by assuming the gaussian shape
%         I(rperp) ~ exp(-rperp^2/(2*drdamp^2))
%     which after Fourier transform translates into
%         I(nperp) ~ exp(-(nperp*drdamp*k0)^2/2)
%     so we see that I(npdamp) ~ exp(-evanescent_const).
%     If the source is not a point, but close to it (npmax_source is too
%     large), then we need to "moderate" I(nperp) by multiplying it by a
%     Gaussian given above.
%   3. If all observations are at a different altitude from the source,
%     then maximum nperp is determined by the minimum vertical distance
%     dzsi between the altitudes of the source and the altitudes of an
%     observation point. This distance is different for different
%     observation heights. For each observation height, thus we have a
%     separate
%         npevsi = sqrt((evanescent_const/dzdsi)^2+1)
%     where dzdsi=dzsi*k0. This formula is equivalent to
%         exp(i*nz*dzdsion)=exp(-evanescent_const) (for up-wave)
%     where nz=sqrt(1-npev1^2)=i*sqrt(npev1^2-1) (for up-wave). For
%     down-wave, the result is, of course, the same.
%
% The SECOND goal is determined by evanescence between the source and the
% ionosphere. Analogously to the formula above, we have
%     npevion = sqrt((evanescent_const/dzdsion)^2+1)
% where dzdsion = dzsion*k0, and dzsion is the minimum vertical distance
% between the sources and the ionosphere.
% [We could also take into account the distance between ionosphere and
% observation points in order to restrict calculations to lower np even
% more, but this has to wait until the next version. Usually the conditions
% above are enough for efficient calculations.]
%
% The THIRD goal is done by determining whether the contribution at high
% nperp is small due evanescence. If it is not, then gaussian moderation is
% needed.
%
% The output is the following:
%    npmax1 -- we take into account ionosphere for np=[0,npmax1]
%    npmax2 -- we use vacuum for np=[npmax1,npmax2] (extended calculations)
%    need_gauss1 -- moderate source by gaussian at np=[0,npmax1] for
%                   observation altitudes without extended calculations
%    need_gauss2 -- moderate source by gaussian at np=[0,npmax2]
%                   observation altitudes with extended calculations
%    extend -- a logical array showing at which altitudes the calculations
%              in the interval [npmax1,npmax2] are needed (extended
%              calculations)
%    kion -- the altitude index where ionosphere starts
%
% See also: FWM_AXISYMMETRIC, FWM_NONAXISYMMETRIC
% Author: Nikolai G. Lehtinen

%% 
M=length(zd);
Mi=length(zdi);
evanescent_const=30; % we neglect exp(-evanescent_const)
isvac=zeros(size(zd));
for k=1:M
    isvac(k)=(max(max(abs(perm(:,:,k)-eye(3))))<=10*eps);
end
kion=find(isvac==0, 1); % The ionosphere starts at h(kion)
if isempty(kion)
    zdion=inf;
else
    zdion=zd(kion);
end
% npevion - determined by evanescence between source and ionosphere.
% Calculations at np>npevion are simplified because they do not need
% ionosphere for all points.
dzdsion=zdion-max(zds);
if dzdsion>0
    npevion=sqrt((evanescent_const/dzdsion)^2+1);
else % dzdsion<=0, zero distance through vacuum
    npevion=inf;
end
% npevsi - evanescence between source and observation points (in
% vacuum only).
% npevsi always >= npevion because dzsion >= dzdsi
npevsi=zeros(size(zdi));
% Should the folloing lines be changed to zdil=zdi(zdi<zdion) etc.?
zdil=min(zdi,zdion);
% -- this is equivalent to putting all ionosphere observation points at
% lower ionosphere boundary
zdsl=min(zds,zdion);
% -- same for sources
for ki=1:Mi
    % dzdsi is the min vertical distance from ki-th observation point to
    % the source
    dzdsi=min(abs(zdil(ki)-zdsl));
    if dzdsi==0
        npevsi(ki)=inf;
    else
        % Same use as above
        npevsi(ki)=sqrt((evanescent_const/dzdsi)^2+1);
    end
end
npevobs=max(npevsi);
% In interval >npevion we can do an axisymmetric calculation.
extend0=(npevsi>npevion); % same as npevsi~=npevion because npevsi>=npevion
extend0=extend0(:).'; % a row is more convenient to display
% npcur - from the shape of the current
if point_source
    npcur=inf;
    if drdampd==0
        error('drdamp==0 for a point source');
    end
else
    npcur=npmax_source;
end
% npdamp - from gaussian approximation to a point source
if drdampd==0
    npdamp=inf;
else
    npdamp=sqrt(2*evanescent_const)/(drdampd);
end
% choose the min of the two
npshape=min(npdamp,npcur);
need_gauss=(npdamp<npcur); % the given source must be moderated by gaussian

%% We have npevion, npevobs, extend0, npshape and need_gauss.
% Determine npmax1, npmax2, need_gauss1, need_gauss2, extend
npmax1=min(npevion,npshape);
need_gauss1=(need_gauss && (npevion>npshape));
extend=(extend0 & npevion<=npshape);
if any(extend)
    npmax2=min(npevobs,npshape);
    need_gauss2=(need_gauss && (npevobs>npshape));
else
    npmax2=[]; need_gauss2=[];
end

if isinf(npmax1) || (any(extend) && isinf(npmax2))
    error('internal error: infinite npmax')
end
if any(extend) && npmax1>=npmax2
    error('internal error: npmax1>=npmax2')
end

calculation_plan=struct('npmax1',npmax1,'need_gauss1',need_gauss1,...
    'npmax2',npmax2,'need_gauss2',need_gauss2,'extend',extend,'kion',kion);
