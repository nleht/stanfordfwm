function EH=fwm_axisymmetric_any_source(f,h,ground_bc,perm,...
        ksa,coornperp,nperp1,nperp2,In,...  % Input: the (nx,ny) current
        coorrperp,rperp1,rperp2,hi,...      % Output: the spatial coordinates
        rmaxkm,drkm,retol,Nhdefault) % optional
%FWM_AXISYMMETRIC_ANY_SOURCE Use axial harmonic expansion of any source
% Usage is similar to FWM_NONAXISYMMETRIC:
%     EH=fwm_axisymmetric_any_source(f,h,ground_bc,perm,...
%        ksa,coornperp,nperp1,nperp2,In,...  % Input: the (nx,ny) current
%        coorrperp,rperp1,rperp2,hi,...      % Output: the spatial coordinates
%        rmaxkm,drkm,retol,Nhdefault) % optional
%
% Inputs: The same as for FWM_NONAXISYMMETRIC.
% However, "perm" must describe an axially-symmetric medium.
% Optional inputs:
%     Nhdefault -- number of harmonics to expand the source into
%         (default==3, which is enough for a point source)
%
% Implementation similar to the "extension" part of FWM_NONAXISYMMETRIC
%
% See also: FWM_AXISYMMETRIC, FWM_NONAXISYMMETRIC, FWM_HARMONICS
%
% Author: Nikolai G. Lehtinen

%% Add the missing arguments
nargtot = 17;
if nargin<nargtot
    Nhdefault=[];
end
if nargin<nargtot-1
    retol=[];
end
if nargin<nargtot-2
    drkm=[];
end
if nargin<nargtot-3
    rmaxkm=[];
end

%% Expand the source into axial harmonics
if coornperp~=1
    error(['coornperp==' num2str(coornperp) ' is not implemented'])
else
    nx0=nperp1; ny0=nperp2;
end
if isempty(Nhdefault)
    Nhdefault=3; % we know that for a point source this is enough
end
[Inh, np0, m0]=fwm_harmonics(nx0,ny0,In,Nhdefault);
Nh=length(m0);
% np0 is the array of horizontal refractive indices
% np0==[] for a point source

%% Output coordinates
Mi=length(hi);
% Output horizontal coordinates
if coorrperp==1
    xkmi=rperp1;
    ykmi=rperp2;
    drkmi=min(min(diff(xkmi)),min(diff(ykmi)))/2;
    rmaxkmi=sqrt(max(abs(xkmi))^2+max(abs(ykmi))^2);
    rkmi0=0:drkmi:rmaxkmi;
elseif coorrperp==2
    rkmi0=rperp1;
    phi0=rperp2;
end
Nri=length(rkmi0);

%% A relatively long calculation
EHh=zeros(6,Mi,Nri,Nh);
for kh=1:Nh
    m = m0(kh);
    disp(['******* Harmonic m=' num2str(m) ' *******']);
    if max(abs(Inh(:,:,kh)),[],'all')/max(abs(Inh),[],'all')<1e-10
        disp('Skipping ...')
        EHh(:,:,:,kh)=0;
    else
        EHh(:,:,:,kh)=fwm_axisymmetric(...
            f, h, ground_bc, perm, ksa, np0, m, Inh(:,:,kh), ...
            rkmi0, hi, rmaxkm, drkm, retol);
    end
end
% Convert Ar, Aphi to polar chiral (on x-axis) Ar +- i*Aphi
tmp=EHh([1 4],:,:,:);
EHh([1 4],:,:,:)=tmp+1i*EHh([2 5],:,:,:);
EHh([2 5],:,:,:)=tmp-1i*EHh([2 5],:,:,:);

%% The output field in 2D
% TODO: Get rid of fwm_EHh2EH_polar (?) and duplicate code
% Update fwm_example_horizcompare accordingly (?)
% Maybe factor out code from fwm_nonaxisymmetric extension part
if coorrperp==1
    % Interpolate in cartesian coors
    Nrperp1=length(xkmi);
    Nrperp2=length(ykmi);
    [xkmim,ykmim]=ndgrid(xkmi,ykmi);
    rkmim=sqrt(xkmim.^2+ykmim.^2);
    phim=atan2(ykmim,xkmim);
elseif coorrperp==2
    Nrperp1=length(rkmi0);
    Nrperp2=length(phi0);
    [rkmim,phim]=ndgrid(rkmi0,phi0);
end
Nrperp=Nrperp1*Nrperp2;
rkmi=rkmim(:);
phi=phim(:);

% size(EHhi)==[6 Mi Nrperp Nh]
% Reminder: EHhi contains Ar +- i*Aphi (on x-axis)
EHhi=permute(interp1(rkmi0,permute(EHh,[3 1 2 4]),rkmi),[2 3 1 4]);
EHi=zeros(6,Mi,Nrperp);
phimm = repmat(shiftdim(phi,-2),[2 Mi 1]); % size==[2 Mi Nrperp]
for kh=1:Nh
    m=m0(kh);
    EH1=EHhi(:,:,:,kh); % size=[6 Mi Nrperp]
    % Now EH1 needs to be rotated according to its harmonic
    EH1([3 6],:,:)=EH1([3 6],:,:).*exp(1i*m*phimm);
    % Convert Ar +- i*Aphi to Ax +- i*Ay
    tmp=EH1([1 4],:,:).*exp(1i*(m+1)*phimm);
    EH1([2 5],:,:)=EH1([2 5],:,:,:).*exp(1i*(m-1)*phimm);
    % And, finally, Ax +- i*Ay to Ax and Ay
    EH1([1 4],:,:)=(tmp+EH1([2 5],:,:))/2;
    EH1([2 5],:,:)=(tmp-EH1([2 5],:,:))/2i;
    EHi=EHi+EH1;
end
EH=reshape(EHi,[6 Mi Nrperp1 Nrperp2]);
