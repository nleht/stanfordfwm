function EH=fwm_assemble_xy(k0,nx,ny,da,EHf,x,y,bkp_file)
%FWM_ASSEMBLE_XY Inverse Fourier transform from the polar mesh
% Version accelerated by Drew Compston, for large memory computers
% It is much slower than FFT
% This version uses parallel processing
% Usage:
%    EH=fwm_assemble_par(k0,nx,ny,da,EHf,x,y);
% Inputs:
% Outputs:
% See also: FWM_ASSEMBLE_XY
% Author: Nikolai G. Lehtinen, Drew Compston

% In the corrected version, we fixed the following bug:
% For real ph, when abs(ph)>pi, and for non-integer x
%    exp(1i*ph*x)~=exp(1i*ph).^x
% The same happens when using "power" instead of ".^"
% This is related to the fact that for such values of ph,
%    1i*ph==log(exp(1i*ph))+2i*pi

if nargin >= 8 && ~isempty(bkp_file)
    bkp_file_ext = [bkp_file '_FWM_ASSEMBLE_XY.mat'];
    if exist(bkp_file_ext, 'file')
        s = load(bkp_file_ext, 'EH');
        if isfield(s, 'EH')
            EH = s.EH;
            disp('FWM_ASSEMBLE_XY: loaded precalculated output arguments!');
            return;
        else
            disp(['WARNING: the backup file ' bkp_file_ext ' is invalid!']);
        end
    end
else
    bkp_file_ext = [];
end

N = numel(nx);
Mi = size(EHf, 2);
Nx = numel(x);
Ny = numel(y);

expx = exp(1i*k0*bsxfun(@times, x(:), nx(:).')); % Size [Nx x N]
expy = exp(1i*k0*bsxfun(@times, y(:).', ny(:))); % Size [N x Ny]
EHda = reshape(bsxfun(@times, permute(EHf, [3 1 2]), (k0/2/pi)^2*da(:)), [N 6*Mi]);

EH(Nx, Ny, 6*Mi) = 0;
for i1 = 1:(6*Mi)
	EH(:, :, i1) = expx * bsxfun(@times, EHda(:, i1), expy);
end
EH = reshape(permute(EH, [3 1 2]), [6 Mi Nx Ny]);
if ~isempty(bkp_file)
    disp(['Saving results of FWM_ASSEMBLE_XY into ' bkp_file_ext ' ...']);
    save(bkp_file_ext, 'EH', '-v7.3');
    disp(' ... done');
end

