function [Inh,np0,m0]=fwm_harmonics(nx0,ny0,In,Nh,Nhshift)
%FWM_HARMONICS Convert the current to axial harmonics
% This is done for input into FWM_AXISYMMETRIC.
% Usage:
%    [Inh,np0,m0]=fwm_harmonics(nx0,ny0,In);
% Arguments:
%    nx0 (Nnx0), ny0 (Nny0) - values of (nx,ny)
%    In (6 x Ms x Nnx0 x Nny0) - current
% Outputs:
%    Ih (6 x Ms x Nnp0 x Nh)
%    np0 (Nnp0)
%    m0 (Nh)
% See also: FWM_AXISYMMETRIC
% Author: Nikolai G. Lehtinen

% Convert to polar system of coordinates
if nargin<4
    Nh=2^5;
end
if nargin<5
    Nhshift=floor(Nh/2);
end
th=[0:Nh-1]*2*pi/Nh;
if length(nx0)<=1
    assert(length(ny0)<=1)
    s = size(In);
    assert(length(s)<=2 && s(1)==6)
    point_source=1;
else
    s = size(In);
    assert(s(1)==6 && s(3)==length(nx0) && s(4)==length(ny0))
    point_source=0;
end
Ms=size(In,2);
m0=[0:Nh-1].'-Nhshift;
if point_source
    % Only m=-1,0,1 are nonzero
    % Convert to I+, I-
    tmp=In([1 4],:); % Ix
    In([1 4],:)=tmp+1i*In([2 5],:); % I+
    In([2 5],:)=tmp-1i*In([2 5],:); % I-
    Inh=zeros(6,Ms,Nh);
    Inh([1 4],:,(m0==-1)) = In([1 4],:); % shift harmonic by -1
    Inh([2 5],:,(m0==1))  = In([2 5],:); % shift harmonic by +1
    Inh([3 6],:,(m0==0))  = In([3 6],:); % just copy z-component zeroth harmonic
    % Go back to Ix, Iy
    tmp=Inh([1 4],:,:);
    Inh([1 4],:,:)=(tmp+Inh([2 5],:,:))/2; % Ix, or really, Ir
    Inh([2 5],:,:)=(tmp-Inh([2 5],:,:))/2i; % Iy, or really, Iphi
    np0 = [];
    return;
end
nperpmax=max([nx0(:);ny0(:)]);
dnx0=min([diff(nx0(:));diff(ny0(:))]);
np0=[0:dnx0:nperpmax];
Nnp0=length(np0);
[npm,thm]=ndgrid(np0,th);
nxm=npm.*cos(thm);
nym=npm.*sin(thm);
% Convert to I+, I-
tmp=In([1 4],:,:,:); % Ix
In([1 4],:,:,:)=tmp+1i*In([2 5],:,:,:); % I+
In([2 5],:,:,:)=tmp-1i*In([2 5],:,:,:); % I-
Ip=permute(In,[3 4 1 2]); % Nnx0 x Nny0 x 6 x Ms
%Inth=zeros(6,Ms,Nnp0,Nh);
Inh=zeros(6,Ms,Nnp0,Nh);
for ks=1:Ms
    for c=1:6
        %Inth(c,ks,:,:)=interp2(ny0,nx0,Ip(:,:,c,ks),nxm,nym);
        Inh(c,ks,:,:)=fft(interp2(ny0,nx0,Ip(:,:,c,ks),nxm,nym),[],2)/Nh;
    end
end
% Take into account that I+ == const corresponds to m==-1 etc.
tmp=circshift(Inh([1 4],:,:,:),[0 0 0 -1]); % was: Ih([1 4],:,:,:), i.e. I+
Inh([2 5],:,:,:)=circshift(Inh([2 5],:,:,:),[0 0 0 1]);
% Go back to Ix, Iy
Inh([1 4],:,:,:)=(tmp+Inh([2 5],:,:,:))/2; % Ix, or really, Ir
Inh([2 5],:,:,:)=(tmp-Inh([2 5],:,:,:))/2i; % Iy, or really, Iphi
% We could have also transformed (Ix,Iy)->(Ir,Iphi) and then do the Fourier
% transform in phi. Then there would be no need for dm == +-1 shifts.
% Do the shift to the center
Inh=circshift(Inh,[0 0 0 Nhshift]);
