function EH=fwm_assemble_xy_par(k0,nx,ny,da,EHf,x,y,bkp_file)
%FWM_ASSEMBLE_XY_PAR Inverse Fourier transform from the polar mesh
% It is much slower than FFT
% This version uses parallel processing
% Usage:
%    EH=fwm_assemble_par(k0,nx,ny,da,EHf,x,y);
% Inputs:
% Outputs:
% See also: FWM_ASSEMBLE_XY
% Author: Nikolai G. Lehtinen

% In the corrected version, we fixed the following bug:
% For real ph, when abs(ph)>pi, and for non-integer x
%    exp(1i*ph*x)~=exp(1i*ph).^x
% The same happens when using "power" instead of ".^"
% This is related to the fact that for such values of ph,
%    1i*ph==log(exp(1i*ph))+2i*pi

%% Constants
program='FWM_ASSEMBLE_XY';
arginnames={'k0','nx','ny','da','EHf','x','y'};
argoutnames={'EH'};

%% Initial setups
stdargin=length(arginnames);
stdargout=length(argoutnames);
if nargin < stdargin+1
    bkp_file=[];
end
if nargin < stdargin
    error('Not enough arguments!')
end
global output_interval backup_interval
if isempty(output_interval)
    output_interval=20;
end
if isempty(backup_interval)
    backup_interval=3600;
end
do_backups=~isempty(bkp_file);

%% Various
Mi=size(EHf,2);
% This would be SOO much faster if we did an FFT
% The weighted field
%EHfw=EHf.*repmat(da,[6 1 Mi])*(k0^2/(2*pi)^2);
% - Multiply by usual integration coefficient

%% Divide all n-space into regions, 1 region for each processor
%Nproc=matlabpool('size')
% Large number of processors allows smaller memory chunk to be passed to
% each processor.
N=length(nx);
Nproc=ceil(N/1024) % The optimal number here is determined by on-processor cache (?)
N1=ceil(N/Nproc) % Portion of n-space going to each CPU
if length(ny)~=N || length(da)~=N
    error([program ': arguments of wrong length'])
end
ipend=round([1:Nproc]*N/Nproc);
ipstart=[1 ipend(1:Nproc-1)+1];

%% Check if the arrays x, y are uniformly spaced
Nx=length(x); Ny=length(y);
if Nx>1
    tmp=diff(x);
    dx=mean(tmp);
    xunif=max(abs(tmp-dx))<10*eps;
else
    xunif=0;
end
if Ny>1
    tmp=diff(y);
    dy=mean(tmp);
    yunif=max(abs(tmp-dy))<10*eps;
else
    yunif=0;
end

disp(['x uniform = ' num2str(xunif) '; y uniform = ' num2str(yunif)]);

nxmat=repmat(nx,[6 1]);
nymat=repmat(ny,[6 1]);
if xunif
    expdx=exp(1i*k0*dx*nxmat);
    %expx0=expdx.^(x(1)/dx); % This produces a bug
    %expx0=exp(1i*k0*x(1)*nxmat); % Not needed
end
if yunif
    expdy=exp(1i*k0*dy*nymat);
    % Pre-calculate the Fourier coefficients
    %expy0=expdy.^(y(1)/dy);  % This produces a bug
    expy0=exp(1i*k0*y(1)*nymat);
    % Sliced arrays for parallel processing
    expdy1=zeros(6,N1,Nproc);
    expy01=zeros(6,N1,Nproc);
    for iproc=1:Nproc
        expdy1(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=expdy(:,ipstart(iproc):ipend(iproc));
        expy01(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=expy0(:,ipstart(iproc):ipend(iproc));
    end
else
    % Sliced arrays for parallel processing
    nymat1=zeros(6,N1,Nproc);
    for iproc=1:Nproc
        nymat1(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=nymat(:,ipstart(iproc):ipend(iproc));
    end
end

%% Determine if we can restore from a backup
if do_backups
    bkp_file_ext=[bkp_file '_' program '.mat'];
    if ~exist(bkp_file_ext,'file')
        disp([program ': creating a new backup file ' bkp_file_ext])
        status='starting';
        save(bkp_file_ext,arginnames{:},'status');
        restore=0;
    elseif ~check_bkp_file(bkp_file_ext,arginnames,{k0,nx,ny,da,EHf,x,y})
        disp(['WARNING: the backup file ' bkp_file_ext ' is invalid! Not doing backups']);
        do_backups=0;
    else % File exists and is valid
        disp(['Found backup file ' bkp_file_ext]);
        tmp=load(bkp_file_ext,'status');
        if ~isfield(tmp,'status')
            error('no status!')
        end
        status=tmp.status;
        if strcmp(status,'done')
            disp([program ': loaded precalculated output arguments!']);
            % No need to do anything
            tmp=load(bkp_file_ext,argoutnames{:});
            if nargout>stdargout
                error('Too many output arguments');
            end
            EH=tmp.EH;
            return;
        end
        restore=strcmp(status,'in progress');
    end
end
if do_backups
    disp([program ' BACKUP STATUS = ' status]);
end

%% Restore
if do_backups && restore
    % The file is guaranteed to exist, and to contain relevant information
    tmp=load(bkp_file_ext,'ki','ix','EH');
    kistart=tmp.ki; % I think there should be +1
    ixsaved=tmp.ix+1;
    EH=tmp.EH;
    disp([program ': restored from backup']);
else
    kistart=1; ixsaved=1;
    EH=zeros(6,Mi,Nx,Ny);
    disp([program ': starting a new calculation']);
end

%% Cycle over ki
tstart=now*24*3600; toutput=tstart; timebkup=tstart;
EHtmp=zeros(6,Ny,Nproc);
for ki=kistart:Mi
    EHfw=squeeze(EHf(:,ki,:)).*repmat(da(:).',[6 1])*(k0^2/(2*pi)^2);
    if ki==kistart
        ixstart=ixsaved;
    else
        ixstart=1;
    end
    if xunif
        expx=exp(1i*k0*x(ixstart)*nxmat);
    end
    for ix=ixstart:Nx
        if xunif
            tmp=EHfw.*expx;
        else
            tmp=EHfw.*exp(1i*k0*x(ix)*nxmat);
        end
        % Parallel CPUs: slice the inputs
        tmp1=zeros(6,N1,Nproc);
        for iproc=1:Nproc
            tmp1(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=tmp(:,ipstart(iproc):ipend(iproc));
        end
        if yunif
            parfor iproc=1:Nproc
                EHtmp1=zeros(6,Ny);
                tmp2=tmp1(:,:,iproc);
                expy=expy01(:,:,iproc); % =exp(i*k0*y(1)*nymat);
                expdy2=expdy1(:,:,iproc);
                for iy=1:Ny
                    %EHsat(:,ix,iy)=sum(EHfw.*exp(i*k0*(x(ix)*nxmat+y(iy)*nymat)),2);
                    EHtmp1(:,iy)=sum(tmp2.*expy,2);
                    expy=expy.*expdy2;
                end
                EHtmp(:,:,iproc)=EHtmp1;
            end
        else % Not yunif
            parfor iproc=1:Nproc
                EHtmp1=zeros(6,Ny);
                tmp2=tmp1(:,:,iproc);
                nymat2=nymat1(:,:,iproc);
                for iy=1:Ny
                    EHtmp1(:,iy)=sum(tmp2.*exp(1i*k0*y(iy)*nymat2),2);
                end
                EHtmp(:,:,iproc)=EHtmp1;
            end
        end % yunif
        EH(:,ki,ix,:)=sum(EHtmp,3);
        if xunif
            expx=expx.*expdx;
        end
        
        timec=now*24*3600;
        ttot=timec-tstart;
        if timec-toutput>output_interval
            toutput=timec;
            if ki==kistart
                isdonenow=ix-ixsaved+1;
            else
                isdonenow=(ki-kistart-1)*Nx+ix+(Nx-ixsaved+1);
            end
            isremaining=(Mi-ki+1)*Nx-ix;
            disp([program ': Done=' num2str((ki-1+ix/Nx)/Mi*100) '%; ' ...
                'Time=' hms(ttot) ...
                ', ETA=' hms(ttot/isdonenow*isremaining)]);
        end
        if do_backups
            % Do backups every hour
            if timec-timebkup>backup_interval
                timebkup=timec;
                disp(['Backing up to ' bkp_file_ext ' ...']);
                status='in progress';
                save(bkp_file_ext,'ki','ix','EH','status','-append');
                disp(' ... done');
            end
        end
    end
end

%% REUSABLE: Final backup
if do_backups
    disp(['Saving results of ' program ' into ' bkp_file_ext ' ...']);
    status='done';
    save(bkp_file_ext,argoutnames{:},'status','-append');
    disp(' ... done');
end
% Output
if nargout>stdargout
    error('Too many output arguments');
end
for k=1:nargout
    eval(['varargout{k}=' argoutnames{k} ';']);
end
