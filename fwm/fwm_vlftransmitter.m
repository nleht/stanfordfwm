function EH=fwm_vlftransmitter(varargin)
% FWM_VLFTRANSMITTER Calculate the VLF field of a transmitter
% Usage:
%   [EH=]fwm_vlftransmitter(VLF,IonosphereProfileInfo,xkmi,hi,...
%          fnamebase[,options]);
% Inputs:
%   VLF - VLF transmitter info as a structure loaded by VLFTRANSMITTERS
%   IonosphereProfileInfo: either an (M x 3) array with columnts
%     corresponding to altitude (km), Ne (m^{-1}) and nue (s^{-1}), or
%     string, 'iri' or the name of one of the fixed profiles (such as
%     'HAARPsummernight', 'Stanford_eprof1' etc). If it is 'iri', the IRI
%     model is called with specified options.
%   xkmi - horizontal outputs in km (same in both directions)
%   hi - vertical outputs in km
%   fnamebase - start of file names to be used for backups and outputs
% Options:
%   If IonosphereProfileInfo is 'iri', or a named profile, a boolean
%     'daytime' must be given (there is no default value).
%   If IonosphereProfileInfo is 'iri', the following options may be given:
%     'month' (default=6), 'day' (default=22), 'year' (default=2010).
%   Options for extra attenuation (only if IonosphereProfileInfo is 'iri' or
%     a named profile): 'D-region','Coulomb_collisions' (boolean, default=0)
%   'geomagnetic_coordinates' (default=0) - use geomagnetic coordinates with
%     Bgeo pointing to the North.
%   Options for FMW: 'retol' (default=1e-3), 'drkm' (default is automatic),
%     'rmaxkm' (default is automatic). Changing not recommended.
% Output: EH (6 x Mi x Nx x Nx) - complex components of E and H*Z0, in
%   the "physics convention" (~exp(-i*w*t)).
%   Field component (Ex,Ey,Ez,Z0*Hx,Z0*Hy,Z0*Hz) is the first index, vertical
%   grid is the second index, horizontal x (West-East) and y (South-North)
%   grid are the 3rd and 4th indeces.
% Example:
%   vlftransmitters
%   fwm_vlftransmitter(NML,'iri',[-1280:5:1280],[150 700],'NML/',...
%      'year',2008,'daytime',1,'month',12,'D-region',1);
%   % This creates a file
%   % NML/NML_iri_12h_22-Dec-2008_Dregion_r1280_dr5_err3_EH.mat

keys={'daytime','month','day','year','D-region','Coulomb_collisions',...
    'retol','drkm','rmaxkm','geomagnetic_coordinates'};
[VLF,IonosphereProfileInfo,xkmi,hi,fnamebase,options]=parsearguments(varargin,5,keys);

%% Setup
global clight eps0 impedance0
if isempty(clight)
    loadconstants
end
f=VLF.f;
w=2*pi*f;
k0=w/clight;
P0=VLF.P0;
I=[0;0;sqrt(6*pi*P0/k0^2*impedance0);0;0;0];  % Z0*I*l, current moment in Budden units V*m

%% Stratification (vertical) grid h, Ne and nue
if ~ischar(IonosphereProfileInfo)
    h=IonosphereProfileInfo(:,1);
    Ne=IonosphereProfileInfo(:,2);
    nue=IonosphereProfileInfo(:,3);
    Ne_name='';
else
    daytime=getvaluefromdict(options,'daytime',[]);
    if isempty(daytime)
        error('no time of day given for an IRI or named profile');
    end
    %% h
    hmax1=137.5; hmax2=150; hmax3=300;
    if daytime
        hmin=65;
    else
        hmin=80;
    end
    h=[0 hmin:.25:hmax1].';
    if any(hi>hmax1)
        h=[h.' hmax1+.5:.5:hmax2].';
    end
    if any(hi>hmax2)
        h=[h.' (hmax2+1):1:hmax3].';
    end
    if any(hi>hmax3)
        hmax=5*round(max(hi)/5);
        h=[h.' (hmax3+5):5:hmax].';
    end
    %% Ne
    if strcmp(IonosphereProfileInfo,'iri')
        iut=0;
        % Default day is June 22, 2010
        month=getvaluefromdict(options,'month',6);
        day=getvaluefromdict(options,'day',22);
        year=getvaluefromdict(options,'year',2010);
        stime=[sprintf('%02d',daytime*12) 'h'];
        sdate=datestr([year month day 0 0 0]);
        if year>2006
            warning('Taking the previous solar cycle')
            % take the previous solar cycle
            year=year-11;
        end
        Ne=iri('Ne',h,VLF.lat,VLF.lon,year,month,day,daytime*12,iut);
        Ne_name=['iri_' stime '_' sdate];
    else
        Ne_name=IonosphereProfileInfo;
        Ne=getNe(h,IonosphereProfileInfo);
    end
    % Extend the D-region if needed
    do_Dregion=getvaluefromdict(options,'D-region',0);
    if do_Dregion
        % Note that the choice of 30 km for D-region cutoff, in order to
        % avoid strict requirements on evanescence
        hlow=[0 30:5:hmin-10 hmin-9:hmin-5 hmin-4:0.25:hmin-0.25].';
        hDregion=[hlow;h(2:40)]; % Altitude in km
        % Calculate various species densities
        NeDregion=[zeros(size(hlow));Ne(2:40)];
        % We make a small error because Tn is assumed from this
        % profile, even during daytime or winter.
        MSISProfile='HAARPsummernight';
        Nspec0=ionochem_6spec(MSISProfile,daytime,hDregion,1e-6*NeDregion);
        % Convert to m^{-3} -- IMPORTANT!
        NeDregionNew=1e6*Nspec0(:,1); % re-calculated value
        % Merge the two results
        hnew=[hDregion;h(41:end)];
        Nenew=[NeDregionNew; Ne(41:end)];
        h=hnew; Ne=Nenew;
        Ne_name=[Ne_name '_Dregion'];
    end
    %% nue
    % Collisions with neutrals
    nue=get_nu_electron_neutral_swamy92(h);
    do_Coulomb=getvaluefromdict(options,'Coulomb_collisions',0);
    if do_Coulomb
        % Coulomb collisions, as specified in Helliwell
        if daytime
            nuei_factor=7e-10;
        else
            nuei_factor=15e-10;
        end
        nuei=nuei_factor*Ne;
        nue=nue+nuei;
        Ne_name=[Ne_name '_Coulomb'];
    end
end

%% Ground conductivity
if isempty(VLF.sground)
    eground='E=0';
elseif ischar(VLF.sground)
    eground=VLF.sground;
else
    eground=1+1i*VLF.sground/(eps0*w);
end

%% Geomagnetic field
B=VLF.Bgeo;
Babs=sqrt(sum(B.^2));
thB=acos(B(3)/Babs);
do_geomagnetic=getvaluefromdict(options,'geomagnetic_coordinates',0);
if do_geomagnetic
    phB=pi/2; % y-axis points to geomagnetic north;
    Ne_name=[Ne_name '_Geomag'];
else
    phB=atan2(B(2),B(1));
end

%% Permittivity tensor
perm=get_perm_with_ions(h,w,'Ne',Ne,'nue',nue,'Babs',Babs,'thB',thB,'phB',phB,'need_ions',0);

%% Horizontal output grid
dxkmi=mean(diff(xkmi)); xmaxkmi=max(abs(xkmi));
ykmi=xkmi;

%% FWM options
retol=getvaluefromdict(options,'retol',1e-3);
% value of the width of the source dr for which the evanescence for propagation between Earth and
% ionosphere beats the damping due to the width of the source
drkm=getvaluefromdict(options,'drkm',dxkmi);
% We stop our calculations at high kperp when field decreases by a factor
% of exp(-evanescent_const)
evanescent_const=30; % one used in fwm_npmax
% Determine the width of the source which still does not distort the
% signal. The condition is
%   kperpmax_source > kperpmax_evanescence
% where kperpmax is kperp at which field decreases by exp(-evanescent_const).
% For a gaussian source ~exp(-r^2/(2*width^2)), the kperp-domain dependence
% is ~exp(-width^2*kperp^2/2), therefore
%   width^2*kperpmax_source^2/2 == evanescent_const
%   kperpmax_source = sqrt(2*evanescent_const)/width
% The evanescence is ~exp(-dh*kappa) where kappa=sqrt(kperp^2-k0^2) and
% in this case dh=h(2). Thus,
%   dh*sqrt(kperpmax_evanescence^2 - k0^2) == evanescent_const
%   kperpmax_evanescence = sqrt((evanescent_const/dh)^2+k0^2)
% Condition kperpmax_source > kperpmax_evanescence is equivalent to
%   width < sqrt(2*evanescent_const)/sqrt((evanescent_const/dh)^2+k0^2)
maxwidthkm=sqrt(2*evanescent_const/((evanescent_const/(k0*h(2)*1000))^2+1))/k0/1e3;
if maxwidthkm<drkm
    warning(['narrowing the source down to ' num2str(0.9*maxwidthkm) ' km']);
    drkm=0.9*maxwidthkm;
end
rmaxkm=getvaluefromdict(options,'rmaxkm',[]);

%% Output file(s)
filenamebase=[fnamebase VLF.name];
if ~isempty(Ne_name)
    filenamebase=[filenamebase '_' Ne_name];
end
fname=[filenamebase '_r' num2str(xmaxkmi) '_dr' num2str(dxkmi) '_err' num2str(-log10(retol))]
eval(['save ' filenamebase '.mat h Ne eground nue w Babs thB phB'])

%% Output and backup options
global output_interval backup_interval
% If they are given globally, leave them alone
if isempty(output_interval)
    output_interval=60;
end
if isempty(backup_interval)
    backup_interval=1200;
end

%% StanfordFWM
if ~exist([fname '_EH.mat'],'file')
    EH=fwm_nonaxisymmetric(f,h,eground,perm,...
        [1],1,[],[],I,...
        1,xkmi,ykmi,hi,...
        rmaxkm,drkm,retol,fname);
    eval(['save ' fname '_EH.mat EH hi xkmi ykmi']);
else
    disp(['*** NOTHING TO DO, FILE ' fname '_EH.mat ALREADY EXISTS ***']);
    eval(['load ' fname '_EH.mat']);
end
