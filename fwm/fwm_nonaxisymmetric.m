function [EH,calculation_plan,npe_struct,waves_struct]=...
    fwm_nonaxisymmetric(f,h,eground,perm,...
        ksa,coornperp,nperp1,nperp2,In,...  % Input: the (nx,ny) current
        coorrperp,rperp1,rperp2,hi_core,...      % Output: the spatial coordinates
        rmaxkm,drkm,retol,bkp_file,...      % Optional (fixed-position args)
        varargin)                           % Named options
%FWM_NONAXISYMMETRIC Full-wave method, the general case
% Usage:
%    EH=fwm_nonaxisymmetric(f,h,eground,perm,...
%       ksa,{1,nx0,ny0|2,np0,phi0|3,np0,m0},In,...
%       {1,xkm,ykm|2,rkm,phi|3,rkm,m},hi ...
%       [,rmaxkm,drkm,retol,bkp_file]);
% Advanced usage:
%    [EH,calculation_plan,npe_struct,waves_struct]=fwm_nonaxisymmetric(...);
% (must multiply by impedance0 after calculation);
% Inputs:
%    f - frequency (Hz)
%    h (M) - altitudes in km
%    eground - ground permittivity (scalar) or boundary condition (string),
%       chosen from 'E=0','H=0' or 'free'
%    perm (3 x 3 x M) - dielectric permittivity tensor in each layer
%    ksa, coornperp, nperp1, nperp2, In - the horizontal Fourier transform
%       of the current:
%       ksa (Ms) - indeces of altitudes in which the current is present,
%          i.e. at h(ksa)
%       Depending on the value of "coornperp":
%       coornperp==1: nperp1==nx0, nperp2==ny0
%          nx0 (Nnx0), ny0 (Nny0) - cartesian coordinates (nx,ny) at which
%             the current is given
%       coornperp==2: nperp1==np0, nperp2==phin0
%          np0 (Nnp0) - points in np at which the current is given
%          phin0 (Nphin0) - angles at which Ix,Iy,Iz are given
%       coornperp==3: nperp1==np0, nperp2==m0
%          np0 (Nnp0) - points in np at which the current is given
%          m0 (Nh0) - harmonic, the current Ir,Ith,Iz~exp(i*m*th)
%       In (6 x Ms x {Nnx0|Nnp0|Nnp0} x {Nny0|Nphin0|Nh0}) - the value of
%          the current moment (for vertical components) or surface current
%          (for horizontal components) at the given value of
%          nperp and h(ksa) (in V*m, both electric and magnetic). IMPORTANT
%          NOTE: It is a Fourier transform! For the units, see Note 2 in
%          FWM_FIELD.
%    coorrperp,rperp1,rperp2,hi - output coordinates
%       Depending on the value of "coorrperp":
%       coorrperp==1: rperp1==xkm, rperp2==ykm
%          xkm (Nx), ykm (Ny) - radial distance in km
%       coorrperp==2: rperp1==rkm, rperp2==phi
%          rperp1 (Nr) - distance in km
%          phi (Nphir) - 
%       hi (Mi) - output altitudes in km
% Optional inputs:
%    rmaxkm (scalar) - max perp distance at which the results are still
%       valid, default=sqrt(max(xkm)^2+max(ykm)^2)
%    drkm (scalar) - the size of the source in km (important for a point
%       source and calculations on the ground), default==xkm(2)-xkm(1)
%    retol (scalar) - relative error tolerance, default=1e-3 (take a
%       smaller value for more accurate results)
% Output:
%    EH (6 x Mi x {Nx|Nr} x {Ny|Nphi|Nh}) - E, H components on the positive
%       x-axis at coordinates (rperp,hi) (in V/m) (see note 1)
% Optional outputs (diagnostic) [THESE ARE CONTAINED IN OUTPUT STRUCTS]:
%    EHn (6 x Mi x Nmodes) - field values in (nperp,z)-space (in V*m). The
%       grid is specified by the following arguments:
%    nx (Nmodes), ny (Nmodes) - values of nperp at which the Fourier
%       components EHn are calculated
%    da (Nmodes) - the area elements in nperp plane
%    npb (Nnp+1) - values of |nperp| interval boundaries on the grid
%    np (Nnp) - values of |nperp| on the grid
%    Nphi (Nnp) - number of points for each np(ip) at different angles,
%       phi==[0:Nphi(ip)-1]*2*pi/Nphi(ip);
% Notes (IMPORTANT!):
% 1. See notes 1-2 to FWM_AXISYMMETRIC, and note 3 if using axial harmonics
% Author: Nikolai G. Lehtinen
% See also: FWM_AXISYMMETRIC, FWM_FIELD, FWM_RADIATION
% Examples:
%    fwm_example_horizcompare_nonaxisymmetric
%    fwm_example_with_horizontal_B (old version),
%    fwm_example_horiz_b (new version)
%    fwm_nonaxisymmetric_antenna_example,
%    fwm_nonaxisymmetric_antenna_horizb

%% Optional arguments
allowedkeys = {'chi0','new_last_step','do_exact_if_possible','no_vacuum_extension','npmax_source_given',...
        'hi_extra','log2Nphimin'};
options = parsearguments(varargin,0,allowedkeys);
% Note that all pre-default values are [] so that the correct default can be set
log2Nphimin=getvaluefromdict(options,'log2Nphimin',[]);
hi_extra=getvaluefromdict(options,'hi_extra',[]);
npmax_source_given=getvaluefromdict(options,'npmax_source_given',[]);
no_vacuum_extension=getvaluefromdict(options,'no_vacuum_extension',[]);
if isempty(no_vacuum_extension)
    no_vacuum_extension=false;
end
do_exact_if_possible=getvaluefromdict(options,'do_exact_if_possible',[]);
if isempty(do_exact_if_possible)
    do_exact_if_possible=false;
end
new_last_step=getvaluefromdict(options,'new_last_step',[]);
if isempty(new_last_step)
    new_last_step='';
end
only_last_step=~isempty(new_last_step);
chi0=getvaluefromdict(options,'chi0',[]);
if isempty(chi0)
    chi0=0;
end

%%  Optional arguments with fixed position (to preserve the interface)
if nargin<17
    bkp_file=[];
end
if nargin<16
    retol=[];
end
if nargin<15
    drkm=[];
end
if nargin<14
    rmaxkm=[];
end

%% Choose the interpretation of arguments
% The current is given in what coordinates?
point_source=isempty(nperp1);
switch coornperp
    case 1
        % cartesian
        nx0=nperp1; ny0=nperp2;
        if isempty(npmax_source_given)
            npmax_source=sqrt(max(abs(nx0))^2+max(abs(ny0))^2);
        else
            npmax_source=npmax_source_given;
        end
    case 2
        % polar
        np0=nperp1; phin0=nperp2;
        if isempty(npmax_source_given)
            npmax_source=max(np0);
        else
            npmax_source=npmax_source_given;
        end
    case 3
        % harmonics
        np0=nperp1; m0=nperp2;
        if isempty(npmax_source_given)
            npmax_source=max(np0);
        else
            npmax_source=npmax_source_given;
        end
    otherwise
        error(['unknown coordinate sys for nperp: ' num2str(coornperp)]);
end
% if point_source, we have npmax_source=[] at any case
switch coorrperp
    case 1
        % cartesian
        xkmi=rperp1; ykmi=rperp2;
    case 2
        % polar
        rkm=rperp1; phir=rperp2;
    case 3
        % harmonics
        rkm=rperp1; mr=rperp2;
    otherwise
        error(['unknown coordinate sys for rperp: ' num2str(coorrperp)]);
end

%% Default argument values
do_backups=~isempty(bkp_file);
if isempty(retol)
    retol=1e-3;
end
if isempty(drkm)
    if coorrperp==1
        dxkm=min(diff(xkmi));
        dykm=min(diff(ykmi));
        drkm=min([dxkm dykm]);
    else
        drkm=min(diff(rkm));
    end
end
if isempty(rmaxkm)
    if coorrperp==1
        rmaxkm=sqrt(max(abs(xkmi))^2+max(abs(ykmi))^2);
    else
        rmaxkm=max(rkm);
    end
end

%% Save a backup of all arguments
if do_backups
    fname=[bkp_file '_FWM_NONAXISYMMETRIC_arg'];
    disp(['Saving arguments into ' fname]);
    save([fname '.mat'],'f','h','eground','perm', ...
        'ksa','coornperp','nperp1','nperp2','In', ...
        'coorrperp','rperp1','rperp2','hi_core', ...
        'rmaxkm','drkm','retol','bkp_file','chi0','new_last_step',...
        'do_exact_if_possible','no_vacuum_extension','npmax_source_given',...
        'hi_extra','log2Nphimin');
end

%% Various constants
Ms=length(ksa);
global clight
if isempty(clight)
    loadconstants
end
w=2*pi*f;
k0=w/clight;
M=length(h);
zd=h*1e3*k0;
Mi_core=length(hi_core);
zdi_core=hi_core*1e3*k0;
zds=zd(ksa); % source altitudes
switch coorrperp
    case 1
        x=xkmi*1e3;
        y=ykmi*1e3;
        wnotresolved=(max(diff(x))*k0>1) || (max(diff(y))*k0>1);
    case {2,3}
        r=rkm*1e3;
        wnotresolved=max(diff(r))*k0>1;
end
drdampd=drkm*1e3*k0;

if wnotresolved
    disp('WARNING: the wavelength is not resolved')
end
[kia,dzdil,dzdih]=fwm_get_layers(zd,zdi_core);

% The minimum dnperp, determined by max(x)
magic_scale=0.9; % 0.9 works for vacuum; probably =0.5 will work for anything.
% To resolve it, we need at least
dn0=magic_scale*2*pi/(k0*1e3*rmaxkm)

%% Determine the maximum nperp
calculation_plan=fwm_calculation_plan(zd,perm,zds,zdi_core,point_source,npmax_source,drdampd)
if no_vacuum_extension
    % Just do a complete calculation all at once
    if any(calculation_plan.extend)
        calculation_plan.need_gauss1=calculation_plan.need_gauss2;
        calculation_plan.npmax1=calculation_plan.npmax2+dn0;
        calculation_plan.extend(:)=false;
        calculation_plan.npmax2=[]; calculation_plan.need_gauss2=[];
        disp('*********** Correcting the plan for calculation, the new one: ************');
        calculation_plan
    end
end
    
%% Initial np grid
npbi=fwm_initial_npb(calculation_plan.npmax1,dn0);

if only_last_step
    tmp=load([bkp_file '_FWM_BESTGRID.mat']);
    npb=tmp.npb; np=tmp.np;
else
    % Find the best grid
    Nphitry=8;
    %phitry0=atan2(Bgeo(2),Bgeo(1));
    phitry=(0:Nphitry-1)*2*pi/Nphitry;

    % Try twice, supposedly spend less time with coarser grids when many phi
    % points
    % In has size 6 x Ms [x Nnx0 [x Nny0]]
    program='FWM_BESTGRID';
    if do_backups
        bkp_file_ext=[bkp_file '_' program '.mat'];
        % See if we can retreive the best grid from the backup
        argnames={'f','h','perm','eground',...
            'ksa','coornperp','nperp1','nperp2','In','hi_core','rmaxkm','retol'};
        args={f,h,perm,eground,...
            ksa,coornperp,nperp1,nperp2,In,hi_core,rmaxkm,retol};
        if ~exist(bkp_file_ext,'file')
            disp(['Creating a new backup file ' bkp_file_ext])
            status='starting';
            save(bkp_file_ext,argnames{:},'status');
            restore=0;
        elseif ~check_bkp_file(bkp_file_ext,argnames,args)
            disp(['WARNING: the backup file ' bkp_file_ext ' is invalid! Not doing backups']);
            do_backups=0;
        else % File exists and is valid
            disp(['Found backup file ' bkp_file_ext]);
            tmp=load(bkp_file_ext,'status');
            status=tmp.status;
            restore=strcmp(status,'done');
        end
    end
    if do_backups
        disp([program ' BACKUP STATUS = ' status]);
    end
    if do_backups && restore
        disp([program ': restoring from backup']);
        tmp=load(bkp_file_ext,'npb','np');
        npb=tmp.npb; np=tmp.np;
        % - results of bestgrid only!
    else
        disp([program ': starting a new calculation']);
        debug_bestgrid=1; % default=1
        [npb,np,EHntry,relerror]=bestgrid(@fwm_bestgrid,{[chi0],zd,eground,perm,...
            ksa,coornperp,nperp1,nperp2,In,...
            kia,dzdil,dzdih},...
            npbi,dn0,retol,1,debug_bestgrid);
        % Don't have to improve the grid, miniter==0
        [npb,np,EHntry,relerror]=bestgrid(@fwm_bestgrid,{phitry,zd,eground,perm,...
            ksa,coornperp,nperp1,nperp2,In,...
            kia,dzdil,dzdih},...
            npb,dn0,retol,0,debug_bestgrid);
        if do_backups
            status='done';
            disp(['Saving results of ' program ' into ' bkp_file_ext ' ...']);
            save(bkp_file_ext,'npb','np','EHntry','status','-append');
            disp(' ... done');
        end
    end
end

%% Calculate the field in (nx,ny)-space
if only_last_step
    disp('***** LOAD the field in (nx,ny)-space *****');
    bkp_file_ext=[bkp_file '_FWM_WAVES.mat'];
    if ~exist(bkp_file_ext,'file')
        error(['File ' bkp_file_ext ' does not exist!'])
    end
    tmp=load(bkp_file_ext,'status');
    if ~isfield(tmp,'status')
        error(['File ' bkp_file_ext ' is invalid: no status!'])
    end
    if ~strcmp(tmp.status,'done')
        error(['File ' bkp_file_ext ' is not finished!'])
    end
    arginnames={'zd','eground','perm','dn0','npb','np',...
        'ksa','coornperp','nperp1','nperp2','In','kia','dzdil','dzdih'};
    argoutnames={'EHn','nx','ny','da','Nphi'};
    nin=length(arginnames);
    sgiven=struct();
    for k=1:nin
        eval(['sgiven.' arginnames{k} '=' arginnames{k} ';']);
    end
    sloaded=load(bkp_file_ext,arginnames{:});
    different=false(1,nin);
    for k=1:nin
        argname=arginnames{k};
        if  ~isequalwithequalnans(getfield(sgiven,argname),getfield(sloaded,argname))
            different(k)=true;
        end
    end
    if any(different)
        disp(['The following variables are different in file ' bkp_file_ext ':']);
        disp(arginnames{different})
        for k=1:nin
            if different(k)
                argname=arginnames{k};
                tmpgiven=getfield(sgiven,argname); tmploaded=getfield(sloaded,argname);
                if max(size(tmpgiven))==1 && max(size(tmploaded))==1
                    disp([argname ': given=' num2str(tmpgiven) '; loaded=' num2str(tmploaded)]);
                else
                    disp([argname ': given size=' num2str(size(tmpgiven)) '; loaded size=' num2str(size(tmploaded))]);
                end
            end
        end
        if dn0<sloaded.dn0
            disp('WARNING: results may be inaccurate because dn0 has insufficient precision');
        end
    end
    disp(['Loading precalculated results from ' bkp_file_ext]);
    tmp=load(bkp_file_ext,argoutnames{:});
    for k=1:length(argoutnames)
        eval([argoutnames{k} '=tmp.' argoutnames{k} ';']);
    end
    disp(['Number of (nx,ny) modes = ' num2str(length(nx))]);
    bkp_file=[bkp_file '_' new_last_step]
else
    disp('***** Calculate the field in (nx,ny)-space *****');
    % We discard the value of EHntry
    % Calculate the field at extra altitudes, too (if given)
    if isempty(hi_extra)
        kia_tot = kia;
        dzdil_tot = dzdil;
        dzdih_tot = dzdih;
        Mi_tot = Mi_core;
        hi_tot = hi_core;
        zdi_tot = zdi_core;
    else
        hi_tot = [hi_core hi_extra];
        Mi_tot = length(hi_tot);
        zdi_tot = hi_tot*1e3*k0;
        [kia_tot,dzdil_tot,dzdih_tot]=fwm_get_layers(zd,zdi_tot);
    end
    [EHn,nx,ny,da,Nphi]=fwm_waves(zd,eground,perm,dn0,npb,np,...
        ksa,coornperp,nperp1,nperp2,In,kia_tot,dzdil_tot,dzdih_tot,bkp_file,log2Nphimin);
    % Note: EHn is now forwarded to the next step. We could avoid this
    % but it is not necessary.
end

%%
if nargout>3
    waves_struct=struct('EHn',EHn,'np',np,'npb',npb,'nx',nx,'ny',ny,'da',da,'Nphi',Nphi);
end

%% Inverse Fourier transform from k-space to r-space
disp('***** Sum the modes with appropriate weights (last step) *****');
if calculation_plan.need_gauss1 || (any(calculation_plan.extend) && calculation_plan.need_gauss2)
    nptmp=sqrt(nx.^2+ny.^2);
    nptmp=nptmp(:);
    efactor=exp(-(nptmp.*drdampd).^2/2);
end
if any(calculation_plan.extend)
    if do_exact_if_possible
        if calculation_plan.need_gauss1
            ki=~calculation_plan.extend;
            EHn(:,ki,:)=EHn(:,ki,:).*repmat(shiftdim(efactor,-2),[6 sum(ki) 1]);
        end
        if calculation_plan.need_gauss2
            kie=calculation_plan.extend;
            EHn(:,kie,:)=EHn(:,kie,:).*repmat(shiftdim(efactor,-2),[6 sum(kie) 1]);
        end
    else
        % CORRECTION: To have comparable values at '~extend' and 'extend', we damp '~extend' too
        % if 'extend' has to be damped.
        if calculation_plan.need_gauss2
            EHn=EHn.*repmat(shiftdim(efactor,-2),[6 Mi_tot 1]);
        end
    end
    % NOTE: This is incomplete solution, the waves with np>npmax1 are
    % missing still for ki==ki2.
    EH=fwm_assemble(k0,nx,ny,da,EHn,coorrperp,rperp1,rperp2,bkp_file);
else
    % Finish here
    if calculation_plan.need_gauss1
        EHn=EHn.*repmat(shiftdim(efactor,-2),[6 Mi_tot 1]);
    end
    EH=fwm_assemble(k0,nx,ny,da,EHn,coorrperp,rperp1,rperp2,bkp_file);
    % Size is 6 x Mi_tot x length(rperp1) x length(rperp2)
    npe_struct=[];
    return
end

%% Extend the calculation to higher np for cases that require it
disp('Extending ...');
if coorrperp~=1 || coornperp~=1
    error('not implemented');
end

% Use the axisymmetric calculation for EHn(:,extend,:) at np>npmax1
Me=min([calculation_plan.kion-1,max(ksa)])
zde=zd(1:Me); perme=perm(:,:,1:Me); % only a few layers, to include the source
[kiavac,dzdilvac,dzdihvac]=fwm_get_layers(zde,zdi_core(calculation_plan.extend));
%npebi=[npmax1:dn0:npmax2+dn0].';
% Convert the current to harmonics
Nh=3;
Nhshift=1;
% I0h is (6 x Ms x Nnp0 x Nh) or (6 x Ms x Nh) for point_source
% np0 is (Nnp0) or empty (for point_source); m0 is (Nh)
[I0h,np0,m0]=fwm_harmonics(nx0,ny0,In,Nh,Nhshift);

%% Cycle over harmonics
[xm,ym]=ndgrid(x,y);
rm=sqrt(xm.^2+ym.^2);
phi=atan2(ym,xm); % size is Nx x Ny
kie=calculation_plan.extend;
for kh=1:Nh
    m=m0(kh)
    if point_source
        In=I0h(:,:,kh);
        % FWM_FIELD takes current of size [6 Ms Nnp]
        fun=@(np_arg) permute(fwm_field(zde,eground,perme,ksa,np_arg,0,...
            repmat(In,[1 1 length(np_arg)]),kiavac,dzdilvac,dzdihvac),[3 1 2]);
    else
        I0p=permute(I0h(:,:,:,kh),[3 1 2]); % size=[Nnp0 6 Ms]
        % - move the interpolation dimension to 1st place
        % We managed to squeeze the call into a lambda form
        % Output of fwm_field is EHn (6 x Mi_core x N);
        % BESTGRID takes (N x 6 x Mi_core)
        fun=@(np_arg) permute(fwm_field(zde,eground,perme,ksa,np_arg,0,...
            permute(interp1(np0,I0p,np_arg),[2 3 1]),kiavac,dzdilvac,dzdihvac),[3 1 2]);
    end
    [npeb,npe,EHne0,relerror]=bestgrid(fun,{},...
        [calculation_plan.npmax1;calculation_plan.npmax2+dn0],dn0,retol);
    EHne0=permute(EHne0,[2 3 1]); % (6 x Ms x Nnp0)
    % Hankel transform of the current mode
    if calculation_plan.need_gauss2
        efactor=exp(-(npe.*drdampd).^2/2);
        EHne0=EHne0.*repmat(shiftdim(efactor,-2),[6 sum(kie) 1]);
    end
    %dr=min(min(diff(x)),min(diff(y)))/10;
    drinterp=min([min(diff(x)),min(diff(y))])/2;
    re=(0:drinterp:rmaxkm*1e3);
    EHe0=k0^2*fwm_hankel(npeb,EHne0,re*k0,m); % size==[6 sum(kie) length(re)]
    % Interpolate in cartesian coors
    % size(EH1)==[6 sum(kie) length(rperp1) length(rperp2)]
    EH1=permute(interp1(re,permute(EHe0,[3 1 2]),rm),[3 4 1 2]);
    % Now EH1 needs to be rotated according to its harmonic
    phim = repmat(shiftdim(phi,-2),[2 sum(kie) 1 1]);
    EH1([3 6],:,:,:)=EH1([3 6],:,:,:).*exp(1i*m*phim);
    % Convert A=EH1 to A+- == Ar +- i*Aphi
    tmp=EH1([1 4],:,:,:);
    EH1([1 4],:,:,:)=tmp+1i*EH1([2 5],:,:,:);
    EH1([2 5],:,:,:)=tmp-1i*EH1([2 5],:,:,:);
    % Convert A+- to Ac+- = Ax +- i*Ay
    tmp=EH1([1 4],:,:,:).*exp(1i*(m+1)*phim);
    EH1([2 5],:,:,:)=EH1([2 5],:,:,:).*exp(1i*(m-1)*phim);
    % And, finally, Ac+- to Ax and Ay
    EH1([1 4],:,:,:)=(tmp+EH1([2 5],:,:,:))/2;
    EH1([2 5],:,:,:)=(tmp-EH1([2 5],:,:,:))/2i;
    % Correction
    EH(:,kie,:,:)=EH(:,kie,:,:)+EH1;
    if nargout>2
        npe_struct{kh}=struct('npe',npe,'EHne0',EHne0,'EHe0',EHe0,'re',re);
    end
end
