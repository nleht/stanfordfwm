function [Ruh,Rdl,Ux,Dx,ul,dh]=...
    fwm_algorithm(Pu,Pd,Qcl,Qch,Rbot,Rtop,ksa,Dfc,ul0,dh0)
%FWM_ALGORITHM Full-wave method, most general form.
%
% Radiation by sheet currents at boundaries between layers.
% Description: Lehtinen and Inan [2008], doi:10.1029/2007JA012911
% DOI link: http://dx.doi.org/10.1029/2007JA012911
%
% -----
% Usage
% -----
% For wave generation by current sources:
%   1. Find the jump in the perpendicular field Dfc using one of the two methods:
%    - by the use of FWM_DEH:
%        Dfc=fwm_deh(I,permute(perm(:,3,ksa),[1 3 2]),nx,ny)
%    - by multiplication by Bext (more general, preferred, the only version
%      valid for bianisotropic case):
%        [~,~,~,~,~,Bext] = fwm_booker('cold_bianisotropic',perm(:,:,ksa),nx,ny); 
%        Dfc2=Bext*I
%   2. Radiated field amplitudes are found by 
%        [ul,dh] = fwm_algorithm(Pu,Pd,Qcl,Qch,Rbot,Rtop,ksa,Dfc);
% For wave propagation:
%   [ul,dh] = fwm_algorithm(Pu,Pd,Qcl,Qch,Rbot,Rtop,[],[],ul0,dh0);
% After that, find "ud" vector at any point using FWM_INTERMEDIATE, and
% convert to E,H fields by "Fext" matrix.
% Advanced usage for wave propagation (gives same result as above):
%    [Ruh,Rdl,Ux,Dx] = fwm_algorithm(Pu,Pd,Qcl,Qch,Rbot,Rtop);
%    ul=zeros(2,M); dh=zeros(2,M-1);
%    % For propagation up:
%    ul(:,1)=ul0; % [1;0] for TE wave; [0;1] for TM wave
%    for k=1:M-1
%       uh=Pu(:,:,k)*ul(:,k);
%       ul(:,k+1)=Ux(:,:,k)*uh;
%       dh(:,k)=Ruh(:,:,k)*uh;
%    end
%    % Or, for propagation down:
%    dli=[1;0]; % whistler wave
%    ul(:,M)=Rdl(:,:,M)*dli;
%    dl=dli;
%    for k=M-1:-1:1
%       dh(:,k)=Dx(:,:,k)*dl; % dh in layer k
%       dl=Pd(:,:,k)*dh(:,k); % dl in layer k
%       ul(:,k)=Rdl(:,:,k)*dl;
%    end
% To calculate the fields (for non-cylindrical case):
%    [ud,kia]=fwm_interpolate(k0*zi,ul,dh,nz,k0*z);
%    EH=zeros(6,Mi);
%    for ki=1:Mi
%        k=kia(ki);
%        EH(:,ki)=Q(:,:,k)*ud(:,ki);
%    end

%
% ------------------
% Inputs and outputs
% ------------------
% Variable dimensions:
%    M - number of layers, number of boundaries==M+1 (outer two have no
%        through transport)
% The index corresponding to the dimension of size {M|M-1} is denoted k,
% and to dimension of size Ms is denoted ks.
% Input:
%    P{u|d} (2 x 2 x M) - {up|down} propagators inside layer k
%    nz (4 x M) - vertical refractive index in layer k, for 4 modes sorted
%       by decreasing imaginary part, so that the first two values
%       correspond to upward modes u=[u(1);u(2)] and the second two to
%       downward modes d=[d(1);d(2)]
%    Qc{l|h} (4 x 4 x M) - mode structure at {lower|higher} boundary of
%       layer k, i.e. the matrix to convert
%       the wave variables ud=[u(1);u(2);d(1);d(2)] which correspond to nz
%       described above, to fields EH=[Ex;Ey;Hx;Hy]
%    Rbot,Rtop - boundary conditions at z(1) and z(M+1) given as 2x2
%       reflection coefficients (e.g. computed by FWM_RGROUND).
% nz and Qc can correspond, e.g., to a wave with a horizontal wave vector
% (kx,ky)=k0*(nx,ny) (fixed due to Snell's law). Then nz and Q are found
% by solving the Booker equation with FWM_BOOKER. Qc is a submatrix
% Qc=Q([1:2 4:5],:) of Q returned by FWM_BOOKER.
% Optional inputs (the sources):
%    ksa (Ms) - layer numbers k=ksa(ks) corresponding to the sources
%    Dfc (Mc x Ms) - sources [DEx;DEy;DHx;DHy] at the boundaries between
%       layers; the sources are assumed to be surface currents flowing in
%       the plane just above boundary z(k), in layer k==layers(ks). These
%       are found from given surface currents by using FWM_DEH.
% Optional inputs (initial waves at lower and higher boundary):
%    {u|d}{l|h}0 (2 x 1) - {up|down} wave just {above|below} {z(1)|z(M+1)},
%       default=[0;0] (given also by an empty matrix)
%    Note that the {lower|upper} boundary condition is ignored for the
%    field due to {u|d}{l|h}i. If there are also nonzero sources, this leads to
%    a mixup in the boundary conditions -- avoid such situations.
% Outputs:
%    {U|D}x (2 x 2 x M-1) - {up|down} propagators across boundary z(k+1)
%    R{u|d}{l|h} (2 x 2 x M) - reflection coefficient for {up|down}
%       mode at the {low|high} boundary of layer k
%    {u|d}{l|h} (2 x M) - {up|down} mode amplitudes at {low|high}
%       boundaries of layer k
% We consider {low|high} boundaries of layers,
% which is indicated by letter {l|h} in the notation.
%
% IMPORTANT NOTES:
%    1. For complex amplitudes, we use the physics convention:
%       E,H ~ e^{-iwt}
%    2. We use the Budden normalized magnetic field H=Z0*H_SI, where
%       Z0=sqrt(mu0/eps0) is the impedance of free space.
%
% See also: FWM_INTERMEDIATE_BIDIRECTIONAL, FWM_BOOKER.
% Author: Nikolai G. Lehtinen

%% Default inputs
if nargin<10
    dh0=[];
end
if nargin<9
    ul0=[];
end
if nargin<8
    Dfc=[]; ksa=[];
end
if nargin<6
    Rtop=[];
end
if nargin<5
    Rbot=[];
end
if nargin<4
    error('not enough arguments')
end
if isempty(Qch)
    Qch=Qcl; % usual situation for uniform media, but not true in cylindrical
end

%% Relevant dimensions, check input sizes
[tmp,dummy,M]=size(Pu);
Mc=tmp*2;
if (dummy~=Mc/2 || ~all(size(Pd)==[Mc/2 Mc/2 M]) || ~all(size(Qcl)==[Mc Mc M]) || ~all(size(Qch)==[Mc Mc M]))
    error('wrong sizes of Pu, Pd, Qcl, Qch')
end
if ~isempty(Dfc)
    Ms=length(ksa);
    if ~all(size(Dfc)==[Mc Ms])
        error('wrong sizes of ksa, Dfc');
    end
end

%% Determine what we need to calculate
do_propagation_up = ~isempty(Rtop);
do_propagation_down = ~isempty(Rbot);
only_matrices=(nargout<=4);

if only_matrices
    ksa=[]; Dfc=[];
end

if ~only_matrices && isempty(Dfc)
    error('Dfc is not given');
end

%% The matrix for transporting w=(u,d) thru a boundary between k-th and k+1-th layers, k=1:M-1
% NOTE: there is no transport thru upper boundary of layer M or lower
% boundary of layer 1.
Tu=zeros(Mc,Mc,M-1); Td=zeros(Mc,Mc,M-1);
for k=1:M-1
    % Up (from layer k to layer k+1)
    Tu(:,:,k)=Qcl(:,:,k+1)\Qch(:,:,k);
    if any(isnan(Tu(:,:,k)))
        error(['k=' num2str(k) ': Tu']);
    end
    % Down (just an inverse)
    Td(:,:,k)=Qch(:,:,k)\Qcl(:,:,k+1);
    if any(isnan(Td(:,:,k)))
        zd(k)
        nz(:,k)
        Qcl(:,:,k)
        error(['k=' num2str(k) ': Td']);
    end
end

%% Reflection coefficients
% Reflection from above, in layer k, at the lower or higher boundary.
iup=[1:Mc/2]; ido=[Mc/2+1:Mc];
if do_propagation_up
    Rul=zeros(Mc/2,Mc/2,M);
    Ruh=zeros(Mc/2,Mc/2,M);
    for k=M:-1:1
        if k==M
            Ruh(:,:,k)=Rtop; % zeros(2,2) for no reflection
        else
            Ruh(:,:,k)=(Td(ido,iup,k)+Td(ido,ido,k)*Rul(:,:,k+1))/ ...
                (Td(iup,iup,k)+Td(iup,ido,k)*Rul(:,:,k+1));
        end
        Rul(:,:,k)=Pd(:,:,k)*Ruh(:,:,k)*Pu(:,:,k);
    end
else
    Ruh=[];
end
% Reflection from below
if do_propagation_down
    Rdl=zeros(Mc/2,Mc/2,M);
    Rdh=zeros(Mc/2,Mc/2,M);
    for k=1:M
        if k==1
            Rdl(:,:,k)=Rbot;
        else
            Rdl(:,:,k)=(Tu(iup,iup,k-1)*Rdh(:,:,k-1)+Tu(iup,ido,k-1))/ ...
                (Tu(ido,iup,k-1)*Rdh(:,:,k-1)+Tu(ido,ido,k-1));
        end
        Rdh(:,:,k)=Pu(:,:,k)*Rdl(:,:,k)*Pd(:,:,k);
    end
else
    Rdl=[];
end

%% Matrices for propagation
% NOTE: {U|D}(:,:,k) correspond to crossing boundary between layers k and k+1
if do_propagation_up
    Ux=zeros(Mc/2,Mc/2,M-1);
    for k=1:M-1
        Ux(:,:,k)=Tu(iup,iup,k)+Tu(iup,ido,k)*Ruh(:,:,k);
        if any(isnan(Ux(:,:,k)))
            error(['k=' num2str(k) ': Ux']);
        end
    end
else
    Ux=[];
end
if do_propagation_down
    Dx=zeros(Mc/2,Mc/2,M-1);
    for k=1:M-1
        Dx(:,:,k)=Td(ido,iup,k)*Rdl(:,:,k+1)+Td(ido,ido,k);
        if any(isnan(Dx(:,:,k)))
            error(['k=' num2str(k) ': B']);
        end
    end
else
    Dx=[];
end

%% Return the matrices, if no sources are given
if only_matrices
    % We only calculate propagation, no radiation
    return;
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Second part: The wave propagation

if isempty(ul0)
    ul0=[0;0];
end
if isempty(dh0)
    dh0=[0;0];
end

%% If the sources are given, calculate the radiation
% All sources are just above the lower boundary of a layer number ksa.
Ms=length(ksa); 
% Cycle over sources at the boundaries
% This cycle is usually fast because Ms << M
% u_+ and d_-
upb=zeros(Mc/2,M); dmb=zeros(Mc/2,M);
for ks=1:Ms
    k=ksa(ks); % Number of the containing layer
    Dw=Qcl(:,:,k)\Dfc(:,ks); % change in the amplitude
    % Rd and Ru at the source altitude
    rd=Rdl(:,:,k);
    ru=Rul(:,:,k);
    % Solution for u_+ and d_- due to source kb, at the altitude of the
    % source.
    % "+" stands for above the source, "-" for below the source
    % Both are just above the lower boundary of layer k
    tmp=(eye(Mc/2)-rd*ru)\(Dw(iup)-rd*Dw(ido));
    upb(:,k)=tmp;
    dmb(:,k)=ru*tmp-Dw(ido);
end

%% Propagate up: find "us", up-wave due only to sources below
% (i.e., no reflected waves coming from sources above), at altitudes z(k)
% NOTE: sources are at the lower boundaries of layers
% NOTE: usl, dsl are calculated at the lower boundary, just above the source
usl=zeros(Mc/2,M); ush=zeros(Mc/2,M);
if do_propagation_up
    for k=1:M
        if k==1
            usl(:,k)=upb(:,k)+ul0; % Include sources at z(1)==0, + initial wave
        else
            % Go across the boundary, and add sources at the boundary
            usl(:,k)=Ux(:,:,k-1)*ush(:,k-1)+upb(:,k);
        end
        % Note: usl(:,k), ush(:,k) include also the sources at the lower
        % boundary of layer k
        ush(:,k)=Pu(:,:,k)*usl(:,k);
    end
end

%% Propagate down: find "ds", down-wave due only to sources above
% (no reflected waves coming from sources below).
dsl=zeros(Mc/2,M); dsh=zeros(Mc/2,M);
if do_propagation_down
    for k=M:-1:1
        if k==M
            dsh(:,M)=dh0; % There are no sources above this point
        else
            % Add the sources in layer k+1 (including at boundary z(k+1)),
            % and go across the boundary
            dsh(:,k)=Dx(:,:,k)*(dsl(:,k+1)+dmb(:,k+1));
        end
        % Note: dsl(:,k) only includes sources from layers >=k+1
        dsl(:,k)=Pd(:,:,k)*dsh(:,k);
    end
end

%% Include all the remaining sources by including the reflected waves
ul=zeros(Mc/2,M); dh=zeros(Mc/2,M);
for k=1:M
    ul(:,k)=usl(:,k)+Rdl(:,:,k)*dsl(:,k);
    dh(:,k)=dsh(:,k)+Ruh(:,:,k)*ush(:,k);
end
