function npb=fwm_initial_npb(npmax,dn0,dense)
%FWM_INITIAL_NPB Find the initial grid for nperp
% Usage:
%     npb=fwm_initial_npb(npmax,dn0[,dense])
% The initial trial grid for nperp is such that theta is equally spaced.
% theta is the angle of incidence (nperp==sin(theta))
% dn0 gives an estimate for the minimum diff(npb).
%
% The theta contour is the one that gives expansion of a dipole field in
% plane waves at theta.
% theta is from 0 to pi/2, then from pi/2 to pi/2-i*inf
%
% See, e.g., Banos "Dipole radiation ...", 1966, or Budden, "The wave-guide
% mode theory of wave propagation", 1961
%
% Author: Nikolai G. Lehtinen
if nargin<3
    dense=0;
end
if npmax>1
    a=acosh(npmax); % theta goes to pi/2-i*a
    N1=ceil(pi/2/dn0); % Number of real thetas
    dth1=(pi/2)/N1; % <~ dn0
    N2=ceil(a/dth1);
    dth2=a/N2;
    % Boundary theta points:
    thb=[(0:N1)*dth1 pi/2-1i*(1:N2)*dth2];
else
    thmax=asin(npmax);
    N=ceil(thmax/dn0);
    dth0=thmax/N;
    thb=(0:N)*dth0;
end
% Boundary np points (initial):
npb=real(sin(thb));
if dense
    % Find where dnp>dn0 and replace with dn0
    dnp=diff(npb);
    ii=find(dnp>dn0,1);
    if ~isempty(ii)
        N3=ceil((npb(end)-npb(ii))/dn0);
        npbadd=linspace(npb(ii),npb(end),N3+1);
        npb=[npb(1:ii) npbadd(2:end)];
    end
end
