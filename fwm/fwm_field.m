function EHn=fwm_field(zd,eground,perm,ksa,nx,ny,In,kia,dzdil,dzdih,do_source)
%FWM_FIELD Calculate E, H for given (nx,ny) and currents
% This is an older (but working) version which relies on FWM_DEH,
% FWM_BOOKER_BIANISOTROPIC, FWM_RADIATION and FWM_INTERMEDIATE, also uses
% FWM_RGROUND and assumes TE/TM waves in isotropic media. The new more
% general version (valid in either curved or warm media) is FWM_STRATIFIED
% (which is currently under development, DO NOT USE IT!!!).
% Usage:
%    EHn=fwm_field(...
%        zd,eground,perm,ksa,nx,ny,In,kia,dzdil,dzdih);
% Inputs:
%    zd (M) - dimensionless altitudes (zd==z*k0, k0==w/c)
%    eground - ground permittivity (scalar) or boundary condition (string),
%       chosen from 'E=0','H=0' or 'free'
%    perm (3 x 3 x M) - dielectric permittivity tensor in each layer
%    nx (N), ny (N or scalar) - horizontal refractive index (=k/k0);
%       if ny is scalar, it is automatically extended to the same length as
%       nx
%    ksa (Ms) - indeces k=ksa(ks) of layers with source currents
%       The layer currents flow in the surface just above the bottom
%       boundary of the layer.
%    In (6 x Ms x N) - surface electric and magnetic currents for
%       fixed (nx,ny), in Budden units V*m (see note 2).
%    kia (Mi), dzdi{l|h} (Mi) - output layers and distances from the
%       output altitudes to {lower|upper} boundary of the containing layer.
%       This is an output of FWM_GET_LAYERS. dzdi{l|h} are also
%       dimensionless (multiplied by k0=w/c). If not given, the default is
%       only two output altitudes: ground zd(1) and satellite zd(M).
% Outputs:
%    EHn (6 x Mi x N) - the E, H field at output altitudes (Fourier
%       components for each (nx,ny)), in V*m. If output is required in V/m,
%       then the input current In must also be given in V/m.
% Notes (IMPORTANT!):
% 1. We use "physics" convention for complex values E,H~exp(-i*w*t)
% 2. Magnetic field H in EH is given in V/m [Budden, 1985], so that
%    H_SI=H/impedance0, where impedance0=sqrt(mu0/eps0) is the impedance
%    of free space. The currents also are in the same units, i.e. electric
%    currents are Ie=impedance0*Ie_SI and magnetic currents Im=Im_SI
%    (in Weber convention).
%    See also Full_Wave_Method_for_Stratified_Media.pdf in "doc" folder.
%    The currents in (x,y) space are given as surface currents flowing
%    between layers, so in SI their units are A/m. This unit also applies
%    to vertical currents which are given as vertical current moment per
%    unit area. After conversion to Budden units, we get V/m.
%    In (kx, ky) or (nx,ny) space, the currents In are the horizontal
%    Fourier transform (HFT) of I, so the SI unit of In is A*m
%    (which is converted to V*m in the Budden units):
%        In(nx,ny) = Integral[I(x,y)*exp(-i*k0*(nx*x+ny*y), {x, y}]
%    The point sources are given as
%        I(x,y) = IL*delta(x,y)
%    where IL is the current moment (in A*m). After HFT, we get
%        In(nx,ny) = const = IL
%    The routines which take the currents as an input, usually take In
%    (fwm_axisymmetric, fwm_nonaxisymmetric etc). Thus, it must be given
%    in Budden units of V*m. The output of these routines is EH in (x,y)
%    space in Budden units of V/m. The conversion V*m -> V/m is done
%    by the inverse HFT (IHFT):
%        EH(x,y) = (k0/(2*pi))^2*
%             Integral[EHn(nx,ny)*exp(i*k0*(nx*x+ny*y), {nx, ny}]
% 3. At h(1)==0, there must be vacuum, perm(:,:,1)==eye(3), for correct
%    application of the boundary condition at h==0 (except 'free').
% 4. If the incoming {downward|upward} wave is given at the {upper|lower}
%    boundary, the best way to solve this problem is to represent the field
%    as surface currents at that boundary:
%       Ie = -(H0 x p)
%       Im =  (E0 x p)
%    where p={-z|z} is the unit {downward|upward} vector. The boundary
%    condition at that boundary must be 'free' (which is the only condition
%    available at the upper boundary). This conversion is easily done by a
%    matrix
%       EH_to_I=[0 0 0 -1;0 0 1 0;0 1 0 0;-1 0 0 0];
%    and can be implemented in the following way for a given field
%    EHn{d|u}0 (array of size 4 x N):
%       Ms=1;
%       ksa=[{M|1}];
%       for ip=1:N % cycle over Fourier components
%          In([1:2 4:5],1,ip)={-|}EH_to_I*EHn{d|u}0(:,ip);
%       end
% See also: FWM_BOOKER_BIANISOTROPIC, FWM_RADIATION, FWM_DEH, FWM_GET_LAYERS,
%    FWM_INTERMEDIATE
% Author: Nikolai G. Lehtinen

if nargin<11
    do_source=1;
end
M=length(zd);
global output_interval
if isempty(output_interval)
    output_interval=20;
end
Ms=length(ksa); N=length(nx);
if length(ny)==1
    ny=ny*ones(size(nx));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Full-Wave Model starts here and consists of 4 parts
% In wavenumber space, instead of iterating over nx and ny separately,
% i.e., ix=1:Nx and iy=1:Ny, we iterate over all modes, kt=1:Ntot

% 1. Sources (Delta E, Delta H) in Fourier space
% This step is fast, no loading is necessary
%disp('1. Sources (Delta E, Delta H) in Fourier space');
% Only on the positive nx axis

[is3a,is3b,isM]=size(perm);
if is3a~=3 || is3b~=3 || isM~=M
	error('incorrect size of perm')
end
eiz=permute(perm(:,3,ksa),[1 3 2]);
% In must be (6 x Ms x N)
DEHn=fwm_deh(In,eiz,nx,ny);
% DEHn now has the size 4 x Ms x N

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Refractive index and mode structure in each layer
% Calculate nz and Fext for phi==0 only
nz=zeros(4,M,N); Fext=zeros(6,4,M,N);
parfor k=1:M
    [nz(:,k,:),Fext(:,:,k,:)]=fwm_booker_bianisotropic(perm(:,:,k),nx,ny);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. FWM solved for given DEH
Rground=fwm_Rground(eground,nx,ny);
ul=zeros(2,M,N); dh=zeros(2,M-1,N);
F=Fext([1:2 4:5],:,:,:);
parfor ip=1:N
    [ul(:,:,ip),dh(:,:,ip)]=fwm_radiation(zd,nz(:,:,ip),...
        F(:,:,:,ip),Rground(:,:,ip),ksa,DEHn(:,:,ip));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4. Find the waves and fields at intermediate points
% This step is fast
% Output altitudes
if nargin<8
    % We only have 2 points:
    % z=z(1)=0, layer 1, so dz_low=0, dz_high=z(2),
    % and z=z(M), layer M, so dz_low=0, dz_high=nan.
    kia=[1 M]; dzdil=[0 0]; dzdih=[zd(2) nan];
end
Mi=length(kia);
ud=fwm_intermediate(nz,ul,dh,kia,dzdil,dzdih); % 4 x Mi x N
% For a correction to the vertical field due to vertical currents
dzdinv=[1./diff(zd(:));0];
Izk=zeros(2,M,N);
for k=1:M
    Izk(:,k,:)=sum(In([3 6],ksa==k,:),2);
end
EHn=zeros(6,Mi,N);
perm33kia=permute(perm(3,3,kia),[3 1 2]);
Izkkia=Izk(:,kia,:);
dzdinvkia=dzdinv(kia); % Mi x 1
parfor ip=1:N
    Fexttmp=Fext(:,:,:,ip); % 6 x 4 x M
    udtmp=ud(:,:,ip); % 4 x Mi
    Iztmp=permute(Izkkia(:,:,ip),[2 1 3]);
    % Multiple matrix-vector product
    EHntmp=permute(sum(Fexttmp(:,:,kia).*repmat(shiftdim(udtmp,-1),[6 1 1]),2),[1 3 2]);
    if do_source
        EHntmp(3,:)=EHntmp(3,:)+shiftdim(Iztmp(:,1).*dzdinvkia./(1i*perm33kia),-1);
        EHntmp(6,:)=EHntmp(6,:)+shiftdim(Iztmp(:,2).*dzdinvkia./1i,-1);
    end
    EHn(:,:,ip)=EHntmp;
end
