function [kappa,Ql,Qh,Pu,Pd,cyl_info]=...
    cyl_booker_bianisotropic(medium_type,Kinfo,m,beta,r)
%CYL_BOOKER_BIANISOTROPIC Solve Booker equation in a cylindrical system
%
% Find a solution to Maxwell's equations in cylindrical coordinates
% (r,phi,z). So far we implemented a bi-anisotropic medium which is uniform
% (and axially-symmetric as a requirement for FWM applicability).
%
% Usage:
%   [kappa,Flext,Fhext,Pu,Pd,cyl_info]=...
%       cyl_booker_bianisotropic(medium_type,Kinfo,m,beta,r);
%   % ... Apply FWM (same algorith as in planaer case) to get ul, dh
%   % (see FWM_RADIATION for guidance)
%   EH=cyl_interpolate(r,ul,dh,ri,kappa,m,cyl_info);
%
% Inputs:
%   (M used below is the number of layers)
%   medium_type = 'uniform' (1st case, gyrotropic) or 'nonuniform'
%     (2nd case, still cylindrically symmetric, but must have m==0, this
%     case is not implemented yet);
%   Kinfo - constitutive tensor information, with structure depending on
%     medium_type (see below).
%   m - scalar integer>0, axial harmonic number (solutions are ~exp(i*m*phi) )
%     analogous to n_phi*r. Note that n_phi->0 as r->inf, which determines
%     the dispersion relation (value of kappa)
%   beta - scalar, wave number along z (solutions are ~exp(i*beta*z) ),
%     dimensionless, analogous to n_z
%   r - 1D array of length M+1, dimensionless, scaled as r=r_real*k0, gives
%     the boundaries between layers: r(k) and r(k+1) the lower and upper
%     boundary of layer k.
%
% Kinfo contents:
%
% For medium_type=='uniform':
%   Kinfo.S, Kinfo.P, Kinfo.D - either 1D arrays of length M or 2 x 2 x M
%   arrays which determine the constitutive tensor of a medium which is
%   both axially symmetric and uniform.
%   The constitutive tensor is
%         ( epsilon  xi)
%         (   eta    mu)
%   Each of epsilon,xi,eta,mu have the shape
%               (  epsilonS   -i*epsilonD      0    )
%     epsilon = ( i*epsilonD    epsilonS       0    )
%               (     0            0       epsilonP )
%   the elements of which are stored in
%     Kinfo.S(1,1,k)=epsilonS(k), Kinfo.D(1,1,k)=epsilonD(k), Kinfo.P(1,1,k)=epsilonP(k)
%   etc., i.e.
%     Kinfo.S(:,:,k) = ( epsilonS(k) xiS(k) )
%                      (   etaS(k)   muS(k) )
%   If Kinfo.S,Kinfo.P,Kinfo.D are 1D arrays, we assume
%     xi==zeros(3,3), eta==zeros(3,3), mu==eye(3)
%   i.e., regular magnetized plasma.
%
% For medium_type=='nonuniform':
%   Kinfo is a 3 x 3 x 3 or 6 x 6 x M array, its treatment is not
%     implemented yet. 

% Outputs:
%   kappa - 2 x M array of "perpendicular wave numbers", i.e., solutions
%     are Bessel functions Z(kappa*r), analogous to n_r
%   F{l|h}ext - 6 x 4 x M array, the matrix which converts amplitudes to
%     field components (E_r,E_phi,E_z,H_r,H_phi,H_z). Unlike the planar
%     case, there are two such matrices, one on the bottom and one on the
%     top of a layer k, given by F{l|h}ext(:,:,k).
%   Pu, Pd - 2 x 2 x M arrays, the propagation matrices for upward and
%     downward waves.
%   cyl_info - a structure with internal information, needed for
%     CYL_INTERMEDIATE
%
% For more detail, see report "cyl_fwm.pdf" (in the same folder)
% Author: Nikolai G. Lehtinen
% See also: fwm_booker_bianisotropic, cyl_intermediate

%% Initial checks
% Splasma etc are 2 x 2 x M, if not, expand to this shape
if (length(beta)~=1 || length(m)~=1)
    error('beta, m must be scalars')
end
if m<0
    error('m<0')
end
if strcmp(medium_type,'nonuniform')
    if m>0
        error('Must have m==0');
    end
    error('not implemented')
end

%% Now we have medium_type=='uniform', check inputs
Splasma=Kinfo.S; Dplasma=Kinfo.D; Pplasma=Kinfo.P;
if ismatrix(Splasma) && (size(Splasma,1)==1 || size(Splasma,2)==1)
    % Expand to 2 x 2
    M=length(Splasma);
    Stmp=zeros(2,2,M); Dtmp=Stmp; Ptmp=Stmp;
    Stmp(1,1,:)=Splasma; Stmp(2,2,:)=1;
    Dtmp(1,1,:)=Dplasma; Ptmp(1,1,:)=Pplasma; Ptmp(2,2,:)=1;
    Splasma=Stmp; Dplasma=Dtmp; Pplasma=Ptmp;
else
    M=size(Splasma,3);
end
if ~(all(size(Splasma)==[2 2 M]) && all(size(Dplasma)==[2 2 M]) && all(size(Pplasma)==[2 2 M]))
    error('wrong size of S, D, P')
end
if length(r)~=M+1
    error('wrong size r');
end
r=r(:).'; % use a row

%% Some matrices which we can calculate outside a cycle
% Pauli matrices, for future use
spauli2=[0 -1i; 1i 0]; spauli3=[1 0;0 -1];
Rplasma=Splasma+Dplasma;
Lplasma=Splasma-Dplasma;
% A shortcut to avoid for-loop to calculate det for an array of matrices
det3 = @(mat_arg) cellfun(@det,num2cell(mat_arg,[1 2]));
detR=squeeze(det3(Rplasma)-1i*beta*(Rplasma(1,2,:)-Rplasma(2,1,:)))-beta^2;
detL=squeeze(det3(Lplasma)+1i*beta*(Lplasma(1,2,:)-Lplasma(2,1,:)))-beta^2;
detS=squeeze(det3(Splasma));
% Matrix [xi_R mu_R; epsilon_R eta_R]
tmp=reshape(Rplasma,[4 M]);
Rrot=reshape(tmp([3 1 4 2],:),[2 2 M]);
% Matrix [xi_L mu_L; epsilon_L eta_L]
tmp=reshape(Lplasma,[4 M]);
Lrot=reshape(tmp([3 1 4 2],:),[2 2 M]);
% (B+iC), which is to be multiplied by Z_{m-1}
%tmp=1i*beta*repmat(spauli3,[1 1 M]);
%Gp=repmat([-1 0; -1i 0; 0 1; 0 1i]*((Rrot-tmp)./repmat(shift(detR,-2),[2 2 1]));
% (B-iC), which is to be multiplied by Z_{m-1}
%Gm=[-1 0; 1i 0; 0 1; 0 -1i]*((Lrot+tmp)./repmat(shift(detL,-2),[2 2 1]));

%% Things we can determine without solving eigenvalue equation
GpVk=zeros(4,2,M); GmVk=GpVk;
kappa=zeros(2,M); V=zeros(2,2,M);
for k=1:M % Cycle over layers
    Mmat=kron(Splasma(:,:,k),eye(2))+kron(Dplasma(:,:,k),spauli2)+...
        beta*kron(spauli2,spauli2);
    Amatp=detR(k)*detL(k)*inv(Mmat);
    Rmat=Amatp([2 4],[2 4]).'*Pplasma(:,:,k)/detS(k);
    % (B+iC), which is to be multiplied by Z_{m-1}
    Gp=[-1 0; -1i 0; 0 1; 0 1i]*(Rrot(:,:,k)-1i*beta*spauli3)/detR(k);
    % (B-iC), which is to be multiplied by Z_{m-1}
    Gm=[-1 0; 1i 0; 0 1; 0 -1i]*(Lrot(:,:,k)+1i*beta*spauli3)/detL(k);
    % Eigenvalues/vectors
    [V1,d]=eig(Rmat);
    kappa1=sqrt(diag(d)); % no need to sort
    GmVk1=Gm*V1*diag(kappa1); GpVk1=Gp*V1*diag(kappa1);
    % Save the stuff we will need later
    V(:,:,k)=V1;
    kappa(:,k)=kappa1;
    GmVk(:,:,k)=GmVk1; GpVk(:,:,k)=GpVk1;
end

%% We choose a Bessel function on the basis of smallness of its argument
threshold_z=sqrt(m+1); % somewhat arbitrary
small_z=(abs(kappa.*repmat(r(1:M),[2 1]))<threshold_z);

%% Scaled Bessel solutions
% At small z
besselzu_small_z=@(m_arg,z_arg) bessely(m_arg,z_arg).*(z_arg/2).^m/factorial(m);
besselzd_small_z=@(m_arg,z_arg) besselj(m_arg,z_arg)./(z_arg/2).^m*factorial(m);
% At large z
besselzu_large_z=@(m_arg,z_arg) besselh(m_arg,1,z_arg,1);
besselzd_large_z=@(m_arg,z_arg) besselh(m_arg,2,z_arg,1);
% Columns of length 2
besselzu=@(m_arg,z_arg,k_arg) besselzu_small_z(m_arg,z_arg).*small_z(:,k_arg)+...
    besselzu_large_z(m_arg,z_arg).*(~small_z(:,k_arg));
besselzd=@(m_arg,z_arg,k_arg) besselzd_small_z(m_arg,z_arg).*small_z(:,k_arg)+...
    besselzd_large_z(m_arg,z_arg).*(~small_z(:,k_arg));
besselzmat=@(m_arg,z_arg,k_arg) [diag(besselzu(m_arg,z_arg,k_arg)) diag(besselzd(m_arg,z_arg,k_arg))];

%% The field matrix
Ql=zeros(6,4,M); Qh=Ql;
Pu=zeros(2,2,M); Pd=Pu;
dr=diff(r);
for k=1:M
    kappa1=kappa(:,k); V1=V(:,:,k);
    GmVk1=GmVk(:,:,k); GpVk1=GpVk(:,:,k);
    Pu(:,:,k)=diag((r(k)/r(k+1))^m*small_z(:,k)+exp(1i*kappa1*dr(k)).*(~small_z(:,k)));
    Pd(:,:,k)=Pu(:,:,k);
    for dk=0:1
        % dk==0 => bottom of layer, calculate Ql (except at zero)
        % dk==1 => top of layer, calculate Qh
        if r(k+dk)>0
            z=kappa1*r(k+dk);
            Qz=V1*besselzmat(m,z,k);
            Qp=0.5*(GpVk1*besselzmat(m-1,z,k)+GmVk1*besselzmat(m+1,z,k));
            % Rearrange Fp,Fz to get Fext(phi,z,r)
            tmp=[Qp;Qz];
            Q1=tmp([2 5 1 4 6 3],:);
            switch dk
                case 0
                    Ql(:,:,k)=Q1;
                case 1
                    Qh(:,:,k)=Q1;
            end
        end
    end
end
cyl_info=struct('V',V,'GpVk',GpVk,'GmVk',GmVk,'small_z',small_z); % internal information

