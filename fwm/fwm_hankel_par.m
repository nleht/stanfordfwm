function EH0=fwm_hankel(npb,EHf0,k0r,m)
%FWM_HANKEL Do the Hankel transform to get E, H as a function of radius r
% Usage:
%    EH0=k0^2*fwm_hankel(npb,EHf0,k0*rp[,m]);
% Notations:
%    k0=w/c - vacuum wave number
%    kx=kp*cos(chi), ky=kp*sin(chi) - horizontal components of wavevector
%    vector n = vector k/k0 - refractive index vector
%    kp, np=kp/k0 - transverse wave number and refractive index
%    x=rp*cos(phi), y=rp*sin(phi) - horizontal coordinates
%    rp (in meters) - transverse distance
% Inputs:
%    EHf0 (6 x Mi x N) - Fourier transform of E, H, inside regions bounded
%       by npb, on the positive nx axis (i.e., for chi==0);
%    npb (N+1) - boundaries of np grid
%    k0*rp (Nr) - dimensionless transverse distance grid (for output)
%    m (default=0) - axial mode number: i.e., fields (polar components)
%       (E_rp,E_phi,E_z)(x,y,z)~exp(i*phi*m) and
%       (E_rp,E_phi,E_z)(kx,ky,z)~exp(i*chi*m)
% Outputs:
%    EH0 (6 x Mi x Nr) - E, H as a function of rp, on the positive x-axis
%    (i.e., for phi==0).
% Notes:
% 1. E, H ~ exp(i*(k.r)) (physics convention)
% 2. H is in Budden's units (V/m)
% 3. Relation between Cartesian {E|H}_{x|y} and polar {E|H}_{rp|phi}
%    components:
%       {E|H}_x(rp,phi) = {E|H}_rp(rp,phi)*cos(phi) - {E|H}_phi(rp,phi)*sin(phi)
%       {E|H}_y(rp,phi) = {E|H}_rp(rp,phi)*sin(phi) + {E|H}_phi(rp,phi)*cos(phi)
%    It is convenient to use linear combinations (chiral components)
%       {E|H}_{+|-} = {E|H}_x +- i*{E|H}_y (chiral Cartesian components)
%       {E|H}_p{+|-} = {E|H}_rp +- i*{E|H}_phi (chiral polar components)
%    Then the relation is simply
%       {E|H}_{+|-}(rp,phi)={E|H}_p{+|-}(rp,phi)*exp(+-i*phi)
%    Assume polar components have ~exp(i*m*phi) dependence, i.e.,
%       {E|H}_{rp|phi}(rp,phi)={E|H}0_{rp|phi}(rp)*exp(i*m*phi),
%       {E|H}_p{+|-}(rp,phi)={E|H}0_{+|-}(rp)*exp(i*m*phi),
%    where we have introduced
%       {E|H}0_{+|-}(rp)={E|H}0_rp(rp) +- i*{E|H}0_phi(rp)
%    so we get
%       {E|H}_{+|-}(rp,phi)={E|H}0_{+|-}(rp)*exp[i*(m+-1)*phi]
%    Transformation from chiral to Cartesian components is
%       {E|H}_x=({E|H}_+ + {E|H}_-)/2;
%       {E|H}_y=({E|H}_+ - {E|H}_-)/(2*i);
%    {E|H}_z does not require any transformations. Analogous formulas are
%    also applicable in kp-space.
% 4. Here is how we use the relations outlined above in this function:
%       EH0=fwm_hankel(npb,EHf0,k0*rp,m);
%    EHf0 has 6 components specified on the positive kx-axis:
%       E0_rp(kp),E0_phi(kp),E0_z(kp),H0_rp(kp),H0_phi(kp),H0_z(kp)
%    In (kx,ky)-space, we have
%       {E|H}_{kp|chi|z}(kp,chi) = {E|H}0_{kp|chi|z}(kp)*exp(i*m*chi)
%    Then
%       {E|H}_{+|-|z}(kp,chi) = {E|H}0_{+|-|z}(kp)*exp(i*(n+m)*chi)
%    where n={+1|-1|0} and
%       {E|H}0_{+|-}(kp)={E|H}0_rp(kp) +- i*{E|H}0_phi(kp)
%    The EH0 field calculated by this function is on the positive x-axis,
%    i.e., it has components
%       E0_rp(rp),E0_phi(rp),E0_z(rp),H0_rp(rp),H0_phi(rp),H0_z(rp)
%    For arbitrary phi we have
%       {E|H}_{rp|phi|z}(rp,phi) = {E|H}0_{rp|phi|z}(rp)*exp(i*m*phi)
%    To convert to {E|H}_{x|y}(rp,phi), we use
%       {E|H}_{+|-|z}(rp,phi) = {E|H}0_{+|-|z}(rp)*exp(i*(n+m)*phi)
%    where n={+1|-1|0}, and then convert {E|H}_{+|-} to {E|H}_{x|y}
% See also: FWM_AXISYMMETRIC, FWM_RADIATION
% Author: Nikolai G. Lehtinen

if nargin<4
    m=[];
end
if isempty(m)
    m=0;
    % In general case, we separate the currents into vectors I_m, such that
    %    (I_m)_{r|phi|z}(rvec)=I_{m,r|phi|z}*exp(i*m*phi)
    % i.e., r and phi components have exp(i*m*phi) dependence.
    % This is done by utilizing I+- == Ix +- i*Iy and Iz.
    % Then 
    %    (I_m)_{+|-|z}(phi) ~ exp(i*(m+n)*phi)
    % where n=+1 for {+}-component, n=-1 for {-}-component at n=0 for
    % z-component.
	% Note that m==0 corresponds to an axially symmetric case.
    % After FT, we still have the same phi_k dependence:
    %    (I_m)_{+|-|z}(chi) ~ exp(i*(m+n)*chi)
    % by the property of the Fourier/Hankel transforms.
    % EXAMPLE: If I==const(phi) (an important example of which is a
    %    horizontal dipole), then we separate the currents into
    %    3 vectors: m==0 component I_0=zu*Iz, and m==+-1 components
    %    I_{+-1}=(Ix-+i*Iy)*(xu+-i*yu)/2.
end
global output_interval
if isempty(output_interval)
	output_interval=20;
end
Nr=length(k0r);
[dummy,Mi,N]=size(EHf0);
npb=npb(:).';
np=(npb(1:end-1)+npb(2:end))/2;
if length(np)~=N || dummy~=6
    error('wrong sizes')
end

% Ex,Ey -> E+, E-
% We reuse the same array to save memory
tmp=EHf0([1 4],:,:);
EHf0([1 4],:,:)=tmp+1i*EHf0([2 5],:,:);
EHf0([2 5],:,:)=tmp-1i*EHf0([2 5],:,:);
% Ez stays the same.

EH0=zeros(6,Mi,Nr);
% Integrate
dnp=diff(npb);
weight=dnp.*np/(2*pi); % size == 1 x N
% - Multiply by usual integration coefficient. We define
% A(x)=\int A(kx)*exp(i*kx*x) dkx/(2*pi)
% A(kx)=\int A(x)*exp(-i*kx*x) dx
% We use the fact that in the polar coordinates
% E_{+|-|z}(r,phi_r)=
%    =\iint E_{+|-|z}(kr,phi_k)*exp(i*kr*r*cos(phi_r-phi_k))*
%     kr d kr dphi_k/(2*pi)^2
% We use Bessel function property
% 2*pi*i^{+-n}*J_n(x)=\int_{-pi}^{+pi} exp(i*n*phi+-i*x*cos(phi)) d phi
% and obtain
% E_{+|-|z}(r,phi_r)=
%    =exp(i*n*phi_r) *
%     \iint E_{+|-|z}(kr,phi_k=0)*
%      exp(i*n*(phi_k-phi_r)+i*kr*r*cos(phi_r-phi_k))*
%      kr d kr dphi_k/(2*pi)^2=
%    =\int_0^\infty E_{+|-|z}(kr,phi_k=0) * i^n * J_n(kr*r) *kr*d kr/(2*pi)
%    = i^n/(2*pi)* Hankel transform of order n of E_{+|-|z}(kr,phi_k=0)
% where n={1|-1|0} for E_{+|-|z}.
% This remains true when we E_k, E_phi with exp(i*m*phi_r) dependence, in
% which case we substitute n -> n+m. Note for m==0 we can use
% i^{-n}*J_{-n}=i^n*J_n

parfor ir=1:Nr
    w0=(1i^m)*besselj(m,k0r(ir)*np); % n==0, size == 1 x N
    wp=(1i^(m+1))*besselj(m+1,k0r(ir)*np); % n==1
    % Calculate for n==-1:
    if m==0
        wm=wp; % save some time for the important case m==0
    else
        wm=1i^(m-1)*besselj(m-1,k0r(ir)*np);
    end
	wt=repmat([wp;wm;w0],[2 1]).*repmat(weight,[6 1]); % 6 x N
    % EHf0 is 6 x Mi x N
    EH0ir=sum(EHf0.*repmat(permute(wt,[1 3 2]),[1 Mi 1]),3); % 6 x Mi
    % This is equivalent to
    % for ki=1:Mi
    %   % 6 x Mi x N times 6 x N
    %	EH0ir(:,ki)=sum(squeeze(EHf0(:,ki,:)).*wt,2);
    % end
    EH0(:,:,ir)=EH0ir;
end

% E+, E- -> Ex, Ey
tmp=EH0([1 4],:,:);
EH0([1 4],:,:)=(tmp+EH0([2 5],:,:))/2;
EH0([2 5],:,:)=(tmp-EH0([2 5],:,:))/2i;
% Ez stays the same.

