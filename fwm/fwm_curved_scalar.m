function [zdc,nc,F,Tu,Td,Ru,Rup,udm,udp,EH]=fwm_curved_scalar(a,zd,n,np)
%FWM_CURVED_SCALAR Propagation in a scalar medium using the new FWM method
% Usage:
%    fwm_curved_scalar(a,zd,n);
% Inputs:
%    a - dimensionless curvature (=1/(k0*R))
%    zd - dimensionless heights (=k0*z)
%    n - refractive index (=sqrt(epsilon); Im n>=0)
%    np - horizontal refractive index (=kp/k0)
% Outputs:
% Notes:
% We take advantage of the fact that in the scalar case the modification of
% the refractive index is simple: 
% See also: REFLECTCURVED

M=length(zd);
% The "curved" vertical coordinate
if a~=0
    zdc=log(1+a*zd)/a;
    % - works for negative curvature, too!
else
    zdc=zd;
end
dzc=diff(zdc);
% zcen is taken in the middle of each layer
zcen=zeros(1,M+1);
zcen(2:M)=(zd(1:M-1)+zd(2:M))/2;
zcen(1)=zcen(2)-(zd(2)-zd(1));
zcen(M+1)=zcen(M)+(zd(M)-zd(M-1));
% The modified refractive index
nscaled=(1+a*zcen).*n; % Baker's [1927] rescaling
% Vertical refractive index (Im nz>0):
nc=repmat(nscaled,[2 1]);
nc(1,:)=nc(1,:)+a*1i/2; % For propagation up
nc(2,:)=nc(2,:)-a*1i/2; % For propagation down
kh=nc(:,2:M).*repmat(dzc,[2 1]);
Pu=exp(1i*kh(1,:)); Pd=exp(1i*kh(2,:)); % P(k) is P in layer k=1..M-1
P2=Pu.*Pd;

% Transfer coefs T(k) through boundary k=1..M
% Now it is easier to use matrices
F=zeros(2,2,M+1); % F(:,:,k+1) is the mode structure matrix in layer k=0..M
for k=1:M+1
    F(:,:,k)=[1 1;nc(1,k) -nc(2,k)];
end
Tu=zeros(2,2,M); Td=zeros(2,2,M);
for k=1:M
    Tu(:,:,k)=F(:,:,k+1)\F(:,:,k);
    Td(:,:,k)=F(:,:,k)\F(:,:,k+1);
end
% Reflection coefs
Ru=zeros(1,M); Rup=zeros(1,M);
Ru(M)=0;
for k=M:-1:1
    Rup(k)=(Td(2,1,k)+Td(2,2,k)*Ru(k))/(Td(1,1,k)+Td(1,2,k)*Ru(k));
    if k>1
        Ru(k-1)=P2(k-1)*Rup(k);
    end
end
% Wave amplitude
u=zeros(1,M); up=zeros(1,M);
u(1)=1;
d=zeros(1,M); dp=zeros(1,M);
for k=1:M
    dp(k)=Rup(k)*u(k);
    up(k)=(Tu(1,1,k)+Tu(1,2,k)*Rup(k))*u(k);
    d(k)=Ru(k)*up(k);
    if k<M
        u(k+1)=Pu(k)*up(k);
    end
end
udm=[u;dp]; udp=[up;d];
EH=zeros(2,M);
for k=1:M
    EH(:,k)=F(:,:,k)*udm(:,k);
    % Must be the same as F(:,:,k+1)*udp(:,k);
end
