function [Ru,Rufull]=fwm_Ru0(dz,nz1,F)
% nz1 is shorter by 1 than F
[Pu,Pd]=fwm_PuPd(dz,nz1);
M=length(dz)+1;
Ru=zeros(2,2); % Rul(M) - no reflection
% There is no R{u,d}h(:,:,M) because layer M has no upper boundary.
if nargout>1
    Rufull=zeros(2,2,M);
end
for k=M-1:-1:1
    Td=F(:,:,k)\F(:,:,k+1);
    Ru=Pd(:,:,k)*((Td(3:4,1:2)+Td(3:4,3:4)*Ru)/ ...
        (Td(1:2,1:2)+Td(1:2,3:4)*Ru))*Pu(:,:,k); % Rul(k);
    if nargout>1
        Rufull(:,:,k)=Ru;
    end
end
