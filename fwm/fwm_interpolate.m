function [ud,kia]=fwm_interpolate(z,ul,dh,zi,nz)
%FWM_INTERPOLATE Find the wave amplitudes at intermediate points
%
% Analogous to an older (soon to be deprecated) FWM_INTERMEDIATE which uses
% a grid with infinite last layer. This version uses finite domain, i.e.,
% the last layer is finite.
%
% Usage:
%    % (Pre-calculate the amplitude-to-field matrix Q in each layer)
%    [ud,kia]=fwm_interpolate(z,ul,dh,zi,nz);
%    EH=zeros(6,Mi);
%    for ki=1:Mi
%        EH(:,ki)=Q(:,:,kia(ki))*ud(:,ki);
%    end
%
% Inputs (can have an optional third dimension N, e.g., when cycling over
% horizontal wave vectors):
%    z          (M+1) - layer boundaries, there are M layers.
%    {u|d}{l|h} (2 x M x N) - {upward|downward} mode amplitudes at
%       {lower|upper} boundaries of each layer
%    zi         (Mi) - output altitudes
%    nz         (4 x M x N) - vertical refractive index
%
% Output:
%    ud         (4 x M x N) - up/down wave amplitudes at required altitudes
%    kia        (Mi) - the layer numbers for each output point
%
% See also: CYL_INTERPOLATE, FWM_INTERMEDIATE
%
% Author: Nikolai G. Lehtinen

%% Check inputs
[Mc,M,N]=size(nz);
% N is additional size
if size(ul,2)~=M || size(dh,2)~=M || size(ul,3)~=N || size(dh,3)~=N || length(z)~=M+1
    error('wrong size');
end

%% Find the layer numbers (used to be done by FWM_GET_LAYERS)
Mi=length(zi);
% Length of z is M+1
[~,kia]=histc(zi,z);
% When the output altitudes are equal to grid altitudes, we choose the
% output altitude just above the grid altitude, except for the top point,
% which is chosen just below the top boundary if it coincides with it.
kia(kia>M)=M;
dzl=zi-z(kia); dzh=z(kia+1)-zi;

%% Field amplitude at each output altitude
ud=zeros(Mc,Mi,N);
iu=1:Mc/2; id=Mc/2+1:Mc;
for ki=1:Mi
    k=kia(ki); % The layer number
    ui=exp(1i*dzl(ki)*nz(iu,k,:)).*ul(:,k,:);
    di=exp(-1i*dzh(ki)*nz(id,k,:)).*dh(:,k,:);
    ud(:,ki,:)=cat(1,ui,di);
end
