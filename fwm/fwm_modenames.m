function names0=fwm_modenames(C0,k0,h,eground,perm)
% Determine the types of modes
[tmp,isort]=sort(real(C0));
C=C0(isort);
nC=length(C);
names={};
isTE=zeros(1,nC);
for kC=1:nC
    [EHf,name]=fwm_modestruct(C(kC),k0,h,eground,perm,[]);
    isTE(kC)=strcmp(name(end-1:end),'TE');
    names={names{:},name};
end

% Sort TM modes
ii=find(~isTE);
for ki=1:length(ii)
    % TM1 is also TEM
    names{ii(ki)}=[names{ii(ki)} num2str(ki)];
end
ii=find(isTE);
for ki=1:length(ii)
    names{ii(ki)}=[names{ii(ki)} num2str(ki)];
end

% Un-sort
names0=cell(1,nC);
for kC=1:nC
    names0{isort(kC)}=names{kC};
end

