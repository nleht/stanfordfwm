function EH=fwm_cartesian_ift(nxb,EHf,k0x)
% Inverse Fourier transform
global output_interval
if isempty(output_interval)
	output_interval=20;
end
[dummy,Mi,N]=size(EHf);
Nx=length(k0x);
nxb=nxb(:).';
EH=zeros(6,Mi,Nx);
dnx=diff(nxb);
nx=(nxb(1:end-1)+nxb(2:end))/2;
tstart=now*24*3600; toutput=tstart;
for ix=1:Nx
    wt=repmat(dnx.*exp(i*nx*k0x(ix))/(2*pi),[6 1]);
    for ki=1:Mi
        EH(:,ki,ix)=sum(squeeze(EHf(:,ki,:)).*wt,2);
    end
    timec=now*24*3600;
    ttot=timec-tstart;
    if timec-toutput>output_interval
        toutput=timec;
        disp(['FWM_CARTESIAN_IFT: Done=' num2str(ix/Nx*100) '%; ' ...
            'Time=' hms(ttot) ...
            ', ETA=' hms(ttot/ix*(Nx-ix))]);
    end
end
