function EH=fwm_assemble_rp_par(k0,nx,ny,da,EHf,r,phi,bkp_file)
%FWM_ASSEMBLE Inverse Fourier transform from the polar mesh
% It is much slower than FFT
% Usage:
%    EH=fwm_assemble(k0,nx,ny,da,EHf,x,y);
% Inputs:
% Outputs:
% Author: Nikolai G. Lehtinen

%% Constants
program='FWM_ASSEMBLE_RP';
arginnames={'k0','nx','ny','da','EHf','r','phi'};
argoutnames={'EH'};

%% REUSABLE: Arguments
stdargin=length(arginnames);
stdargout=length(argoutnames);
if nargin < stdargin+1
    bkp_file=[];
end
if nargin < stdargin
    error('Not enough arguments!')
end
global output_interval backup_interval
if isempty(output_interval)
    output_interval=20;
end
if isempty(backup_interval)
    backup_interval=3600;
end
do_backups=~isempty(bkp_file);

%% Various
Mi=size(EHf,2);
% This would be SOO much faster if we did an FFT
% The weighted field
%EHfw=EHf.*repmat(da,[6 1 Mi])*(k0^2/(2*pi)^2);
% - Multiply by usual integration coefficient

%% Divide all n-space into regions, 1 region for each processor
%Nproc=matlabpool('size')
% Large number of processors allows smaller memory chunk to be passed to
% each processor.
N=length(nx);
Nproc=ceil(N/1024) % The optimal number here is determined by on-processor cache (?)
N1=ceil(N/Nproc) % Portion of n-space going to each CPU
if length(ny)~=N || length(da)~=N
    error([program ': arguments of wrong length'])
end
ipend=round([1:Nproc]*N/Nproc);
ipstart=[1 ipend(1:Nproc-1)+1];

%% Check if the array r is uniformly spaced
Nr=length(r); Nphi=length(phi);
if Nr>1
    tmp=diff(r);
    dr=mean(tmp);
    runif=max(abs(tmp-dr))<10*eps;
else
    runif=0;
end

disp(['r uniform = ' num2str(runif)]);

nxmat=repmat(nx,[6 1]);
nymat=repmat(ny,[6 1]);

%% REUSABLE: determine if we can restore from a backup
if do_backups
    bkp_file_ext=[bkp_file '_' program '.mat'];
    if ~exist(bkp_file_ext,'file')
        disp([program ': creating a new backup file ' bkp_file_ext])
        status='starting';
        save(bkp_file_ext,arginnames{:},'status');
        restore=0;
    elseif ~check_bkp_file(bkp_file_ext,arginnames,{k0,nx,ny,da,EHf,r,phi})
        disp(['WARNING: the backup file ' bkp_file_ext ' is invalid! Not doing backups']);
        do_backups=0;
    else % File exists and is valid
        disp(['Found backup file ' bkp_file_ext]);
        tmp=load(bkp_file_ext,'status');
        if ~isfield(tmp,'status')
            error('no status!')
        end
        status=tmp.status;
        if strcmp(status,'done')
            disp([program ': loaded precalculated output arguments!']);
            % No need to do anything
            tmp=load(bkp_file_ext,argoutnames{:});
            if nargout>stdargout
                error('Too many output arguments');
            end
            EH=tmp.EH;
            return;
        end
        restore=strcmp(status,'in progress');
    end
end
if do_backups
    disp([program ' BACKUP STATUS = ' status]);
end

%% Restore
if do_backups && restore
    % The file is guaranteed to exist, and to contain relevant information
    tmp=load(bkp_file_ext,'ki','iphi','EH');
    kistart=tmp.ki; % I think there should be +1
    iphisaved=tmp.iphi+1;
    EH=tmp.EH;
    disp([program ': restored from backup']);
else
    kistart=1; iphisaved=1;
    EH=zeros(6,Mi,Nr,Nphi);
    disp([program ': starting a new calculation']);
end

%% Cycle over ki
EHtmp=zeros(6,Nr,Nproc);
nrmat1=zeros(6,N1,Nproc);
EHfw1=zeros(6,N1,Nproc);
tmp1=zeros(6,N1,Nproc);
expdr1=zeros(6,N1,Nproc);
tstart=now*24*3600; toutput=tstart; timebkup=tstart;
for ki=kistart:Mi
    EHfw=squeeze(EHf(:,ki,:)).*repmat(da,[6 1])*(k0^2/(2*pi)^2);
    if ki==kistart
        iphistart=iphisaved;
    else
        iphistart=1;
    end
    for iphi=iphistart:Nphi
		nrmat=cos(phi(iphi))*nxmat+sin(phi(iphi))*nymat;
        if runif
			tmp=EHfw.*exp(1i*k0*r(1)*nrmat);
			expdr=exp(1i*k0*dr*nrmat);
            for iproc=1:Nproc
                tmp1(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=tmp(:,ipstart(iproc):ipend(iproc));
                expdr1(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=expdr(:,ipstart(iproc):ipend(iproc));
            end
            parfor iproc=1:Nproc
                EHtmp1=zeros(6,Nr);
                tmp2=tmp1(:,:,iproc);
                expdr2=expdr1(:,:,iproc);
                for ir=1:Nr
                    %EH=sum(EHfw.*exp(i*k0*r(ir)*nr),2);
                    EHtmp1(:,ir)=sum(tmp2,2);
                    tmp2=tmp2.*expdr2;
                end
                EHtmp(:,:,iproc)=EHtmp1;
            end
        else
            % Parallel CPUs: slice the inputs
            for iproc=1:Nproc
                EHfw1(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=EHfw(:,ipstart(iproc):ipend(iproc));
                nrmat1(:,1:ipend(iproc)-ipstart(iproc)+1,iproc)=nrmat(:,ipstart(iproc):ipend(iproc));
            end
            parfor iproc=1:Nproc
                EHtmp1=zeros(6,Nr);
                EHfw2=EHfw1(:,:,iproc);
                nrmat2=nrmat1(:,:,iproc);
                for ir=1:Nr
                    EHtmp1(:,ir)=sum(EHfw2.*exp(1i*k0*r(ir)*nrmat2));
                end
                EHtmp(:,:,iproc)=EHtmp1;
            end
        end
        EH(:,ki,:,iphi)=sum(EHtmp,3);
        
        timec=now*24*3600;
        ttot=timec-tstart;
        if timec-toutput>output_interval
            toutput=timec;
            isdonenow=(ki-kistart)*Nphi+(iphi-iphistart+1);
            isremaining=(Mi-ki+1)*Nphi-iphi;
            disp([program ': Done=' num2str((ki-1+iphi/Nphi)/Mi*100) '%; ' ...
                'Time=' hms(ttot) ...
                ', ETA=' hms(ttot/isdonenow*isremaining)]);
        end
        if do_backups
            % Do backups every hour
            if timec-timebkup>backup_interval
                timebkup=timec;
                disp('Backing up ...');
                status='in progress';
                save(bkp_file_ext,'ki','iphi','EH','status','-append');
                disp(' ... done');
            end
        end
    end
end

%% REUSABLE: Final backup
if do_backups
    disp(['Saving results of ' program ' into ' bkp_file_ext ' ...']);
    status='done';
    save(bkp_file_ext,argoutnames{:},'status','-append');
    disp(' ... done');
end
% Output
if nargout>stdargout
    error('Too many output arguments');
end
for k=1:nargout
    eval(['varargout{k}=' argoutnames{k} ';']);
end
