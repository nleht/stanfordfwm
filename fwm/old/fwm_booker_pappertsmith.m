function [nz,Fext]=fwm_booker_pappertsmith(perm,nx,ny,tolerance)
%FWM_BOOKER_PAPPERTSMITH Solve the Booker equation using "forward" operator
% Solve the Booker equation for a plasma or any other
% anisotropic medium. The Booker equation is
%   det(perm-(n.'*n)*eye(3)+n*n.')==0 in MATLAB notation, or
%   det(perm-(n^2)*Iperp)==0 in usual notation,
% where n=[nx;ny;nz] is the refraction coefficient vector. The equation is
% solved for nz, while nx and ny are known.
% In this function, we, however, do not solve the above equation directly.
% Instead, we find nz as the eigenvalues of the "forward" operator L of
% Pappert and Smith [1972, doi:10.1029/RS007i002p00275, equation (1), with
% orthogonal coordinates x,y]. The horizontal fields (Ex,Ey,Z0*Hx,Z0*Hy)
% for different modes are the eigenvectors of L.
% For the direct solution of the above equation, see FWM_BOOKER.
% Usage:
%   [nz,Fext]=fwm_booker_pappertsmith(perm,nx,ny);
% Inputs:
%   perm     - 3 x 3 dielectric permittivity tensor;
%   nx,ny    - horizontal refraction coefficients nx=kx/k0, ny=ky/k0;
%              k0=w/c;
% nx and ny are 1D arrays of length N.
% Outputs:
%   nz   - 4 x N array of vertical refraction coefficients = kz/k0,
%          for 2 modes and 2 directions, sorted by decreasing imaginary
%          part, so that the first two values correspond to upgoing modes
%          (u1,u2) and the second two to downgoing modes (d1,d2), for each
%          value of nx and ny;
%   Fext - 6 x 4 x N matrix converting the mode variables
%          (u1,u2,d1,d2) into field (Ex,Ey,Ez,Z0*Hx,Z0*Hy,Z0*Hz) for each
%          value of nx and ny.
% NOTE: Z0=sqrt(mu0/eps0) is the impedance of free space.
% See also: FWM_BOOKER, FWM_RGROUND, FWM_DEH, FWM_RADIATION,
% FWM_INTERMEDIATE
% Previous versions: SOLVE_BOOKER_3D (worked the same way, needed some
% unnecessary arguments)
% Author: Nikolai G. Lehtinen

if nargin<4
    tolerance=3e2*eps;
end
if any(~isfinite(perm(:))) || any(~isfinite(nx(:))) || any(~isfinite(ny(:)))
    perm
    nx
    ny
    error('input must be finite');
end
% nx, ny must be 1D arrays
nx=nx(:);
if length(ny)==1
    ny=ny*ones(size(nx));
end
ny=ny(:);
N=length(nx);
np2=nx.^2+ny.^2;

% lame temporary fix (nz=0 is not handled well ...)
ii=find(np2==1);
if ~isempty(ii)
    disp('FWM_BOOKER: WARNING: Lame fix applied ...');
    nx(ii)=nx(ii)+1e-5;
    np2(ii)=nx(ii).^2+ny(ii).^2;
end

epsilon=perm(1,1);
% Determine the special input: epsilon==Inf
if any(isnan(perm(:))) || any(isinf(perm(:)))
    % Infinite conductivity => perfect reflection
    nz=zeros(4,N); Fext=zeros(6,4,N);
    nz(1:2,:)=Inf; nz(3:4,:)=-Inf;
    np=sqrt(np2);
    inz=find(np~=0);
    cp=ones(size(np)); sp=zeros(size(np));
    cp(inz)=nx(inz)./np(inz); sp(inz)=ny(inz)./np(inz);
    Fext(4,1,:)=-cp; Fext(4,2,:)=-sp; Fext(4,3,:)=cp; Fext(4,4,:)=sp; 
    Fext(5,1,:)=-sp; Fext(5,2,:)=cp; Fext(5,3,:)=sp; Fext(5,4,:)=-cp;
    return;
end

isotropic=(max(max(abs(perm-eye(3)*epsilon)))<=10*eps);
% Determine if there is any absorption
%permi=(perm-perm')/(2*1i);
% - must be zero if no absorption, pos. determinate otherwise
%no_absorption=(max(abs(permi(:)))/max(abs(perm(:)))<tolerance);
% - is not used anymore

if ~isotropic
    nz=zeros(4,N);
    Fext=zeros(6,4,N);
    parfor knp=1:N
        % knp
        nx0=nx(knp); ny0=ny(knp);
        perm1=perm; % I don't know why "parfor" needs it
        np=[nx0;ny0];
        nr=[-ny0 nx0]; % rotated by 90 degrees (phys. meaning unknown)
        Ezop=-[perm1(3,1:2) nr]/perm1(3,3); % Operator to get Ez from (Eperp,Hperp)
        % Pappert-Smith "forward" operator
        L=[0 0 0 1;0 0 -1 0;np*nr+[-perm1(2,1:2);perm1(1,1:2)] zeros(2,2)] + ...
            [np;-perm1(2,3);perm1(1,3)]*Ezop;
        [F,dd]=eig(L); % All calculations are done here
        nz0=diag(dd);
        % Sort nz correctly, same as in FWM_BOOKER
        % Sort in descending order according to real part
        [dummy,ii]=sort(real(nz0),1,'descend');
        nz0=nz0(ii);
        % Sort in descending order according to imaginary part
        % Note that if the imaginary part is equal to zero, no sorting should
        % take place!
        tmp=imag(nz0);
        maxreal=max(abs(real(nz0([1 4]))));
        smallimag=abs(tmp)./repmat(maxreal,[4 1]);
        tmp(smallimag<tolerance)=0;
        nz0=real(nz0)+1i*tmp;
        [dummy,ii2]=sort(tmp,1,'descend');
        nz(:,knp)=nz0(ii2);
        % Add Ez, Hz
        Fext(:,:,knp)=[eye(2,2) zeros(2,2);Ezop;zeros(2,2) eye(2,2);[nr 0 0]]*F(:,ii(ii2));
    end
else
    % isotropic medium
    nz0=sqrt(epsilon-np2);
    % Make sure imag(nz0)>=0
    % This is only a problem for complex nx.
    ii=find(imag(nz0)<0);
    nz0(ii)=-nz0(ii);
    nz=[nz0,nz0,-nz0,-nz0].';
    E=zeros(3,4,N);
    n0=sqrt(epsilon);
    np=sqrt(np2);
    parfor knp=1:N
        % knp
        nx0=nx(knp); ny0=ny(knp);
        % nz0=nz(1,knp); % >0 (at least for real np0), ==
        % sqrt(perm(1,1)-np0^2);
        np0=np(knp); % >0
        if np0>0
            cp=nx0/np0; sp=ny0/np0;
        else
            cp=1; sp=0;
        end
        ct=nz0(knp)/n0; st=np0/n0; % cos(th), sin(th), 0<th<pi, st>0
        % Isotropic medium:
        % Upgoing waves:
        % TE
        % Fext(:,1,knx)= ...
        %   [-sp ; cp ; 0 ; -nz0*cp ; -nz0*sp ; np0];
        % TM
        % Fext(:,2,knx)= ...
        %   [cp*ct ; sp*ct ; -st ; -n0*sp ; n0*cp ; 0];
        % Downgoing waves
        % Fext(:,3,knx)= ...
        %   [-sp ; cp ; 0 ; nz0*cp ; nz0*sp ; np0];
        % Fext(:,4,knx)= ...
        %   [cp*ct ; sp*ct ; st ; n0*sp ; -n0*cp ; 0];
        E(:,:,knp)=[-sp cp*ct -sp cp*ct ; cp sp*ct cp sp*ct ; 0 -st 0 st];
    end
    nvec=zeros(3,4,N);
    nvec(1,:,:)=repmat(permute(nx,[2 3 1]),[1 4 1]);
    nvec(2,:,:)=repmat(permute(ny,[2 3 1]),[1 4 1]);
    nvec(3,:,:)=permute(nz,[3 1 2]);
    Fext=cat(1,E,cross(nvec,E));
end

