function [G,ormat]=fwm_mode_excitation(zmode,EHmode,EHmodeadj)
%FWM_MODE_EXCITATION Mode excitation by transverse currents
% Inputs:
%   zmode (Mmode x 1) - heigts in m at which height gains are given
%   EHmode, EHmodeadj (6 x Mmode x nC) - mode height gains
%     (direct and adjoint);
% Outputs:
%   G (4 x Mmode x nC) - excitation coefficients (see usage below)
%   ormat (nC x nC) - bi-orthogonality check for EHmode and EHmodeadj
%     (must be unit)
% Usage:
% We use rationalized units (similar to those of Budden) for the currents,
% so that the current densities are in V/m^2.
% Below in both cases
%    R - distance in m
%    k0 - vacuum wave number.
%    nxmode (nC x 1) - horizontal refractive indeces for modes
%    kxmode=k0*nxmode - horizontal wave number
%    G - height-dependent excitation [units of 1/V=1/([E]*meters)]
%    kI - the current flows at altitude zmode(kI)
%    a0 - auxiliary mode amplitudes (dimensionality depends on the problem,
%        see below)
%    a - mode amplitudes at distance R (dimensionless)
% 1. Currents in plane x=0, translationally symmetric along y-axis.
%    % Emission is in +x direction.
%    % I (4 x Ms) - Electric and magnetic currents Iey, Iez, Imy, Imz,
%    % given at heights hmode(ks). They are assumed to be translationally
%    % symmetric in y-direction and are in units of V (Volts) (i.e., Ie is
%    % multiplied by impedance0, Im is multiplied by mu0), i.e., they are
%    % current moments per unit y-length.
%    [G,ormat]=fwm_mode_excitation(zmode,EHmode,EHmodeadj);
%    I=[Iey; Iez; Imy; Imz];
%    a0=zeros(nC,1);
%    for k=1:nC
%        a0(k)=sum(I.*G(:,kI,k)); % "initial amplitude", integration over z
%    end
%    % For several current altitudes, when kI is array,
%    % and I is (length(kI) x 4) array:
%    % a0=squeeze(sum(sum(repmat(I,[1 1 nC]).*G(:,kI,:)))); (nC x 1)
%    % Now, attenuate (and change the phase of) amplitude to distance R:
%    a=a0.*exp(-1i*kxmode*R);
%    % Finally, calculate the field at distance R:
%    EH=zeros(6,Mmode);
%    for k=1:nC
%        EH=EH+a(k)*EHmode(:,:,k);
%    end
% 2. Radial emission by currents at (x=0,y=0)
%    % Note the differences!
%    % Iez, Imz - VERTICAL current MOMENTS in units of V*m!
%    % There are NO horizontal currents!
%    [G,ormat]=fwm_mode_excitation(zmode,EHmode,EHmodeadj);
%    a0=zeros(nC,1);
%    for k=1:nC
%        a0(k)=Iez.*G(2,kI,k)+Imz*G(4,kI,k);
%    end
%    a=a0.*kxmode.*besselh(0,1,kxmode*R)/2;
%    % Then, the field is calculated from "a" and EHmode in the same way as
%    % above.
% Author: Nikolai G. Lehtinen

[dummy,Mmode,nC]=size(EHmode);
dzmode=([0;diff(zmode)]+[diff(zmode);0])/2;
% Check the orthogonality of modes
S=cross(EHmodeadj(1:3,:,:),EHmode(4:6,:,:))+cross(EHmode(1:3,:,:),EHmodeadj(4:6,:,:));
EHnorm=sum(squeeze(S(1,:,:)).*repmat(dzmode(:),[1 nC])); % -\int (f^a)^T B f dz = - (B(f^a)^*,f) = - <f^a,f>

% The Pappert-Smith [1972] vector and conjugate of its adjoint
fvec=EHmode([2 3 5 6],:,:); % perpendicular to x-direction
% Adjoint
% gvecc==B*e^a=g^*
gvecc=EHmodeadj([6 5 3 2],:,:); gvecc([2 3],:,:)=-gvecc([2 3],:,:);
% Normalize gvecc
% EHnorm=zeros(nC,1);
for k=1:nC
    % Integrate over hi
    %EHnorm(k)=sum(sum(gvecc(:,:,k).*fvec(:,:,k)).*dzmode(:).');
    gvecc(:,:,k)=gvecc(:,:,k)/EHnorm(k);
end
% Bi-orthogonality matrix "ormat"
ormat=zeros(nC,nC);
for k1=1:nC
    for k2=1:nC
        ormat(k1,k2)=sum(sum(gvecc(:,:,k1).*fvec(:,:,k2)).*dzmode(:).');
    end
end
% - ormat must be unit matrix
tmp=ormat-eye(nC);
disp(['Error in ormat=' num2str(max(abs(tmp(:))))]);
%imagesc([1:nC],[1:nC],abs(ormat-eye(nC))); colorbar;
% Now, the amplitudes are obtained from
% Am=\int (-Kz*g1ccm+Ky*g2ccm+Iz*g3ccm-Iy*g4ccm) dz
% If we have I=I0z delta(z), with I0z=1 V*m (Budden units), then
% Am= g3ccm(z=0)

%G=gvecc([4 3 2 1],:,:);
%G([1 4],:,:)=-G([1 4],:,:);
%size(repmat(shiftdim(EHnorm(:),-2),[4 Mmode 1]))
G=-EHmodeadj([2 3 5 6],:,:)./repmat(shiftdim(EHnorm(:),-2),[4 Mmode 1]);


