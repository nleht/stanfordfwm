function [EH0,EHf0,nx,ki2,nxt,EHft0]=fwm_cartesian(f,h,eground,perm,...
	ksa,nx0,I0,xkm,hi,rmaxkm,dxkm,retol)
%FWM_CARTESIAN NOT FINISHED!!!! Full-wave method for 2D (x,z)
% Usage:
%    EH=fwm_axisymmetric(f,h,eground,perm,...
%       ksa,nx0,I0,xkm,hi[,rmaxkm,dxkm,retol])
% Advanced usage:
%    [EH,EHf,np]=fwm_axisymmetric(...)
% Inputs:
%    f - frequency (Hz)
%    h (M) - altitudes in km
%    eground - ground permittivity (scalar) or boundary condition (string),
%       chosen from 'E=0','H=0' or 'free'
%    perm (3 x 3 x M) - dielectric permittivity tensor in each layer
%    ksa, nx0, m, I0 - the current
%       ksa (Ms) - indeces of altitudes at which current flows, i.e. h(ksa)
%       nx0 (N0) - points in np at which the current is given
%       I0 (6 x Ms x N0) - the value of the electric and magnetic current
%          moments or surface currents (in rationalized units of V/m) at
%          altitudes h(ksa). Given are the current component
%          values on the positive x-axis (at phi==0) (see note 3!)
%    xkm (Nx) - horisontal distance in km
%    hi (Mi) - output altitudes in km
% Optional inputs:
%    rmaxkm (scalar) - maximum distance
%    drkm (scalar) - the size of the source in km (for a point source and
%       calculations on the ground), default==xkm(2)-xkm(1)
%    retol (scalar) - relative error tolerance, default=1e-3 (take a
%       smaller value for more accurate results)
% Output:
%    EH (6 x Mi x Nx) - E, H components on the positive x-axis at
%       coordinates (rkm,hi) (see note 3!)
% Optional outputs:
%    np (N) - values of nx at which the Fourier components EHf0 are
%       calculated
%    EHf (6 x Mi x N) - field values in np-space
% Notes (IMPORTANT!):
% 1. See notes 1-2 to FWM_AXISYMMETRIC.
% Author: Nikolai G. Lehtinen
% See also: FWM_NONAXISYMMETRIC, FWM_AXISYMMETRIC
% Examples:

%% Optional arguments
if nargin<12
    retol=[];
end
if nargin<11
    dxkm=[];
end
if nargin<10
	rmaxkm=[];
end

%% Default argument values
if isempty(retol)
    retol=1e-3;
end
if isempty(dxkm)
	if length(xkm)==1
		dxkm=xkm;
	else
		dxkm=min(diff(xkm));
	end
end
if isempty(rmaxkm)
	rmaxkm=max(abs(xkm));
end

global clight
if isempty(clight)
    loadconstants
end
w=2*pi*f;
k0=w/clight;
M=length(h);
zd=h*1e3*k0;
Mi=length(hi);
zdi=hi*1e3*k0;
zds=zd(ksa); % source altitudes
x=xkm*1e3;
Nx=length(x);
rmax=rmaxkm*1e3;
dxdamp=dxkm*1e3;
if max(diff(x))*k0>1
    disp('WARNING: the wavelength is not resolved')
end
[kia,dzl,dzh]=fwm_get_layers(zd,zdi);

% The minimum dnperp, determined by max(x)
magic_scale=0.9; % 0.9 works for vacuum; probably =0.5 will work for anything.
% To resolve it, we need at least
dn0=magic_scale*2*pi/(k0*rmax)


%% Determine the maximum nperp
% The effect of ionosphere is limited by evanescence of waves between the
% source and the ionosphere, or by nperp-size of the source. For point
% source, its shape is gaussian with width determined by dxdamp.
% For points close to the source (closer than the ionosphere), there is a
% different nperp, determined either by evanescence between source and
% point, or by nperp-size of the source, or by the gaussian profile point
% source.
point_source=(length(nx0)<=1);
zds
[nxmax1,ki1,need_gauss1,kion,do_extend,nxmax2,ki2,need_gauss2]=...
	fwm_npmax(zd,perm,zds,zdi,point_source,max(abs(nx0)),dxdamp*k0)

%% Initial nx grid
nxbi2=fwm_initial_npb(nxmax1,dn0);
nxbi=[-nxbi2(end:-1:1) nxbi2(2:end)];

%% Find the field in nx-space
if point_source
	% Point source - just repeat the matrix I0 (3 x Ms)
    fun=@(nx_arg) permute(fwm_field(zd,eground,perm,ksa,nx_arg,0,...
        repmat(I0,[1 1 length(nx_arg)]),kia,dzl,dzh),[3 1 2]);
else
   I0p=permute(I0,[3 1 2]);
	% - move the interpolation dimension to 1st place
    % Iip=interp1(nx0,I0p,nx_arg);
    % I0 is (3 x Ms x N)
	% I0p is (N x 3 x Ms)
    % We managed to squeeze the call into a lambda form
	% Output of fwm_field is EHf (6 x Mi x N);
	% BESTGRID takes (N x 6 x Mi)
    fun=@(nx_arg) permute(fwm_field(zd,eground,perm,ksa,nx_arg,0,...
        permute(interp1(nx0,I0p,nx_arg),[2 3 1]),kia,dzl,dzh),[3 1 2]);
end
[nxb,nx,EHf0,relerror]=bestgrid(fun,{},nxbi,dn0,retol);
EHf0=permute(EHf0,[2 3 1]); % Back to (6 x Mi x N)

%% Inverse Fourier transform from k-space to r-space
if do_extend
	EH0=zeros(6,Mi,Nx);
	EH0(:,ki1,:)=k0*fwm_cartesian_ift(nxb,EHf0(:,ki1,:),x*k0);
else
	% Finish here
	if need_gauss1
		efactor=exp(-(nx.*k0*dxdamp).^2/2);
		EHf0=EHf0.*repmat(shiftdim(efactor,-2),[6 Mi 1]);
	end
	EH0=k0*fwm_cartesian_ift(nxb,EHf0,x*k0);
	npt=[]; EHft0=[]; ki2=[];
	return
end

%% Extend the calculation to higher np for cases that require it
disp('Extending ...');
% Extend (only if evanescence is smaller than the effect of the
% size of the source)
Me=min([kion-1,max(ksa)])
zde=zd(1:Me); perme=perm(:,:,1:Me); % only a few layers, to include the source
[kiavac,dzlvac,dzhvac]=fwm_get_layers(zde,zdi(ki2));
% Unlike FWM_AXISYMMETRIC, we have to extend in both directions!
%nxebi_p=[nxmax1:dn0:nxmax2+dn0].';
%nxebi_m=[-(nxmax2+dn0):dn0:-nxmax1].';
if point_source
    % Point source
    fun=@(nx_arg) permute(fwm_field(zde,eground,perme,ksa,nx_arg,0,...
        repmat(I0,[1 1 length(nx_arg)]),kiavac,dzlvac,dzhvac),[3 1 2]);
else
    fun=@(nx_arg) permute(fwm_field(zde,eground,perme,ksa,nx_arg,0,...
        permute(interp1(nx0,I0p,nx_arg),[2 3 1]),kiavac,dzlvac,dzhvac),[3 1 2]);
end
[nxeb_p,nxe_p,tmp,relerror]=bestgrid(fun,{},[nxmax1;nxmax2+dn0],dn0,retol);
EHfe0_p=permute(tmp,[2 3 1]);
[nxeb_m,nxe_m,tmp,relerror]=bestgrid(fun,{},[-(nxmax2+dn0);-nxmax1],dn0,retol);
EHfe0_m=permute(tmp,[2 3 1]);
% Concatenate three solutions and do the inverse Fourier transform
nxt=[nxe_m ; nx(:) ; nxe_p];
nxtb=[nxeb_m(1:end-1) ; nxb(:) ; nxeb_p(2:end)];
EHft0=cat(3,EHfe0_m,EHf0(:,ki2,:),EHfe0_p);
if need_gauss2
	efactor=exp(-(nxt.*k0*dxdamp).^2/2);
	EHft0=EHft0.*repmat(shiftdim(efactor,-2),[6 length(ki2) 1]);
end
EH0(:,ki2,:)=k0*fwm_cartesian_ift(nxtb,EHft0,x*k0);
