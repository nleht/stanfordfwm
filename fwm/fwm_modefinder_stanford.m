% Tabulate the modes for Stanford profiles
% in order to compare to Bainbridge and Inan [2003, doi:??]
global clight kB ech
if isempty(clight)
    loadconstants
end

%% Bainbridge and Inan [2003] for NAA (f=24 kHz)
% Daytime
BIDalf=[3.3 7 11.1 22.1 27.4 62.6 51.5 79.3 59.4 98.4];
BIDnames={'QTM1','QTE1','QTM2','QTE2','QTM3','QTE3','QTM4','QTE4','QTE5','QTM5'};
% Nighttime
BINalf=[];
BINnames={};

%% INPUTS
VLFProfile='NAA'
NeProfile='Stanford_daytime';
nuProfile='nues';
hProfile='fine';

%% Frequency
switch VLFProfile
    case 'NAA'
        f=24e3; % NAA
end
w=2*pi*f;
k0=w/clight;

%% We can try various altitude grids
switch hProfile
    case 'coarse'
        h=[0 20:110].';
    case 'fine'
        h=[0:.5:110].';
    case 'superfine'
        h=[0:.25:110].';
end
M=length(h);

%% Collision freq
T0=kB*getTn(h)/ech; % in eV
Nm=getNm(h); % in m^{-3}
nuek=zeros(size(h));
for iz=1:M
    [enT,neT]=getneT(T0(iz)); % enT in eV, sum(neT)=1
    nuek(iz)=Nm(iz)*sum(getnumom(enT).*neT);
end
% Which one do we use?
switch nuProfile
    case 'nuek'
        nue=nuek;
    case 'nuek2'
        nue=nuek/2;
    case 'nues'
        nue=get_nu_electron_neutral_swamy92(h);
    case 'nuev'
        nue=get_nu_electron_neutral_vuthaluru02(h);
    case 'nuezero'
        nue=zeros(size(Ne));
        nue(M)=1e-5; % add small attenuation
end

%% Boundary condition
eground='E=0';
bcProfile='E0';

%% Electron density
Ne=getNe(h,NeProfile);
Ne(1)=0; % To get TE, TM at h=0;

%% Calculate the modes
Crmin=1e-5; Crmax=1; Cimin=-.1; Cimax=.02;
dCtol=1e-10; dftol=1e-10;
debugflag=2;
doplot=0;
Babsarr=[0 3 4 5]*1e-5;
for kBabs=1:length(Babsarr)
    %% One cycle
    Babs=Babsarr(kBabs);
    BProfile=num2str(Babs/1e-6); % B in uT
    perm=get_perm_with_ions(h,w,'Ne',Ne,'nue',nue,'Babs',Babs,'need_ions',0);
    fnamebase=[VLFProfile '_' NeProfile '_B' BProfile '_' nuProfile '_' hProfile '_bc' bcProfile];
    fname=[fnamebase '_roots.mat']
    if exist(fname)~=2
        [C,Cmul,alf,permrot,Cbox,Fbox]=fwm_modefinder(...
            k0,h,eground,perm,[],Crmin,Crmax,Cimin,Cimax,[],dCtol,dftol,debugflag,doplot);
        % The mode names
        modename=fwm_modenames(C,k0,h,eground,perm);
        eval(['save ' fname ' f w k0 h Babs Ne nue eground perm C Cmul alf Cbox Fbox modename']);
    else
        eval(['load ' fname]);
    end
end

% %% The mode height gains
% % Output heights for mode height gains
% hi=[0:.1:110].';
% Mi=length(hi);
% nC=length(C);
% EH=zeros(Mi,6,nC);
% for kC=1:nC
%     EH(:,:,kC)=fwm_modestruct(C(kC),k0,h,eground,permi,hi);
% end
% nCi=length(Ci);
% EHi=zeros(Mi,6,nCi);
% for kC=1:nCi
%     EHi(:,:,kC)=fwm_modestruct(Ci(kC),k0,h,eground,permi,hi);
% end
