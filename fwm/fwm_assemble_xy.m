function EH=fwm_assemble_xy(k0,nx,ny,da,EHn,x,y,bkp_file)
%FWM_ASSEMBLE_XY Inverse Fourier transform from the polar mesh
% Version accelerated by Drew Compston, for large memory computers
% It is much slower than FFT
% This version uses parallel processing
% Usage:
%    EH=fwm_assemble_xy(k0,nx,ny,da,EHn,x,y,bkp_file);
% Inputs:
% Outputs:
% See also: FWM_ASSEMBLE_XY_SLOW, FWW_ASSEMBLE_XY_HIMEM
% Author: Nikolai G. Lehtinen, Drew Compston

% In the corrected version, we fixed the following bug:
% For real ph, when abs(ph)>pi, and for non-integer x
%    exp(1i*ph*x)~=exp(1i*ph).^x
% The same happens when using "power" instead of ".^"
% This is related to the fact that for such values of ph,
%    1i*ph==log(exp(1i*ph))+2i*pi

if nargin<8
    bkp_file=[];
end
do_backups=~isempty(bkp_file);
global output_interval backup_interval memory_use_coef
if isempty(output_interval)
    output_interval=20;
end
if isempty(backup_interval)
    backup_interval=3600;
end
if isempty(memory_use_coef)
    memory_use_coef = 0.25;
end
if memory_use_coef>1
    memory_use_coef=1; % do not overstretch
end
if memory_use_coef<=0
    error('memory_use_coef cannot be nonpositive')
end
program = 'FWM_ASSEMBLE_XY';
restore = 0;
if do_backups
    bkp_file_ext = [bkp_file '_' program '.mat'];
    if exist(bkp_file_ext, 'file')
        disp(['Found backup file ' bkp_file_ext]);
        s=load(bkp_file_ext,'status');
        if ~isfield(s,'status')
            fprintf('%s: backup file invalid (no status)\n', program);
        else
            status=s.status;
            if strcmp(status,'done')
                fprintf('%s: loading precalculated output arguments ... ', program);
                % No need to do anything
                s=load(bkp_file_ext,'EH');
                fprintf('done!\n');
                if isfield(s, 'EH')
                    EH = s.EH;
                    return;
                else
                    fprintf('%s: backup file invalid (done, but no EH)\n', program);
                end
            elseif strcmp(status,'in progress')
                restore = 1;
                s = load(bkp_file_ext);
            end
        end
    end
end

N = numel(nx);
Mi = size(EHn, 2);
Nx = numel(x);
Ny = numel(y);

% EHf is of size [6 x Mi x N]
x=x(:); y=y(:); nx=nx(:); ny=ny(:); % for definiteness
% Separate into chunks
%maxmem=2e8; % maximum possible for 16 GB
% get_memtotal returns total RAM in kB
%maxmem=get_memtotal*12;
% - maximum possible, for any memory size (only on Linux!)
maxmem = get_memtotal*12*memory_use_coef;
%maxmem = get_memtotal*3*memory_use_coef;
% - conservative, takes about the same time (or slightly faster) for "more
%   square" matrices but 3 times longer if small dimension is decreased 
% Make the matrices as square as possible
L=floor(sqrt(maxmem)); % length of a chunk
Lx=min(L,Nx);
Ly=min(L,Ny);
Ln=floor(maxmem/max(Lx,Ly));
disp([program ': Chunk size = (Ln=' num2str(Ln) ') x (Lx=' num2str(Lx) ') x (Ly=' num2str(Ly) ')']);
Ncx=ceil(Nx/Lx);
Ncy=ceil(Ny/Ly);
Ncn=ceil(N/Ln);
EH(Nx, Ny, 6*Mi) = 0;
EHda = reshape(bsxfun(@times, permute(EHn, [3 1 2]), (k0/2/pi)^2*da(:)), [N 6*Mi]); % [N x 6 x Mi] reshaped into [N x 6*Mi]
EHda = dedenormal(EHda,1);
if restore
    % Check if the backup file is valid
    if s.Ncn~=Ncn || s.Ncx~=Ncx || s.Ncy~=Ncy || s.Mi~=Mi
        fprintf('%s: backup file invalid, not doing backups\n',program);
        fprintf('Ncn=%d, Ncx=%d, Ncy=%d, Mi=%d, but restored Ncn=%d, Ncx=%d, Ncy=%d, Mi=%d\n', ...
            Ncn, Ncx, Ncy, Mi, s.Ncn, s.Ncx, s.Ncy, s.Mi);
        restore = 0;
    end
end
if restore
    fprintf('%s: Restoring ... ', program);
    cn0=s.cn; cx0=s.cx; cy0=s.cy; i10=s.i1; EH=s.EH;
    fprintf('done!\n');
else
    fprintf('%s: Starting new calculation\n', program);
    cn0=1; cx0=1; cy0=1; i10=1;
end
Ntot = 6*Mi*Ncx*Ncy*Ncn;
icur0 = (i10-1) + (cy0-1)*(6*Mi) + (cx0-1)*(Ncy*6*Mi) + (cn0-1)*(Ncx*Ncy*6*Mi);
fprintf('%s: Calculating, currently %d/%d is done\n', program, icur0, Ntot);
tstart=now*24*3600; toutput=tstart; timebkup=tstart;
% Make sure we start the cycles at the right spot
cndone = 0; cxdone = 0; cydone = 0; cnstart = cn0;
for cn=cnstart:Ncn
    tic
    in1=1+(cn-1)*Ln; in2=min(cn*Ln,N);
    if (cndone) cxstart = 1; else cxstart = cx0; end
    for cx=cxstart:Ncx
        ix1=1+(cx-1)*Lx; ix2=min(cx*Lx,Nx);
        expx = exp(1i*k0*bsxfun(@times, x(ix1:ix2), nx(in1:in2).')); % Size [Nx x N] where N,Nx could be limited by chunk length
        %expx = dedenormal(expx);
        if (cxdone) cystart = 1; else cystart = cy0; end
        for cy=cystart:Ncy
            iy1=1+(cy-1)*Ly; iy2=min(cy*Ly,Ny);
            chunk_id = ['(' num2str(cn) '/' num2str(Ncn) ',' num2str(cx) '/' num2str(Ncx) ',' num2str(cy) '/' num2str(Ncy) ')'];
            disp([program ': Chunk ' chunk_id '=(' num2str(in1) ':' num2str(in2) ',' num2str(ix1) ':' num2str(ix2) ',' num2str(iy1) ':' num2str(iy2) ')']);
            expy = exp(1i*k0*bsxfun(@times, y(iy1:iy2).', ny(in1:in2))); % Size [N x Ny] where N,Ny could be limited by chunk length
            %expy = dedenormal(expy);
            if (cydone) i1start = 1; else i1start = i10; end
            for i1 = i1start:(6*Mi)
                icur = (i1-1) + (cy-1)*(6*Mi) + (cx-1)*(Ncy*6*Mi) + (cn-1)*(Ncx*Ncy*6*Mi);
                % Show the progress, but do not do backups
                timec=now*24*3600;
                if timec-toutput>output_interval
                    ttot=timec-tstart;
                    toutput=timec;
                    disp([program ': Done=' num2str(icur/Ntot*100) '%; ' ...
                        'Time=' hms(ttot) ', ETA=' hms(ttot/(icur-icur0)*(Ntot-icur))]);
                end
                if do_backups
                    % Do backups every hour
                    if timec-timebkup>backup_interval
                        timebkup=timec;
                        fprintf('Backing up to %s ... ', bkp_file_ext);
                        status='in progress';
                        save(bkp_file_ext,'EH','i1','cy','cx','cn',...
                            'Ncn','Ncx','Ncy','Mi',... % for validity check
                            'status','-v7.3');
                        fprintf('done!\n');
                    end
                end
                % Do backup before the calculation! In order to restore at
                % the correct point and avoid double counting.
                EH(ix1:ix2, iy1:iy2, i1) = EH(ix1:ix2, iy1:iy2, i1) + expx * bsxfun(@times, EHda(in1:in2, i1), expy); % Size [Nx x Ny]
            end
            cydone = 1;
        end
        cxdone = 1;
    end
    cndone = 1;
    toc
end

EH = reshape(permute(EH, [3 1 2]), [6 Mi Nx Ny]);

if do_backups
    fprintf('Saving results of %s into %s ... ', program, bkp_file_ext);
    status = 'done';
    save(bkp_file_ext, 'EH', 'status', '-v7.3');
    fprintf('done!\n');
end
