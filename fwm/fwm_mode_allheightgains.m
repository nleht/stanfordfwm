function [EHmode,EHmodeadj,namemode,dF,dFadj,udTop,nzTop,FextTop]=fwm_mode_allheightgains(Cmode,k0,h,eground,perm0,phmode,hmode,debugflag)

%% Default arguments
if nargin<8
    debugflag=0
end

%% Modes for select angle
Mmode=length(hmode); nC=length(Cmode);
EHmode=zeros(6,Mmode,nC);
EHmodeadj=zeros(6,Mmode,nC);
namemode=cell(1,nC);
dF=zeros(1,nC);
dFadj=zeros(1,nC);

%% Modes
if debugflag>0
    disp(['Angle ' num2str(phmode*180/pi)]);
end
sp=sin(phmode); cp=cos(phmode);
% Active
rotmxa=[cp -sp 0; sp cp 0; 0 0 1];
% Passive (inverse of active)
rotmxp=[cp sp 0; -sp cp 0; 0 0 1];
M=size(perm0,3);
permph=zeros(3,3,M);
for k=1:M
    permph(:,:,k)=rotmxa*perm0(:,:,k)*rotmxp;
end
dF=zeros(1,nC);
udTop=zeros(4,nC); nzTop=zeros(4,nC); FextTop=zeros(6,4,nC);
for kC=1:nC
    if debugflag>0
        disp(['Mode ' num2str(kC) '/' num2str(nC)]);
    end
    [EHmode(:,:,kC),namemode{kC},dF(kC),udTop(:,kC),nzTop(:,kC),FextTop(:,:,kC)]=fwm_mode_heightgain(Cmode(kC),k0,h,eground,permph,hmode,debugflag);
end

%% Adjoint modes
phadj=pi-phmode;
if debugflag>0
    disp(['Adjoint angle ' num2str(phadj*180/pi)]);
end
sp=sin(phadj); cp=cos(phadj);
% Active
rotmxa=[cp -sp 0; sp cp 0; 0 0 1];
% Passive (inverse of active)
rotmxp=[cp sp 0; -sp cp 0; 0 0 1];
permphadj=zeros(3,3,M);
for k=1:M
    permphadj(:,:,k)=rotmxa*perm0(:,:,k)*rotmxp;
end
dFadj=zeros(1,nC);
udTopadj=zeros(4,nC); nzTopadj=zeros(4,nC); FextTopadj=zeros(6,4,nC);
for kC=1:nC
    if debugflag>0
        disp(['Adjoint mode ' num2str(kC) '/' num2str(nC)]);
    end
    [EHmodeadj(:,:,kC),dummy,dFadj(kC),udTopadj(:,kC),nzTopadj(:,kC),FextTopadj(:,:,kC)]=fwm_mode_heightgain(Cmode(kC),k0,h,eground,permphadj,hmode,debugflag);
end

%% Go over increasing real parts and attach number
[dummy,isort]=sort(real(Cmode));
kTE=1; kTM=1;
for kC=1:nC
    mname=namemode{isort(kC)};
    if strcmp(mname(end-1:end),'TE')
        namemode{isort(kC)}=[mname num2str(kTE)];
        kTE=kTE+1;
    else
        namemode{isort(kC)}=[mname num2str(kTM)];
        kTM=kTM+1;
    end
end
