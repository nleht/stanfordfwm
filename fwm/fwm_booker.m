function [nz,Q,B,L,Ld,Bext,ic,id]=...
    fwm_booker(medium_type,Kinfo,nx,ny,sort_by_power,ax,ay,zd)
%FWM_BOOKER Booker equation solution and Clemmow-Heading operator (dev version)
% NOTE: STILL UNDER DEVELOPMENT! HANDLING OF ALMOST DEFECTIVE MATRICES IS FLAWED!
%
% Generalization of FWM_BOOKER_BIANISOTROPIC, FWM_BOOKER_WARM and
% FWM_CURV_BOOKER.
%
% Usage (please read the definitions below first):
%    [nz,Q]=fwm_booker(medium_type,Kinfo,nx,ny);
% Advanced usage:
%    [nz,Q,B,L,Ld,Bext,ic,id]=...
%        fwm_booker(medium_type,Kinfo,nx,ny,sort_by_power,a1,a2,zd);
%    DEH=Bext*I;
%
% Dimensions of variables:
%   N  - # of different waves (sets of nx,ny)
%   Mt - # of field components (6 in cold medium, 10 in warm medium)
%   Mc, Md - # of continuous and discontinous field components,
%      respectively, total Mc+Md=Mt. Cold medium: Mc=4,Md=2; warm medium:
%      Mc=6,Md=4.
%
% Inputs:
%   medium_type: currently we have 'cold_bianisotropic', 'cold_electron' or
%           'warm_electron'
%   Kinfo - parameters for the constitutive tensor (see below)
%   nx,ny - horizontal refractive indeces nx=kx/k0, ny=ky/k0;
%           1D arrays of length N (if ny is scalar, it is
%           extended to be a 1D array filled with the same value).
%
% Optional inputs:
%   sort_by_power - boolean, default==false. If true, sort the output nz by
%           power direction in the mode; if false (default), but imaginary
%           part of nz.
%           Implementation note: if the imaginary parts are zero, the
%           sorting is by real part, implemented by multiplication by
%           exp(i*sortangle) and again sorting by imaginary part.
%           "sortangle" is a small angle, hardcoded to be =pi/100.
%   ax,ay - dimensionless curvatures. Note: hx=1+ax*zd,hy=1+ay*zd; hx and hy
%           are stretching factors from curvilinear system to Cartesian
%           system in x and y directions, while hz has been chosen to 
%           be one
%   zd    - the altitude of the field point to the surface of the
%           earth (dimensionless, i.e. normalized by k0)
%
% Outputs:
%   nz - Mc x N vertical component of the refractive index
%   Q  - Mt x Mc x N operator converting Mc independent mode amplitudes into
%        Mt field componenets
%
% Optional outputs:
%   B  - Mc x Mc operator which (1) converts surface currents into
%        discontinuity of usually continuous field components; (2) gives
%        the negative energy flux from continuous field components (see
%        below). It is independent of the medium.
%   L  - Mc x Mc x N Clemmow-Heading propagation operator
%   Ld - Md x Mc x N operator to get the dependent fields from the
%        continous ones
%   Bext - Mc x Mt x N matrix converting surface/dual layer currents into
%        the jump in the continuous field components (similar to FWM_DEH);
%        Bext(:,ic,k)==B where "ic" is defined in the next paragraph;
%   ic, id - 1D arrays of lengths Mc and Md, indeces of continuous and
%        dependent components, respectively.
%
% THEORY OVERVIEW
%
% The theory details are described in the writeup "waveguidefwm.pdf".
%
% The combined electro-hydro-dynamic equations for single species (of
% charge q and mass m) plasma are described by
%   (i*k0)^{-1}*D*f=K*f-J/(i*k0)
% where k0=w/c is the vacuum wave number, w is frequency, c is speed of
% light.
%
% The elements of this equation are Mt x 1 vector of fields f, generalized
% current density J (sources), Mt x Mt differential operator
% D and MxM constitutive tensor K. The dimensionality Mt and shape of the
% tensors depend on medium_type. The differential operator may be separated
% into parts responsible for differentiation in each coordinate x,y,z:
%   D=Dx+Dy+Dz=Ux(d/dx)+Uy(d/dy)+Uz(d/dz)
% The rank of Dz is Mc<Mt, which allows the components of vector f to be
% separated into components {fc,fd}, where fc represents fields continuous
% across a z=const plane and fd are the dependent components that can be
% calculated from fc.
%
% The continuous components satisfy
%   (ik0)^{-1}*(d/dz)fc=L*fc+B*j/(i*k0)
% where L (Mc x Mc) is the Clemmow-Heading operator, expressed in terms of
% the transverse gradient (ik0)^{-1}*{d/dx,d/dy}. The sources j=Jceff
% include also the effects of Jd. The matrix B=-Uzcc^{-1} converts
% the surface currents (flowing in the plane) into discontinuity of fc (we
% use a shortened notation, e.g., Uzcc=Uz(ic,ic) etc).
%
% The power flux along z (or bilinear concomitant in Budden's term) is
%   Sz=fc'*Uzcc*fc/(4*Z0)=-fc'*inv(B)*fc/(4*Z0)=-fc'*B*fc/(4*Z0)
% (the factor Z0 may be avoided by different normalization of f, see NOTE
% below in the definition of f for medium_type=='warm_electron'). Also,
% we used inv(B)==B which can be verified explicitly for all cases listed
% below.
%
% fd may be expressed in terms of fc as
%   fd=Ld*fc
% where the operator Ld (Mc x Mt) is a function of (ik0)^{-1}*{d/dx,d/dy}.
%
% In plane-stratified media we can choose transversly harmonic solutions so
% that
%   (ik0)^{-1}*{d/dx,d/dy}={nx,ny}
% are just numbers. Then L is an algebraic matrix, and there are Mc
% different modes, which may be separated into Mc/2 upward and Mc/2
% downward modes.
%
% Here are the details for different medium types:
%
% ***** medium_type=='warm_electron' *****
% Dimensions: Mt==10, Mc==6, Md==4.
%
% The warm plasma was considered by Budden and Jones, "The theory of radio
% windows in planetary magnetospheres" [1987, jstor_2398292].
%
% f is a 10-dimensional vector of variables of dimensionality V/m:
%   f={E_SI,Z0*H_SI,m*w/q*v_SI,Z0*q/m*w*p_SI}={E,H,v,p}
% NOTE1: We can choose a more symmetric fnew=f/sqrt(Z0) in order for fnew^2
%        to have dimensions of power flux, to avoid Z0 in expression for Sz.
% NOTE2: The density perturbation is expressed in terms of pressure
%        perturbation as n_SI=p_SI/(m*cs^2), where 
%           cs^2=gamma*p0/(m*n0)=gamma*kB*T/m - sound speed squared
%           p0, n0 - background pressure and number density
% The components are normalized from SI values in order to convert to V/m:
%   E - vector (dim=3) electric field
%   H - vector (dim=3) magnetic field
%   v - vector (dim=3) velocity
%   p - scalar (dim=1) pressure perturbation
% For scaling, we use
%   Z0=sqrt(mu0/eps0) - vacuum impedance
%   mu0, eps0 - vacuum permeability and permittivity
% The separation into continous and dependent components is
%   fc={Ep,Hp,vz,n} (6 components, where p denotes transverse, {x,y})
%   fd={Ez,Hz,vp} (4 components)
%
% D is a 10x10 differential operator
%   D=(    -rot         )
%     (rot              )
%     (             grad)
%     (         div     )
%
% The generalized 10x10 constitutive tensor is
%   K=( I            iXI               )
%     (       I                        )
%     (-iXI       X(U+iYx)             )
%     (                    1/(Gamma*X) )
% where I is 3x3 unit matrix, Yx is the vector multiplication by Y. Here X,
% Y, U=1+i*Z and Z are plasma parameters, X=wp^2/w^2, Y=q*B0/m, Z=nu/w.
%   wp=sqrt(q^2*n0/(m*eps0)) - plasma frequency
% B0 is a vector background magnetic field, and Gamma=(cs/c)^2. Parameters
% X, Y, Z and Gamma are stored in structure Kinfo as fields X, Y, Z and
% Gamma.
%
%
% ***** medium_type=='cold_bianisotropic' *****
% Dimensions: Mt==6, Mc==4, Md==2.
%
% The field vector is f={E,Z0*H}, the differential operator is
%   D = (    -rot)
%       (rot     )
% and the constitutive tensor K for a general bianisotropic linear medium
% is defined by
%   ( D )   ( epsilon xi ) ( E )     ( E )
%   (   ) = (            ) (   ) = K (   )
%   ( B )   (   eta   mu ) ( H )     ( H )
% 6x6 array K is stored in variable Kinfo. In the case the medium is
% non-magnetic (such as cold plasma), Kinfo is just "epsilon" which is 3x3.
% For isotropic medium, Kinfo is a structure containing permittivity and
% magnetic permeability in fields "eps" and "mu".
%
% The (4 x 4) operator L acts on perpendicular fields only, see Clemmow and
% Heading [1954, doi:10.1017/S030500410002939X]. The continous fields
% are fc={Ep,Hp}, and the dependent fields are fd={Ez,Hz}.
%
% The Clemmow-Heading operator was denoted by L by Pappert and Smith [1972,
% doi:10.1029/RS007i002p00275, equation (1), with transverse coordinates
% x,y] and T by Clemmow and Heading [1954] and Budden [1961: Budden, K. G.,
% "Radio Waves in Ionosphere", Cambridge University Press, 1961, p. 389,
% eq. (18.18)], who used permuted transverse field components.
%
% The independent (continous) fields for different modes are the
% eigenvectors of L.
%
% ***** medium_type='cold_electron' *****
% Dimensions: Mt==6, Mc==4, Md==2.
%
% Particular case of 'warm_electron' with Gamma=0. It is different because
% the number of field components is reduced and is the same as for
% 'cold_bianisotropic'.
%
% Author: Nikolai G. Lehtinen and Linhai Qiu (for curvature)
% See also: FWM_BOOKER_BIANISOTROPIC

persistent lame_count lame_count2
if isempty(lame_count)
    lame_count=20;
end
if isempty(lame_count2)
    lame_count2=20;
end

need_L=(nargout>3);
need_Bext=(nargout>5);
if nargin<9
    debug_flag=1;
end
if nargin<6 || isempty(ax)
    use_curvature=0;
    ax=0; ay=0; zd=0;
else
    % NOTE: we use_curvature even if the given curvature is zero (this is
    % for testing purposes)
    use_curvature=1;
end
if nargin<5
    sort_by_power=[];
end

if isempty(sort_by_power)
    sort_by_power=0;
end

%% nx, ny must be 1D arrays
nx=nx(:);
if length(ny)==1
    ny=ny*ones(size(nx));
end
ny=ny(:);
N=length(nx);
if length(ny)~=N
    error('ny has a size incompatible with nx');
end
if ~(length(ax)==1 && length(ay)==1 && length(zd)==1)
    error('ax, ay and zd should be scalar in this function');
end

%% Basic stuff
syi=[0 -1;1 0]; % sigma_y/i, where sigma_y is the second Pauli matrix
% Antisymmetric symbol
levicivita=cat(3,[0 0 0;0 0 1;0 -1 0],[0 0 -1;0 0 0;1 0 0],[0 1 0;-1 0 0;0 0 0]);
% Electromagnetic part
Uem=zeros(6,6,3);
for k=1:3
    Uem(:,:,k)=kron(syi,-levicivita(:,:,k));
end
I3=eye(3);

%% U-matrix
switch medium_type
    case {'cold_bianisotropic','cold_electron'}
        Mt=6; Mc=4; Md=2;
        U=Uem;
        ic=[1:2 4:5];
        id=[3 6];
    case 'warm_electron'
        Mt=10; Mc=6; Md=4; % dimensions of vectors
        U=zeros(Mt,Mt,3);
        for k=1:3
            U(:,:,k)=[Uem(:,:,k) zeros(6,4); zeros(3,9) I3(:,k) ; zeros(1,6) I3(k,:) 0];
        end
        % The indeces of continuous and dependent components
        ic=[1:2 4:5 9:10];
        id=[3 6 7:8];
    otherwise
        error('unknown medium type')
end

% Note that U has the properties which are necessary for this method:
% U(id,id,:)==0; U(id,ic,3)==0; U(ic,id,3)==0; U(ic,ic,1:2)==0;

%% Setup K tensor
switch medium_type
    case 'cold_bianisotropic'
        tmp=size(Kinfo);
        if (tmp(1)==6 && tmp(2)==6)
            K=Kinfo;
        elseif (tmp(1)==3 && tmp(2)==3)
            K=eye(6); K(1:3,1:3)=Kinfo;
        else
            % Test for Kinfo.eps and Kinfo.mu
            if all(isfield(Kinfo,{'eps','mu'}))
                warning('isotropic case, treated as anisotropic')
                K=eye(6); K(1:3,1:3)=Kinfo.eps*eye(3); K(4:6,4:6)=Kinfo.mu*eye(3);
            else
                error('wrong input for Kinfo')
            end
        end
    case {'warm_electron','cold_electron'}
        % Indeces of various fields in f
        iE=1:3; iv=7:9;
        K=eye(10);
        if Kinfo.X<0
            error('X<0')
        end
        K(iE,iv)=1i*Kinfo.X*I3; K(iv,iE)=-K(iE,iv);
        % Collisions
        K(iv,iv)=(1+1i*Kinfo.Z)*I3; % Z=nu/w
        for k=1:3
            K(iv,iv)=K(iv,iv)-1i*Kinfo.Y(k)*levicivita(:,:,k);
        end
        K(iv,iv)=K(iv,iv)*Kinfo.X;
        if strcmp(medium_type,'warm_electron')
            K(10,10)=1/(Kinfo.Gamma*Kinfo.X);
            % det(K(id,id))=(1+iZ)^2-Y(3)^2=0 only when Y(3)=+-(1+iZ)
            % The resonance is never achieved for collisional case.
        else
            % Cold electrons, just solve the equation for v in terms of E
            % using n=0 and therefore K(iv,iE)*E+K(iv,iv)*v=0
            perm=K(iE,iE)-K(iE,iv)*(K(iv,iv)\K(iv,iE));
            K=eye(6); K(1:3,1:3)=perm;
        end
    otherwise
        error('unknown medium type')
end
if det(K(id,id))==0
    error('Resonance!')
end

%% Curvature
if use_curvature
    hx=1+ax*zd;
    hy=1+ay*zd;
    switch medium_type
        case {'cold_bianisotropic','cold_electron'}
            Ix=zeros(4,4); Iy=Ix; Ix(1,1)=1; Ix(3,3)=1; Iy(2,2)=1; Iy(4,4)=1;
            Lcurv=ax/(1i*hx)*Ix+ay/(1i*hy)*Iy;
        case 'warm_electron'
            if (a1~=0 || a2~=0)
                error('not implemented')
            else
                Lcurv=zeros(Mc,Mc);
            end
        otherwise
            error('unknown medium type')
    end
else
    Lcurv=zeros(Mc,Mc); % Required by parfor
end

%% Go over horizontal n
L=zeros(Mc,Mc,N);
Ld=zeros(Md,Mc,N);
B=-inv(U(ic,ic,3)); % also =-U(ic,ic,3) from theory
Ux=U(:,:,1); Uy=U(:,:,2);
if use_curvature
    Ux=Ux/hx; Uy=Uy/hy;
end
sortangle=pi/100; % if imaginary part is extremely small, we can sort by real part
nz=zeros(Mc,N);
Q=zeros(Mt,Mc,N);
if need_Bext
    % Matrix converting current to field change, similar to DEH
    Bext=zeros(Mc,Mt,N); Bext(:,ic,:)=repmat(B,[1 1 N]);
end
for knp=1:N % may be parallellized, somehow gets slower (?)
    % knp
    nx0=nx(knp); ny0=ny(knp);
    A=nx0*Ux+ny0*Uy-K; % Watch out: Ux, Uy are different with curvature.
    %% Calculate L and Ld
    % Operator to convert fc into fd
    Ld1=K(id,id)\A(id,ic); % Md x Mc
    L1=B*(A(ic,id)*Ld1-K(ic,ic));
    if use_curvature
        L1=L1-Lcurv;
    end
    if need_L
        L(:,:,knp)=L1;
        Ld(:,:,knp)=Ld1;
    end
    if need_Bext
        Bext(:,id,knp)=B*A(ic,id)/K(id,id);
    end
    %% Eigenmodes
    % MATLAB keeps failing to find independent eigenvectors when multiple
    % eigenvalues are present
    [Qc1,d]=eig(L1);
    % Check if the eigenvectors are distinct
    if abs(det(Qc1))<1e-5*max(abs(Qc1(:)))
        debug_string=['nperp=(' num2str(nx0) ',' num2str(ny0) '): failed to find good eigenvectors, applying the lamest fix ever'];
        if lame_count>0
            disp(debug_string)
            lame_count=lame_count-1;
        elseif lame_count==0
            disp('Suppressing the "lamest fix ever" debug output')
            lame_count=lame_count-1;
        end
        % [Qc1,d]=eig(L1,'nobalance'); % does not help
        if 0 % save these weird matrices, debugging only!
            if exist('weirdstuff.mat')
                tmp2=load('weirdstuff.mat');
                M={tmp2.M{:},L1};
            else
                M={L1};
            end
            save('weirdstuff','M');
        end
        % Apply the lamest fix ever. Note that it will also "correct" the
        % defective matrices.
        fluctuation=10*eps;
        debug_display=true;
        while 1
            [Qc1,d]=eig(L1+randn(Mc,Mc)*fluctuation);
            if fluctuation>0.1
                %fname=input('Save L1 to a file? Fname: ','s');
                %if ~isempty(fname)
                %    save(fname,'L1');
                %end
                save('erroneous_L1','L1')
                error('internal MATLAB bug error crash incorrect algorithm a program that works 99.9% of the time is 100% useless')
            end
            if abs(det(Qc1))>=1e-5*max(abs(Qc1(:)))
                break;
            else
                fluctuation=fluctuation*10;
                if lame_count2>0
                    if lame_count<=0 && debug_display
                        % Display the output that was suppressed
                        disp(debug_string)
                        debug_display=false; % don't display this string anymore
                    end
                    disp([' > failed again, new fluctuation=' num2str(fluctuation)])
                    if lame_count<=0
                        lame_count2=lame_count2-1;
                    end
                elseif lame_count2==0
                    disp('Suppressing the "failed_again" debug output')
                    lame_count2=lame_count2-1;
                end
            end
        end
    end
    nz1=diag(d);
    if sort_by_power
        % Sort by descending power flux Sz=f'*(-B/(4*impedance0)*f
        [~,ii]=sort(diag(real(Qc1'*B*Qc1)));
    else
        [~,ii]=sort(-imag(nz1*exp(1i*sortangle)));
    end
    nz(:,knp)=nz1(ii);
    Qc1=Qc1(:,ii);
    Q1=zeros(Mt,Mc); Q1(ic,:)=Qc1; Q1(id,:)=Ld1*Qc1;
    Q(:,:,knp)=Q1;
end
