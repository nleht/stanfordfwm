function [Pu,Pd]=fwm_PuPd(dz,nz)
%FWM_PUPD Transport matrices thru layers in FWM
% Usage: [Pu,Pd]=fwm_PuPd(dz,nz);
% Inputs: dz=diff(zd) (length M, dimensionless), nz (Mc x M)
% Outputs: Pu, Pd (Mc/2 x Mc/2 x M)

%% The matrices for transporting (u,d) through a slab k, z(k)<z<z(k+1), k=1:M-1
[Mc, M1]=size(nz);
if length(dz)~=M1
    size(dz)
    size(nz)
    error('wrong size')
end
kh=nz.*repmat(dz(:).',Mc,1);
kh(Mc/2+1:Mc,:)=-kh(Mc/2+1:Mc,:); % so that imag(kh)>0
Ep=exp(1i*kh);
% Transporting up (|Pu|<=1)
Pu=zeros(Mc/2,Mc/2,M1); Pd=Pu;
for c=1:Mc/2
    Pu(c,c,:)=Ep(c,:); Pd(c,c,:)=Ep(Mc/2+c,:);
end
% Note that for evanescent waves, |Pu|<=1
% Transporting down (|Pd|<=1)
