# StanfordFWM Manuals

Stanford Full-Wave Method (StanfordFWM) code is code for calculation of electromagnetic wave propagation in stratified media, plus some other useful routines, all written in MATLAB.

Just a reminder: the files here are protected by copyright, even though there is no license attached, except for some general-purpose files which have BSD license and are free to use according to that license. If you would like to use the models in your work, I would prefer if you contact the author (@nleht).

All work using code in [`fwm`](fwm) folder **must** cite the following articles:

* Lehtinen, N. G. and U. S. Inan (2008), Radiation of ELF/VLF waves by harmonically varying currents into a stratified ionosphere with application to radiation by a modulated electrojet, _J. Geophys. Res._, **113**, A06301, [`doi:10.1029/2007JA012911`](http://dx.doi.org/10.1029/2007JA012911).

* Lehtinen, N. G. and U. S. Inan (2009), Full-wave modeling of transionospheric propagation of VLF waves, _Geophys. Res. Lett._, **36**, L03104, [`doi:10.1029/2008GL036535`](http://dx.doi.org/10.1029/2009JA014776).

This folder, beside these two papers, contains an unpublished manuscript entitled [**Full-wave method for stratified media**](Full_Wave_Method_for_Stratified_Media.pdf). It may not be further distributed, and it is here only to help understand the code, in particular:
* implementation of the Booker equation solution in file [`fwm_booker_bianisotropic.m`](fwm/fwm_booker_bianisotropic.m) which was implemented in a different way in _Lehtinen and Inan_ (2008)
* implementation of arbitrary sources in file [`fwm_deh.m`](fwm/fwm_deh.m). Only electric current source in vacuum was described in  _Lehtinen and Inan_ (2008).

