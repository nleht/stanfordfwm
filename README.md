# StanfordFWM

Stanford Full-Wave Method (StanfordFWM) code for calculation of electromagnetic wave propagation in stratified media, plus some other useful routines, all written in MATLAB.

Below are example outputs from running [axially symmetric](examples/fwm_example_horizcompare_axisymmetric.m) and [general](examples/fwm_example_horizcompare_nonaxisymmetric.m) versions of the same calculation. Axially-symmetric calculation may be done only with a dielectric permittivity tensor which is axially-symmetric in respect to the vertical axis, while the general calculation may have any direction of the geomagnetic field:

| *Poynting flux calculated with the axis-symmetric subroutine [`fwm_axisymmetric_any_source.m`](fwm/fwm_axisymmetric_any_source.m)* | *Poynting flux calculated with the general subroutine [`fwm_nonaxisymmetric.m`](fwm/fwm_nonaxisymmetric.m)* |
| ---      | ---      |
| ![Poynting flux (axis-symmetric)](fig/Sz_ax.png)   | ![Poynting flux (non-axis-symmetric)](fig/Sz_nonax.png)   |

Just a reminder: the files here are protected by copyright, even though there is no license attached, except for some general-purpose files which have BSD license and are free to use according to that license. If you would like to use the models in your work, I would prefer if you contact the author (@nleht).

All work using code in [`fwm`](fwm) folder **must** cite the following articles:

* Lehtinen, N. G. and U. S. Inan (2008), Radiation of ELF/VLF waves by harmonically varying currents into a stratified ionosphere with application to radiation by a modulated electrojet, _J. Geophys. Res._, **113**, A06301, [`doi:10.1029/2007JA012911`](http://dx.doi.org/10.1029/2007JA012911).

* Lehtinen, N. G. and U. S. Inan (2009), Full-wave modeling of transionospheric propagation of VLF waves, _Geophys. Res. Lett._, **36**, L03104, [`doi:10.1029/2008GL036535`](http://dx.doi.org/10.1029/2009JA014776).

