#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "IRI2016f2c.h"

int main(void) {
	int iy,mmdd,numsteps;
	double xlat,xlon,hour,vbeg,vend,vstp;
	double outf[20000],oar[100],h[100];
	int k;
	char data_dir[1000];
	
	/* The argument is the directory containing the IRI data, i.e. files in 00_ccir-ursi.tar, 00_iri2016.tar, and also apf107.dat and ig_rz.dat */
	strcpy(data_dir,getenv("DATA_DIR"));
	strcat(data_dir,"/IRI2016data");
	if(iri_constructor(data_dir)) {
		fprintf(stderr,"iri_constructor failed\n");
		return 1; }

	xlat=0.; xlon=0.; iy=2010; mmdd=-111; hour=0.;
	vbeg=150.; vend=200.; vstp=5.;
	printf("Starting program ...\n");
	if (iri_sub(	/* inputs */
			xlat,xlon,iy,mmdd,hour,vbeg,vend,vstp,
			/* outputs */
			outf,oar,&numsteps,h)) {
		fprintf(stderr,"iri_sub failed\n");
		return 1;
	}
	printf("Finished!\n");
	fflush(stdout);
	printf("Here are the results:\n");
	printf("OUTF:\n");
	printf("%10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s\n",
		"h, km","Ne(m^-3)","Tn(K)","Ti(K)","Te(K)",
		"N[O+]","N[H+]","N[He+]","N[O2+]",
		"N[NO+]","N[Clus]","N[N+]");
	for (k=0; k<numsteps; k++){
		printf("%10.1f  %10.4e  %10.1f  %10.1f  %10.1f  "
			"%10.4e  %10.4e  %10.4e  %10.4e  %10.4e  %10.4e  %10.4e\n",
			h[k],outf[k*20], outf[k*20+1], outf[k*20+2], outf[k*20+3],
			outf[k*20+4], outf[k*20+5], outf[k*20+6], outf[k*20+7],
			outf[k*20+8], outf[k*20+9], outf[k*20+10]);
		//for (k2=0; k2<20; k2++) printf("%g ",outf[k1*20+k2]);
		//printf("\n");
	}
	printf("OAR:\n");
	for (k=0; k<86; k++) printf("%g ",oar[k]);
	printf("\n");
	if(iri_destructor()) { fprintf(stderr,"iri_destructor failed\n"); return 1; }
	return 0;
}

