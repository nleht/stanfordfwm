#!/usr/bin/python3
import os
import numpy as np
from matplotlib import pyplot as plt
from IRIpy import IRIClass, str_coor

#%% Simple interface
IRI = IRIClass(os.path.join(os.environ['DATA_DIR'], 'IRI_data'))
lat, lon = 39.732323572674794, -105.00513971586595
year, month, day, hour = 2020, 7, 1, 0
UTC = False
for run in range(2):
    # This subroutine is a bit more cryptic than just IRI.__call__(...)
    h, outf, oarr = IRI.iri_sub(0, lat, lon, year, 100*month+day, hour+int(UTC)*25, 100, 200, 10)
    print(outf[:,4])
    print(50*'*')

#%% A more convenient interface
# No extras if output='default' (default)
h, ionosphere, extras = IRI(lat, lon, year, month=month, day=day, hour=hour, UTC=False,
                     hstart=0, hstop=200, hstep=1, output='extras')

plt.figure(1)
plt.clf()
plt.semilogx(ionosphere['Ne(m^-3)'], h)
plt.title('Profile for {:s} {:d}-{:02d}-{:02d} {:02d}:00 {:s}'.format(
    str_coor(lat, lon), year, month, day, hour, ('UTC' if UTC else 'LOCAL')))
plt.xlabel(r'$N_e$, m$^{-3}$')
plt.ylabel('h, km')
plt.grid('True')

#%% IRIWeb
# Arguments are alati, along, iyyyy, mmdd, iut, dhour, height, h_tec_max, vbeg, vend, vstp):
h, outf, oarr = IRI.iri_web(lat, lon, year, 100*month+day, 0, hour, 300, 700, 0, 200, 1)
extras_web = IRI.interpret_oarr(oarr)
ionosphere_web = IRI.interpret_outf(outf)
print('Only in IRIWeb: TEC =', extras_web['TEC/m-2'], ', TEC_top/TEC =',
      extras_web['TEC_top/TEC*100'], '%')
print('or, ', oarr[IRI.oarr_special_IRIWeb_index])

#%% Compare to IGRF
xl, icode, dipl, babs = IRI.igrf_sub(lat, lon, year, extras['HMF2/KM'])
print('Dip latitude (deg) =', extras['DIP LATITUDE/deg'], '(IRI),', dipl, '(IGRF)')

#%% Insanity check
hundred = np.zeros((len(h),))
for ion in ['O+', 'H+', 'He+', 'O2+', 'NO+', 'N+', 'Clus']:
    tmp = ionosphere['N['+ion+'](%)'].copy()
    tmp[np.isnan(tmp)]=0
    hundred += tmp
hundred[np.isnan(ionosphere['Ne(m^-3)'])] = np.nan
error = hundred-100
print('Ion densities sum up to electron density with error of',
      np.nanmin(error), '--', np.nanmax(error), '%')
