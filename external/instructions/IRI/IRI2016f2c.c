/* File: IRIc.c */
/* Ripoff from IRIpy.py */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "IRI2016f.h"

#define ROUND(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))

#define USE_IRIWEB 0

/* Keep these variables global */
static real outf_sub[20000]; // 20 x 1000 fortran, nummax=1000
static real oarr_sub[100];
static logical jf[50];
static const integer nummax = 1000;
static integer jmag;
static integer ivar;
static char *iri_dir=0;
static char cwd[10000];
#if USE_IRIWEB
/* Variables for iri_web */
static real h[1000];
static real outf_web[20000]; // 20 x 1000 fortran, nummax=1000
static real oarr_web[100000]; // 100 x 1000 fortran
static real hxx=0;
static real htec_max=0;
#endif

int iri_constructor(char *iri_dir_in) {
	int k;

	if (!(iri_dir=(char*)malloc((strlen(iri_dir_in)+1)*sizeof(char))) ) {
		fprintf(stderr,"malloc() error\n");
		return 1;
	}
	strcpy(iri_dir,iri_dir_in);
	/* change directory to IRI */
	if (!getcwd(cwd, sizeof(cwd))) {
		fprintf(stderr,"getcwd() error\n");
		return 1; }
	if (chdir(iri_dir)) {
		fprintf(stderr,"cannot change to IRI directory %s\n",iri_dir);
		return 1; }
	read_ig_rz_();
	readapf107_();
	/* change directory back */
	if (chdir(cwd)) {
		fprintf(stderr,"cannot change back to %s\n",cwd);
		return 1;
	}
	jmag=0; ivar=1;
	for (k=0; k<100; k++) {
		oarr_sub[k]=-1.0;
#if USE_IRIWEB
		oarr_web[k]=-1.0;
#endif
	}
	for (k=0; k<50; k++) jf[k]=1;
//     defaults for jf(1:50)
//     jf(1)=.false.      ! f=no electron densities 
//     jf(2)=.false.      ! f=no temperatures 
//     jf(3)=.false.      ! f=no ion composition 
	jf[3]=0;	// t=B0table f=other models (f)
	jf[4]=0;	// t=CCIR  f=URSI foF2 model (f)
	jf[5]=0;	// t=DS95+DY85   f=RBV10+TTS03 (f)
//     jf(7)=.false.      ! t=tops f10.7<188 f=unlimited (t)
	jf[20]=0;	// f=ion drift not computed (f)
//     jf(22)=.false.     ! ion densities in m-3 (t)
	jf[22]=0;	// t=AEROS/ISIS f=TTS Te with PF10.7
//     jf(24)=.false.     ! t=D-reg-IRI-1990 f=FT-2001 (t)
//     jf(26)=.false.	  ! f=STORM model turned off (t)
	jf[27]=0;	// f=spread-F not computed (f)
	jf[28]=0;	// t=old  f=New Topside options (f)
	jf[29]=0;	// t=corr f=NeQuick topside (f)
//     jf(31)=.false.     ! t=B0ABT f=Gulyaeva (t)
	jf[32]=0;	// f=auroral boundary off (f)
//     jf(34)=.false. 	  ! t=messages on 
	jf[34]=0; 	// f=auroral E-storm model off
//     jf(36)=.false. 	  ! t=hmF2 w/out foF2_storm f=with
//     jf(37)=.false. 	  ! t=topside w/out foF2_storm f=with
//     jf(38)=.false. 	  ! t=WRITEs off in IRIFLIP f=on 
	jf[38]=0;	// new hmF2 models 
//     jf(40)=.false. 	  ! t=AMTB-model, f=Shubin-COSMIC model 
//     jf(41)=.false. 	  ! COV=f(IG12) (IRI before Oct 2015) 
//     jf(42)=.false. 	  ! Te w/o PF10.7 dependance 
//     jf(43)=.false. 	  ! B0 user input
	return 0;
}

int iri_sub(	/* inputs */
		double xlat, double xlon,int iy, int mmdd, double hour,
		double vbeg, double vend, double vstp,
		/* outputs */ double *outfp, double *oarp, int *numstepsp, double *hp) {
	real alati,along,dhour,heibeg,heiend,heistp;
	integer iyyyy,mmddp;
	int numsteps,k,k1;

	// Convert types
	alati=xlat; along=xlon; iyyyy=iy; mmddp=mmdd; dhour=hour;
	heibeg=vbeg; heiend=vend; heistp=vstp;

	numsteps = ROUND((vend-vbeg)/vstp) + 1;
	if (numsteps > nummax || numsteps < 0) {
		fprintf(stderr,"too many steps\n");
		return 1; }
	/* change directory to IRI */
	if (!getcwd(cwd, sizeof(cwd))) {
		fprintf(stderr,"getcwd() error\n");
		return 1; }
	if (chdir(iri_dir)) {
		fprintf(stderr,"cannot change to IRI directory %s\n",iri_dir);
		return 1; }
	printf("Calling iri_sub_ ...\n");
	iri_sub_(
		/* inputs */
		jf,&jmag,&alati,&along,&iyyyy,&mmddp,&dhour,&heibeg,&heiend,&heistp,
		/* outputs */
		outf_sub,oarr_sub);
	/* change directory back */
	if (chdir(cwd)) {
		fprintf(stderr,"cannot change back to %s\n",cwd);
		return 1;
	}
	printf(" ... done!\n"); fflush(stdout);
	/* Copy the results to the externally provided array */
	for (k=0; k<numsteps; k++) {
		hp[k]=vbeg+k*vstp;
		for (k1=0; k1<20; k1++) outfp[20*k+k1]=outf_sub[20*k+k1];
	}
	for (k=0; k<100; k++) oarp[k]=oarr_sub[k];
	*numstepsp=numsteps;
	return 0;
}

#if USE_IRIWEB
int iri_web(	/*inputs*/
		double xlat, double xlon, int iy, int mmdd, int iut, double hour,
		double vbeg, double vend, double vstp,
		/*outputs*/ float **outfp, float **oarp, int *numstepsp, float **hp) {
	real alati,along,dhour,heibeg,heiend,heistp;
	integer iyyyy,mmddp,iuti;
	int k,numsteps;

	// Convert types
	alati=xlat; along=xlon; iyyyy=iy; mmddp=mmdd; dhour=hour;
	heibeg=vbeg; heiend=vend; heistp=vstp;
	iuti=iut;
	numsteps = ROUND((vend-vbeg)/vstp) + 1;
	if (numsteps > nummax || numsteps < 0) {
		fprintf(stderr,"too many steps\n");
		return 1; }
	/* change directory to IRI */
	if (!getcwd(cwd, sizeof(cwd))) {
		fprintf(stderr,"getcwd() error\n");
		return 1; }
	if (chdir(iri_dir)) {
		fprintf(stderr,"cannot change to IRI directory %s\n",iri_dir);
		return 1; }
	printf("Calling iri_web_ ...\n");
	iri_web_(/*inputs*/
		&jmag, jf, &alati, &along, &iyyyy, &mmddp, &iuti, &dhour, &hxx, &htec_max, &ivar, &heibeg, &heiend, &heistp,
		/*outputs*/
		outf_web, oarr_web);
	/* change directory back */
	if (chdir(cwd)) {
		fprintf(stderr,"cannot change back to %s\n",cwd);
		return 1;
	}
	printf(" ... done!\n"); fflush(stdout);
	for (k=0; k<numsteps; k++) h[k]=vbeg+k*vstp;
	*outfp=outf_sub; *oarp=oarr_sub; *hp=h; *numstepsp=numsteps;
	return 0;
}
#endif

int iri_destructor(void) {
	free(iri_dir);
	iri_dir=0;
	return 0;
}

