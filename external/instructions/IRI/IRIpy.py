#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains class IRIClass, see its documentation

Author: Nikolai G. Lehtinen
"""

import numpy as np
import os
import IRI2016f2py

def str_coor(lat, lon):
    "String representation of latitude and longitude used in IRI inputs"
    slat = '{:.2f}N'.format(lat) if lat>=0 else '{:.2f}S'.format(-lat)
    slon = '{:.2f}E'.format(lon) if lon>=0 else '{:.2f}W'.format(-lon)
    return slat+','+slon

class IRIClass:
    """An interface to the binary IRI2016 package.
    
    Initialization:
        IRI = IRIClass(data_dir)
    Input:
        data_dir - a string which points to a directory containing files
                apf107.dat, ccir*.asc, [d]igrf*.dat, ig_rz.dat, mcsat*.dat, ursi*.asc
            On my system, all data is stored in directory $DATA_DIR, so
                data_dir = os.path.join(os.environ['DATA_DIR'],'IRI2016data')
    
    Usage:    
        >>> lat, lon = 16.4274, -77.6599
        >>> year, month, day, hour = 2016, 10, 21, 0
        >>> h, ionosphere = IRI(lat, lon, year, month=month, day=day, hour=hour,
                            hstart=0, hstop=500, hstep=1)
    or
        >>> h, ionosphere, extras = IRI(lat, lon, year, month=month, day=day, hour=hour,
                                    hstart=0, hstop=500, hstep=1, output='extras')
    Outputs:
        
        h - height in km;
        
        ionosphere - a dict with fields:
            'Ne(m^-3)', 'Tn(K)', 'Ti(K)', 'Te(K)', 'N[O+](%)', 'N[H+](%)',
            'N[He+](%)', 'N[O2+](%)', 'N[NO+](%)', 'N[Clus](%)', 'N[N+](%)'
            The unknown values are NaNs.
  
        """
    def __init__(self, thedir):
        self.dir=thedir
        self.version='IRI2016, Version Nov 5 2021';
        # Allocate arrays for internal use
        self.outf_sub=np.zeros((20000,),dtype=np.float32)
        self.oarr_sub=np.zeros((100,),dtype=np.float32)
        self.outf_web=np.zeros((20000,),dtype=np.float32)
        self.oarr_web=np.zeros((100000,),dtype=np.float32)
        # Set the defaults -- you can change them (interactively! not in this file of course)
        # C INPUT:  JF(1:50)      true/false switches for several options
        self.jmag=0
        self.ivar=1
        self.jf = np.ones((50,),dtype=np.intc)
        # defaults for jf(1:50)
        #     jf(1)=.false.      ! f=no electron densities 
        #     jf(2)=.false.      ! f=no temperatures 
        #     jf(3)=.false.      ! f=no ion composition 
        self.jf[3]=0 		# t=B0table f=other models (f)
        self.jf[4]=0 		# t=CCIR  f=URSI foF2 model (f)
        self.jf[5]=0 		# t=DS95+DY85   f=RBV10+TTS03 (f)
        #     jf(7)=.false.      ! t=tops f10.7<188 f=unlimited (t)
        self.jf[20]=0 		# f=ion drift not computed (f)
        #     self.jf[21]=0       # ion densities in m-3 (t)
        self.jf[22]=0 		# t=AEROS/ISIS f=TTS Te with PF10.7
        #     jf(24)=.false.     ! t=D-reg-IRI-1990 f=FT-2001 (t)
        #     jf(26)=.false.	  ! f=STORM model turned off (t)
        self.jf[27]=0		# f=spread-F not computed (f)
        self.jf[28]=0		# t=old  f=New Topside options (f)
        self.jf[29]=0		# t=corr f=NeQuick topside (f)
        #     jf(31)=.false.     ! t=B0ABT f=Gulyaeva (t)
        self.jf[32]=0		# f=auroral boundary off (f)
        #     jf(34)=.false. 	  ! t=messages on 
        self.jf[34]=0		# f=auroral E-storm model off
        #     jf(36)=.false. 	  ! t=hmF2 w/out foF2_storm f=with
        #     jf(37)=.false. 	  ! t=topside w/out foF2_storm f=with
        #     jf(38)=.false. 	  ! t=WRITEs off in IRIFLIP f=on 
        self.jf[38]=0		# new hmF2 models 
        #     jf(40)=.false. 	  ! t=AMTB-model, f=Shubin-COSMIC model 
        #     jf(41)=.false. 	  ! COV=f(IG12) (IRI before Oct 2015) 
        #     jf(42)=.false. 	  ! Te w/o PF10.7 dependance 
        #     jf(43)=.false. 	  ! B0 user input
        self.jf_default = self.jf.copy()
        # Special elements of OARR
        self.oarr_input_output_index = np.array([1,2,3,4,5,6,10,15,16,33,39,41,46])-1
        self.oarr_special_IRIWeb_index = np.array([37, 38])-1
        # Initialize IRI2016
        curdir=os.getcwd()
        os.chdir(self.dir)
        IRI2016f2py.read_ig_rz()
        IRI2016f2py.readapf107()
        os.chdir(curdir)
        
    def set_default(self):
        self.jmag=0
        self.ivar=1
        self.jf = self.jf_default.copy()
        
    def iri_sub(self, is_geomag, alati, along, iyyyy, mmdd, dhour, vbeg, vend, vstp):
        """       SUBROUTINE IRI_SUB(is_geomag,ALATI,ALONG,IYYYY,MMDD,DHOUR,
             &    HEIBEG,HEIEND,HEISTP,OUTF,OARR)
        C-----------------------------------------------------------------
        C
        C         is_geomag          =0 geographic   = 1 geomagnetic coordinates
        C         ALATI,ALONG   LATITUDE NORTH AND LONGITUDE EAST IN DEGREES
        C         IYYYY         Year as YYYY, e.g. 1985
        C         MMDD (-DDD)   DATE (OR DAY OF YEAR AS A NEGATIVE NUMBER)
        C         DHOUR         LOCAL TIME (OR UNIVERSAL TIME + 25) IN DECIMAL 
        C                          HOURS
        C         HEIBEG,       HEIGHT RANGE IN KM; maximal 100 heights, i.e.
        C          HEIEND,HEISTP        int((heiend-heibeg)/heistp)+1.le.100
        """
        numsteps = np.round((vend-vbeg)/vstp).astype(np.int) + 1;
        self.outf_sub[:] = 0.
        self.oarr_sub[:] = 0.
        curdir = os.getcwd()
        os.chdir(self.dir) # needed to read the files
        IRI2016f2py.iri_sub(self.jf, is_geomag, alati, along, iyyyy, mmdd, dhour,
                            vbeg, vend, vstp,
                            self.outf_sub,self.oarr_sub)
        os.chdir(curdir)
        outf = self.outf_sub[:20*numsteps].reshape((numsteps,20)).astype(np.float)
        oarr = self.oarr_sub[:100].astype(np.float)
        h=np.linspace(vbeg,vend,numsteps)
        return h, outf, oarr
    
    def interpret_outf(self, outf):
        assert self.jf[21]==1 # ion densities in persent
        ionosphere = {'Ne(m^-3)':outf[:,0], 'Tn(K)':outf[:,1], 'Ti(K)':outf[:,2],
               'Te(K)':outf[:,3], 'N[O+](%)':outf[:,4], 'N[H+](%)':outf[:,5],
               'N[He+](%)':outf[:,6],
               'N[O2+](%)':outf[:,7], 'N[NO+](%)':outf[:,8],
               'N[Clus](%)':outf[:,9], 'N[N+](%)':outf[:,10]}
        return ionosphere
        
    def interpret_oarr(self, oarr):
        # Have no idea what the following mean, lifted from the comment to IRI_SUB in Fortran
        extras = {'NMF2/M-3':oarr[0], 'HMF2/KM':oarr[1],
                  'NMF1/M-3':oarr[2], 'HMF1/KM':oarr[3],
                  'NME/M-3':oarr[4], 'HME/KM':oarr[5],
                  'NMD/M-3':oarr[6], 'HMD/KM':oarr[7],
                  'HHALF/KM':oarr[8], 'B0/KM':oarr[9],
                  'VALLEY-BASE/M-3':oarr[10], 'VALLEY-TOP/KM':oarr[11],
                  'TE-PEAK/K':oarr[12], 'TE-PEAK HEIGHT/KM':oarr[13],
                  'TE-MOD(300KM)':oarr[14], 'TE-MOD(400KM)/K':oarr[15],
                  'TE-MOD(600KM)':oarr[16], 'TE-MOD(1400KM)/K':oarr[17],
                  'TE-MOD(3000KM)':oarr[18],  'TE(120KM)=TN=TI/K':oarr[19],
                  'TI-MOD(430KM)': oarr[20], 'X/KM, WHERE TE=TI':oarr[21],
                  'SOL ZENITH ANG/DEG':oarr[22], 'SUN DECLINATION/DEG':oarr[23],
                  'DIP/deg':oarr[24], 'DIP LATITUDE/deg':oarr[25],
                  'MODIFIED DIP LAT.':oarr[26], 'Geographic latitude':oarr[27],
                  'sunrise/dec. hours':oarr[28], 'sunset/dec. hours':oarr[29],
                  'ISEASON (1=spring)':oarr[30], 'Geographic longitude':oarr[31],
                  'Rz12':oarr[32], 'Covington Index':oarr[33],
                  'B1':oarr[34], 'M(3000)F2':oarr[35],
                  'TEC/m-2':oarr[36], 'TEC_top/TEC*100':oarr[37],
                  'gind (IG12)':oarr[38], 'F1 probability':oarr[39], 
                  'F10.7 daily':oarr[40],  'c1 (F1 shape)':oarr[41],
                  'daynr':oarr[42], 'equatorial vertical ion drift in m/s':oarr[43],
                  'foF2_storm/foF2_quiet':oarr[44],
                  'F10.7_81':oarr[45], 'foE_storm/foE_quiet':oarr[46],
                  'spread-F probability':oarr[47],          
                  'Geomag. latitude':oarr[48], 'Geomag. longitude':oarr[49],
                  'ap at current time':oarr[50], 'daily ap':oarr[51],
                  'invdip/degree':oarr[52], 'MLT-Te':oarr[53],
                  'CGM-latitude':oarr[54], 'CGM-longitude':oarr[55],
                  'CGM-MLT':oarr[56], 'CGM lat eq. aurl bodry':oarr[57],
                  'CGM-lati for MLT=0..23':oarr[58:82],
                  'Kp at current time':oarr[82], 'magnetic declination':oarr[83],
                  'L-value':oarr[84], 'dipole moment':oarr[85]}
        return extras
    
    def __call__(self, lat, lon, year, month=None, day=None, DOY=None,
                 hour=None, UTC=False, geomag=False, hstart=50.,
                 hstop=550., hstep=5., output='default'):
        "Interface to iri_sub with interpretation of data"
        use_doy = (DOY is not None)
        if use_doy:
            mmdd = -DOY
        else:
            assert month is not None and day is not None
            mmdd = 100*month+day
        h, outf, oarr = self.iri_sub(geomag, lat, lon, year, mmdd, 25*int(UTC) + hour, hstart, hstop, hstep)
        # return (h,ion)
        # Rearrange ionosphere to a dictionary
        outf1 = outf.copy()
        outf1[outf==-1]=np.nan
        ionosphere = self.interpret_outf(outf1)
        oarr1 = oarr.copy()
        oarr1[self.oarr_special_IRIWeb_index] = np.nan
        extras = self.interpret_oarr(oarr1)
        if output=='extras':
            return h, ionosphere, extras
        elif output=='raw':
            return h, outf, oarr
        else:
            return h, ionosphere
        
    def iri_web(self, alati, along, iyyyy, mmdd, iut, dhour, height, h_tec_max, vbeg, vend, vstp):
        numsteps = np.round((vend-vbeg)/vstp).astype(np.int) + 1;
        curdir=os.getcwd()
        os.chdir(self.dir) # needed to read the files
        IRI2016f2py.iri_web(self.jmag, self.jf, alati, along, iyyyy, mmdd, iut, dhour,
                            height, h_tec_max, self.ivar, vbeg, vend, vstp,
                            self.outf_web, self.oarr_web)
        os.chdir(curdir)
        outf = self.outf_web[:20*numsteps].reshape((numsteps,20)).astype(np.float)
        oarr = self.oarr_web[:100].astype(np.float)
        h=np.linspace(vbeg,vend,numsteps)
        return h, outf, oarr
    
    def igrf(self, iy, nm, r, t, f):
        """INPUT PARAMETERS:
            IY  -  YEAR NUMBER (FOUR-DIGIT; 1965 &LE IY &LE 2005)
            NM  -  HIGHEST ORDER OF SPHERICAL HARMONICS IN THE SCALAR POTENTIAL (NM &LE 10)
            R,T,F -  SPHERICAL COORDINATES (RADIUS R IN UNITS RE=6371.2 KM, GEOGRAPHIC
                COLATITUDE  T  AND LONGITUDE  F  IN RADIANS)
        OUTPUT PARAMETERS:
            BR,BT,BF - SPHERICAL COMPONENTS OF THE MAIN GEOMAGNETIC FIELD IN NANOTESLA"""
        br,bt,bf = IRI2016f2py.igrf(iy,nm,r,t,f)
        return (br,bt,bf)

    def igrf_sub(self, xlat, xlong, year, height):
        """INPUT:
            xlat      geodatic latitude in degrees
            xlong     geodatic longitude in degrees
            year      decimal year (year+(month-0.5)/12.0-0.5 or 
                      year+day-of-year/365 or ../366 if leap year) 
            height    height in km
        OUTPUT:
            xl        L value
            icode      =1  L is correct; =2  L is not correct;
                       =3  an approximation is used
            dipl      dip latitude in degrees
            babs      magnetic field strength in Gauss"""
        xl, icode, dipl, babs = IRI2016f2py.igrf_sub(xlat, xlong, year, height)
        return xl, icode, dipl, babs

if __name__=='__main__':
    data_dir = input('Please enter directory containing IRI2016 *.dat files: ')
    IRI = IRIClass(data_dir)
    