#ifndef IRIC_H
#define IRIC_H

int iri_constructor(char *iri_dir_in);
int iri_sub(	/* inputs */ double xlat, double xlon,int iy, int mmdd, double hour,double vbeg, double vend, double vstp,
		/* outputs */ double *outfp, double *oarp, int *numstepsp, double *hp);
int iri_web(	/*inputs*/ double xlat, double xlon, int iy, int mmdd, int iut, double hour,double vbeg, double vend, double vstp,
		/* outputs */ double *outfp, double *oarp, int *numstepsp, double *hp);
int iri_destructor(void);

#endif /* IRIC_H */

