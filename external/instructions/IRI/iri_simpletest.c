#include <stdio.h>
#include "IRI2016f.h"

#define ROUND(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))

/* Keep these variables global */
static real outf[20000]; // 20 x 1000 fortran, nummax=1000
static real oar[100000]; // 100 x 1000 fortran
static logical jf[50];
static integer nummax = 1000;

int iri_init(void) {
	unsigned int k;

	read_ig_rz_();
	readapf107_();
	for (k=0; k<100; k++) oar[k]=-1.0;
	for (k=0; k<50; k++) jf[k]=1;
//     defaults for jf(1:50)
//     jf(1)=.false.      ! f=no electron densities 
//     jf(2)=.false.      ! f=no temperatures 
//     jf(3)=.false.      ! f=no ion composition 
	jf[3]=0;	// t=B0table f=other models (f)
	jf[4]=0;	// t=CCIR  f=URSI foF2 model (f)
	jf[5]=0;	// t=DS95+DY85   f=RBV10+TTS03 (f)
//     jf(7)=.false.      ! t=tops f10.7<188 f=unlimited (t)
	jf[20]=0;	// f=ion drift not computed (f)
//     jf(22)=.false.     ! ion densities in m-3 (t)
	jf[22]=0;	// t=AEROS/ISIS f=TTS Te with PF10.7
//     jf(24)=.false.     ! t=D-reg-IRI-1990 f=FT-2001 (t)
//     jf(26)=.false.	  ! f=STORM model turned off (t)
	jf[27]=0;	// f=spread-F not computed (f)
	jf[28]=0;	// t=old  f=New Topside options (f)
	jf[29]=0;	// t=corr f=NeQuick topside (f)
//     jf(31)=.false.     ! t=B0ABT f=Gulyaeva (t)
	jf[32]=0;	// f=auroral boundary off (f)
//     jf(34)=.false. 	  ! t=messages on 
	jf[34]=0; 	// f=auroral E-storm model off
//     jf(36)=.false. 	  ! t=hmF2 w/out foF2_storm f=with
//     jf(37)=.false. 	  ! t=topside w/out foF2_storm f=with
//     jf(38)=.false. 	  ! t=WRITEs off in IRIFLIP f=on 
	jf[38]=0;	// new hmF2 models 
//     jf(40)=.false. 	  ! t=AMTB-model, f=Shubin-COSMIC model 
//     jf(41)=.false. 	  ! COV=f(IG12) (IRI before Oct 2015) 
//     jf(42)=.false. 	  ! Te w/o PF10.7 dependance 
//     jf(43)=.false. 	  ! B0 user input
	return 0;
}

int main(void) {
	integer jmag,iy,mmdd,numsteps;
	//integer iut,ivar;
	real xlat,xlon,hour,vbeg,vend,vstp;
	//real hxx,htec_max;
	unsigned int k;


	iri_init();

	jmag=0;
	xlat=0.; xlon=0.; iy=2010; mmdd=-111; hour=0.;
	vbeg=150.; vend=200.; vstp=5.;
	numsteps = ROUND((vend-vbeg)/vstp) + 1;
	if (numsteps > nummax || numsteps < 0) {
		fprintf(stderr,"WARNING: too many steps\n");
		return 1;
	}
	printf("Starting program ...\n");
#if 0
	hxx=0.;
	htec_max=0.;
	ivar=1;
	iut=0;
	iri_web_(
		/* inputs */
		&jmag,jf,&xlat,&xlon,&iy,&mmdd,&iut,&hour,&hxx,&htec_max,&ivar,&vbeg,&vend,&vstp,
		/* outputs */
		outf,oar);
#else
	iri_sub_(
		/* inputs */
		jf,&jmag,&xlat,&xlon,&iy,&mmdd,&hour,&vbeg,&vend,&vstp,
		/* outputs */
		outf,oar);
#endif
	printf("Finished!\n");
	fflush(stdout);
	printf("Here are the results:\n");
	printf("OUTF:\n");
	printf("%10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s  %10s\n",
		"h, km","Ne(m^-3)","Tn(K)","Ti(K)","Te(K)",
		"N[O+]","N[H+]","N[He+]","N[O2+]",
		"N[NO+]","N[Clus]","N[N+]");
	for (k=0; k<numsteps; k++){
		printf("%10.1f  %10.4e  %10.1f  %10.1f  %10.1f  "
			"%10.4e  %10.4e  %10.4e  %10.4e  %10.4e  %10.4e  %10.4e\n",
			vbeg+k*vstp,outf[k*20], outf[k*20+1], outf[k*20+2], outf[k*20+3],
			outf[k*20+4], outf[k*20+5], outf[k*20+6], outf[k*20+7],
			outf[k*20+8], outf[k*20+9], outf[k*20+10]);
		//for (k2=0; k2<20; k2++) printf("%g ",outf[k1*20+k2]);
		//printf("\n");
	}
	printf("OAR:\n");
	for (k=0; k<86; k++) printf("%g ",oar[k]);
	printf("\n");
	return 0;
}

