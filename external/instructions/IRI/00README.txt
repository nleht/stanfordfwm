################################################################################
# The following was tested with Python 3.6 and gcc 7.5
# The version of IRI2016 downloaded on Nov 5, 2021 (modified in 2021)

##### Preliminaries

# Download IRI2016, common files and extra files (or extract from the included "IRI2016_Data_Archive.tar.bz2")
mkdir build
wget -c http://irimodel.org/IRI-2016/00_iri2016.tar
wget -c http://irimodel.org/COMMON_FILES/00_ccir-ursi.tar
wget -c http://irimodel.org/indices/apf107.dat
wget -c http://irimodel.org/indices/ig_rz.dat

# Unzip them
tar xvf 00_ccir-ursi.tar
tar xvf 00_iri2016.tar # has both code and data

# Apply the patch and move code to its own directory:
patch -p1 < iri2016_2021.patch
mkdir build
mv *.for build/

# Create a directory where all the data files will be stored (needed for runtime)
mkdir data
mv *.dat data/
mv *.asc data/

# All of this has been done in this repository

# Make sure that all files, including the ones attached, are in the same directory, and go to that directory
# There is a makefile included, but use it only if you have repeated edits to the files, to facilitate compilation. If you are doing it the first time and would not edit any files, just follow the instructions below.

#####################################################################
##### Use a custom compiler and paths
# When using a custom compiler
CC=gcc-7.5
F77=gfortran-7.5
# If you encounter "not found" library problems, set $LD_LIBRARY_PATH to include
# their location and run "ldconfig":
export LD_LIBRARY_PATH=$HOME/.local/lib
ldconfig
# If you don't have enough privileges
# to run "ldconfig", you may use -Wl,-rpath=$LD_LIBRARY_PATH option for compiler
# Alternatively (and better), set LD_RUN_PATH:
export LD_RUN_PATH=$HOME/.local/lib:other_used_library_locations
# There is also LIBRARY_PATH, which replaces -L option for the compiler:
export LIBRARY_PATH=$HOME/.local/lib
# Another useful environment variable:
export CPATH=$HOME/.local/include
# which adds to the "include" path (replaces -I option of the compiler).
# All this should be done in your .bashrc file

#####################################################################
################### Instructions for C ##############################
# There are two options to proceed which give equivalent results. Files needed: IRI2016f.h, iri_simpletest.c

# 1. Dynamic library option
$F77 -fPIC -shared -g cira.for  igrf.for  iridreg.for  iriflip.for  irifun.for  irisub.for  iritec.for -o libIRI2016f.so
# Note the names of the libraries needed, make sure there are no
# "not found" entries. See above about what to do if there are any:
ldd libIRI2016f.so
# If there are problems with "not found" entries, set LD_LIBRARY_PATH as described above
mv libIRI2016f.so $HOME/.local/lib # or any other place which is included in your $LD_LIBRARY_PATH (for compiling other code using this library) and $LD_RUN_PATH (for running it)
$CC -Wall -g ../iri_simpletest.c -o iri_dyn_test.exe -I.. -lIRI2016f
./iri_dyn_test.exe

# 2. Static library option
$F77 -fPIC -c -g cira.for  igrf.for  iridreg.for  iriflip.for  irifun.for  irisub.for  iritec.for
ar crUus libiri_stat.a cira.o  igrf.o  iridreg.o  iriflip.o  irifun.o irisub.o  iritec.o
rm -f *.o
# On your system, the version of the libgfortran may be different, see output from "ldd libiri_dyn.so" above
$CC -Wall -g ../iri_simpletest.c -L. -liri_stat -lgfortran -lm -o iri_stat_test.exe
# You must change to directory containing all the data files
./iri_stat_test.exe

# 3. (OPTIONAL) Needs also files IRI2016f2c.c and iri_main.c
# Make a dynamic C library with an interface, to be used for a more convenient C programming
$CC -fPIC -shared -Wall -g ../IRI2016f2c.c -L. -liri_stat -lgfortran -lm -o libIRI2016c.so
mv libIRI2016c.so $HOME/.local/lib # or any other place which is included in your $LD_LIBRARY_PATH
# Example of use with a C program
$GCC -Wall -g ../iri_main.c -lIRI2016c -o iri_interfaced.exe
./iri_interfaced.exe

############### Instructions for Python3  ###########################
# It is recommended that you make sure that instructions for C work first.
# By the way, this may be done inside a virtual environment.
#
# We must have the static library libiri_stat.a (see above), if not, complete these steps:
cd build
gfortran -fPIC -g -c cira.for  igrf.for  iridreg.for  iriflip.for  irifun.for  irisub.for  iritec.for
# -- all files except iritest.for
ar crUus libiri_stat.a *.o
rm -f *.o

# *.pyf are needed only from files irisub.for, irifun.for, igrf.for and only for functions iri_sub, read_ig_rz, readapf107, igrf.
# *.pyf may be created e.g. by "f2py -h irisub.pyf irisub.for -m IRI2016f2py"
# Thus, IRI2016f2py_i.pyf was composed of several *.pyf files
# We also changed the dimensionality of 2D arrays to 1D (it looks like there is a bug which does not allow 2D arrays) and got rid of all common blocks (we don't need access to them)
# Appropriate variables were chosen as inputs and output using "intent(in)" or "intent(out)" (see http://docs.scipy.org/doc/numpy-dev/f2py/getting-started.html)

# Create the .so file for Python:
cd ..
f2py -c IRI2016f2py_i.pyf build/*.for # one may exclude the file iritest.for which contains the "main()"
# or using the created library
f2py -c IRI2016f2py_i.pyf -Lbuild -liri_stat -lgfortran

# Place the created files "IRI2016f2py.cpython-*.so" and "IRIpy.py" (attached) onto Python3 package search path (e.g., $VIRTUAL_ENVIRONMENT/lib/python*/dist-packages/)

####### Enjoy (or run "python3 iri_pytest.py")

$ python3
Python 3.4.3 (default, Sep 14 2016, 12:36:27) 
[GCC 4.8.4] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import IRIpy,os
# The following should point to the directory where all IRI2016 files are stored:
>>> IRI=IRIpy.IRIClass(os.environ['DATA_DIR'] + '/IRI2016data')
# Now, feel free to use the IRI object as many times as you want:
>>> h, ionosphere, oarr = IRI.sub(0,0,2010,-111,0,150,200,5)
*** IRI parameters are being calculated ***
Ne: NeQuick for Topside
Ne, foF2: URSI model is used.
Ne: B0,B1-ABT-2009
Ne, foF1: probability function used.
Ne, D: IRI1990
Ne, foF2: storm model included
Ion Com.: RBV-10 & TTS-03
Te: TBT-2012 model
Auroral boundary model off
Ne, foE: storm model off
>>> import matplotlib.pyplot as plt
# Plot the electron density
>>> plt.semilogx(ionosphere[:,0],h)
>>> plt.show()
>>> exit()
$

############### Instructions for MATLAB  ############################
# OPTION 2: Do analogously to Python3, but create the C-interface manually (or run "make mex")
# NOTE: must use default gcc to create libiri_stat.a here! Because it is used by mex! Otherwise, segfaults galore!
# 1. Create mexFunction interface to all required IRI Fortran functions, call it "IRI2016f2mex". The first argument is the name of the function to be called. It has been done, see attached file.
# Compile it. This will create IRI2016f2mex.mexa64: 
mex -v GCC=$CC -g IRI2016f2mex.c -Lbuild -liri_stat  -lgfortran

# Put it on your MATLAB path.

# 2. Create MATLAB calss "IRIClass" analogous to IRIpy.py IRIClass object. It has been done, see IRIClass.m in stanfordfwm/external/IRI2016
# Put it on you MATLAB path.

# 3. Enjoy:
# The test file IRI2016_test.m was created in stanfordfwm/external/IRI2016
matlab -nosplash -nodesktop -display :0 -r "IRI2016_test"
# Or run interactively:
>> IRI=IRIClass([getenv('DATA_DIR') '/IRI2016data'])
Calling IRI2016f2mex with argument read_ig_rz
Calling IRI2016f2mex with argument readapf107
IRI = 
  IRIClass with properties:

     version: 'IRI-2016'
         dir: '/scratch2/DATA/IRI2016data'
    outf_sub: [20x1000 double]
    oarr_sub: [100x1 double]
    outf_web: []
    oarr_web: []
        jmag: 0
        ivar: 1
          jf: [50x1 double]
>> [h, ionosphere] = IRI.sub(0,0,2010,-111,0,150,200,5);
Calling IRI2016f2mex with argument iri_sub
>> plot(h,ionosphere(1,:))
>> semilogx(ionosphere(1,:),h)


############### Debugging instructions (ADVANCED! DON'T NEED IF IRI GIVES CORRECT RESULTS.) #########################
# The option -g is needed for all compilations if you will use gdb
# Use either "gdb" (command-line interface) or "ddd" (graphical interface).
# European version of gdb uses a decimal comma by default, you must first disable it:
# export LC_NUMERIC=C


