#include <math.h>
#include <string.h> // for strcmp
#include <stdio.h>
#include "mex.h"
#include "IRI2016f.h"

#define MYDEBUG     0

/* Interface functions */
void read_ig_rz_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void readapf107_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void iri_sub_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void iri_web_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void igrf_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

/*************************************************************************/
/* Some auxiliary stuff */

#if !defined(MAX)
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define	MIN(A, B)	((A) < (B) ? (A) : (B))
#endif

char MexFunctionName[]="IRI2016f2mex";
#define MESSAGE_BUFFER_LENGTH 1000

void ErrorWithID(char *errorName, char *message) {
    static char m[MESSAGE_BUFFER_LENGTH];
    snprintf(m,sizeof(m),"MATLAB:%s:%s",MexFunctionName,errorName);
    mexErrMsgIdAndTxt(m,message);
}


int IsDoubleArray(const mxArray *a, mwSize length) {
    mwSize m,n;
    int ans;
    m = mxGetM(a);
    n = mxGetN(a);
    ans = (mxIsDouble(a) && !mxIsComplex(a) && (MIN(m,n) == 1));
    if (length>0) ans = ans && (MAX(m,n) == length);
    return ans;
}

double GetScalar(const mxArray *a, const char *name) {
    static char message[MESSAGE_BUFFER_LENGTH];
    double ascalar;
    snprintf(message,sizeof(message),"%s requires that %s be a scalar.", MexFunctionName, name);
    if (!IsDoubleArray(a,1)) mexErrMsgTxt(message);
    ascalar = (double)(*mxGetPr(a));
#if MYDEBUG
    printf("%s=%f\n",name,(float)ascalar);
#endif
    return ascalar;
}

void LoadVector(char type, void *aout, const mxArray *a, mwSize length, const char *name) {
    static char message[MESSAGE_BUFFER_LENGTH];
    int k;
    double *aDouble;
    real *aReal;
    logical *aLogical;
    snprintf(message,sizeof(message),"%s requires that %s be a %d x 1 vector.", MexFunctionName, name, (int)length);
    if (!IsDoubleArray(a,length)) mexErrMsgTxt(message);
    aDouble=mxGetPr(a);
    switch (type) {
        case 'r':
            aReal = (real *)aout;
            for(k=0;k<length;k++) aReal[k]=(real)aDouble[k];
#if MYDEBUG
            for(k=0;k<length;k++) printf("%s(%d)=%f\n",name,k+1,(float)aReal[k]);
#endif
            break;
        case 'l':
            aLogical = (logical *)aout;
            for(k=0;k<length;k++) aLogical[k]=(logical)aDouble[k];
#if MYDEBUG
            for(k=0;k<length;k++) printf("%s(%d)=%d\n",name,k+1,(int)aLogical[k]);
#endif
            break;
        default:
            snprintf(message,sizeof(message),"%s: unknown type %c of array %s.", MexFunctionName, type, name);
            mexErrMsgTxt(message);
            break;
    }
    return;
}

mxArray *MatrixFromReal(mwSize M, mwSize N, const real *a) {
    mxArray *b = mxCreateDoubleMatrix(M, N, mxREAL);
    double *bd = mxGetPr(b);
    unsigned long k;
    for (k=0;k<M*N;k++) bd[k]=(double)a[k];
    return b;
}

char *GetString(const mxArray *a) {
    if ( mxIsChar(a) != 1) ErrorWithID("inputNotString", "Input must be a string.");
    /* input must be a row vector */
    if ( mxGetM(a)!=1) ErrorWithID("inputNotVector", "Input must be a row vector.");
    return mxArrayToString(a);
}

/*************************************************************************/
/* The entry function */

void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray *prhs[] ) { 
    char *func_name;
    static char message[MESSAGE_BUFFER_LENGTH];
    /* Dispatch according to the function name given in the first argument */
    func_name = GetString(*(prhs++)); nrhs--;
#if MYDEBUG
    printf("Calling %s with argument %s\n",MexFunctionName,func_name);
#endif
    if (strcmp(func_name,"read_ig_rz")==0)
        read_ig_rz_mex(nlhs,plhs,nrhs,prhs);
    else if (strcmp(func_name,"readapf107")==0)
        readapf107_mex(nlhs,plhs,nrhs,prhs);
    else if (strcmp(func_name,"iri_sub")==0)
        iri_sub_mex(nlhs,plhs,nrhs,prhs);
    else if (strcmp(func_name,"iri_web")==0)
        iri_web_mex(nlhs,plhs,nrhs,prhs);
    else if (strcmp(func_name,"igrf")==0)
        igrf_mex(nlhs,plhs,nrhs,prhs);
    else {
        snprintf(message,sizeof(message),"%s: function %s is not in the package.", MexFunctionName, func_name);
        mexErrMsgTxt(message); }
    return;
}

/*************************************************************************/
/* Interface functions */

void read_ig_rz_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if (nrhs > 0) mexErrMsgTxt("READ_IG_RZ: Too many input arguments");
    if (nlhs > 0) mexErrMsgTxt("READ_IG_RZ: Too many output arguments.");
    read_ig_rz_();
    return;
}

void readapf107_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if (nrhs > 0) mexErrMsgTxt("READAPF107: Too many input arguments");
    if (nlhs > 0) mexErrMsgTxt("READAPF107: Too many output arguments.");
    readapf107_();
    return;
}

void iri_sub_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    /* Inputs */
    logical jf[50];
    integer jmag, iyyyy, mmdd;
    real alati, along, dhour, heibeg, heiend, heistp;
    /* Outputs */
    real outf[20000]; /* 20 x 1000 */
    real oarr[100]; /* both input and output */

    if (nrhs != 11) mexErrMsgTxt("IRI_SUB: Needs exactly 11 input arguments");
    if (nlhs != 2) mexErrMsgTxt("IRI_SUB: Needs exactly 2 output arguments");
    
    /**************** INPUTS ***************/
    LoadVector('l',jf,prhs[0],50,"JF");
    jmag = (integer)GetScalar(prhs[1],"JMAG");
    alati = (real)GetScalar(prhs[2],"ALATI");
    along = (real)GetScalar(prhs[3],"ALONG");
    iyyyy = (integer)GetScalar(prhs[4],"IYYYY");
    mmdd = (integer)GetScalar(prhs[5],"MMDD");
    dhour = (real)GetScalar(prhs[6],"DHOUR");
    heibeg = (real)GetScalar(prhs[7],"HEIBEG");
    heiend = (real)GetScalar(prhs[8],"HEIEND");
    heistp = (real)GetScalar(prhs[9],"HEISTP");
    LoadVector('r',oarr,prhs[10],100,"OARR");

#if MYDEBUG
    printf("IRI_SUB: ");
#endif
    /********* Do the actual computations in a subroutine ******/
    iri_sub_(/*inputs*/ jf, &jmag, &alati, &along, &iyyyy, &mmdd, &dhour, &heibeg, &heiend, &heistp,
            /*outputs*/ outf, oarr);

#if MYDEBUG
    printf(" Done!\n");
#endif

    /**************** OUTPUTS ***************/
    plhs[0] = MatrixFromReal(20, 1000, outf);
    plhs[1] = MatrixFromReal(100, 1, oarr);
    return;
}

void iri_web_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    mexErrMsgTxt("IRI_WEB: Not implemented");
    if (nrhs > 0) mexErrMsgTxt("IRI_WEB: Too many input arguments");
    if (nlhs > 0) mexErrMsgTxt("IRI_WEB: Too many output arguments.");
    return;
}

void igrf_mex(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	real xlat, xlon, year, height, bnorth, beast, bdown, babs;

	//mexErrMsgTxt("IGRF: Not implemented");
	if (nrhs != 4) mexErrMsgTxt("IGRF: Needs exactly 4 input arguments");
	if (nlhs != 3) mexErrMsgTxt("IGRF: Needs exactly 3 output arguments");

	/**************** INPUTS ***************/
	xlat = (real)GetScalar(prhs[0],"xlat");
	xlon = (real)GetScalar(prhs[1],"xlon");
	year = (real)GetScalar(prhs[2],"year");
	height = (real)GetScalar(prhs[3],"height");

	/********* Do the actual computations in a subroutine ******/
	#if MYDEBUG
		printf("IGRF reimplementation: ");
		printf("xlat=%g, xlon=%g, year=%g, height=%g\n", xlat, xlon, year, height);
	#endif
//////////////////////////////////////////////////////////////////////////
// igrf.for, lines 106-143
//////////////////////////////////////////////////////////////////////////
//     subroutine igrf_dip(xlat,xlong,year,height,dec,dip,dipl,ymodip)
//c-----------------------------------------------------------------------        
//c INPUT:
//c    xlat      geodatic latitude in degrees
//c    xlong     geodatic longitude in degrees
//c    year      decimal year (year+month/12.0-0.5 or 
//c                  year+day-of-year/365 or ../366 if leap year) 
//c    height    height in km
//c OUTPUT:
//c    dec       magnetic declination in degrees
//c    dip       magnetic inclination (dip) in degrees
//c    dipl      dip latitude in degrees
//c    ymodip    modified dip latitude = asin{dip/sqrt[dip^2+cos(LATI)]} 
//c-----------------------------------------------------------------------        
//
//      COMMON /CONST/UMR,PI
	// M_Pi is the pi number, defined in <math.h>
	// UMR = M_PI/180.;
	// We do not use them
//	  xlati = xlat
//	  xlongi = xlong
//	  h = height
//c      CALL FELDCOF(YEAR,DIMO)
	// This is a lie, FELDCOF takes only one argument, the year
	feldcof_(&year);
//      CALL FELDG(XLATI,XLONGI,H,BNORTH,BEAST,BDOWN,BABS)
	feldg_(&xlat, &xlon, &height, &bnorth, &beast, &bdown, &babs);
	// We already got the field components, don't need the rest of the subroutine
//          DECARG=BEAST/SQRT(BEAST*BEAST+BNORTH*BNORTH)
//          IF(ABS(DECARG).GT.1.) DECARG=SIGN(1.,DECARG)
//      DEC=ASIN(DECARG)
//          BDBA=BDOWN/BABS
//          IF(ABS(BDBA).GT.1.) BDBA=SIGN(1.,BDBA)
//      DIP=ASIN(BDBA)
//          dipdiv=DIP/SQRT(DIP*DIP+cos(XLATI*UMR))
//          IF(ABS(dipdiv).GT.1.) dipdiv=SIGN(1.,dipdiv)
//      SMODIP=ASIN(dipdiv)        
//c       DIPL1=ATAN(0.5*TAN(DIP))/UMR
//      DIPL=ATAN(BDOWN/2.0/sqrt(BNORTH*BNORTH+BEAST*BEAST))/umr
//      YMODIP=SMODIP/UMR                            
//      DEC=DEC/UMR
//      DIP=DIP/UMR
//      RETURN
//      END
//////////////////////////////////////////////////////////////////////////
	#if MYDEBUG
		printf("Output: b=[%g, %g, %g]\n", bnorth, beast, bdown);
		printf("Common blocks:\n");
		printf("/CONST/UMR=%g, PI=%g\n", const_.umr, const_.pi); 
		printf(" Done!\n");
	#endif

	/**************** OUTPUTS ***************/
	plhs[0] = MatrixFromReal(1, 1, &bnorth);
	plhs[1] = MatrixFromReal(1, 1, &beast);
	plhs[2] = MatrixFromReal(1, 1, &bdown);
	return;
}
