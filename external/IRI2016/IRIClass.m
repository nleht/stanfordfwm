%IRIClass - Run IRI2016 model
%
% Initialization:
%    IRI = IRIClass(); % or just IRI=IRIClass;
%    % Or (a more advanced usage)
%    IRI = IRIClass(IRI_data_dir);
%    % - if you would like to specify your own directory with the IRI data.
%    % The default directory is inputs/IRI_data
%
% In order to run, the binary file 'IRI2016f2mex' must be present.
% The file that is included in this repository,
%    external/IRI2016/IRI2016f2mex.mexa64
% may be used with 64-bit architectures in Linux. If using other
% architectures, the binary file must be created by running 'mex' compiler
% frontend included in MATLAB. On how to do it, see instructions in
%    external/instructions/IRI/00README.txt
%
% To use IGRF:
%    % Geomagnetic field in T
%    Bgeo = IRI.igrf(latitude,longitude,year,height);
%    % Components [Bx,By,Bz] are [B_East, B_North, B_Up]
%
% To use IRI:
%    [h, ionosphere] = IRI.sub(is_geomag, latitude, longitude, year, ...
%         {-DOY|mmdd}, is_utc*24+hour, hstart, hstop, hstep);
%    Ne=ionosphere(1,:);
%    % The columns in 'ionosphere' are
%    % 'Ne(m^-3)', 'Tn(K)', 'Ti(K)', 'Te(K)', 'N[O+](%)', 'N[H+](%)',
%    % 'N[He+](%)', 'N[O2+](%)', 'N[NO+](%)', 'N[Clus](%)', 'N[N+](%)'
%
% See also: IRI2016_TEST
%
% Author: Nikolai G. Lehtinen
classdef IRIClass < handle
    properties
        version
        dir
        outf_sub
        oarr_sub
        %outf_web
        %oarr_web
        jf
        icalls
    end
    methods
        % Constructor
        function self=IRIClass(thedir)
            global global_dir
            if nargin==0
                if isempty(global_dir)
                    loadconstants
                end
                thedir=fullfile(global_dir, 'IRI_data');
            end
            if ~exist(thedir, 'dir')
                error(['Directory ' thedir ' does not exist'])
            end
            self.dir=thedir;
            self.version='IRI2016, Version Nov 5 2021';
            self.outf_sub=zeros(20,1000);
            self.oarr_sub=zeros(100,1);
            curdir=pwd;
            cd(self.dir);
            % Allocate arrays for internal use
            %self.outf=np.zeros((20000,),dtype=np.float32)
            %self.oarr=np.zeros((100,),dtype=np.float32)
            %self.outf_web=np.zeros((20000,),dtype=np.float32)
            %self.oarr_web=np.zeros((100000,),dtype=np.float32)
            % Set the defaults -- you can change them (interactively! not in this file of course)
            self.jf=ones(50,1);
            % defaults for jf(1:50)
            %     jf(1)=.false.      ! f=no electron densities
            %     jf(2)=.false.      ! f=no temperatures
            %     jf(3)=.false.      ! f=no ion composition
            self.jf(4)=0; 		% t=B0table f=other models (f)
            self.jf(5)=0; 		% t=CCIR  f=URSI foF2 model (f)
            self.jf(6)=0; 		% t=DS95+DY85   f=RBV10+TTS03 (f)
            %     jf(7)=.false.      ! t=tops f10.7<188 f=unlimited (t)
            self.jf(21)=0; 		% f=ion drift not computed (f)
            %     jf(22)=.false.     ! ion densities in m-3 (t)
            self.jf(23)=0; 		% t=AEROS/ISIS f=TTS Te with PF10.7
            %     jf(24)=.false.     ! t=D-reg-IRI-1990 f=FT-2001 (t)
            %     jf(26)=.false.	  ! f=STORM model turned off (t)
            self.jf(28)=0;		% f=spread-F not computed (f)
            self.jf(29)=0;		% t=old  f=New Topside options (f)
            self.jf(30)=0;		% t=corr f=NeQuick topside (f)
            %     jf(31)=.false.     ! t=B0ABT f=Gulyaeva (t)
            self.jf(33)=0;		% f=auroral boundary off (f)
            self.jf(34)=0;      % t=messages on
            self.jf(35)=0;		% f=auroral E-storm model off
            %     jf(36)=.false. 	  ! t=hmF2 w/out foF2_storm f=with
            %     jf(37)=.false. 	  ! t=topside w/out foF2_storm f=with
            %     jf(38)=.false. 	  ! t=WRITEs off in IRIFLIP f=on
            self.jf(39)=0;		% new hmF2 models
            %     jf(40)=.false. 	  ! t=AMTB-model, f=Shubin-COSMIC model
            %     jf(41)=.false. 	  ! COV=f(IG12) (IRI before Oct 2015)
            %     jf(42)=.false. 	  ! Te w/o PF10.7 dependance
            %     jf(43)=.false. 	  ! B0 user input
            % Initialize IRI2016
            if ~exist(fullfile(thedir,'ig_rz.dat'),'file') || ~exist(fullfile(thedir,'apf107.dat'),'file')
                error(['Directory ' thedir ' is not complete! (ig_rz or apf107 missing)'])
            end
            IRI2016f2mex('read_ig_rz');
            IRI2016f2mex('readapf107');
            self.icalls = 0;
            cd(curdir)
        end
        function [h,ionosphere]=sub(self,is_geomag,alati,along,iyyyy,mmdd,dhour,vbeg,vend,vstp)
            numsteps = round((vend-vbeg)/vstp) + 1;
            curdir=pwd;
            cd(self.dir) % needed to read the files
            [self.outf_sub,self.oarr_sub]=IRI2016f2mex('iri_sub',self.jf,is_geomag,...
                alati,along,iyyyy,mmdd,dhour,vbeg,vend,vstp,self.oarr_sub);
            self.icalls = self.icalls + 1;
            cd(curdir)
            ionosphere=self.outf_sub(:,1:numsteps);
            h=vbeg+[0:numsteps-1]*vstp;
        end
        function Bgeo=igrf(self,xlat,xlon,year,height)
            curdir=pwd;
            cd(self.dir) % needed to read the files
            if self.icalls==0
                % Must have a dummy call to IRI_SUB to initialize a bunch of
                % common blocks
                [self.outf_sub,self.oarr_sub]=IRI2016f2mex('iri_sub',self.jf,0,...
                    0,0,2020,101,0,50,150,50,self.oarr_sub);
                self.icalls = self.icalls + 1;
            end
            % And now, the real stuff
            [bnorth, beast, bdown]=IRI2016f2mex('igrf',xlat,xlon,year,height);
            cd(curdir)
            % In T and in East-aligned coordinates
            Bgeo=[beast/1e4, bnorth/1e4, -bdown/1e4];
        end
    end
end
