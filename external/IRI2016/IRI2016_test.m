% Test the IRI2016 model

%% Initialize the IRI object
IRI=IRIClass()

%% Set up IRI args
is_geomag = 0;
latitude = 0;
longitude = 0;
year = 2020;
DOY = 111;
hour = 0;
hstart = 0;
hstop = 500;
hstep = 5;
% Day may also be given either as -DOY or as +mmdd
% hour<24 if local; is 24+hour if time is UTC

%% Call IGRF
height=0;
Bgeo = IRI.igrf(latitude,longitude,year,height)

%% Call IRI
[h, ionosphere] = IRI.sub(is_geomag, latitude, longitude, year, -DOY, hour, hstart, hstop, hstep);
% The columns in 'iononspere' are
% 'Ne(m^-3)', 'Tn(K)', 'Ti(K)', 'Te(K)', 'N[O+](%)', 'N[H+](%)',
% 'N[He+](%)', 'N[O2+](%)', 'N[NO+](%)', 'N[Clus](%)', 'N[N+](%)'

%% Plot results
ionosphere(ionosphere<0)=nan; % unknown values are marked with -1
Ne = ionosphere(1,:);
semilogx(Ne,h)
xlabel('Ne, m^{-3}')
ylabel('h, km')
