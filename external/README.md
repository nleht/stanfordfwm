# Linking with non-MATLAB code

This folder contains instructions on how to call useful non-MATLAB models from MATLAB. So far we have:

* **IRI** (International Reference Ionosphere) -- for 2016 version. The newer instructions may be found [here](https://gitlab.com/nleht/small-projects/-/tree/main/IRI2023_instructions).


