function k_a=pasko_nu_att(EN,method)
%PASKO_NU_ATT 2-body attachment coefficient in Pasko's thesis
% Reaction:
%   e* + O2 -> O- + O
%   e* denotes a suprathermal (relatively fast) electron.
% Equation for electron density Ne:
%   (dNe/dt)_att = - k_a*Nm*Ne
% Usage: k_a=pasko_nu_att(EN) for AC fields, also see below
% Input: EN = E/Nm in Townsends (=1e-21 V/m)
% Output: k_a = nu_a/Nm in m^3*s^{-1}
% Usage:
% For AC fields: nu_a=pasko_nu_att(E/sqrt(1+(w/nuk)^2)/Nm*1e21)*Nm
% Here nuk is effective nu_m,
%   nuk=1.6e-13*Nm [Papadopoulos et al, 1993]
% or
%   nuk=2e-13*Nm [Lehtinen et al, 2012].
% E is the amplitude of the AC field.
% Pasko's usage for DC fields (incorrect):
%   nu_a=pasko_nu_att(E/Nm*1e21)*Nm
% Correct usage for DC fields:
%   nu_a=pasko_nu_att(sqrt(2)*E/Nm*1e21)*Nm
% The factor of sqrt(2) comes from the fact that for DC fields, E has
% to be multiplied by a different coefficient in the kinetic equation
% than for AC fields. The correct usage agrees (more or less) with Davies
% [1983] swarm experiment results.

if nargin<2
    method='pasko';
end

k_a=zeros(size(EN));
switch method
    case 'pasko'
        % Pasko [1996] dissertation, page 37
        N0=2.688e25;
        
        x = EN*N0/1e21; % in V/m
        hi=(x > 1.628e6);
        
        a0hi = -2.41e8;
        a1hi = 211.92;
        a2hi = -3.545e-5;
        xhi=x(hi);
        k_a(hi) = (a0hi + a1hi*xhi + a2hi*xhi.^2)/N0;
        
        a0lo = -1073.8;
        a1lo = 465.99;
        a2lo = -66.867;
        a3lo = 3.1970;
        xlo=x(~hi);
        k_a(~hi) = exp(a0lo + a1lo*xlo + a2lo*xlo.^2 + a3lo*xlo.^3)/N0;
        
        k_a(k_a<0)=0;
    case 'sentman'
        % Sentman et al [2008]
        %th=sqrt(2)*EN; % the AC->DC correction (is not needed?)
        th=EN;
        hi=(th>80);
        % Factor 1e-6 is to convert cm -> m
        k_a(hi)  = 1e-6*10.^(-(10.4+57./th(hi)));
        k_a(~hi) = 1e-6*10.^(-(9.575+123./th(~hi)));
end
