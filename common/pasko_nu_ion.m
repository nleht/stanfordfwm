function k_i=pasko_nu_ion(EN,method)
%PASKO_NU_ION Ionization coefficient in Pasko's thesis
% Reactions:
%   e* + N2 -> e + e + N2+
%   e* + O2 -> e + e + O2+
%   e* denotes a suprathermal (relatively fast) electron.
% Equation for electron density Ne:
%   (dNe/dt)_ion = k_i*Nm*Ne
% Usage: k_i=pasko_nu_ion(EN) for AC fields, also see below
% Input: EN = E/Nm in Townsends (=1e-21 V/m)
% Output: k_i = nu_i/Nm in m^3*s^{-1}
% Usage:
% For AC fields: nu_i=pasko_nu_ion(E/sqrt(1+(w/nuk)^2)/Nm*1e21)*Nm
% Here nuk is effective nu_m,
%   nuk=1.6e-13*Nm [Papadopoulos et al, 1993]
% or
%   nuk=2e-13*Nm [Lehtinen et al, 2012].
% E is the amplitude of the AC field.
% Pasko's usage for DC fields (incorrect):
%   nu_i=pasko_nu_ion(E/Nm*1e21)*Nm
% Correct usage for DC fields:
%   nu_i=pasko_nu_ion(sqrt(2)*E/Nm*1e21)*Nm
% The factor of sqrt(2) comes from the fact that for DC fields, E has
% to be multiplied by a different coefficient in the kinetic equation
% than for AC fields. The correct usage agrees (more or less) with Davies
% [1983] swarm experiment results.
if nargin<2
    method='pasko';
end
switch method
    case 'pasko'
        % Pasko [1996] thesis, page 36
        N0=2.688e25;
        ENk=3.2e6/N0*1e21; % in Td
        x=EN/ENk;
        fx=(1+6.3*exp(-2.6./x))/1.5;
        k_i=7.6e-19*x.^2.*fx.*exp(-4.7*(1./x-1));
    case 'sentman'
        % Sentman et al [2008]
        %th=sqrt(2)*EN; % the AC->DC correction (is not needed?)
        th=EN;
        % Factor 1e-6 is to convert cm -> m
        k5=1e-6*10.^(-(8.3+365./th)); % e* + N2 -> e + e + N2+
        k6=1e-6*10.^(-(8.8+281./th)); % e* + O2 -> e + e + O2+
        k_i=k5*0.8+k6*0.2; % assume 80% N2, 20% O2
end
