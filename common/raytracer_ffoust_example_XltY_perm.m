function perm=raytracer_ffoust_example_XltY_perm(dw,r)
N=size(r,2);
x=r(1,:); z=r(3,:);
R2=x.^2+z.^2;
Yx=3*x./R2;
Yz=3*z./R2;
Y=sqrt(Yx.^2+Yz.^2);
X=3./(sqrt(R2)+1);
U=1;
perm=zeros(3,3,N);
% At shifted frequency
w=1+dw;
Xs=X/w^2; Ys=Y/w;
R=1-Xs./(U-Ys); L=1-Xs./(U+Ys);
S=(R+L)/2; P=1-Xs/U; D=(R-L)/2;
ct=Yz./Y; st=Yx./Y;
perm0=perm;
perm0(1,1,:)=S;
perm0(2,2,:)=S;
perm0(1,2,:)=-1i*D;
perm0(2,1,:)=1i*D;
perm0(3,3,:)=P;
for k=1:N
    % Active
    rotmxa=[ct(k) 0 st(k); 0 1 0; -st(k) 0 ct(k)];
    % Passive (inverse of active)
    rotmxp=[ct(k) 0 -st(k); 0 1 0; st(k) 0 ct(k)];
    perm(:,:,k)=rotmxa*perm0(:,:,k)*rotmxp;
end
