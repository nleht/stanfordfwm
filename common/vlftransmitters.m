% The known transmitters
% Magnetic field is from IGRF, at 100 km, rounded to uT.
% http://sidstation.lionelloudet.homedns.org/stations-list-en.xhtml
% sground is ground conductivity in S/m from FCC web page
% http://www.fcc.gov/mb/audio/m3/index.html

VLFTransmitters = [...
    % Hawaii (Lualuahei, HI)
    % The conductivity is that of sea water
    struct('name','NPM','f',21400,'P0',424000,...
    'lat',21.422778,'lon',-158.150278,'Bgeo',[5 26 -21]*1e-6,'sground',5),...

    % Australia (North West Cape, Exmouth, Australia)
    struct('name','NWC','f',19800,'P0',1e6,...
    'lat',-21.81631,'lon',114.16556,'Bgeo',[0 29 42]*1e-6,'sground',[]),...

    % North Dakota (La Moure, ND)
    struct('name','NML','f',25200,'P0',233000,...
    'lat',46.365990,'lon',-98.335638,'Bgeo',[1 16 -51]*1e-6,'sground',30e-3),...
    
    % Cutler, Maine, USA
    % The ground conductivity is 1 mS/m, but it is on the shore
    struct('name','NAA','f',24000,'P0',1e6,...
    'lat',44.646,'lon',-67.281,'Bgeo',[-5 18 -47]*1e-6,'sground',5),...
    
    % Aguado, Puerto Rico, USA
    struct('name','NAU','f',40750,'P0',1e5,...
    'lat',18.399,'lon',292.822,'Bgeo',[-5171.4 25341.7 -26762.1]*1e-9,'sground',5),...
    
    % Jim Creek, Washington, USA
    struct('name','NLK','f',24800,'P0',250e3,...
    'lat',48.203,'lon',238.083,'Bgeo',[5740.0 17156.3 -49532.4]*1e-9,'sground',4e-3),...
    
    ];


for k=1:length(VLFTransmitters)
    eval([VLFTransmitters(k).name '=VLFTransmitters(k);']);
end

