function result = iff(condition,trueResult,falseResult)
narginchk(3,3);
if condition
    result = trueResult;
else
    result = falseResult;
end
