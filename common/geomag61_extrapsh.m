function [gha,nmax]=geomag61_extrapsh(date, dte1, nmax1, gh1, nmax2, gh2)
%FINISHED BUT NOT TESTED!
%***************************************************************************/
%                                                                          */
%                           Subroutine extrapsh                            */
%                                                                          */
%***************************************************************************/
%                                                                          */
%     Extrapolates linearly a spherical harmonic model with a              */
%     rate-of-change model.                                                */
%                                                                          */
%     Input:                                                               */
%           date     - date of resulting model (in decimal year)           */
%           dte1     - date of base model                                  */
%           nmax1    - maximum degree and order of base model              */
%           gh1      - Schmidt quasi-normal internal spherical             */
%                      harmonic coefficients of base model                 */
%           nmax2    - maximum degree and order of rate-of-change model    */
%           gh2      - Schmidt quasi-normal internal spherical             */
%                      harmonic coefficients of rate-of-change model       */
%                                                                          */
%     Output:                                                              */
%           gha or b - Schmidt quasi-normal internal spherical             */
%                    harmonic coefficients                                 */
%           nmax   - maximum degree and order of resulting model           */
%                                                                          */
%     FORTRAN                                                              */
%           A. Zunde                                                       */
%           USGS, MS 964, box 25046 Federal Center, Denver, CO.  80225     */
%                                                                          */
%     C                                                                    */
%           C. H. Shaffer                                                  */
%           Lockheed Missiles and Space Company, Sunnyvale CA              */
%           August 16, 1988                                                */
%                                                                          */
%***************************************************************************/

gha=zeros(size(gh1));

factor = date - dte1;
if (nmax1 == nmax2)
    k =  nmax1 * (nmax1 + 2);
    nmax = nmax1;
else
    if (nmax1 > nmax2)
        k = nmax2 * (nmax2 + 2);
        l = nmax1 * (nmax1 + 2);
        for ii=k+1:l
            gha(ii) = gh1(ii);
        end
        nmax = nmax1;
    else
        k = nmax1 * (nmax1 + 2);
        l = nmax2 * (nmax2 + 2);
        for ii=k+1:l
            gha(ii) = factor * gh2(ii);
        end
        nmax = nmax2;
    end
end
for ii=1:k
    gha(ii) = gh1(ii) + factor * gh2(ii);
end
