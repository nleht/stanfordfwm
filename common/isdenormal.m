function res=isdenormal(x)
%ISDENORMAL Check if a complex number has any denormal components
rr=abs(real(x)); ii=abs(imag(x));
res=(rr<realmin & rr~=0) | (ii<realmin & ii~=0);
