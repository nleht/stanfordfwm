function [numom,nuenel,nuenrot,nuencont]=getnumom(en,species1,composition1,method)
%GETNUMOM - Electron-neutral collision rates at given energies
% Usage:
%    [numom,nuenel,nuenrot]=getnumom(en[,method]);
%    [numom,nuenelrot]=getnumom(en[,method]);
% Input:
%    en       -- energy in eV (1D array)
% Optional input:
%    method   -- interpolation method, default='linear' (according to the
%       recommendation by Phelps)
% Outputs - Collision frequencies divided by Nm (i.e., in s^{-1}m^3):
%    numom    -- momentum transfer frequency (defined as the
%       sum of elastic and inelastic cross-sections)
%    nuenel   -- "energy loss" elastic collision frequency
%    nuenrot  -- energy rotational losses in continuous approximation
%    nuenelrot = nuenel + nuenrot
% Author: Nikolai G. Lehtinen
global ech me uAtomic aBohr species composition
if isempty(ech)
    loadconstants
end
if nargin<4
	method='linear';
end
if nargin<3
    species1=[]; composition1=[];
end
if isempty(species1) || isempty(composition1)
    species1=species;
    composition1=composition;
end
if isempty(species)
    loadconstants
end
veloc=sqrt(2*en*ech/me);
numom=zeros(size(en));
nuenel=zeros(size(en));
nuenrot=zeros(size(en));
nuencont=zeros(size(en));
sig=zeros(size(en)); % Temporary array
for isp=1:length(species1)
    SP=species1(isp);
    % A strange bug:
    %   dnu=Nsp*veloce.*1e-20*interp1(SP.em,SP.qm,ene);
    % doesn't work!!!
    % At low energies - use analytical formulas:
    i1=find(en>SP.em1 & en<=max(SP.em));
    %sig(i1)=exp(interp1(SP.em,log(SP.qm),en(i1),'spline'));
    sig(i1)=interp1(SP.em,SP.qm,en(i1),method);
    i0=find(en<=SP.em1);
    sig(i0)=SP.qm00+SP.qm01*sqrt(en(i0)/SP.em0);
    %sig=interp1(SP.em,SP.qm,en);
    i2=find(en>max(SP.em));
    sig(i2)=SP.qm(end)*(en(i2)/SP.em(end)).^(-0.9);
    dnu=composition1(isp)*veloc.*(1e-20*sig);
    %plot(ene,dnu); pause
    if any(isnan(dnu))
        en
        dnu
        error('dnu is nan');
    end
    numom=numom+dnu;
    if nargout>1
        coef=2*me/(SP.molwt*uAtomic);
        nuenel=nuenel+coef*dnu;
        % The Continuous Approximation to Rotation excitation (CAR)
        % Extremely important at energies < B0*M/m ~ 1 eV
        % Quadrupole moments only
        % See Gerjouy and Stein [1955], eq. 26
        % "B0" is given in eV, and so is "en".
        sig0=8*pi/15*SP.qumom^2*aBohr^2;
        drot=composition1(isp)*4*sig0*SP.B0./en.*veloc;
        nuenrot=nuenrot+drot;
    end
    % Continuous loss - must be included only if not treating the discrete
    % energy loss cross-secions
    if nargout==4
        for v=find(SP.iscontinuous)
            n=SP.numpoints;
            sig=interp1(SP.e(1:n,v),SP.q(1:n,v),en,method);
            sig(isnan(sig))=0;
            % Assume 300 K = 0.02585 eV
            % I am not sure if this expression is correct. Please avoid
            % using the continuous loss cross-sections since the discrete
            % energy loss cross-sections are available.
            dnu=composition1(isp)*veloc.*(1e-20*sig)*SP.eloss(v)^2/0.02585./en;
            nuencont=nuencont+dnu;
        end
    end
end
if nargout==2
    nuenel=nuenel+nuenrot;
end
