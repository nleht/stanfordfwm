function day=astro_doy(yr,mo,dy)
%ASTRO_DOY Day of year
% See also: ASTRO_JD, ASTRO_GMST, ASTRO_JDOY
days=[31,28,31,30,31,30,31,31,30,31,30,31];
day = 0;
for i=1:mo-1
    day = day + days(i);
end
day = day + dy;
if mod(yr,4)==0 && (mod(yr,100) ~=0 || mod(yr,400)==0) && mo > 2
    day = day + 1;
end
