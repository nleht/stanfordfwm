%% Z-mode propagation example for the raytracer

%% This file uses the permittivity given by raytracer_ffoust_example2_perm
% This gives X=3./(R-1); Y=3./R
fun_perm=@raytracer_ffoust_example_XgtY_perm;
% The regularizer
fun_regularizer=@(dw1,r1) (1-3./sqrt(sum(r1.^2))/(1+dw1));
do_figures=1;
close all

%% The radii which denote boundaries between CMA zones
Rx=4; % X=1, between CMA-4 and CMA-3
Ry=3; % Y=1, between CMA-7 and CMA-4
% Hybrid resonance (S=0) radius, between CMA-3 and CMA-2
tmp=solve_cubic(1,-4,-9,9);
Rs=real(tmp(1))
Xs=3./(Rs-1); Ys=3./Rs;
1-Xs./(1-Ys.^2)
% Left-handed cut-off (L=0) radius, between CMA-8 and CMA-7
tmp=solve_quadratic(1,-1,-3);
Rl=tmp(1)
Xl=3./(Rl-1); Yl=3./Rl;
1-Xl./(1+Yl)

%%
Ncase=3;
names={'cma743','cma43','cma3'};
r0s={[0;0;2.5],[0;0;3.5],[0;0;5]};
nx0ss={[.1:.1:.4],[.1:.1:.8],[2:4:10]};
mss={[2 3],[2 3],[2 3]};
% For plotting
nxs={[-1:.01:1],[-1:.01:1],[-5:.01:10]};
xlims={[-0.5 2],[-0.5 5],[-0.5 5]};
ylims={[2 5.5],[1 5.5],[2 5.5]};
if ~exist('raytracer_ffoust_example/XgtY/','dir')
    mkdir('.','raytracer_ffoust_example/XgtY/')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Z-mode propagating in various CMA zones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The CMA zone numbers are encoded in file names, e.g., cma743 means start
% in CMA-7 and continue onto CMA-4 and CMA-3
for kcase=1:Ncase
    fnametot=['raytracer_ffoust_example/XgtY/' names{kcase}]
    %% Test plots to choose initial mode
    r0=r0s{kcase}
    perm1=fun_perm(0,r0);
    nx=nxs{kcase};
    nz=fwm_booker_bianisotropic(perm1,nx,0);
    ii=find(abs(imag(nz))>.1*abs(real(nz))); nz(ii)=nan;
    figure(101);
    plot(nx,real(nz),'r.'); grid on; axis equal; hold on;
    % Plot the initial values
    nx0s=nx0ss{kcase}; Nnx0=length(nx0s); % Initial nx
    ms=mss{kcase}; Nm=length(ms); % The mode number
    Ntraj=Nnx0*Nm; % Number of trajectories
    n0s=zeros(3,Ntraj);
    ktraj=1;
    for knx0=1:Nnx0
        nx0=nx0s(knx0);
        ny0=0;
        nzarr=fwm_booker_bianisotropic(perm1,nx0,ny0);
        for m=ms
            nz0=real(nzarr(m));
            n0s(:,ktraj)=[nx0;ny0;nz0];
            plot(nx0,nz0,'bo','markersize',5,'linewidth',3);
            text(nx0+.01,nz0+0.01,num2str(ktraj));
            ktraj=ktraj+1;
        end
    end
    hold off;
    drawnow
    echo_print({'EPS','PNG'},[fnametot '_n0']);
    %% Initialize the raytracing
    Ns=15000; % number of time steps
    rtrajs=zeros(3,Ns+1,Ntraj); ntrajs=rtrajs; ttrajs=zeros(Ns+1,Ntraj); FFtrajs=zeros(8,Ns+1,Ntraj);
    %% Do the raytracing
    for ktraj=1:Ntraj
        %% Info
        n0=n0s(:,ktraj);
        disp(['Trajectory ' num2str(ktraj) '/' num2str(Ntraj) ', n0=[' num2str(n0.') ']']);
        %% Raytracer
        fname=[fnametot '_' num2str(ktraj)];
        if ~exist([fname '.mat'],'file')
            % We use default values for dt, dr, dn, dw
            [rtraj,ntraj,ttraj,FFtraj]=raytracer_ffoust(fun_perm,...
                r0,n0,'num_steps',Ns,'regularizer',fun_regularizer);
            eval(['save ' fname '.mat rtraj ntraj ttraj FFtraj']);
        else
            eval(['load ' fname '.mat']);
        end
        %% Analysis of the modified Snell's law: n_th*R=const
        nrot=ntraj;
        th=pi/2-atan2(rtraj(3,:),rtraj(1,:));
        for ks=1:Ns+1
            ct=cos(th(ks)); st=sin(th(ks)); rotmxp=[ct 0 -st; 0 1 0; st 0 ct];
            nrot(:,ks)=rotmxp*ntraj(:,ks);
        end
        r=sqrt(sum(rtraj.^2));
        tmp=r.*nrot(1,:);
        disp(['nth*r varies from ' num2str(min(tmp)) ' to ' num2str(max(tmp))]);
        %% Plot the trajectory
        if do_figures
            figure(102);
            pp=[0:360]*pi/180;
            plot(Rl*cos(pp),Rl*sin(pp),'k--');  axis equal; hold on
            plot(Ry*cos(pp),Ry*sin(pp),'k--');
            plot(Rx*cos(pp),Rx*sin(pp),'k--');
            plot(Rs*cos(pp),Rs*sin(pp),'k--');
            plot(rtraj(1,:),rtraj(3,:));
            plot(rtraj(1,Ns+1),rtraj(3,Ns+1),'go');
            plot(r0(1),r0(3),'ro');
            hold off;
            drawnow
            echo_print({'EPS','PNG'},fname);
            pause(1);
        end
        %% Save into array
        rtrajs(:,:,ktraj)=rtraj; ntrajs(:,:,ktraj)=ntraj; ttrajs(:,ktraj)=ttraj; FFtrajs(:,:,ktraj)=FFtraj;
    end
    % Backup the trajectories
    eval(['save ' fnametot ' ntrajs rtrajs ttrajs FFtrajs nx0s ms n0s r0 Ns Ntraj']);
    %% Plot the whole thing
    figure(103);
    pp=[0:360]*pi/180;
    plot(Ry*cos(pp),Ry*sin(pp),'k--');  axis equal; hold on
    plot(cos(pp),sin(pp),'k--');
    plot(Rx*cos(pp),Rx*sin(pp),'k--');
    plot(Rl*cos(pp),Rl*sin(pp),'k--');
    plot(Rs*cos(pp),Rs*sin(pp),'k--');
    set(gca,'xlim',xlims{kcase},'ylim',ylims{kcase})
    for ktraj=1:Ntraj
        rtraj=rtrajs(:,:,ktraj);
        plot(rtraj(1,:),rtraj(3,:));
        plot(rtraj(1,Ns+1),rtraj(3,Ns+1),'go');
    end
    plot(r0(1),r0(3),'ro');
    hold off; drawnow;
    echo_print({'EPS','PNG'},fnametot);
    pause(5);
end


%%
kcase=3
eval(['load raytracer_ffoust_example/XgtY/' names{kcase}]);
ktraj=4;
rtraj=rtrajs(:,:,ktraj); ntraj=ntrajs(:,:,ktraj);
figure(104);
pp=[0:360]*pi/180;
nx=nxs{kcase};
for ks=1:100:Ns
    figure(104);
    subplot(1,2,1);
    plot(Ry*cos(pp),Ry*sin(pp),'k--'); axis equal; hold on
    plot(Rx*cos(pp),Rx*sin(pp),'k--');
    plot(Rs*cos(pp),Rs*sin(pp),'k--');
    plot(rtraj(1,1:ks),rtraj(3,1:ks));
    plot(rtraj(1,[1 ks]),rtraj(3,[1 ks]),'ro');
    set(gca,'xlim',xlims{kcase},'ylim',ylims{kcase})
    hold off;
    figure(104);
    subplot(1,2,2);
    rcur=rtraj(:,ks); ncur=ntraj(:,ks);
    p=fun_perm(0,rcur);
    nz=fwm_booker_bianisotropic(p,nx,0);
    ii=find(abs(imag(nz))>.1*abs(real(nz))); nz(ii)=nan;
    plot(nx,real(nz),'r.'); axis equal;
    hold on; plot(ncur(1),ncur(3),'ko','markersize',5,'linewidth',3); hold off;
    pause(0.1);
end


%%
if 0
figure;
r=sqrt(sum(rtraj.^2,1));
X=3./(r-1); Y=3./r;
figure; plot(r,[X ; Y ; 1-X./(1+Y) ; 1-X./(1-Y)].')
end