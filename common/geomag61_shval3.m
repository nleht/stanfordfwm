function [x,y,z]=geomag61_shval3(igdgc, flat, flon, elev, nmax, iext, ext1, ext2, ext3, gha)
% GEOMAG61_SHVAL3 Calculates field components from spherical harmonic (sh) models.                                                              */
% Input:
%    igdgc     - indicates coordinate system used; set equal
%                to 1 if geodetic, 2 if geocentric
%    latitude  - north latitude, in degrees
%    longitude - east longitude, in degrees
%    elev      - WGS84 altitude above mean sea level (igdgc=1), or
%                radial distance from earth's center (igdgc=2)
%    a2,b2     - squares of semi-major and semi-minor axes of
%                the reference spheroid used for transforming
%                between geodetic and geocentric coordinates
%                or components
%    nmax      - maximum degree and order of coefficients
%    iext      - external coefficients flag (=0 if none)
%    ext1,2,3  - the three 1st-degree external coefficients
%                (not used if iext = 0)
%    gha       - (was global in C) spherical harmonic coefficients
% Output:
%    x         - northward component
%    y         - eastward component
%    z         - vertically-downward component
% Based on subroutine 'igrf' by D. R. Barraclough and S. R. C. Malin,
% report no. 71/1, institute of geological sciences, U.K.
%
% FORTRAN
%    Norman W. Peddie
%    USGS, MS 964, box 25046 Federal Center, Denver, CO.  80225
% C
%    C. H. Shaffer
%    Lockheed Missiles and Space Company, Sunnyvale CA
%    August 17, 1988
% MATLAB
%    Nikolai G. Lehtinen
%    Stanford University, Stanford, CA
%    September 10, 2009

earths_radius = 6371.2;
dtr = pi/180; %0.01745329;
sl=zeros(1,13);
cl=zeros(1,13);
p=zeros(1,118);
q=zeros(1,118);
a2 = 40680631.59; % WGS84 square of major (transverse) radius "a", in km^2
b2 = 40408299.98; % WGS84 square of minor (conjugate) radius "b", in km^2
r = elev;
slat = sin( flat * dtr );
if ((90.0 - flat) < 0.001)
    aa = 89.999; %  300 ft. from North pole
else
    if ((90.0 + flat) < 0.001)
        aa = -89.999; %  300 ft. from South pole
    else
        aa = flat;
    end
end
clat = cos( aa * dtr );
arguement = flon * dtr;
sl(1) = sin( arguement );
cl(1) = cos( arguement );
x = 0;
y = 0;
z = 0;
sd = 0.0;
cd = 1.0;
l = 1;
n = 0;
m = 1;
npq = (nmax * (nmax + 3)) / 2;
if (igdgc == 1) % geodetic
    aa = a2 * clat * clat;
    bb = b2 * slat * slat;
    cc = aa + bb;
    dd = sqrt( cc );
    arguement = elev * (elev + 2.0 * dd) + (a2 * aa + b2 * bb) / cc;
    r = sqrt( arguement );
    cd = (elev + dd) / r;
    sd = (a2 - b2) / dd * slat * clat / r;
    aa = slat;
    slat = slat * cd - clat * sd;
    clat = clat * cd + aa * sd;
end
% Now: slat=sin(lambda), clat=cos(lambda), r=|r|
ratio = earths_radius / r;
aa = sqrt(3.0);
p(1) = 2.0 * slat;
p(2) = 2.0 * clat;
p(3) = 4.5 * slat * slat - 1.5;
p(4) = 3.0 * aa * clat * slat;
q(1) = -clat;
q(2) = slat;
q(3) = -3.0 * clat * slat;
q(4) = aa * (slat * slat - clat * clat);
for k=1:npq
    if (n < m)
        m = 0;
        n = n + 1;
        %arguement = ratio;
        %power =  n + 2;
        rr=ratio^(n+2);
        % rr = arguement^power;
        fn = n;
    end
    fm = m;
    % k={n,m} (corresponds to the pair of indeces), k == n(n+1)/2+m
    % n=1..nmax, m=0..n
    if (k >= 5)
        if (m == n)
            aa = sqrt(1.0 - 0.5/fm);
            j = k - n - 1; % corresponds to {n-1,n-1}
            p(k) = (1.0 + 1.0/fm) * aa * clat * p(j);
            q(k) = aa * (clat * q(j) + slat/fm * p(j));
            sl(m) = sl(m-1) * cl(1) + cl(m-1) * sl(1); % sin(m*lon)
            cl(m) = cl(m-1) * cl(1) - sl(m-1) * sl(1); % cos(m*lon)
        else
            aa = sqrt( fn*fn - fm*fm);
            bb = sqrt( ((fn - 1.0)*(fn-1.0)) - (fm * fm) )/aa;
            cc = (2.0 * fn - 1.0)/aa;
            ii = k - n; % corresponds to {n-1,m}
            j = k - 2 * n + 1; % corresponds to {n-2,m} (or {n-1,0} if m=n-1 (??))
            p(k) = (fn + 1.0) * (cc * slat/fn * p(ii) - bb/(fn - 1.0) * p(j));
            q(k) = cc * (slat * q(ii) - clat/fn * p(ii)) - bb * q(j);
        end
    end
    aa = rr * gha(l);
    if (m == 0)
        % only g_{n,m} is present in array gha
        x = x + aa * q(k);
        z = z - aa * p(k);
        l = l + 1;
    else
        % Both g_{n,m} and h_{n,m} are present in array gha
        bb = rr * gha(l+1); % rr*h_{n,m}
        cc = aa * cl(m) + bb * sl(m);
        x = x + cc * q(k);
        z = z - cc * p(k);
        if (clat > 0)
            y = y + (aa * sl(m) - bb * cl(m)) * ...
                fm * p(k)/((fn + 1.0) * clat);
        else
            y = y + (aa * sl(m) - bb * cl(m)) * q(k) * slat;
        end
        l = l + 2;
    end
    m = m + 1;
end
if (iext ~= 0)
    aa = ext2 * cl(1) + ext3 * sl(1);
    x = x - ext1 * clat + aa * slat;
    y = y + ext2 * sl(1) - ext3 * cl(1);
    z = z + ext1 * slat + aa * clat; 
end
aa = x;
x = x * cd + z * sd;
z = z * cd - aa * sd;

