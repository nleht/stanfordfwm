function decimal_date=geomag61_julday(i_month, i_day, i_year)
%GEOMAG61_JULDAY Decimal day of year from month, day, year
% Computes the decimal day of year from month, day, year.
% Leap years accounted for 1900 and 2000 are not leap years.
%     Input:
%           year - Integer year of interest
%           month - Integer month of interest
%           day - Integer day of interest
%     Output:
%           date - Julian date to thousandth of year
%
%     FORTRAN
%           S. McLean
%           NGDC, NOAA egc1, 325 Broadway, Boulder CO.  80301
%     C
%           C. H. Shaffer
%           Lockheed Missiles and Space Company, Sunnyvale CA
%           August 12, 1988
%     Julday Bug Fix
%           Thanks to Rob Raper

aggregate_first_day_of_month=zeros(1,12);
leap_year = 0;
remainder = 0.0;
divisor = 4.0;
    
aggregate_first_day_of_month(1) = 1;
aggregate_first_day_of_month(2) = 32;
aggregate_first_day_of_month(3) = 60;
aggregate_first_day_of_month(4) = 91;
aggregate_first_day_of_month(5) = 121;
aggregate_first_day_of_month(6) = 152;
aggregate_first_day_of_month(7) = 182;
aggregate_first_day_of_month(8) = 213;
aggregate_first_day_of_month(9) = 244;
aggregate_first_day_of_month(10) = 274;
aggregate_first_day_of_month(11) = 305;
aggregate_first_day_of_month(12) = 335;

% Test for leap year.  If true add one to day.

year = i_year;                               %    Century Years not
if ((i_year ~= 1900) && (i_year ~= 2100))    %  divisible by 400 are NOT leap years
    dividend = year/divisor;
    truncated_dividend = dividend;
    left_over = dividend - truncated_dividend;
    remainder = left_over*divisor;
    if ((remainder > 0.0) && (i_month > 2))
        leap_year = 1;
    else
        leap_year = 0;
    end
end
day = aggregate_first_day_of_month(i_month) + i_day - 1 + leap_year;
if (leap_year)
    decimal_date = year + (day/366.0);  %In version 3.0 this was incorrect
else
    decimal_date = year + (day/365.0);  %In version 3.0 this was incorrect
end

