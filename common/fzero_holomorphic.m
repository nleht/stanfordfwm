function [zsol,zmul,zr,zi,zbox,fbox]=fzero_holomorphic(fun,params,...
    zrmin,zrmax,zimin,zimax,dzmax,dztol,dftol,nref,debugflag,recur,nztarget)
%HOLOMORPHIC_ZEROS Find zeros of a holomorphic (analytic) function
% Usage:
%    [zsol,zmul]=fzero_holomorphic(...
%       fun,params,zrmin,zrmax,zimin,zimax[,dzmax,dztol,dftol,nref,debugflag]);
% Advanced usage:
%    [zsol,zmul,zr,zi,zbox,fbox]=fzero_holomorphic(...
%       fun,params,zrmin,zrmax,zimin,zimax[,dzmax,dztol,dftol,nref,debugflag]);
% Inputs:
%    fun - function handle, the first argument must be "z"
%    params - cell array of parameters (the rest of the arguments of "fun")
%    zrmin,zrmax,zimin,zimax - the min/max or real/imag parts of z
% Optional inputs:
%    dzmax - maximum value of dz for the grid of values of "fun"
%    dztol - maximum distance between separate solutions, otherwise they
%       are treated as multiple roots (default=1e-12)
%    dftol - maximum error in f/(df/dz) (default=dztol or 1e-12)
%    nref - refining coefficient (default=10)
%    debugflag - default=0
% Outputs:
%    zsol, zmul - zeros and their multiplicities
% Optional outputs (for debugging):
%    zr,zi - grid between z{r|i}min and z{r|i}max
%    zbox - 2D grid in the complex plane
%    fbox - values of "fun" on this grid
% See also: fzero_holomorphic_numzeros, fzero_holomorphic_newtonraphson
% Author: Nikolai G. Lehtinen

%% Input args
if nargin<13
    nztarget=[];
end
if nargin<12
    recur=[]; % Must never be called
end
if isempty(recur)
    recur=0;
end
margins=repmat('=> ',[1 recur]);
prompt=[margins 'FZERO_HOLOMORPHIC: '];
if ~isempty(nztarget) && recur==0
    if debugflag>0
        disp([prompt 'Ignoring the given number of zeros: ' num2str(nztarget)])
    end
    nztarget=[];
end
if nargin<11
    debugflag=0;
end
if nargin<10
    nref=[];
end
if nargin<9
    dftol=[];
end
if nargin<8
    dztol=[];
end
if nargin<7
    dzmax=[];
end
% Make sure zr and zi are ordered correctly
if zrmax<=zrmin || zimax<=zimin
    error('zmax<=zmin');
end
% Default values
if isempty(nref)
    nref=10;
elseif nref<2
    error('nref<2');
end
zrrange=zrmax-zrmin; zirange=zimax-zimin;
minrange=min(zrrange,zirange);
maxrange=max(zrrange,zirange);
if isempty(dzmax)
    dzmax=minrange/nref;
end
if isempty(dztol)
    dztol=1e-12;
end
if isempty(dftol)
    dftol=dztol;
end
%% Arguments
if debugflag>1
    disp(['zrmin=' num2str(zrmin) ',zrmax=' num2str(zrmax) ',zimin=' num2str(zimin) ',zimax=' num2str(zimax) ...
        ',dzmax=' num2str(dzmax) ',dztol=' num2str(dztol) ',dftol=' num2str(dftol) ',nref=' num2str(nref) ',debugflag=' num2str(debugflag)]);
end
%% How many zeros?
if isempty(nztarget)
    zcp=[zrmin+1i*zimin ; zrmax+1i*zimin ; zrmax+1i*zimax ; zrmin+1i*zimax];
    while 1
        if debugflag>0
            disp([prompt 'How many zeros? dzmax=' num2str(dzmax)])
        end
        [nztargetnew,nzon,zb,fb]=fzero_holomorphic_numzeros(fun,params,zcp,dzmax,dztol,nref,debugflag,margins);
        if nzon
            disp([prompt 'There are roots directly on the boundary, the return arguments are [[],zb,fb]']);
            disp([prompt 'To debug, check if f(z) is discontinuous'])
            zsol=[]; zmul=zb; zr=fb;
            zi=[]; zbox=[]; fbox=[];
            return
        end
        if debugflag>0
            disp([prompt 'Found ' num2str(nztargetnew) ' zeros'])
        end
        if ~isempty(nztarget)
            if nztargetnew==nztarget
                break
            end
        end
        dzmax=dzmax/2;
        nztarget=nztargetnew;
    end
    if nztarget<0
        disp([prompt 'No roots, only poles!']);
        zsol=[]; zmul=zb; zr=fb;
        zi=[]; zbox=[]; fbox=[];
        return
    elseif nztarget==0
        disp([prompt 'There are no roots, the return arguments are [[],zb,fb]']);
        zsol=[]; zmul=zb; zr=fb;
        zi=[]; zbox=[]; fbox=[];
        return;
    end
end
if debugflag>0
    disp([prompt 'Expecting to find ' num2str(nztarget) ' zeros'])
end

%% Fill a coarse grid
%dz=minrange/max(nref,nztarget);
%dz=min([dzmax,minrange/max(nref,nztarget),maxrange/(nref*nztarget)]);
dz=min(minrange/max(nref,nztarget),maxrange/(nref*nztarget));
%dz=min(dzmax,minrange/nztarget);
if debugflag>0
    disp([prompt 'dz=' num2str(dz)]);
end
zr=linspace(zrmin,zrmax,ceil(zrrange/dz)+1);
zi=linspace(zimin,zimax,ceil(zirange/dz)+1);
[zrm,zim]=ndgrid(zr,zi);
zbox=zrm+1i*zim;
[Nr,Ni]=size(zbox);
% Return if no roots
if nztarget>0 || nargout>5
    if debugflag>0
        disp([prompt 'Filling (' num2str(Nr) ' x ' num2str(Ni) ') matrix ...']);
    end
    tstart=now*24*3600;
    fbox=reshape(fun(zbox(:),params{:}),[Nr Ni]);
    if debugflag>0
        disp([prompt 'Filled the matrix, time=' hms(now*24*3600-tstart)]);
    end
    if debugflag>2
        zsol='box';
        zmul=[];
        return
    end
else
    nztarget
    nargout
end
if nztarget==0
    zsol=[]; zmul=[];
    ir=[]; ii=[];
    return;
end

%% Find boxes in which the roots are approximately located
% Find boxes in which imag(F) and real(F) change sign
tmpi=1*(imag(fbox)>0);
tmpr=1*(real(fbox)>0);
%[indr,indi]=ndgrid(1:Nr,1:Ni);
[indr,indi]=ndgrid(1:Nr-1,1:Ni-1);
% NOTE: This condition has to be improved
dti1=diff(tmpi,[],1); dti2=diff(tmpi,[],2);
dtr1=diff(tmpr,[],1); dtr2=diff(tmpr,[],2);
%ifound=find(([dti1;zeros(1,Ni)]~=0 | [zeros(1,Ni);dti1]~=0 | [dti2 zeros(Nr,1)]~=0 | [zeros(Nr,1) dti2]~=0 ) ...
%    & ([dtr1;zeros(1,Ni)]~=0 | [zeros(1,Ni);dtr1]~=0 | [dtr2 zeros(Nr,1)]~=0 | [zeros(Nr,1) dtr2]~=0 ) ...
%    & indr<Nr & indi<Ni);
ifound=find((dti1(:,1:Ni-1)~=0 | dti1(:,2:Ni)~=0 | dti2(1:Nr-1,:)~=0 | dti2(2:Nr,:)~=0 ) ...
    & (dtr1(:,1:Ni-1)~=0 | dtr1(:,2:Ni)~=0 | dtr2(1:Nr-1,:)~=0 | dtr2(2:Nr,:)~=0 ));
ir=indr(ifound);
ii=indi(ifound);
nboxes=length(ifound);
% Initial box corners
iboxr=zeros(2,nboxes);
iboxi=zeros(2,nboxes);
iboxr(1,:)=ir; iboxr(2,:)=ir+1;
iboxi(1,:)=ii; iboxi(2,:)=ii+1;
if debugflag>0
    disp([prompt 'Found ' num2str(nboxes) ' initial boxes with zeros']);
end

%% Count the roots
while 1
    %% Congregate boxes into continuous regions
    region=zeros(1,nboxes); nregions=0;
    for k=1:nboxes
        if region(k)==0
            nregions=nregions+1;
            region(k)=nregions;
        end
        for kk=k+1:nboxes
            intersectr=any(iboxr(1,k)<=iboxr(:,kk) & iboxr(2,k)>=iboxr(:,kk));
            intersecti=any(iboxi(1,k)<=iboxi(:,kk) & iboxi(2,k)>=iboxi(:,kk));
            %if abs(ir(kk)-ir(k))<=1 && abs(ii(kk)-ii(k))<=1
            if intersectr && intersecti
                % They belong to the same region
                region(kk)=region(k);
            end
        end
    end
    if debugflag>1
        iboxr
        iboxi
        region
    end
    if debugflag>0
        disp([prompt 'Congregated ' num2str(nboxes) ' boxes into ' num2str(nregions) ' regions']);
    end
    % Region corners
    iregionr=zeros(2,nregions);
    iregioni=zeros(2,nregions);
    for k=1:nregions
        ix=find(region==k);
        iregionr(:,k)=[min(iboxr(1,ix)) max(iboxr(2,ix))];
        iregioni(:,k)=[min(iboxi(1,ix)) max(iboxi(2,ix))];
    end
    if debugflag>1
        iregionr
        iregioni
    end
    %% Count the roots in the regions
    if debugflag>0
        disp([prompt 'Counting the roots ...']);
    end
    nzin=zeros(1,nregions);
    for k=1:nregions
        if debugflag>0
            disp([prompt 'Region ' num2str(k) '/' num2str(nregions)]);
        end
        ir1=iregionr(1,k); ii1=iregioni(1,k);
        ir2=iregionr(2,k); ii2=iregioni(2,k);
        z1=zbox(ir1,ii1);
        z2=zbox(ir2,ii2);
        za=zbox(ir2,ii1);
        zb=zbox(ir1,ii2);
        [nztmp,nzon]=fzero_holomorphic_numzeros(fun,params,[z1;za;z2;zb],dz/nref,dztol,nref,debugflag,margins);
        if debugflag>0
            disp([prompt ' - found '  num2str(nztmp) ' roots, total so far = ' ...
                num2str(sum(nzin(1:k-1))+nztmp) '/' num2str(nztarget)]);
        end
        if nzon
            if debugflag>0
                disp([prompt 'Roots on boundary!']);
            end
            % break
        end
        nzin(k)=nztmp;
    end
    nzfound=sum(nzin);
    if debugflag>0
        disp([prompt 'Found ' num2str(nzfound) ' roots, need ' num2str(nztarget)]);
    end
    %% Check if we need to extend the regions
    % Extended regions are called "boxes"
    if nzfound>nztarget
        error('nzfound>nztarget');
    end
    if nzfound==nztarget
        break
    else
        % Extend the regions
        if debugflag>0
            disp([prompt 'Extending the regions and re-counting']);
        end
        nboxes=nregions;
        iboxr=zeros(2,nboxes);
        iboxi=zeros(2,nboxes);
        for k=1:nboxes
            iboxr(1,k)=max(1,iregionr(1,k)-1);
            iboxi(1,k)=max(1,iregioni(1,k)-1);
            iboxr(2,k)=min(Nr,iregionr(2,k)+1);
            iboxi(2,k)=min(Ni,iregioni(2,k)+1);
        end
    end
end

%% Find the roots in the regions
if debugflag>0
    disp([prompt 'Calculating all the roots ...']);
end
zsol=[]; zmul=[];
for k=1:nregions
    ir1=iregionr(1,k); ii1=iregioni(1,k);
    ir2=iregionr(2,k); ii2=iregioni(2,k);
    n=nzin(k);
    if n==0
        if debugflag>0
            disp([prompt 'Region ' num2str(k) '/' num2str(nregions) ' without solutions']);
        end
        continue
    end
    z1=zbox(ir1,ii1);
    z2=zbox(ir2,ii2);
    if debugflag>0
        disp([prompt 'Region ' num2str(k) '/' num2str(nregions) ' with ' ...
            num2str(nzin(k)) ' solutions (z1=' num2str(z1) '; z2=' num2str(z2) ')']);
    end
    if n==1
        % Newton-Raphson
        z=fzero_holomorphic_newtonraphson(fun,params,z1,z2,dztol,dftol,debugflag,recur);
        zm=1;
    else % n>1
        if abs(z2-z1)<dztol
            % Label root as multiple
            z=fzero_holomorphic_newtonraphson(fun,params,z1,z2,dztol,dftol,debugflag,recur);
            zm=n;
            if debugflag>0
                disp([prompt 'Multiple root, multiplicity=' num2str(n)])
            end
        else
            % Subdivide the box again
            if debugflag>0
                disp([prompt 'Subdividing']);
            end
            [z,zm]=fzero_holomorphic(fun,params,real(z1),real(z2),imag(z1),imag(z2),dz/nref,dztol,dftol,nref,debugflag,recur+1,n);
        end
    end
    if debugflag>0
        disp([prompt 'Region ' num2str(k) '/' num2str(nregions) ...
            ': completed calculations, found ' num2str(sum(zm)) ...
            ' solutions (total so far ' num2str(sum([zmul zm])) '/' num2str(nztarget) ')']);
    end
    zsol=[zsol z];
    zmul=[zmul zm];
end
if debugflag>0
    disp([prompt 'Finished with ' num2str(sum(zmul)) ' solutions'])
end
%% There should not be any duplicates
nz=length(zsol);
for k=1:nz
    if length(find(abs(zsol-zsol(k))<dztol))>1
        disp('Duplicate solutions!')
    end
end

%% Final touch: Sort by real part
[y,ii]=sort(real(zsol));
zsol=zsol(ii);
zmul=zmul(ii);
