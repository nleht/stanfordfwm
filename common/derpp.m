function dpp=derpp(pp)
%DERPP Derivative of a polynomial with matrix coefficients
%See also: FNDER
[breaks,coefs,pieces,order,dim] = unmkpp(pp);
dpp = mkpp(breaks,repmat(order-1:-1:1,prod(dim)*pieces,1).*coefs(:,1:order-1),dim);
