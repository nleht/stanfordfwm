function varargout=parsearguments(args,nmin,allowedkeys)
%PARSEARGUMENTS Parse the input arguments for a function
%
% Usage example:
%   function result=myfunction(varargin)
%   allowedkeys={'key1','key2'};
%   nmin=2; % minimum required arguments of myfunction
%   [arg1,arg2,arg3,options]=parsearguments(varargin,nmin,allowedkeys)
%   value1=getvaluefromdict(options,'key1',defaultvalue1);
%   % options is a cell array with keys and values
% Then this function can be called like this:
%   res=myfunction(arg1,arg2,'key2',value2);
% Then arg3 will be assigned a value of empty matrix, OPTIONS is
% {'key2',value2}.
% The following usages of MYFUNCTION will generate an error:
%   res=myfunction(arg1,'key2',value2); % not enough arguments, nmin=2
%   res=myfunction(arg1,arg2,'key3',value3); % unknown key 'key3'
% This will give a warning (in GETVALUEFROMDICT):
%   res=myfunction(arg1,arg2,'key1',value1a,'key1',value1b);
% IMPORTANT NOTE: arguments after nmin cannot be strings!
% See also: GETVALUEFROMDICT, GETSUBDICT
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.

% Similar to standard function "parseparams"
charsrch=[];
for i=nmin+1:length(args),
   charsrch=[charsrch ischar(args{i})];
end
charindx=nmin+find(charsrch);
if isempty(charindx),
   regargs=args;
   proppairs=args(1:0);
else
   regargs=args(1:charindx(1)-1);
   proppairs=args(charindx(1):end);
end

nmax=nargout-1;
nargs=length(regargs);
error(nargchk(nmin,nmax,nargs));
for k=1:nmax
    if k<=nargs
        varargout{k}=regargs{k};
    else
        varargout{k}=[];
    end
end
if rem(length(proppairs),2)~=0
   error('Option value pairs expected.')
end
nprop=length(proppairs)/2;
for k=1:nprop
    key=proppairs{2*k-1};
    ii=find(strcmp(key,allowedkeys));
    if isempty(ii)
        error(['Key ''' key ''' is not allowed']);
    end
end
varargout{nmax+1}=proppairs;
