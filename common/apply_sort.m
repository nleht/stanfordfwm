function f=apply_sort(f,isort)
%APPLY_SORT Apply sorting indeces isort(:,k) to f(:,k)
% Usage: f=apply_sort(f,isort)
% See also: SMOOTH_SORT, SIMPLE_EXTRAP, SORT_MIN_DISTANCE
[nf,nt]=size(f);
if any(size(isort)~=[nf,nt])
    error('wrong size')
end
f=f(isort+repmat([0:nt-1]*nf,[nf 1]));
