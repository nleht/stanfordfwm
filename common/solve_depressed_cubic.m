function [t0,err]=solve_depressed_cubic(p0,q0)
%SOLVE_DEPRESSED_QUBIC Solve t^3+3*p*t+2*q=0
% Can take 1D (column) arrays of p and q.
% Use Cardano method.
% See Wikipedia.
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.

% Check the sizes
p0=p0(:); q0=q0(:);
lengths=[length(p0),length(q0)];
n=max(lengths);
if n>1
    if length(p0)==1
        p0=repmat(p0,n,1);
    end
    if length(q0)==1
        q0=repmat(q0,n,1);
    end
end
lengths=[length(p0),length(q0)];
if any(lengths~=n)
    error('wrong lengths');
end

% Collect data here
t0=nan(n,3);

% Scale the coefficients, so that |p|<=1, |q|<=1:
s=max(sqrt(abs(p0)),abs(q0).^(1/3));
% Trivial equation
trivial=(s==0);
if any(trivial)
	iit=find(trivial);
    t0(iit,:)=0;
end
if all(trivial)
	err=0
    return
end

iint=find(~trivial);
p=p0(iint)./s(iint).^2; q=q0(iint)./s(iint).^3;
% Main thing here
[t,err]=solve_cardano_aux(p,q);
% Merge with trivial solutions
t0(iint,:)=repmat(s(iint),1,3).*t;
