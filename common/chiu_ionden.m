function [qtot,qi]=chiu_ionden(z,rzur,phi,tmo,rlt,rltm,rlgm,dip)
% This subroutine computes the electron density as described in J. Atmos.
% Terres. Phys. 1615, 35, 1973.
% Output
%	qtot	=   total electron density in units of 1.e+5*cm^-3
%	qi(1)	= e layer electron density  "   "   "       "
%	  (2)	= f1  "      "        "     "   "   "       "
%	  (3)	= f2  "      "        "     "   "   "       "
% Input
%	z	= altitude in km (90 < z < 500, see "option" below)
%	rzur	= zurich smoothed sunspot number
%	rlgm	= geomagnetic east longitude in radians 
%	rltm	=      "      latitude in radians
%	dip	=      "      dip angle "    "
%	rlt	= geographic  latitude in radians
%	phi	= local time angle in radians measured from midnight
%	tmo	= annual time in months from dec. 15 of previous year
% Options
%	Set z=0 to obtain the peak density for layers (qi(j),j=1,3)
%		qtot=0. in this case
% Notes
%	Due to lack of data, the f2 density has not been made continuous
%	at the south magnetic pole.
%	rlgm, rltm & dip can be derived from IGRF (JGR 74, 4407, 1969).
%
% This function was adapted from a FORTRAN function from NASA models web
% site.
qi=zeros(1,3);
var=qi;
hmax=[10. 34. 0];
amp=[1.36 2.44 .66];
alf=[.5 .5 1.];
zmax=[110. 180. 0];

sdec=.39795*sin(pi*(tmo-3.167)/6.);
dec=asin(sdec);
delp=abs(abs(rlt)-pi/2.);
if delp<=1.e-03
    seasn=rlt*dec
    if seasn<0
        phi=0;
    end
    if seasn>=0
        phi=pi;
    end
end
r=rzur/100.;
cltm=cos(rltm);
sltm=sin(rltm);
xlam=1.+.5*log(1.+30.*r);
var(1)=chiu_tvef1(1.15,0.,.4,2.,rlt,r,phi,dec);
var(2)=chiu_tvef1(1.24,.25,.25,xlam,rlt,r,phi,dec);
var(3)=chiu_tvarf2(rltm,rlgm,dip,r,phi,tmo,dec,pi,cltm,sltm);
if z~=0
    zalf=-4.5*abs(rltm)-pi;
    zba=240.+10.*cltm*cos(pi*(tmo/3.-1.5));
    zbar=zba+r*(75.+83.*cltm*sin(dec)*sin(rltm));
    zmax(3)=zbar+30.*cos(phi+zalf);
    hmax(3)=.2*zmax(3)+40.;
    if z<zmax(3)
        hmax(3)=.2*z+40.;
    end
    sum=0.;
    for i=1:3
        zp=(z-zmax(i))/hmax(i);
        cz=exp(alf(i)*(1.-zp-exp(-zp)));
        qi(i)=amp(i)*var(i)*cz;
        sum=sum+qi(i);
    end
    qtot=sum;
    return
else
    for i=1:3
        qi(i)=amp(i)*var(i)
    end
    qtot=0.;
    return
end
end

function tvef1=chiu_tvef1 (a,b,c,d,rlt,r,phi,dec)
rf=sqrt(1.+(a+b*r)*r);
csl=cos(rlt);
csx=-csl*cos(dec)*cos(phi)+sin(rlt)*sin(dec);
csx2=sqrt(abs(csx));
if csx<0
    xf=-csx2;
else
    xf=csx2;
end
vut=chiu_w(c,rlt,phi,dec);
vdiur=exp(d*(xf-1.));
tvef1=rf*vdiur*vut;
return
end

function tvarf2=chiu_tvarf2(rltm,rlgm,dip,r,phi,tmo,dec,pi,cltm,sltm)
altm=abs(rltm);
atmo=tmo*pi/6.;
%semi=chiu_semian(atmo);
semi=.5-cos(2*atmo)+cos(atmo);
req=1.-.2*r+.6*sqrt(r);
sd=zeta(dec,rltm);
x=(2.2+(.2+.1*r)*sltm)*cltm;
ff=exp(-x^6);
gg=1.-ff;
cpd=cos(phi-.873);
ef=cos(phi+pi/4.);
emf=ef*ef;
adiur=(.9+.32*sd)*(1.+sd*emf);
bq=cos(altm-.2618);
aqe=cltm^8;
aqt=aqe*cltm*cltm;
aeq=aqe*req*exp(.25*(1.-cpd));
eq=(1.-.4*aqt)*(1.+aeq*bq^12)*(1.+.6*aqt*emf);
veq=eq*(1.+.05*semi);
vdiur=adiur*exp(-1.1*(cpd+1.));
vlt=(exp(3.*cos(rltm*(sin(phi)-1.)/2.)))*(1.2-.5*cltm*cltm);
vlt=vlt*(1.+.05*r*cos(atmo)*sltm^3);
rtl=sqrt((12.*rltm+4.*pi/3.)^2+(tmo/2.-3.)^2);
vlat=vlt*(1.-.15*exp(-rtl));
rf=1.+r+(.204+.03*r)*r*r;
if r>1.1
    cq=1.53*sltm*sltm
    rf=2.39+cq*(rf-2.39)
end
vut=chiu_yonii(rltm,rf,r,phi,tmo,dec,pi,cltm,sltm);
poler=chiu_polar(rltm,rlgm,dip,r,phi,tmo,dec,pi,cltm,sltm);
shift=7.*pi/18.;
vlong=1.+.1*(cltm^3)*cos(2.*(rlgm-shift));
adip=abs(dip);
dp=.15-.5*(1.+r)*(1.-cltm)*exp(-.33*(tmo-6.)^2);
vdp=1.+dp*exp(-18.*(adip-40.*pi/180.)^2);
vdip=vdp*(1.+.03*semi);
f2=vdiur*vlat*vut*veq*rf*vlong*vdip;
tvarf2=ff*poler+gg*f2;
return
end

function polar=chiu_polar(rltm,rlgm,dip,r,phi,tmo,dec,pi,cltm,sltm)
t=pi*tmo/12.;
v=sin(t);
u=cos(t+t);
y=sin(rlgm/2.);
ys=cos(rlgm/2.-pi/20.);
z=sin(rlgm);
za=sqrt(abs(z));
am=1.+v;
if rltm>=0
    c=-23.5*pi/180.;
    polar=(2.+1.2*r)*chiu_w(1.2,rltm,phi,c)*(1.+.3*v);
else
    b=v*(.5*y-.5*z-y^8)-am*u*(z/za)*exp(-4.*y*y);
    polar=2.5+2.0*r+u*(0.5+(1.3+.2*r)*ys^4);
    polar=polar+(1.3+0.5*r)*cos(phi-pi*(1.+b));
    polar=polar*(1.+0.4*(1.-v*v))*exp(-1.0*v*ys^4);
end
return
end

function yonii=chiu_yonii(rltm,rf,r,phi,tmo,dec,pi,cltm,sltm)
b=1.3+(.139*(1.+cos(rltm-pi/4.))+.0517*r)*r*r;
drf=1./rf;
w1=pi/6.;
w2=w1+w1;
de=.1778*r*r;
altm=abs(rltm);
snx=sin(altm-.5236);
ae=.2*(1.-snx);
bltm=abs(altm-pi/9.);
sx=sin(bltm);
fe=.13-.06*sx;
ym=cos(rltm+dec);
cphg=cos(phi);
xtc=ym^3*(1.-cphg)^.25;
ytc=-(.15+.3*sin(altm))*xtc;
t1=ae*(1.+.6*cos(w2*(tmo-4.)))*cos(w1*(tmo-1.));
triv=(cos(rltm-w1))*(cos(w1*(.5*tmo-1.)))^3;
triv=triv+(cos(rltm+pi/4.))*(cos(w1*(.5*tmo-4.)))^2;
qq=1.+.085*triv;
t2=.7*(qq+de*drf*cos(w2*(tmo-4.3)))*chiu_w(b,rltm,phi,dec);
t3=fe*cos(w2*(tmo-4.5))+ytc;
yonii=(t1+t3)*drf+t2;
return
end

function w=chiu_w(b,xi,eta,dec)
p=xi+dec*cos(eta);
w=exp(-b*(cos(p)-cos(xi)));
return
end

