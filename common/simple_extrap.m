function f0=simple_extrap(t,tp,fp)
%SIMPLE_EXTRAP Extrapolate a function from previous values up to 2nd order
% Usage: f0=simple_extrap(t,tp,fp)
% See also: SMOOTH_SORT, APPLY_SORT, SORT_MIN_DISTANCE
np=numel(tp);
if np==0
    error('must have previous values!')
end
if numel(fp)~=np
    error('fp must be of the same length as tp');
end
nf=numel(fp{1});
fx=zeros(size(fp{1}));
% First derivative
dt=t-tp{1};
if np<2
    dfdt=fx;
else
    dfdt=(fp{1}-fp{2})/(tp{1}-tp{2});
end
% Second derivative
if np<3
    d2fdt2=fx;
else
    dfdt1=(fp{2}-fp{3})/(tp{2}-tp{3});
    d2fdt2=2*(dfdt-dfdt1)/(tp{1}-tp{3});
    % re-extrapolate the first derivative
    dfdt=dfdt+d2fdt2*(tp{1}-tp{2})/2;
end
f0=fp{1}+dfdt*dt+d2fdt2*dt^2/2;
