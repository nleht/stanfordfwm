function nu=get_nu_electron_neutral_vuthaluru02(h)
nu=1e8*exp((log(5e4)-log(1e8))/(100-50)*(h-50));
