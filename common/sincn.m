function res=sincn(n,x)
%SINCN Generalization of the SINC function [different from Sýkora, 2007]
% Definition:
%     sincn(n,w*tau)=Integral[t^n*exp(i*w*t),{t,-tau,tau}]/(2*tau)^(n+1)
% or
%     sincn(n,x)=Integral[t^n*exp(i*x*t),{t,-1,1}]/2^(n+1)
% Note that sincn(0,x)=sinc(x/pi) (there is an additional factor of pi).
%
% The application of this function is in Fourier transform or Dyson series
% expansions of Taylor-expanded (slowly-changing) functions on small
% intervals
%
% WARNING: This definition is different from the one introduced by
%     Stanislav Sýkora, K-Space Images of n-Dimensional Spheres and
%     Generalized Sinc Functions, Copyright ©2007.
%
% See also: SINC, SINCN0, SINCMN
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.
if n<0
    error('n<0')
end
if round(n)~=n
    error('n is not integer')
end
switch n
    case 0
        res=sinc(x/pi);
    case 1
        in=find(x~=0);
        res=zeros(size(x));
        xn=x(in);
        res(in)=(cos(xn)-sinc(xn/pi))./(2i*xn);
    case 2
        i0=find(x==0);
        in=find(x~=0);
        res=zeros(size(x));
        xn=x(in);
        res(i0)=1/12;
        res(in)=sinc(xn/pi)/4+(cos(xn)-sinc(xn/pi))./(2*xn.^2);
    otherwise
        % Use recursion
        iseven=(2*floor(n/2)==n);
        i0=find(x==0);
        in=find(x~=0);
        res=zeros(size(x));
        xn=x(in);
        if iseven
            % calculated by direct integration
            res(i0)=1/(2^n*(n+1));
        end
        res(in)=-sincn(n-1,xn)*n./(2i*xn);
        if iseven
            res(in)=res(in)+sinc(xn/pi)/2^n;
        else
            res(in)=res(in)+cos(xn)./(2^n*1i*xn);
        end
end
