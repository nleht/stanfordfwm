function loadconstants
% LOADCONSTANTS Load various physical constants
global me ech kB clight mu0 eps0 impedance0 ...
    rclass0 uAtomic REarth Ggrav hbar ...
    aBohr sigStefan global_dir ...
    N2 O2 species composition atm_unit torr_unit NAvogadro ...
    g_in_toz
disp('**** READING PHYSICAL CONSTANTS ****');

%% Determine the directory with the data
if isempty(global_dir)
    global_dir = fullfile(getenv('HOME'),'Documents','MATLAB','stanfordfwm','inputs');
end

%% Physical constants
me=9.1093826e-31; % electron mass in kg
REarth=6378.137; % IUGG value for the equatorial radius of the Earth, km
uAtomic=1.66053886e-27; % Atomic mass unit, =MC/12.
% Electron charge in C
ech=1.60217653e-19;
% Boltzmann constant
kB=1.3806505e-23; % J/K
mu0=4*pi*1e-7;
clight=299792458;
eps0=1/(mu0*clight^2);
% Free space impedance
impedance0=sqrt(mu0/eps0);
% Classical electron radius
rclass0=ech^2/(4*pi*eps0*me*clight^2);
% Planck constant
hbar=1.05457168e-34;
% Bohr radius
aBohr=4*pi*eps0*hbar^2/(me*ech^2);
% Stefan-Boltzmann constant, J/s/m^2/K^4
sigStefan=pi^2/60*kB^4/hbar^3/clight^2;

% Gravitational constant
Ggrav=6.6742e-11;

%whaarp=2*pi*1e6;
%B=3.569e-5; % To get 1 MHz gyrofrequency
%wH=ech*B/me;
%wHhaarp=geomagb(62*pi/180,-145*pi/180,0)*ech/me;

% Pressure units (see Wikipedia) - in Pascals
atm_unit=101325;
torr_unit=atm_unit/760;
NAvogadro=6.0221415e23; % Dimensionless

%% The electron-molecule collision cross-section data
% All cross-sections are in 10^{-20}m^2=1A*1A
%% Basic cross-section information
N2=readelendifdata(fullfile(global_dir, 'N2elendif.txt'));
O2=readelendifdata(fullfile(global_dir, 'O2elendif.txt'));

%% A quantity used in the ionization cross-section
N2.EBR=13; O2.EBR=17.4;
% ds/dEs=stot/(EBR*atan((Ep-Ei)/(2*EBR)))/(1+(Es/EBR)^2)
% Ei - energy loss (ionization threshold)
% Ep - primary electron energy (before collision)
% Es - secondary electron energy (0<Es<(Ep-Ei)/2) (after collision)
% Reference: Opal et al, 1971, doi:10.1063/1.1676707

%% Momentum transfer cross-section at low energies
% At low energies - use analytical formulas:
% Nitrogen
N2.em1=0.02;
N2.em0=0.02;
N2.qm01=1.95; N2.qm00=0.9;
% Oxygen
% - from Lawton and Phelps [1978], Figure 7
O2.em1=0.05;
% Below em1, Qm ~ qm00+qm01*(en/em0).^(1/2)
O2.em0=0.02;
O2.qm01=1; O2.qm00=0;

%% Special treatment of the 3-body attachment
%O2.q(:,16)=O2.q(:,16)*Nm/Nm0;
N2.numbodies=ones(1,N2.numxsec);
O2.numbodies=ones(1,O2.numxsec);
O2.numbodies(16)=2;
% Collision rate is ~ N[SP]^(SP.numbodies(v))
O2.q(1:36,16)=O2.q(1:36,16)*1e-22; % must be x N[O2] (m^{-3})

%% The continuous energy loss cross-sections
% Must be excluded if considering discrete energy loss cross-sections
N2.iscontinuous=zeros(1,N2.numxsec);
N2.iscontinuous(1)=1;
O2.iscontinuous=zeros(1,O2.numxsec);
O2.iscontinuous(1)=1;

%% Air composition
species=[N2 O2];
composition=[.8 .2];

%% Other
g_in_toz=31.1034768; % grams in troy ounce
