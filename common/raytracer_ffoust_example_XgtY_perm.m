function perm=raytracer_ffoust_example_XgtY_perm(dw,r)
if r<1
    error('should not get there');
end
N=size(r,2);
x=r(1,:); z=r(3,:);
R2=x.^2+z.^2;
Yx=3*x./R2;
Yz=3*z./R2;
Y=sqrt(Yx.^2+Yz.^2);
X=3./(sqrt(R2)-1);
U=1;
perm=zeros(3,3,N);
% At shifted frequency
w=1+dw;
Xs=X/w^2; Ys=Y/w;
R=1-Xs./(U-Ys); L=1-Xs./(U+Ys);
S=(R+L)/2; P=1-Xs/U; D=(R-L)/2;
for k=1:N
    ct=Yz(k)/Y(k); st=Yx(k)/Y(k);
    % Active
    rotmxa=[ct 0 st; 0 1 0; -st 0 ct];
    % Passive (inverse of active)
    rotmxp=[ct 0 -st; 0 1 0; st 0 ct];
    perm(:,:,k)=rotmxa*[S(k) -1i*D(k) 0; 1i*D(k) S(k) 0; 0 0 P(k)]*rotmxp;
end
