function [xb,x,f0arr,relerror]=bestgrid(fun,params,xb,dx0,retol,miniter,debugflag)
%BESTGRID Find the best grid for trapezoidal integration
% Usage:
%   [xb,x,f0arr,relerror]=bestgrid(fun,params,xb,dx0,retol);
% The method is based that the integration error is |f"|*dx^3/24.
% Inputs:
%   fun -- a function which is being integrated;
%   params -- additional parameters for the function, {} if none;
%   xb -- starting values of boundaries of intervals;
%   dx0 -- minimum tolerable dx;
%   retol -- tolerance of the relative integration error
% Output:
%   xb -- new boundaries
% Optional outputs:
%   x -- see below
%   f0arr -- values of the function at x=(xb1+xb2)/2
%   relerror -- relative integration error
% Note:
%   Output of "fun" can be multidimensional. The first dimension should
%   correspond to x, the second is assumed to be uniform (have the same
%   magnitude), like components of fields at the same point. This is done
%   because sometimes some components are exactly zero (like Ex, Ey on the
%   superconducting ground), and finding the error in integration in these
%   components does not work. Instead, this program relies on other
%   components, like Ez, Hx, Hy. Of course, if all of them are zero at
%   particular point, then we are in trouble.

%% Check parameters
if length(xb)<2
    error('xb must have at least 2 points');
end
if nargin<7
    debugflag=[];
end
if nargin<6
    miniter=[];
end
if isempty(miniter)
    miniter=1;
    % Do at least one iteration before exiting, so that we don't use
    % the initial inefficient grid (to save time, the result should not
    % change).
end
if isempty(debugflag)
    debugflag=1;
end
xb=xb(:);
iteration=0;
if length(xb)<=1
    error('too few initial points');
end
% Quick fix to keep the number of intervals >=3
dx0max=(xb(end)-xb(1))/3;
if dx0>dx0max
    if debugflag>0
        disp(['Reducing dx0 from ' num2str(dx0) ' to ' num2str(dx0max)]);
    end
    dx0=dx0max;
end
N0=ceil((xb(end)-xb(1))/dx0);
if length(xb)<=3
    % No grid is given, create own
    xb=linspace(xb(1),xb(end),N0+1).';
end

%% Cycle
while 1
    %% Update the grid
    N=length(xb)-1;
    if debugflag>0
        if iteration==0
            disp(['BESTGRID: Initial estimate, N=' num2str(N)]);
        else
            disp(['BESTGRID: Re-do, iteration=' num2str(iteration) ', N=' num2str(N)]);
        end
    end
    dxb=diff(xb);
    x=(xb(1:N)+xb(2:N+1))/2;
    dx=diff(x);
    dxav=(dx(1:N-2)+dx(2:N-1))/2;
    f0arr=fun(x,params{:});
    %% Estimate the integration error
    % The first dimension of f0arr corresponds to x
    % The second dimension to a parameter in which fun is assumed to be
    % uniform
    % The third and higher dimensions - to those in which f0arr is not
    % uniform, so the relative error has to be calculated in a proper way.
    s=size(f0arr);
    nsunif=s(2); % the number of uniform parameters
    if length(s)<3
        si=[s(2) 1];
    else
        si=s(2:end);
    end
    ns=prod(si);
    nsextra=prod(si(2:end));
    if debugflag>1
        disp(['N=' num2str(N) '; nsunif=' num2str(nsunif) '; nsextra=' num2str(nsextra)]);
    end
    f0=reshape(f0arr,[N ns]);
    abserror=zeros(nsunif,nsextra);
    total=zeros(nsunif,nsextra);
    f2=zeros(N,ns);
    for k=1:ns
        % The integration error is = |f"|*dx^3
        f2(2:N-1,k)=abs(diff(diff(f0(:,k))./dx)./dxav);
        f2(1,k)=f2(2,k); f2(N,k)=f2(N-1,k);
        % - approximation to |f"|
        total(k)=sum(abs(f0(:,k)).*dxb); % integral
        % We coarsen the mesh at some points but preserve the
        % relative error
        %figure; plot(abs(f2(:,k)))
        %f2z=f2(:,k); f2z(f0(:,k)==0)=0;
        abserror(k)=max(abs(f2(:,k)).*dxb.^3)/24;
        %abserror(k)=max(abs(f2z).*dxb.^3)/24;
    end
    if max(total)<eps
        % Detect a function which is zero everywhere
        relerror=0
        break
    end
    relerror=abserror./repmat(max(total),[nsunif 1]);
    if debugflag>0
        disp(['Relative error=' num2str(max(relerror(:))*N)]);
        switch debugflag
            case 1
            case 2
                disp('DEBUG2> Relative error detail =');
                disp(relerror*N);
            otherwise
                disp(['DEBUG3> min(dx)=' num2str(min(dx))]);
                disp('DEBUG3> Relative error detail =');
                disp(relerror*N);
                figure(999);
                % The plotting values are N x nsunif x nsextra
                err_plot=bsxfun(@times,abs(reshape(f2,[N nsunif nsextra])),dxb.^2)/24;
                fun_plot=abs(reshape(f0arr,[N nsunif nsextra]));
                plotcol=floor(sqrt(nsextra));
                plotrow=ceil(nsextra/plotcol);
                for k=1:nsextra
                    subplot(plotrow,plotcol,k);
                    semilogy(x,err_plot(:,:,k),'x-'); hold on;
                    semilogy(x,fun_plot(:,:,k),'x--'); hold off; grid on;
                    title(['set ' num2str(k)]);
                end
                if debugflag>3
                    disp('DEBUG4> Saving the data to a file');
                    fname = input('Please enter file name, <enter> to skip: ','s');
                    if ~isempty(fname)
                        save(fname,'x');
                    end
                else
                    % Instead of a pause
                    input('DEBUG3> press <enter> to continue ','s');
                end
        end
    end
    %% ********* EXIT FROM THE CYCLE IS HERE ***********
    if max(relerror(:))*N<retol && iteration>=miniter
        break
    end
    %% The new grid - boundary points
    e0=24*retol*max(total)/max(N,N0)/10; % size == 1 x si(2:end) % was: retol*max(total)/10
    % -- target for |f"|*dx^3, for each extra parameter
    % Estimate for new value of 1/dx
    maxf2=max(reshape(f2,[N si]),[],2); % N x 1 x si(2:end)
    f2e=maxf2./repmat(shiftdim(e0,-1),[N 1]);
    maxf2e=max(reshape(f2e,[N nsextra]),[],2); % N x 1
    g=max(maxf2e.^(1/3),1/dx0);
    % -- take max of g over the spectator indeces
    G=[0 ; cumsum(g.*dxb)]; % dG=1 if dn is optimal
    % Choose the intervals so that the minimum interval <=1
    Gd=linspace(G(1),G(end),ceil(G(end)-G(1)+1)).';
    % The new grid
    xb=interp1(G,xb,Gd);
    iteration=iteration+1;
end
