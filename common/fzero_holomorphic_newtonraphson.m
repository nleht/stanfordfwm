function zsol=fzero_holomorphic_newtonraphson(fun,params,z10,z20,dztol,dftol,debugflag,recur)
%FZERO_HOLOMORPHIC_NEWTONRAPHSON Newton-Raphson method
% Newton-Raphson method, should only be called when there is exactly 1 root
% in the region! This condition includes no roots at the boundary.
% Usage:
% zsol=fzero_holomorphic_newtonraphson(fun,params,z10,z20,dztol,dftol,debugflag,recur)
if nargin<8
    recur=0;
end
if nargin<7
    debugflag=0;
end
if nargin<6
    dftol=[];
end
if nargin<5
    dztol=[];
end
if isempty(dftol)
    dftol=1e-6;
end
if isempty(dztol)
    dztol=1e-6;
end
margins=repmat('=> ',[1 recur]);
prompt=[margins 'FZERO_HOLOMORPHIC_NEWTONRAPHSON: '];
z1=z10;
f1=fun(z1,params{:});
z2=(z10+z20)/2;
f2=fun(z2,params{:});
dfdz=(f2-f1)./(z2-z1);
df=dftol*abs(dfdz);
if debugflag>0
    disp([prompt 'Initial z1=' num2str(z1) ', z2=' num2str(z2) ', df=' num2str(df)]);
end
failed=0; count=0;
while 1
    count=count+1;
    dfdz=(f2-f1)./(z2-z1);
    dz=-f2/dfdz;
    if any(~isfinite([dz z1 z2 f1 f2])) || count>=100
        failed=1;
        break
    end
    z1=z2;
    z2=z2+dz;
    % no thinking outside the box!
    if real(z2)<real(z10)
        z2=real(z10)+1i*imag(z2);
    elseif real(z2)>real(z20)
        z2=real(z20)+1i*imag(z2);
    end
    if imag(z2)<imag(z10)
        z2=real(z2)+1i*imag(z10);
    elseif imag(z2)>imag(z20)
        z2=real(z2)+1i*imag(z20);
    end
    f1=f2;
    f2=fun(z2,params{:});
    if debugflag>1
        disp([prompt 'z=' num2str(z2) '; f=' num2str(f2)]);
    end
    if abs(f2)<df
        break
    end
end
if failed
    % Failed to find the root!
    if debugflag>0
        disp([prompt 'Failed to find the root, calling FZERO_HOLOMORPHIC']);
    end
    zsol=fzero_holomorphic(fun,params,real(z10),real(z20),imag(z10),imag(z20),[],dztol,dftol,[],debugflag,recur+1,1);
else
    % Found the root
    zsol=z2;
    if debugflag>0
        disp([prompt 'Found root = ' num2str(zsol)]);
    end
end
