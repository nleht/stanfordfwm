function abox=get_box(bbox,pos,boxgridx,boxgridy,nx,ny)
%GET_BOX Get a box in a given grid
% A box is a rectangle [left bottom width height], same format as used for
% 'Position' property in MATLAB axes.
% Usage:
%   box=get_box(parentbox,position); % same as get_box_sub
%   box=get_box(parentbox,position,boxgridx,boxgridy,nx,ny);
% where
%   boxgrid{x|y} - grid points along x and y, respectively (default=[0 1])
%   nx, ny - location of the box in the grid (default nx=1 and ny=1)
%   position - position of the box in the grid element (default=[0 0 1 1])
%   parentbox - also [0 0 1 1] by default (if empty)
% Note: for padding between plots, use half-values of nx, ny
% See also: GET_BOX_SUB, GET_BOX_GRID
if nargin<3
    boxgridx=[0 1]; boxgridy=[0 1]; nx=1; ny=1;
end
if isempty(pos)
    pos=[0 0 1 1];
end
if isempty(bbox)
    pos=[0 0 1 1];
end
nx2=round(2*nx); ny2=round(2*ny);
gbox=[boxgridx(nx2-1) boxgridy(ny2-1) boxgridx(nx2)-boxgridx(nx2-1) boxgridy(ny2)-boxgridy(ny2-1)];
abox=get_box_sub(get_box_sub(bbox,gbox),pos);
