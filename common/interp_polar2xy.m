function [Fxy,Frp,r,phi,intFdr]=interp_polar2xy(Nphi,rb,Fpolar,x,y,method)
% INTERP_RP2XY Plot k-space Sz
%  [Fxy,Frp,r,phi]=interp_polar2xy(Nphi,rb,Fpolar,x,y);
%  imagesc(x,y,Fxy');
% or
%  [rm,phim]=ndgrid(r,[phi 2*pi]);
%  hh=pcolor(rm.*cos(phim),rm.*sin(phim),[Frp Frp(:,1)]); set(hh,'edgecolor','none');
% Inputs:
% Output: Fxy
% Made from /nleht/Documents/MATLAB/fwm_vlftransmitter_plots.m
% See also: POINTILLISM

if nargin<6
    method='interp2'; % simplest
end
if nargin<5
    method='none';
end
Fpolar=Fpolar(:).';

%% Interpolate in (r,phi) space
Nr=length(rb)-1;
r=(rb(1:Nr)+rb(2:Nr+1))/2;
Nphimax=Nphi(Nr);
phi=[0:Nphimax-1]/Nphimax*2*pi; % full-range phi
i2=cumsum(Nphi); i1=[1 i2(1:Nr-1)+1];
Frp=zeros(Nr,Nphimax);
for kr=1:Nr
    phitmp=[0:Nphi(kr)]*2*pi/Nphi(kr); % sparcely spaced phi
    Frp(kr,:)=interp1(phitmp,[Fpolar(i1(kr):i2(kr)) Fpolar(i1(kr))],phi);
end
if strcmp(method,'none')
    % Don't calculate Fxy, return only Frp
    Fxy=[];
    return
end
%% Convert to Cartesian
dr=diff(rb);
dx=mean(diff(x));
[xm,ym]=ndgrid(x,y);
phim=mod(atan2(ym,xm),2*pi);
rm=sqrt(xm.^2+ym.^2);
phiext=[phi 2*pi]; % add the periodic point
switch method
    case 'interp2'
        Fxy=interp2(r,phiext,[Frp Frp(:,1)].',rm,phim);
    case 'intdr'
        % Interpolate through the integrated value so that we don't miss the peaks
        intFdr=cumsum(Frp.*repmat(dr,[1 Nphimax]),1);
        intFdrext=[intFdr intFdr(:,1)];
        intFupper=interp2(r,phiext,intFdrext.',rm+dx/2,phim);
        intFlower=interp2(r,phiext,intFdrext.',rm-dx/2,phim);
        Fxy=(intFupper-intFlower)/dx;
end
Fxy(isnan(Fxy))=0;
