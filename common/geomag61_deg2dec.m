function decimal=geomag61_deg2dec(degrees,minutes,seconds)
%GEOMAG61_DEG2DEC Converts degrees,minutes, seconds to decimal degrees
% Based on subroutine "degrees_to_decimal"
% Input:
%    degrees - Integer degrees
%    minutes - Integer minutes
%    seconds - Integer seconds
% Output:
%    decimal - degrees in decimal degrees
%
% C
%    C. H. Shaffer
%    Lockheed Missiles and Space Company, Sunnyvale CA
%    August 12, 1988
% MATLAB
%    Nikolai G. Lehtinen
%    Stanford University, Stanford, CA
%    September 10, 2009

deg = degrees;
min = minutes/60.0;
sec = seconds/3600.0;

decimal = abs(sec) + abs(min) + abs(deg);
    
if (deg < 0)
    decimal = -decimal;
elseif (deg == 0)
    if (min < 0)
        decimal = -decimal;
    elseif (min == 0)
        if (sec<0)
            decimal = -decimal;
        end
    end
end
