function zx=fzero_holomorphic_extrap(fun,params,t,tp,zp,dztol,dftol,debugflag,unlimit_box)
%FZERO_HOLOMORPHOC_EXTRAP Find roots when parameter is slightly changed
% Usage:
%   zx=fzero_holomorphic_extrap(fun,params,t,tp,zp,dztol,dftol,debugflag)
% Inputs (fun, params, dztol, dftol are the same as in FZERO_HOLOMORPHIC):
%   fun - functions handle, the first argument must be "z"
%   params - cell array of parameters (the rest of the arguments of "fun")
%   t - new value of parameter (must be repeated in "params")
%   tp - (cell array) - up to three closest values of parameter for which
%     the roots are known
%   zp - (cell array) - these roots (i.e., at parameter equal to "tp")
%   dztol - maximum distance between separate solutions (default=1e-12)
%   dftol - maximum error in f/(df/dz) (default=dztol or 1e-12)
%   debugflag - default=0
% Output: zx - the new set of roots at the new value of parameter
% See also: FZERO_HOLOMORPHIC
if nargin<9
    unlimit_box=0;
end
box=[-1-1i ; 1-1i ; 1+1i ; -1+1i];
np=numel(tp);
if np==0
    error('must have previous values!')
end
if numel(zp)~=np
    error('zp must be of the same length as tp');
end
nz=numel(zp{1});
zx=zeros(size(zp{1}));
% First derivative
dt=t-tp{1};
if np<2
    fp1=fun(zp{1},params{:}); % params already include t!; =dfdt*dt+0
    dfdz=(fun(zp{1}+dztol,params{:})-fp1)/dztol;
    dfdt=fp1/dt;
    dzdt=-dfdt./dfdz;
else
    dzdt=(zp{1}-zp{2})/(tp{1}-tp{2});
end
% Second derivative
if np<3
    d2zdt2=zx;
else
    dzdt1=(zp{2}-zp{3})/(tp{2}-tp{3});
    d2zdt2=2*(dzdt-dzdt1)/(tp{1}-tp{3});
end
z0=zp{1}+dzdt*dt+d2zdt2*dt^2/2;
% Find the shifted roots
if debugflag>0
    f0=fun(z0,params{:});
    disp(['  Error in f=' num2str(max(abs(f0)),12)]);
end
parfor k=1:nz
    if debugflag>1
        disp(['  Root z0(' num2str(k) ')=' num2str(z0(k))]);
    end
    if np<3
        dzest=abs(2*dzdt(k)*dt);
    else
        dzest=abs(2*d2zdt2(k)*dt^2);
    end
    % Prevent the box from being too small
    dz=max(dzest,1e-5);
    exitloop=0;
    while ~exitloop
        zcp=z0(k)+dz*box;
        if ~unlimit_box
            % Limit box to avoid branch cuts
            ii=find(imag(zcp)>0); zcp(ii)=real(zcp(ii));
        end
        % Count the roots
        %fun
        %params
        %dztol
        %debugflag
        [nzin,nzon,zb,fb,phase]=fzero_holomorphic_numzeros(fun,params,...
            zcp,[],dztol,[],debugflag);
        if debugflag>1
            disp(['    dz=' num2str(dz) ', found ' num2str(nzin) ' roots']);
        end
        if nzin>1
            disp('more than one root!')
            zx(k)=NaN;
            exitloop=1;
        elseif nzin==1 && nzon==0
            exitloop=1;
        else
            % expand the region
            if debugflag>0
                disp(['    Expanding region to ' num2str(dz*2) ' ...']);
            end
            dz=dz*2;
        end
    end
    zx(k)=fzero_holomorphic_newtonraphson(fun,params,...
        z0(k)-dz*(1+1i),z0(k)+dz*(1+1i),dztol,dftol);
    if ~isfinite(zx(k))
        disp('no root!')
        zx(k)=NaN;
        continue;
    end
    if debugflag>1
        disp(['    z=' num2str(zx(k)) ', dz/dt=' num2str(dzdt(k)) ', d2z/dt2=' num2str(d2zdt2(k))]);
    end
    if debugflag==1
        disp(['  z(' num2str(k) ')=' num2str(zx(k))]);
    end
end
