function [ys,a,x]=lsqsmooth(y,x,N)
%LSQSMOOTH Least square polynomial fit
% Usage:
%    [ys,a]=lsqsmooth(y,x,N);
%    [ys,a,x]=lsqsmooth(y,[],N);
% Inputs:
%    y - points to be fitted, taken at uniformly spaced grid x
%    x - uniformly spaced grid; default=uniformly spaced points from [0,1]
%    N - degree of the fitting polynomial, default=1
% Outputs:
%    ys - fitting values
%    a - coefficients of polynomial
%    x - see above
% Author: Nikolai G. Lehtinen
% See also: POLYFIT

% Parse args
n=length(y);
if nargin<3
    N=[];
end
if nargin<2
    x=[];
end
if isempty(N)
    N=1;
end
if isempty(x)
    x=y; % copy size
    x(:)=([0:n-1].')/(n-1);
else
    if any(size(x)~=size(y))
        error('Must be the same size')
    end
end

% Simple fitting
p=zeros(2*N+1,1);
yp=zeros(N+1,1);
for k=0:2*N
    p(k+1)=sum(x.^k);
end
for k=0:N
    yp(k+1)=sum(y.*(x.^k));
end
A=zeros(N+1,N+1);
for k=0:N
    for l=0:N
        A(k+1,l+1)=p(k+l+1);
    end
end
a=A\yp;
ys=zeros(size(y));
for k=0:N
    ys=ys+x.^k*a(k+1);
end
