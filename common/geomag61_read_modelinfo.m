function modelinfo=geomag61_read_modelinfo(mdfile)
%GEOMAG61_READ_MODEL Read Geomag 6.1 model info
% Part of the driver

% Constants
MAXMOD=31; % was 30 in C
RECL=80;

irec_pos=zeros(MAXMOD,1);
model=cell(MAXMOD,1);
epoch=zeros(MAXMOD,1);
max1=zeros(MAXMOD,1);
max2=zeros(MAXMOD,1);
max3=zeros(MAXMOD,1);
yrmin=zeros(MAXMOD,1);
yrmax=zeros(MAXMOD,1);
altmin=zeros(MAXMOD,1);
altmax=zeros(MAXMOD,1);

stream=fopen(mdfile,'r');
fileline = 0; % First line will be 1
modelI = 0;  % First model will be 1 (in C it was 0)

%% 
while 1
    fileline=fileline+1; % On new line
    inbuff = fgetl(stream);
    if ~ischar(inbuff)
        break
    end
    if length(inbuff)~=RECL
        fprintf(2,'\nCorrupt record in file %s on line %d.\n',mdfile,fileline);
        fclose(stream);
        modelinfo=[];
        return
    end   
    
    % old statement Dec 1999
    % if (!strncmp(inbuff,"    ",4)) % If 1st 4 chars are spaces
    % New statement Dec 1999 changed by wmd  required by year 2000 models
    if (strncmp(inbuff,'   ', 3))       % If 1st 3 chars are spaces
        modelI=modelI+1;  % New model
        if (modelI > MAXMOD) % If too many headers
            fprintf(2,'\nToo many models in file %s on line %d.\n',mdfile,fileline);
            fclose(stream);
            modelinfo=[];
            return;
        end
        
        irec_pos(modelI)=ftell(stream);
        % Get fields from buffer into individual vars.
        [model1, epoch(modelI),...
            max1(modelI), max2(modelI), max3(modelI), ...
            yrmin(modelI), yrmax(modelI), ...
            altmin(modelI), altmax(modelI)]=strread(inbuff, '%s%f%d%d%d%f%f%f%f',1);
        model{modelI}=model1{1};
        % Compute date range for all models
        if (modelI == 1) % If first model
            minyr=yrmin(1);
            maxyr=yrmax(1);
        else
            if (yrmin(modelI)<minyr)
                minyr=yrmin(modelI);
            end
            if (yrmax(modelI)>maxyr)
                maxyr=yrmax(modelI);
            end
        end
        
    end % If 1st 3 chars are spaces
    
end % While not end of model file
            
nmodel = modelI; % was modelI+1 in C
modelinfo=struct('model',model(1:nmodel),'irec_pos',limcell(irec_pos,nmodel),'epoch',limcell(epoch,nmodel),...
    'max1',limcell(max1,nmodel),'max2',limcell(max2,nmodel),'max3',limcell(max3,nmodel),...
    'yrmin',limcell(yrmin,nmodel),'yrmax',limcell(yrmax,nmodel),...
    'altmin',limcell(altmin,nmodel),'altmax',limcell(altmax,nmodel));
fclose(stream);

function b=limcell(a,n)
b=mat2cell(a(1:n),ones(1,n),1);

