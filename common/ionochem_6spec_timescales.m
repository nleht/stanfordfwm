function [a,lambda]=ionochem_6spec_timescales(varargin)
% NOT FINISHED!!!

% Parse the arguments
keys_ionocoeffs={'debug','NX','Nac','Nclus',...
    'gamma','gamma_coef','gamma_photo',...
    'gammaX','alfai','alfad','alfadc',...
    'beta','beta_coef','ENm',...
    'Xbar','Xbar_coef',...
    'Nac_profile','Nac_coef','Nac_daytime_coef'};
keys={keys_ionocoeffs{:}};
[prof,daytime,h,Nspec0,options]=parsearguments(varargin,3,keys);

debugflag=getvaluefromdict(options,'debug',1);
relaxtime=getvaluefromdict(options,'relaxtime',1e7);
Sscale=getvaluefromdict(options,'Sscale',2); % from figures in [R]


Nm=getNm(h)/1e6; % convert to cm^{-3}
Tn=getTn(h);
options_ionocoeffs=getsubdict(options,keys_ionocoeffs);
[alfad,alfadc,alfai,gamma,beta,Bcoef,gammaX,Xbar,gamac,betaas,bran]=...
    ionocoeffs_6spec(prof,daytime,h,options_ionocoeffs{:});

if isempty(Nspec0)
    % Call the model
end

% Currently works only for 5 species

a=zeros(4,4,nh);
