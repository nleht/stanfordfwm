function [Aee,Bee]=enuseg_ee_AB(nen,en,ene,den,deni,debugflag)
%ENUSEG_EE_AB A and B matrices for a new e-e collision treatment (ENUSEG)
% A new scheme
% It conserves energy without Rockwood's massaging (symmetrization)
% The A, B are normalized so that
%    d(en)/dt=alfa*ne.'*(A-B)*ne
%    d(ne(k))/dt=-1./deni(k)*(Je(k+1)-Je(k))
%    Je(k+1)=alfa./den(k+1)*(-b(k+1)*ne(k+1)+a(k)*ne(k))
% where Je(k+1)=J_{k+1/2}, den(k+1)=den_{k+1/2}, a=A*ne, b=B*ne
%
% Usage:
%
%    %% Calculate the normalizing coefficient
%    b0=ech^2/(4*pi*eps0*(ech*T0))
%    lDebye=sqrt((T0*ech)*eps0/(Ne*ech^2))
%    % Plasma parameter -- these expressions bust be same
%    Lambda=lDebye/b0
%    Lambda=4*pi*Ne*lDebye^3
%    % Coefficient used in the kinetic equation for f(v)
%    Y=4*pi*(ech^2/(4*pi*eps0*me))^2*log(Lambda)
%    % Coefficient used in the kinetic equation for ne(en)
%    alfa=Y*me^(3/2)/sqrt(2)/3
%    % Convert to eV
%    alfa=alfa/ech^(3/2)
%    % Normalize to Ne
%    alfa=alfa*Ne
%
%    %% Setup the matrices
%    [Aee,Bee]=enuseg_ee_AB(nen,en,ene,den,deni[,debugflag]);
%    Aden=diag(1./den(2:nen+1))*Aee;
%    Bden=diag(1./den(1:nen))*Bee;
%
%    %% Cycle over time
%    ne=ne0;
%    for kt=1:nt
%        aden=Aden*ne; bden=Bden*ne; % a/den, b/den
%        C=alfa*spdiags(1./deni,0,nen,nen)*...
%            spdiags([aden -(aden+bden) bden],-1:1,nen,nen);
%        ne=(speye(nen)-dt*C)\ne;
%    end
%
% See ENUSEG_TEST for a more advanced usage (better energy conservation)
% Author: Nikolai G. Lehtinen
% See also: ENUSEG_A_COLLISIONAL, ENUSEG_TEST

if nargin<6
    debugflag=0;
end
% See notebook #10, p. 130 ("scheme 1") for explanation.
% Summary:
%    d(ne)/dt=-dJ/d(en),
% where [Rockwood, 1973, doi:10.1103/PhysRevA.8.2348]
%    J=-alfa*(P*sqrt(en)*(d/den)(ne./sqrt(en))+Q*ne)
%    P(en) = 2/sqrt(en)* \int_0^en en' ne(en') den' + ...
%            2*en* \int_en^inf ne(en')/sqrt(en') den'
%    Q(en) = 3/sqrt(en)* \int_0^en ne(en') den'
% Note that P, Q may be considered operators with kernels P(en,en'),
% Q(en,en'):
%    P(en)=\int_0^inf P(en,en') ne(en') den'
% In finite difference scheme, the kernel becomes a matrix.
% We note that the kernel of operator
%    F=1/sqrt(en)*(d/den)(P*sqrt(en))
% is a "transpose" of the kernel of operator Q. Thus the matrix for F
% should be transpose of the matrix for Q -- this is the condition for
% energy conservation.
% Then from the matrix for F we get the matrix for P.
% For this particular scheme ("scheme 1"), we take at en_{k+1/2}
%    P*sqrt(en)*(d/den)(ne./sqrt(en)) = sqrt(en_{k+1/2})*P_{k+1/2}* ...
%       (ne_{k+1}/sqrt(en_{k+1})-ne_{k}/sqrt(en_{k})/deltaen_{k+1/2}
% This is equivalent to the following choice of F at en_{k}:
%    F_{k} = (P_{k+1/2}*sqrt(en_{k+1/2})-P_{k-1/2}*sqrt(en_{k-1/2}))/ ...
%          sqrt(en_k)*deltaen_{k}
% Notation:
%    deltaen_{k+1/2}=den(k+1); deltaen_{k}=deni(k);
%    en_{k+1/2}=ene(k+1); en_{k}=en(k) etc.
H=tril(ones(nen+1,nen),-1);
up1=sqrt(ene(2:nen+1)./en)./den(2:nen+1);
um1=sqrt(ene(1:nen)./en)./den(1:nen);
Q=H.*repmat(3./sqrt(ene),[1 nen]);
tmp1=diag(den)*Q;
Qav=diag(0.5./deni)*(tmp1(1:nen,:)+tmp1(2:nen+1,:));
tmp=[zeros(1,nen);cumsum(Qav.'.*repmat(deni.*sqrt(en),[1 nen]))];
P=diag(1./sqrt(ene))*tmp;
Ptmp=diag(den)*P*diag(deni);
Qtmp=tmp1*diag(deni);
Aee=Ptmp(2:nen+1,:).*repmat(up1,[1 nen])-Qtmp(2:nen+1,:)/2;
Bee=Ptmp(1:nen,:).*repmat(um1,[1 nen])+Qtmp(1:nen,:)/2;
if debugflag>0
    E=Aee-Bee+(Aee-Bee).';
    maxAB=min(max(abs(Aee(:))),max(abs(Aee(:))));
    disp(['Error=' num2str(max(abs(E(:)))/maxAB)]); % must be zero for energy conservation
end

%% Particle number conservation
% The requirement is Aee(nen,:)==0 and Bee(1,:)==0
% We have Bee(1,:)==0 already
if 1
    tmp=Aee(nen,:);
    Aee(nen,:)=0; % required! 
    % The rest follow from symmetry required by energy conservation
    Bee(nen,:)=Bee(nen,:)-tmp;
else
    % This also works, but obviously there is more massaging here
    Aee(nen,:)=0;
    Aee(:,nen)=0;
    Bee(nen,:)=0;
    Bee(:,nen)=0;
end
