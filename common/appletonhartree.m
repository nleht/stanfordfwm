function [n2,thg,n,ng,vgc,dn2dX,dn2dY]=appletonhartree(th,X,Y,mode)
%APPLETONHARTREE Appleton-Hartree refractive index
% Appleton-Hartree equation gives the refractive index of electron waves
% (no ions). This function assumes collisionless plasma (Z=0).
% Usage: [n2,thg]=appletonhartree(th,X,Y,mode)
% Inputs (must be of the same size for vectorized operations):
%   th - angle between k and the magnetic field (wave-normal angle)
%   X=wp^2/w^2; Y=wH/w;
%   mode=+1 for R or -1 for L (NOTE: Whistler is R)
% Outputs:
%   n2 - square of the refractive index
%   thg - the angle between the group velocity and the magnetic field
%   ng - group velocity refractive index
% Use coldplasma(th,X,Y,m/M) for plasma with one species of ions
s=sin(th);
c=cos(th);
P=1-X;
Del=sqrt(Y.^2.*s.^4+4.*P.^2.*c.^2);
denom=2.*P-Y.^2.*s.^2+mode*Y.*Del;
n2=1-(2*P.*X)./denom;
% refractive index
n=sqrt(n2);
n(n2<0)=nan;
% For ray tracing: the alfa
tana=mode*2*P.*X.*Y.*s.*c./(Del.*denom);
dn2dth=-2*tana.*n2;
% The group velocity vector direction
thg=th+atan(tana);
thg(n2<0)=nan;
% Group velocity refractive index (see Helliwell's book, ch. 3)
dn2dX=2.*((2.*X-1).*denom-2.*P.*X.*(1+mode*2.*Y.*P.*c.^2./Del))./denom.^2;
dn2dY=4.*X.*P.*(-Y.*s.^2.*Del+mode.*(Y.^2.*s.^4+2.*P.^2.*c.^2))./denom.^2./Del;
ngn=1-(X.*dn2dX+Y.*dn2dY/2)./n2; % ng/n=(d(nw)/dw)/n
cosa=1./sqrt(1+tana.^2);
ng=cosa.*n.*ngn;
vgc=1./ng;
