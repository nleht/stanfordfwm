function EH=vacuum_dipole_field(Ndim,k0,x,y,z)
%VACUUM_DIPOLE_FIELD Field of a verical dipole in vacuum
% Usage:
%    EH=impedance0*Iz0*vacuum_dipole_field(2,k0,x,z);
%    EH=impedance0*Iz0*vacuum_dipole_field(3,k0,x,y,z);
% Inputs:
%    Ndim (== 2 or 3) - the dimensionality of the problem
%    k0 (scalar) == w/c
%    x (Nx), y (Ny), z(Nz) - coordinates in m
% Output:
%    EH (6 x Nx x Nz or 6 x Nx x Ny x Nz) - complex amplitude of the field
%       (electric and magnetic), in V/m, with physics convention
%       exp(-i*w*t)
% Other notations:
%    impedance0==sqrt(mu0/eps0), the vacuum impedance
%    Iz0 (scalar) - dipole moment in z-direction at the origin, in A (for
%       Ndim==2) or A*m (for Ndim==3)
% NOTE: if the field is not in vacuum but in a uniform isotropic medium, we
% change everywhere mu0->(mu*mu0), eps0->(eps*eps0), impedance0->impedance,
% and the H scaling is H=impedance*H_SI.
if Ndim==2
    z=y;    % The correct arguments
    Nx=length(x); Nz=length(z);
    EH=zeros(6,Nx,Nz);
    [xm,zm]=ndgrid(x,z);
    rm=sqrt(xm.^2+zm.^2);
    bh0=besselh(0,1,k0*rm);
    bh1=besselh(1,1,k0*rm);
    bh2=besselh(2,1,k0*rm);
    bh1p=k0*(bh0-bh2)/2; % derivative of the hankel function H(1)_1
    EH(1,:,:)=1/4*zm.*xm./rm.^3.*(rm.*bh1p-bh1);
    EH(3,:,:)=1/4*(-k0*bh0+1./rm.^3.*(rm.*zm.^2.*bh1p+xm.^2.*bh1));
    EH(5,:,:)=(1i*k0/4)*bh1.*xm./rm; % Hy
elseif Ndim==3
    Nx=length(x); Ny=length(y); Nz=length(z);
    EH=zeros(6,Nx,Ny,Nz);
    [xm,ym,zm]=ndgrid(x,y,z);
    rm=sqrt(xm.^2+ym.^2+zm.^2);
    e=exp(1i*k0*rm);
    ikr=1i*k0*rm;
    tmp=(ikr-1).*(ikr-3)./ikr+1;
    EH(1,:,:,:)=-1/(4*pi).*e.*zm.*xm./rm.^4.*tmp;
    EH(2,:,:,:)=-1/(4*pi).*e.*zm.*ym./rm.^4.*tmp;
    EH(3,:,:,:)=-1/(4*pi).*e./rm.^2.*(-ikr+(ikr-1)./ikr+zm.^2./rm.^2.*tmp);
    EH(4,:,:,:)= 1/(4*pi).*e.*ym./rm.^3.*(ikr-1);
    EH(5,:,:,:)=-1/(4*pi).*e.*xm./rm.^3.*(ikr-1);
else
    error('incorrect Ndim');
end
