function [isort,fsort,failed]=smooth_sort(t,f,debugflag)
%SMOOTH_SORT Find smooth curves f(:,k) as a function of time
% Usage: [isort,fsort]=smooth_sort(t,f)
% See also: SIMPLE_EXTRAP, SORT_MIN_DISTANCE, APPLY_SORT
if nargin<3
    debugflag=0;
end
nt=length(t);
[nf,nt2]=size(f);
if nt2~=nt
    error('f is of wrong size');
end
isort=zeros(nf,nt);
fsort=f;
failed=0;
for k=1:nt
    %k
    %isort(:,1:k-1)
    if k==1
        isort(:,k)=[1:nf].';
    else
        if k==2
            tp={t(k-1)}; fp={fsort(:,k-1)};
        else
            tp={t(k-1),t(k-2)}; fp={fsort(:,k-1),fsort(:,k-2)};
        end
        f0=simple_extrap(t(k),tp,fp);
        % Compare f0 to f(:,k) and find the best order
        [fsort(:,k),isort(:,k),failed]=sort_min_distance(f0,f(:,k));
        if failed
            if debugflag>0
                f(:,k)
                disp(['cannot sort at k=' num2str(k)]);
            end
            return
        end
    end
end
