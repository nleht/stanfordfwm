function [gmst,du]=astro_gmst(utc,method)
%ASTRO_GMST Greenwich Mean Sidereal Time (in radians), using J2000.0 epoch
% The GMST is defined as the sidereal time at the Greenwich meridian (at
% longitude 0°). The (Local) Sidereal Time (LST) is defined as the Hour
% Angle (HA) of the vernal equinox. The Hour Angle of a celestial object
% at a given observation point on the Earth is the angle between the
% observation point meridian half-plane and the half plane determined by
% the Earth axis and the object. The angle is taken with minus sign if the
% object is eastward of the meridian plane and with the plus sign if the
% point is westward of the meridian plane. Usually HA is measured
% in units of time, but here all angles are in radians.
% See http://en.wikipedia.org/wiki/Equatorial_coordinate_system
% Usage:
%   LST=ASTRO_GMST(UTC[,method])+LON;
% Inputs:
%   UTC - Coordinated Universal Time (an array of 1x6, e.g. output of
%         DATEVEC)
%   LON - longitude (in radians)
% Optional input:
%   method - 'srshmitt' (default) or 'kelso' - two different algorigthms
%            which should be equivalent.
% Output:
%   LST - Local Sidereal Time
%
% Other useful relations (all angles are in radians):
%
% Hour Angle:
%     HA=LST-RA; % RA is the Right Ascension
% Conversion of HA and DEC (Declination) into ALT (altitude) and
% AS (azimuth): Using the RA, DEC, and HA for the object, and the
% latitude (LAT) of the observing site, the following formulas give the
% ALT and AZ of the object at the time and longitude that was used to
% calculate HA.
%
%     sin(ALT) = sin(DEC)·sin(LAT) + cos(DEC)·cos(LAT)·cos(HA)
%
%                sin(DEC) - sin(ALT)·sin(LAT)
%     cos(A)   = ----------------------------
%                     cos(ALT)·cos(LAT)
%
%    If sin(HA) is negative, then AZ = A, otherwise AZ = 2*pi - A.
% To calculate the sub-star point, we use
%    LAT=DEC;
%    LON=RA-ASTRO_GMST(UTC);
%    % -- follows from HA==0 => LST==ASTRO_GMST(UTC)+LON==RA
% See also: ASTRO_JD, ASTRO_DOY, ASTRO_JDOY
year=utc(1);
month=utc(2);
day=utc(3);
hour=utc(4);
minute=utc(5);
second=utc(6);

if nargin<2
    method='srschmitt';
end

switch method
    case 'srschmitt'
        % From http://home.att.net/~srschmitt/script_celestial2horizon.html
        if (month == 1)||(month == 2)
            year  = year - 1;
            month = month + 12;
        end
        a = floor(year/100);
        b = 2 - a + floor(a/4);
        c = floor(365.25*year);
        d = floor(30.6001*(month + 1));
        % du is the number of days of Universal Time elapsed since
        % JD 2451545.0 (2000 January 1, 12h UT1)
        du=b + c + d - 730550.5 + day;
        % days since J2000.0
        jd = du + (hour + minute/60.0 + second/3600.0)/24.0;
        % julian centuries since J2000.0
        jt = jd/36525.0;
        % the mean sidereal time in degrees
        gmst_deg = 280.46061837+360.98564736629*jd+0.000387933*jt*jt ...
            - jt*jt*jt/38710000;
        % In degrees modulo 360.0, and convert to radians
        gmst=mod(gmst_deg,360)*pi/180;
    case 'kelso'
        % Another algorithm, from
        % http://celestrak.com/columns/v02n02/
        du=astro_jdoy(year)+astro_doy(year,month,day)-2451545.0;
        % - MUST BE same as du above
        Tu=du/36525;
        % GMST at 0h, in seconds of right ascension
        gmst0_sec=24110.54841 + Tu*(8640184.812866 +Tu*(0.093104-Tu*6.2e-6));
        ut_sec=hour*3600 + minute*60 + second;
        % GMST at UTC, using rate of rotation of the Earth
        % w=7.29211510e-5 rad/s
        gmst_sec=mod(gmst0_sec + 1.00273790934*ut_sec,86400);
        gmst=gmst_sec*pi/12/3600; % in radians
    otherwise
        error('unknown algorithm, use srschmitt or kelso');
end

