%% 1. Re-create figure 3-15 from Helliwell
th=[0:.1:90].'*pi/180; Nth=length(th);
Lambda=[.8 .5 .3 .1 .05 .025].';
Yarr=1./Lambda; NY=length(Yarr);
MLambda=zeros(Nth,NY); thgarr=MLambda;
X=1e4;
for kY=1:NY
    Y=Yarr(kY);
    [n2,thg,n,ng,vgc]=appletonhartree(th,X,Y,1);
    MLambda(:,kY)=vgc.*sqrt(X)/Y;
    thgarr(:,kY)=thg;
end
plot(MLambda.*sin(thgarr),MLambda.*cos(thgarr));
axis equal; grid on;
legend(num2str(Lambda));

%% 2. Nose whistler, angle=0
f=[0:.001:.9]; fH=1; fp2=1e4;
X=fp2./f.^2; Y=fH./f;
[n2,thg,n,ng,vgc,dn2dX,dn2dY]=appletonhartree(0,X,Y,1);
figure;
plot(ng,f);
% Note the minimum group delay at Y=4

%% 3. Group delay as a function of ray angle and frequency
th=[0:.1:360].'*pi/180; Nth=length(th);
f=[.01:.01:.9].'; Nf=length(f);
fH=1; fp2=1e4;
X=fp2./f.^2; Y=fH./f;
ngarr=zeros(Nth,Nf); thgarr=ngarr;
for kf=1:Nf
    [n2,thg,n,ng]=appletonhartree(th,X(kf),Y(kf),1);
    ngarr(:,kf)=ng;
    thgarr(:,kf)=thg;
end
figure;
plot(sin(thgarr)./ngarr,cos(thgarr)./ngarr); axis equal;

% Fixed the ray angle
thdirarr=[0:.1:20 21:90].*pi/180; Nthdir=length(thdirarr);
ngf=nan(10,Nf,Nthdir);
for kthdir=1:Nthdir
    thdir=thdirarr(kthdir);
    for kf=1:Nf
        % Where it changes sign?
        tmp=mod(thgarr(:,kf)-thdir+pi,2*pi)-pi;
        idir=find(tmp(1:end-1).*tmp(2:end)<=0 & abs(tmp(1:end-1))<pi/2);
        for k=1:length(idir)
            range=[idir(k):idir(k)+1];
            ngf(k,kf,kthdir)=interp1(mod(thgarr(range,kf)-thdir+pi,2*pi)-pi,ngarr(range,kf),0);
        end
    end
end

figure;
for kthdir=1:Nthdir
    plot(ngf(:,:,kthdir),f,'kx'); title(thdirarr(kthdir)*180/pi);
    pause;
end
