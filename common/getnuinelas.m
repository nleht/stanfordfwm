function [nu,eloss]=getnuinelas(en,SP,comp,kproc)
global ech me
if isempty(ech)
    loadconstants
end
nkproc=length(kproc);
eloss=zeros(1,nkproc);
nu=zeros(length(en),nkproc);
en=en(:);
tmp=comp*sqrt(2*en*ech/me);
for v=1:nkproc
    n=SP.numpoints(kproc(v));
    sig=1e-20*interp1(SP.e(1:n,kproc(v)),SP.q(1:n,kproc(v)),en);
    sig(find(isnan(sig)))=0;
    nu(:,v)=tmp.*sig;
    eloss(v)=SP.eloss(kproc(v));
end
