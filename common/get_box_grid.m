function mygrid=get_box_grid(n,d,full_length)
%GET_BOX_GRID Get grid points for subplots
% A box is a rectangle [left bottom width height], same format as used for
% 'Position' property in MATLAB axes.
% Usage: mygrid=get_box_grid(n,d,full_length);
%  n - number of subplots in a given direction (e.g., x or y)
%  d - padding between the subplots as a fraction of:
%      total axis length if full_length==true
%      subplot axis length if full_length==false
% See also: GET_BOX, GET_BOX_SUB
if nargin<3
    full_length=true;
end
if full_length
    l=(1-(n-1)*d)/n; % d is the fraction of the whole axis
    dtot=d;
else
    l=1/(1+d)/n; dtot=d*l;
end
tmp=cumsum(repmat([l dtot],[1 n]));
mygrid=[0 tmp(1:2*n-1)];
