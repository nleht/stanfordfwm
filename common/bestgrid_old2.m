function [xb,x,f0arr,relerror]=bestgrid(fun,params,xb,dx0,retol,miniter,debugflag)
%BESTGRID Find the best grid for trapezoidal integration
% Usage:
%   [xb,x,f0arr,relerror]=bestgrid(fun,params,xb,dx0,retol);
% The method is based that the integration error is |f"|*dx^3/24.
% Inputs:
%   fun -- a function which is being integrated;
%   params -- additional parameters for the function, {} if none;
%   xb -- starting values of boundaries of intervals;
%   dx0 -- minimum tolerable dx;
%   retol -- tolerance of the relative integration error
% Output:
%   xb -- new boundaries
% Optional outputs:
%   f0arr -- values of the function at x=(xb1+xb2)/2
%   relerror -- relative integration error
% Note:
%   Output of "fun" can be multidimensional. The first dimension should
%   correspond to x, the second is assumed to be uniform (have the same
%   magnitude), like components of fields at the same point. This is done
%   because sometimes some components are exactly zero (like Ex, Ey on the
%   superconducting ground), and finding the error in integration in these
%   components does not work. Instead, this program relies on other
%   components, like Ez, Hx, Hy. Of course, if all of them are zero at
%   particular point, then we are in trouble.
if length(xb)<2
    error('xb must have at least 2 points');
end
if nargin<7
    debugflag=[];
end
if nargin<6
    miniter=[];
end
if isempty(miniter)
    miniter=1;
    % Do at least one iteration before exiting, so that we don't use
    % the initial inefficient grid (to save time, the result should not
    % change).
end
if isempty(debugflag)
    debugflag=1;
end
xb=xb(:);
iteration=0;
N0=ceil((xb(end)-xb(1))/dx0);
if length(xb)==2
    % No grid is given, create own
    xb=linspace(xb(1),xb(end),N0+1).';
end
if length(xb)<=2
    error('too few initial points');
end
while 1
    N=length(xb)-1;
    if debugflag>0
        if iteration==0
            disp(['BESTGRID: Initial estimate, N=' num2str(N)]);
        else
            disp(['BESTGRID: Re-do, iteration=' num2str(iteration) ', N=' num2str(N)]);
        end
    end
    dxb=diff(xb);
    x=(xb(1:N)+xb(2:N+1))/2;
    dx=diff(x);
    dxav=(dx(1:N-2)+dx(2:N-1))/2;
    f0arr=fun(x,params{:});
    % The first dimension of f0arr corresponds to x
    % The second dimension to a parameter in which fun is assumed to be
    % uniform
    % The third and higher dimensions - to those in which f0arr is not
    % uniform, so the relative error has to be calculated in a proper way.
    s=size(f0arr);
    if length(s)<3
        si=[s(2) 1];
    else
        si=s(2:end);
    end
    ns=prod(si);
    nsextra=prod(si(2:end));
    f0=reshape(f0arr,[N ns]);
    abserror=zeros(si);
    total=zeros(si);
    ga=zeros(N,ns);
    f2=zeros(N,ns);
    for k=1:ns
        % The integration error is = |f"|*dx^3
        f2(:,k)=[0 ; abs(diff(diff(f0(:,k))./dx)./dxav) ; 0];
        % - approximation to |f"|
        total(k)=sum(abs(f0(:,k)).*dxb); % integral
        % We coarsen the mesh at some points but preserve the
        % relative error
        %figure; plot(abs(f2(:,k)))
        abserror(k)=max(abs(f2(:,k)).*dxb.^3)/24;
    end
    %total
    relerror=abserror./repmat(max(total),[si(1) 1]);
    if debugflag>0
        if debugflag==1
            disp(['Relative error=' num2str(max(relerror(:))*N)]);
        else
            disp('Relative error=');
            disp(relerror*N)
        end
    end
    if max(relerror(:))*N<retol && iteration>=miniter
        break
    end
    % EXIT FROM THE CYCLE IS HERE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    e0=24*retol*max(total)/max(N,N0)/10; % size == 1 x si(2:end) % was: retol*max(total)/10
    % -- target for |f"|*dx^3, for each extra parameter
    % Estimate for new value of 1/dx
    maxf2=max(reshape(f2,[N si]),[],2); % N x 1 x si(2:end)
    f2e=maxf2./repmat(shiftdim(e0,-1),[N 1]);
    maxf2e=max(reshape(f2e,[N nsextra]),[],2); % N x 1
    g=max(maxf2e.^(1/3),1/dx0);
    % -- take max of g over the spectator indeces
    G=[0 ; cumsum(g.*dxb)]; % dG=1 if dn is optimal
    Gd=linspace(G(1),G(end),ceil(G(end)-G(1))).';
    % The new grid
    xb=interp1(G,xb,Gd);
    iteration=iteration+1;
end
