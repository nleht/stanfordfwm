function [shf,AE]=enuseg_a_electric(nen,ene,den,deni,Ie,De,numome,weff)
%ENUSEG_A_ELECTRIC - Electric part of A matrix in ENUSEG
% For usage, see ENUSEG_A_COLLISIONAL.
% This function is part of
% ENUSEG - ELENDIF with Non-Uniformly Spaced Energy Grid
% Author: Nikolai G. Lehtinen
% See also: ENUSEG_A_COLLISIONAL (extensive explanation), ENUSEG_A
global ech me
if isempty(ech)
    loadconstants
end
% Diffusion due to electic field and conductivity kernel
shfke=2*ech^2./(3*me)./(numome-1i*weff); % conductivity "kernel" (per electron)
DEe=real(shfke)/ech; % scale energies from J to eV
% Operator of -en^(3/2)*d(ne/sqrt(en))/d(en) = ne/2-en*d(ne)/d(en)
% (applied to ne at en, values at ene)
tmp=Ie/2-diag(ene)*De;
% The kernel in the sense \int sigma(en)ne(en) d(en)=sigma is
%   sigma(en)=shf./dnei
% At low en, numome=C*sqrt(en) => sigma(en)=2*ech^2/(3*me*numom)
shf=((den.*shfke).'*tmp).'; % conductivity=Ne*sum(shf.*ne)
if nargout>1
    % The flux associated with DE
    %   JE = JEe*ne = DE*(ne/2-en*d(ne)/d(en)) at ene,
    % assuming ne(0)=ne(en==0)=0, ne(enmax+dne)=0
    JEe=diag(DEe)*tmp;
    % - we assume
    % Cut off the flux at the bounds
    JEe(1,:)=0; JEe(nen+1,:)=0;
    % AE*ne=-d(JE)/d(en) at en
    AE=-diag(1./deni)*(JEe(2:nen+1,:)-JEe(1:nen,:));
end
