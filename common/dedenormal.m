function x=dedenormal(x,et,en,alg)
% DEDENORMAL Get rid of denormal numbers
% A very thorough version
% Recommended call (fastest, relative numerical error = eps):
%   x=dedenormal(x,1);
% The "true" removal (zeroing) of denormals is given by
%   x=dedenormal(x,0);
% However, this gives suboptimal performance because the subnormals may
% arise at the next step of a calculation since very small numbers still remain.
if nargin<4
    alg='';
end
if isempty(alg)
    alg='simple';
end
if nargin<3
    en=0;
end
if nargin<2
    et=0;
end
e=eps(max(abs(x(:))));
if imag(en)~=0
    noise=realmin;
else
    noise=en*e; % doesn't matter if negative
end
% thresh must be positive
if et<=0
    thresh=realmin;
else
    thresh=et*e;
end
switch alg
    case 'simple'
        ii=(abs(real(x))<thresh);
        x(ii)=1i*imag(x(ii))+noise;
        ii=(abs(imag(x))<thresh);
        x(ii)=real(x(ii))+noise*1i;
    case 'old'
        % Same as 'simple' but skip the zeros from the beginning
        tmp=abs(real(x));
        ii=(tmp<thresh & tmp~=0);
        x(ii)=1i*imag(x(ii))+noise;
        tmp=abs(imag(x));
        ii=(tmp<thresh & tmp~=0);
        x(ii)=real(x(ii))+noise*1i;
    case 'blank'
        % Do nothing
    otherwise
        error(['Algorithm ''' alg ''' is unknown']);
end
