% Test of non-uniformly spaced grid (NUSG)

%% Setup
global kB ech me species composition eps0
if isempty(kB)
    loadconstants
end

T0=0.02; % in eV
%en=0.001*sinh([0.05:0.05:12.5].');
en=0.0001*sinh([0.025:0.025:15].');
%en=0.001*sinh([0.025:0.025:12.5].');
neT=sqrt(en).*exp(-en/T0)*sqrt(4/pi/T0^3);

% Other inputs
Ebr=1.137e-19;
Erms=Ebr*0.1;
%Erms=0;
weff=0;
Nm=1;
debugflag=2

%% Now assume that en was chosen arbitrarily
tic
[nen,ene,den,deni,Ie,De,numome,Ael,Arot,Aie,Ase,Aatt,Aatt3,Aion]=enuseg_a_collisional(en,T0,species,composition,1);
toc
nen

%% Diffusion due to electic field and conductivity kernel
tic
[shf,AE]=enuseg_a_electric(nen,ene,den,deni,Ie,De,Nm*numome,weff);
toc
%conductivity=Ne*sum(shf.*ne)

%% e-e collisions

%% Screening effect: Landau's approximation (Debye radius screening)
% See Shkarofsky "The particle kinetics of plasmas", p. 267 for more exact
% derivation, based on Kihara and Aono [1963], doi:10.1143/JPSJ.18.837
Ne=getNe(120); % at 120 km
% Minimum target parameter
b0=ech^2/(4*pi*eps0*(ech*T0))
lDebye=sqrt((T0*ech)*eps0/(Ne*ech^2))
% Plasma parameter -- these expressions bust be same
Lambda=lDebye/b0
Lambda=4*pi*Ne*lDebye^3
% Coefficient used in the kinetic equation for f(v)
Y=4*pi*(ech^2/(4*pi*eps0*me))^2*log(Lambda)
% Coefficient used in the kinetic equation for ne(en)
alfa=Y*me^(3/2)/sqrt(2)/3
% Convert to eV
alfa=alfa/ech^(3/2)
% Normalize to Ne
alfa=alfa*Ne

%% A and B matrices - A new scheme
tic
[A3,B3]=enuseg_ee_AB(nen,en,ene,den,deni,debugflag);
toc
Aden=diag(1./den(2:nen+1))*A3;
Bden=diag(1./den(1:nen))*B3;

%% Evolve
tic
nt=101;
dt=1e-1;
t=[0:nt-1]*dt;
doplot=0;
for niter=1:4
    % Energy conservation increases very well for higher "niter"
    % Each iteration reduces energy error by a factor of ~10
    %% Initial beam
    % 1 eV beam
    en0=1; den0=0.05;
    % ne=exp(-(en-en0).^2/(2*den0^2))./deni;
    % ne=ne/sum(ne.*deni);
    [dummy,ii]=min(abs(en-en0));
    ne0=zeros(size(neT));
    ne0(ii)=1/deni(ii);
    ne=ne0;
    c='bgrcmyk';
    ntot=zeros(1,nt);
    entot=zeros(1,nt);
    if doplot
        figure;
    end
    %% Cycle
    ne2d=zeros(nen,nt);
    for kt=1:nt
        % kt
        ntot(kt)=sum(ne.*deni); % must be 1
        entot(kt)=sum(ne.*en.*deni);
        switch doplot
            case 1
                semilogy(en,ne./sqrt(en),c(mod(kt-1,7)+1));
                set(gca,'xlim',[0 3],'ylim',[1e-6 1e4]); hold on;
            case 2
                plot(en,ne,c(mod(kt-1,7)+1)); hold on;
            otherwise
        end
        % Calculate conductivity, consistent with the "DE" calculation above
        %sig(k)=sum(shf.*ne);
        nef=ne;
        for kiter=1:niter
            % Multiple iterations are for better energy conservation
            % Each iteration reduces energy error by a factor of ~10
            % Using sparse matrices is MUCH faster (3.5 sec vs 128 sec)
            aden=Aden*nef; bden=Bden*nef; % a/de, b/den
            if 1
                C=alfa*spdiags(1./deni,0,nen,nen)*spdiags([aden -(aden+bden) bden],-1:1,nen,nen);
            else
                C=alfa*diag(1./deni)*(-diag(aden+bden,0)+diag(aden(1:nen-1),-1)+diag(bden(2:nen),1));
            end
            nef=(speye(nen)-dt*C)\ne;
            % sum(deni.*en.*(C*ne)) % is zero
            % sum(deni.*en.*(C*nef)) % must be zero
        end
        ne=nef;
        ne2d(:,kt)=ne;
        if doplot
            pause
        end
    end
    eval(['dentot' num2str(niter) '=entot-entot(1);']);
    eval(['ne2d' num2str(niter) '=ne2d;']);
end
Te=entot(1)*2/3; neTe=sqrt(en).*exp(-en/Te)*sqrt(4/pi/Te^3);
toc

%%
figure; semilogy(t,abs([dentot1 ; dentot2 ; dentot3 ; dentot4]))
figure;
semilogy(t,abs([dentot2./dentot1;dentot3./dentot2;dentot4./dentot3]))
figure;
semilogy(en,[neTe ne2d1(:,nt) ne2d2(:,nt) ne2d3(:,nt) ne2d4(:,nt)]./repmat(sqrt(en),[1 5]));
%%
figure;
for kt=1:nt
    semilogy(en,[ne2d1(:,kt) ne2d2(:,kt) ne2d3(:,kt) ne2d4(:,kt)]./repmat(sqrt(en),[1 4]),'x-');
    set(gca,'xlim',[0 3],'ylim',[1e-6 1e4]);
    pause;
end

%% Figure demonstrating evolution
figure;
ktch=[1 2 10 20 100];
semilogy(en,1e-11+[ne0 ne2d4(:,ktch)]./repmat(sqrt(en),[1 length(ktch)+1])); hold on
plot(en,neTe./sqrt(en),'k');
set(gca,'xlim',[0 5],'ylim',[1e-10 1e4],'fontsize',10); grid on;
legend(num2str([0 t(ktch+1) inf]'))
xlabel('E, eV'); ylabel('n(E), normalized to 1');
title(['Ne=' num2str(Ne/1e9) '\times10^9 m^{-3} as a function of time, s'])
%echo_print('EPS','eecollisions')

%% Other methods
% NOTE: the energy conservation is provided by (A-B)+(A-B).'==0 only for
% an explicit scheme.
if 0
    %% Operators P and Q as matrices
    % Normalization:
    % Pen.*den==P*ne, P(k,l)=Pker(ene(k),en(l))*den(k)*deni(l)
    P1=2*(den./sqrt(ene))*(en.*deni).';
    P2=2*(ene.*den)*(deni./sqrt(en)).';
    P=H.*P1+(1-H).*P2;
    Q=H.*((3*den./sqrt(ene))*deni.');

    %% P*sqrt(en)*d/den(ne/sqrt(en)) is represented P*(dne/den-ne/(2*en))
    up2=1./den(2:nen+1)+1./(4*ene(2:nen+1));
    um2=1./den(1:nen)-1./(4*ene(1:nen));
    A2=P(2:nen+1,:).*repmat(up2,[1 nen])-Q(2:nen+1,:)/2;
    B2=P(1:nen,:).*repmat(um2,[1 nen])+Q(1:nen,:)/2;
    A2(nen,:)=0; B2(1,:)=0; % required for particle conservation!
    
    %% Energy conservation (Rockwood's symmetrization)
    E2=A2-B2+(A2-B2).'; % always symmetric, should be zero
    A2p=A2-E2/4;
    B2p=B2+E2/4;
    
    % A2p=A2;
    % B2p=A2.';
    
    %% Must have zero flux at the boundaries
    % A2p(nen,:)=0; B2p(1,:)=0; % required!
    % The rest must follow from the symmetry
    A2p([1 nen],:)=0;
    B2p([1 nen],:)=0;
    A2p(:,[1 nen])=0;
    B2p(:,[1 nen])=0;
    
    %%  P*sqrt(en)*d/den(ne/sqrt(en)) is differenced directly
    up1=sqrt(ene(2:nen+1)./en)./den(2:nen+1);
    um1=sqrt(ene(1:nen)./en)./den(1:nen);
    A1=P(2:nen+1,:).*repmat(up1,[1 nen])-Q(2:nen+1,:)/2;
    B1=P(1:nen,:).*repmat(um1,[1 nen])+Q(1:nen,:)/2;
    
    %% Rockwood's way - sqrt(ene) is represented as average of sqrt(en)
    P1rh=2*(1./sqrt(en))*(en.*deni).';
    P1r=zeros(nen+1,nen);
    P1r(1:nen,:)=P1r(1:nen,:)+P1rh/2;
    P1r(2:nen+1,:)=P1r(2:nen+1,:)+P1rh/2;
    P1r=P1r.*repmat(den,[1 nen]);
    
    P2rh=2*(en)*(deni./sqrt(en)).';
    P2r=zeros(nen+1,nen);
    P2r(1:nen,:)=P2r(1:nen,:)+P2rh/2;
    P2r(2:nen+1,:)=P2r(2:nen+1,:)+P2rh/2;
    P2r=P2r.*repmat(den,[1 nen]);
    Pr=H.*P1r+(1-H).*P2r;
    
    Qrh=(3./sqrt(en))*deni.';
    Qr=zeros(nen+1,nen);
    Qr(1:nen,:)=Qr(1:nen,:)+Qrh/2;
    Qr(2:nen+1,:)=Qr(2:nen+1,:)+Qrh/2;
    Qr=H.*Qr.*repmat(den,[1 nen]);
    Ar=Pr(2:nen+1,:).*repmat(up2,[1 nen])-Qr(2:nen+1,:)/2;
    Br=Pr(1:nen,:).*repmat(um2,[1 nen])+Qr(1:nen,:)/2;
    
end

%% No e-e collisions
NmAcoll=Nm*(Ael+Arot+Aie+Ase+Aatt+Aion); % Everything except E field
%NmAcoll=Nm*(Ael+Aie+Ase+Aatt+Aion); % No rotational losses

sum(neT.*deni) % must be 1
s0=sum(shf.*neT)/sum(neT.*deni)

EEbr=[0 10.^[-5:.1:0.4]];
nE=length(EEbr);
nuatt=zeros(1,nE);
nuion=zeros(1,nE);
sighf=zeros(1,nE);
neE=zeros(nen,nE);

%% Cycle
for kE=1:nE

    Erms=Ebr*EEbr(kE)

    %% Initial condition
    A=Erms^2*AE+NmAcoll;
    ne=neT;

    %% Run
    dt=1e15; % somewhat arbitrary
    U=inv(eye(nen)-dt*A);
    c='bgrcmyk';
    nt=1000;
    ntot=zeros(1,nt); %sig=zeros(1,nt);
    %figure;
    t=[0:nt-1]*dt;
    for k=1:nt
        ntot(k)=sum(ne.*deni); % must be 1
        %semilogy(en,ne./sqrt(en),c(mod(k-1,7)+1)); hold on;
        % Calculate conductivity, consistent with the "DE" calculation above
        %sig(k)=sum(shf.*ne);
        ne=U*ne;
        %pause
    end
    %hold off
    ntotf=sum(ne.*deni)
    nenorm=ne/ntotf;
    
    %%
    nuatt(kE)=-sum((Aatt*nenorm).*deni);
    nuion(kE)=sum((Aion*nenorm).*deni);
    sighf(kE)=sum(shf.*nenorm);
    neE(:,kE)=nenorm;
    disp(['Done: ' num2str(kE) '/' num2str(nE) '=' num2str(100*kE/nE) '%']);
end

%% Example of steady-state distribution
figure;
kEch=[2+10*4:5:2+10*5];
semilogy(en,neE(:,kEch)./repmat(sqrt(en),[1 length(kEch)]),'x-','linewidth',1)
grid on
set(gca,'fontsize',14,'xlim',[0 15],'ylim',[1e-7 1])
legend(['E/E_{br}=' num2str(EEbr(kEch(1)))],...
    ['E/E_{br}=' num2str(EEbr(kEch(2)))],...
    ['E/E_{br}=' num2str(EEbr(kEch(3)))])
xlabel('E, eV'); ylabel('n(E), normalized to 1');
if 0
    echo_print('EPS','enuseg');
end

%% Attachment and ionization factor
figure;
semilogy(EEbr,[nuatt ; nuion]);

%% Conductivity and temperature change
ds=-(sighf(2:nE)-sighf(1));
T=2/3*(deni.*en).'*neE;
dT=T(2:nE)-T(1);
figure;
loglog(EEbr(2:end),[ds ; dT]);

%% Energy losses
figure;
Lrot=-(deni.*en).'*Arot;
Lel=-(deni.*en).'*Ael;
Lie=-(deni.*en).'*Aie;
Lse=(deni.*en).'*Ase;
LE=(deni.*en).'*AE;
lossplot=[[Lrot ; Lel ; Lie ; Lse]*neE ; ((Lie-Lse)*neE) ; (Ebr*EEbr).^2.*(LE*neE)];
loglog(EEbr,lossplot); hold on;
loglog(EEbr,-lossplot,'--'); hold off;
legend('Rotational','Elastic','Inelastic','Superelastic (gain)','Inelastic (net)','Electric (gain)')
grid on;
xlabel('E/Ebr'); ylabel('per Nm')

%% Error in energy losses
figure; loglog(EEbr,abs(lossplot(1,:)+lossplot(2,:)+lossplot(3,:)-lossplot(4,:)-lossplot(6,:)))

%% Ginzburg's plasma field
[numom,nuen]=getnumom(en);
EpEbr=sqrt(3*me*ech*T0*(numom.^2+weff^2).*nuen./numom)/ech/Ebr;
% Taken at the average energy
EpEbrav=interp1(en,EpEbr,T*3/2);
figure; 
loglog(EEbr,[EEbr ; EpEbrav])
% Where the curves are intersecting, the transition to non-maxwellian
% regime.
itrans=find(EpEbrav<EEbr,1)
EEbr(itrans)

figure;
neTtrans=sqrt(en).*exp(-en/T(itrans))*sqrt(4/pi/T(itrans)^3);
sum(deni.*neTtrans)
semilogy(en,[neE(:,itrans)./sqrt(en) neTtrans./sqrt(en)])
