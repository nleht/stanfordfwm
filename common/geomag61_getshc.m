function [gh,ios]=geomag61_getshc(mdfile, iflag, strec, nmax_of_gh)
%GEOMAG61_GETSHC Reads spherical harmonic coefficients
% Usage: [gh,ios]=geomag61_getshc(mdfile, iflag, strec, nmax_of_gh);
%***************************************************************************/
%                                                                          */
%                           Subroutine getshc                              */
%                                                                          */
%***************************************************************************/
%                                                                          */
%     Reads spherical harmonic coefficients from the specified             */
%     model into an array.                                                 */
%                                                                          */
%     Input:                                                               */
%           stream     - Logical unit number                               */
%           iflag      - Flag for SV equal to ) or not equal to 0          */
%                        for designated read statements                    */
%           strec      - Starting record number to read from model         */
%           nmax_of_gh - Maximum degree and order of model                 */
%                                                                          */
%     Output:                                                              */
%           gh1 or 2   - Schmidt quasi-normal internal spherical           */
%                        harmonic coefficients                             */
%                                                                          */
%     FORTRAN                                                              */
%           Bill Flanagan                                                  */
%           NOAA CORPS, DESDIS, NGDC, 325 Broadway, Boulder CO.  80301     */
%                                                                          */
%     C                                                                    */
%           C. H. Shaffer                                                  */
%           Lockheed Missiles and Space Company, Sunnyvale CA              */
%           August 15, 1988                                                */
%                                                                          */
%**************************************************************************

    
stream = fopen(mdfile, 'r');
if stream == -1
    fprintf(2,'\nError on opening file %s', mdfile);
    ios=-1;
    return
end

gh=zeros(1,196);
ii = 0;
ios = 0;
fseek(stream, strec, 'bof');
for nn = 1:nmax_of_gh
    for mm = 0:nn
        inbuff=fgetl(stream);
        m=strread(inbuff(1:2),'%d');
        n=strread(inbuff(3:4),'%d');
        if iflag == 1
            [g,hh,trash,trash,irat,line_num]=strread(inbuff(5:end),'%f%f%f%f%s%d',1);
        else
            [trash,trash,g,hh,irat,line_num]=strread(inbuff(5:end),'%f%f%f%f%s%d',1);
        end
        if ((nn ~= n) || (mm ~= m))
            ios = -2;
            fclose(stream);
            return
        end
        ii = ii + 1;
        gh(ii) = g;
        if (m ~= 0)
            ii = ii + 1;
            gh(ii) = hh;
        end
    end
end
fclose(stream);
gh=gh(1:ii);


