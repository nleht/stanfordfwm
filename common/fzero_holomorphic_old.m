function [zsol,zmul,zr,zi,zbox,fbox,ir,ii]=fzero_holomorphic(fun,params,zrmin,zrmax,zimin,zimax,dzmax,dztol,debugflag,recur)
%HOLOMORPHIC_ZEROS Find zeros of a holomorphic (analytic) function
% Usage:
%    [zsol,zmul]=fzero_holomorphic(...
%       fun,params,zrmin,zrmax,zimin,zimax[,dzmax,dztol,debugflag]);
% Advanced usage:
%    [zsol,zmul,zr,zi,zbox,fbox]=fzero_holomorphic(...
%       fun,params,zrmin,zrmax,zimin,zimax[,dzmax,dztol,debugflag]);
% Inputs:
%    fun - function handle, the first argument must be "z"
%    params - cell array of parameters (the rest of the arguments of "fun")
%    zrmin,zrmax,zimin,zimax - the min/max or real/imag parts of z
% Optional inputs:
%    dzmax - maximum value of dz for the grid of values of "fun"
%    dztol - maximum distance between separate solutions, otherwise they
%       are treated as multiple roots (default=1e-6)
%    debugflag - default=0
% Outputs:
%    zsol, zmul - zeros and their multiplicities
% Optional outputs (for debugging):
%    zr,zi - grid between z{r|i}min and z{r|i}max
%    zbox - 2D grid in the complex plane
%    fbox - values of "fun" on this grid
% Author: Nikolai G. Lehtinen

%% Input args
if nargin<10
    recur=0; % Must never be called
end
margins=repmat('*',[1 recur]);

if nargin<9
    debugflag=0;
end
if nargin<8
    dztol=[];
end
if nargin<7
    dzmax=[];
end
if isempty(dztol)
    dztol=1e-6;
end
if zrmax<zrmin
    tmp=zrmax;
    zrmax=zrmin;
    zrmin=tmp;
end
if zimax<zimin
    tmp=zimax;
    zimax=zimin;
    zimin=tmp;
end

[nztarget,zr,zi,dz]=fzero_holomorphic_nzeros(fun,params,zrmin,zrmax,zimin,zimax,dzmax,debugflag,margins);
if debugflag>0
    disp([margins 'FZERO_HOLOMORPHIC: Expecting to find ' num2str(nztarget) ' zeros'])
end

%% Find boxes in which the roots are approximately located
tstart=now*24*3600;
[zrm,zim]=ndgrid(zr,zi);
zbox=zrm+1i*zim;
[Nr,Ni]=size(zbox);
fbox=reshape(fun(zbox(:),params{:}),[Nr Ni]);
if debugflag>0
    disp([margins 'Filled the matrix, time=' hms(now*24*3600-tstart)]);
end
% Find boxes in which imag(F) and real(F) change sign
tmpi=1*(imag(fbox)>0);
tmpr=1*(real(fbox)>0);
ifound=find(([diff(tmpi,[],1);zeros(1,Ni)]~=0 | [diff(tmpi,[],2) zeros(Nr,1)]~=0) ...
    & ([diff(tmpr,[],1);zeros(1,Ni)]~=0 | [diff(tmpr,[],2) zeros(Nr,1)]~=0));
[indr,indi]=ndgrid(1:Nr,1:Ni);
ir=indr(ifound); ir=ir(:).'; % convert to a row - for display purposes
ii=indi(ifound); ii=ii(:).';
nboxes=length(ifound);
if debugflag>0
    disp([margins 'Found ' num2str(nboxes) ' boxes with zeros']);
end
if debugflag>2
    figure;
    imagesc(zr,zi,log10(abs(fbox.'))); set(gca,'ydir','normal');
    axis equal
    colorbar
    hold on
    for k=1:nboxes
        z=zbox(ir(k),ii(k));
        text(real(z),imag(z),num2str(k));
    end
    hold off
end

%% Congregate boxes into continuous regions
region=zeros(1,nboxes); nregions=0;
for k=1:nboxes
    if region(k)==0
        nregions=nregions+1;
        region(k)=nregions;
    end
    for kk=k+1:nboxes
        if abs(ir(kk)-ir(k))<=1 && abs(ii(kk)-ii(k))<=1
            % They belong to the same region
            region(kk)=region(k);
        end
    end
end
if debugflag>1
    ir
    ii
    region
end
if debugflag>0
    disp([margins 'Congregated into ' num2str(nregions) ' regions']);
end
% Region corners
iregionr=zeros(2,nregions);
iregioni=zeros(2,nregions);
for k=1:nregions
    ix=find(region==k);
    tmpr=ir(ix);
    tmpi=ii(ix);
    iregionr(:,k)=[min(tmpr) max(tmpr)+1];
    iregioni(:,k)=[min(tmpi) max(tmpi)+1];
end
if debugflag>1
    iregionr
    iregioni
end

%% Find the roots in the regions. Extend regions if necessary
while 1
    % Cycle until we find all the roots
    zsol=[]; zmul=[];
    for k=1:nregions
        ir1=iregionr(1,k); ii1=iregioni(1,k);
        ir2=iregionr(2,k); ii2=iregioni(2,k);
        if debugflag>0
            disp([margins 'Region ' num2str(k) '/' num2str(nregions)]);
        end
        z1=zbox(ir1,ii1);
        z2=zbox(ir2,ii2);
        if debugflag>0
            disp([margins 'z1=' num2str(z1) '; z2=' num2str(z2)]);
        end
        n=fzero_holomorphic_nzeros(fun,params,real(z1),real(z2),imag(z1),imag(z2),dz/10,debugflag,margins);
        if debugflag>0
            disp([margins ' - contains ' num2str(n) ' solutions']);
        end
        if n==1
            % Newton-Raphson
            z=fzero_holomorphic_single(fun,params,z1,z2,debugflag,margins);
            zsol=[zsol z];
            zmul=[zmul 1];
        elseif n>1
            if abs(z2-z1)<dztol
                % Label root as multiple
                z=fzero_holomorphic_single(fun,params,z1,z2,debugflag,margins);
                zsol=[zsol z];
                zmul=[zmul n];
            else
                % Subdivide the box again
                if debugflag>0
                    disp([margins 'Subdividing']);
                end
                [z,zm]=fzero_holomorphic(fun,params,real(z1),real(z2),imag(z1),imag(z2),dz/10,[],debugflag,recur+1);
                zsol=[zsol z];
                zmul=[zmul zm];
            end
        end
    end
    % Discard duplicate solutions
    nz=length(zsol);
    duplic=zeros(1,nz);
    k=0;
    while k<nz
        k=k+1;
        if duplic(k)
            continue
        end
        ii=find(abs(zsol-zsol(k))<dztol);
        if length(ii)>=2
            duplic(ii(2:end))=1;
        end
    end
    zsol=zsol(find(~duplic));
    zmul=zmul(find(~duplic));
    % Check if all solutions have been found
    if sum(zmul)<nztarget
        disp([margins 'WARNING: Failed to find all roots! (found=' ...
            num2str(sum(zmul)) ', expected=' num2str(nztarget) ')']);
        disp([margins 'Extending ALL regions']);
        for k=1:nregions
            % Warning: we must not touch other regions! (not implemented)
            iregionr(1,k)=max(1,iregionr(1,k)-1);
            iregioni(1,k)=max(1,iregioni(1,k)-1);
            iregionr(2,k)=min(Nr,iregionr(2,k)+1);
            iregioni(2,k)=min(Ni,iregioni(2,k)+1);
        end
    else
        break
    end
end

%% Final touch: Sort by real part
[y,ii]=sort(real(zsol));
zsol=zsol(ii);
zmul=zmul(ii);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% How many zeros
function [nztarget,zr,zi,dz]=fzero_holomorphic_nzeros(fun,params,zrmin,zrmax,zimin,zimax,dzmax,debugflag,margins)
zrrange=zrmax-zrmin;
zirange=zimax-zimin;
dz=min(zrrange,zirange)/10;
if ~isempty(dzmax)
    dz=min(dz,dzmax);
end
while 1
    zr=linspace(zrmin,zrmax,ceil(zrrange/dz)+1);
    zi=linspace(zimin,zimax,ceil(zirange/dz)+1);
    zb=[zr+1i*zimin zrmax+1i*zi(2:end-1) zr(end:-1:1)+1i*zimax zrmin+1i*zi(end-1:-1:1)];
    fb=fun(zb,params{:});
    phase=unwrap(angle(fb)-angle(fb(1)))/(2*pi);
    if max(abs(diff(phase)))<0.25
        break
    else
        dz=dz/2;
        if debugflag>0
            disp([margins 'FZERO_HOLOMORPHIC_NZEROS: Refining mesh to dz=' num2str(dz)]);
        end
    end
end
nztarget=round(phase(end));
if abs(nztarget-phase(end))>1e-3
    error([margins 'Phase calculation failure (' num2str(phase(end)) ' roots)'])
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Newton-Raphson method
function zsol=fzero_holomorphic_single(fun,params,z10,z20,debugflag,margins)
% Newton-Raphson method
z1=z10;
f1=fun(z1,params{:});
z2=(z10+z20)/2;
f2=fun(z2,params{:});
if debugflag>0
    disp([margins 'Newton-Raphson method, z1=' num2str(z1) ', z2=' num2str(z2)]);
end
while 1
    dz=-f2*(z2-z1)./(f2-f1);
    z1=z2;
    z2=z2+dz;
    % no thinking outside the box!
    if real(z2)<real(z10)
        z2=real(z10)+1i*imag(z2);
    elseif real(z2)>real(z20)
        z2=real(z20)+1i*imag(z2);
    end
    if imag(z2)<imag(z10)
        z2=real(z2)+1i*imag(z10);
    elseif imag(z2)>imag(z20)
        z2=real(z2)+1i*imag(z20);
    end
    f1=f2;
    f2=fun(z2,params{:});
    if debugflag>0
        disp([margins 'z=' num2str(z2) '; f=' num2str(f2)]);
    end
    if abs(f2)<1e-6
        break
    end
end
zsol=z2;
end