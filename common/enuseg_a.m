function A=enuseg_a(en,T0,Nm,Erms,weff)
%ENUSEG_A - A matrix in ENUSEG
%
% This function is part of
% ENUSEG - ELENDIF with Non-Uniformly Spaced Energy Grid
%
% Author: Nikolai G. Lehtinen
% See also: ENUSEG_A_COLLISIONAL (extensive explanation),
%    ENUSEG_A_ELECTRIC
tic
[nen,ene,den,deni,Ie,De,numome,Acont,Aie,Ase,Aatt,Aion]=enuseg_a_collisional(en,T0);
toc
% Diffusion due to electic field and conductivity kernel
tic
[AE,shf]=enuseg_a_electric(nen,ene,den,deni,Ie,De,Nm*numome,weff);
toc
%conductivity=Ne*sum(shf.*ne)
A=Erms^2*AE+Nm*(Acont+Aie+Ase+Aatt+Aion);
