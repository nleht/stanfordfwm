function [Ne,nu]=get_Helliwell_ionosphere(h,daytime,geomag_latitude)
% Helliwell's (1965) electron density and collision frequency profiles
f0F2lat=[0:5:90];
f0F2d=[13.2 14.5 14.6 14 13.2 12.5 12.1 12.2 12.5 13.4 13.9 14.1 14 ...
    13.5 12.4 10 7.6 6.7 6.5]/12.5;
f0F2n=[8.5 10.2 11.5 11 9 7.2 6.5 5.9 5.6 5.4 5.3 5.2 5.1 5 5 5 5.1 ...
    5.3 5.6]/5.5;
switch daytime
    case 'day'
        f0F2=f0F2d;
        hhell=[60 100 200]; Nehell=[10 1e5 3e5]*1e6;
    case 'night'
        f0F2=f0F2n;
        hhell=[60 110 200]; Nehell=[1 1e4 2e4]*1e6;
    otherwise
        error('unknown helliwell profile')
end
Necoefhell=interp1(f0F2lat,f0F2,geomag_latitude)^2
ii=find(h>=60);
Ne=zeros(size(h)); nu=zeros(size(h));
Ne(ii)=Necoefhell*exp(interp1(hhell,log(Nehell),h(ii)));
% The collision frequency
htmp=[60 110 120]; nutmp=[6e7 2e4 8e3];
nu(ii)=exp(interp1(htmp,log(nutmp),h(ii)));
