function jdoy=astro_jdoy(year)
%ASTRO_JDOY Julian date of 0.0 Jan year
% Page 61 of Astronomical Algorithms by Jean Meeus.
% The Julian Date of January 0.0 of any year would be:
% See also: ASTRO_JD, ASTRO_DOY, ASTRO_GMST
year = year - 1;
A = floor(year/100);
B = 2 - A + floor(A/4);
jdoy = floor(365.25 * year) + floor(30.6001 * 14) + 1720994.5 + B;
