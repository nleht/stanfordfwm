function KtBG=brucegolde(t,stroke,speed)
%BRUCEGOLDE Current moment in Bruce-Golde model for a typical (30 kA) discharge
% as quoted in
% H. Volland (1995), "Handbook of Atmospheric Electrodynamics", Volume 1,
% p. 113.
% Using
% [1] Bruce, C.E.R. and Golde, R.H. (1941), "The lightning discharge", J.
% Inst. Electr. Eng. Part 2, 88, 487
% [2] Cianos, N. and Pierce, E.T (1972), "A ground-lightning environment
% for engineering usage", Tech Report 1, Stanford Research Institute, Menlo
% Park, CA
if nargin<3
    speed='default';
end
if nargin<2
    stroke='default';
end
%%%%%% Velocity of return strokes is v0*e^(-eta*t) (1st) or v0 (subsequent)
switch speed
    case 'default'
        v0=8e7; eta=3e4;
    case 'fast'
        v0=24e7; eta=9e4;
    otherwise
        error('unknown speed')
end
%%%%%% d=\int v dt
d0=v0/eta; % max length of the channel
Iwindow=(t>=0); % Non-zero current window
d1=d0*(1-exp(-eta*t)).*Iwindow; % 1st return stroke
t0=d0/v0;
d2=v0.*t.*Iwindow.*(t<t0)+d0*(t>=t0); % subsequent return strokes
%%%%%% Velocity (not really needed)
%v1=v0*exp(-eta*t).*Iwindow1;
% The velocity of subsequent strokes is approximately constant
%v2=v0*Iwindow2;
%%%%%% Return stroke current
% Continuing (intermediate) current (Cianos and Pierce, 1972)
I0i=2e3; gi=1e3; di=1e4;
Ii=I0i*(exp(-gi*t)-exp(-di*t)).*Iwindow;
% The first stroke
I01=30e3; a1=4.4e4; b1=4.6e5;
I1=(I01*(exp(-a1*t)-exp(-b1*t))).*Iwindow;
% Subsequent strokes
I02=20e3; a2=2e4; b2=2e6;
I2=((I02/2)*(exp(-a2*t)-exp(-b2*t))).*Iwindow;
%%%%%% Current moment (with image) = 2 I \int v dt = 2*I*d
% Current moment (with image) = 2 I \int v dt
% However, the image is already taken into account by FWM.
switch stroke
    case 'default'
        KtBG=(I1+Ii).*d1;
    case 'first'
        KtBG=I1.*d1;
    case 'subsequent'
        KtBG=I2.*d2;
    otherwise
        error('unknown stroke')
end
