function echo_print(plot_formats, fname)
%ECHO_PRINT Print current figure to a file
% Usage:
%    echo_print({'FIG','EPS','PDF','PNG'},filename)
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.

drawnow;
if ischar(plot_formats)
    plot_formats={plot_formats};
end
if iscell(plot_formats)
    s=0;
    n=length(plot_formats);
    for k=1:n
        switch plot_formats{k}
            case 'EPS'
                s=bitor(s,2^0);
            case 'FIG'
                s=bitor(s,2^1);
            case 'PNG'
                s=bitor(s,2^2);
            case 'PDF'
                s=bitor(s,2^3);
            case 'M'
                s=bitor(s,2^4);
            case 'MMAT'
                s=bitor(s,2^5);
            case 'SVG'
                s=bitor(s,2^6);
            otherwise
                error(['Unknown format: ' plot_formats{k}]);
        end
    end
    plot_formats=s;
end
if plot_formats
    disp(['Saving picture ''' fname ''' ...']);
end
if bitget(plot_formats,1)
    print('-depsc2',[fname '.eps'])
    disp(' EPS');
end
if bitget(plot_formats,2)
    hgsave([fname '.fig']);
    disp(' FIG');
end
if bitget(plot_formats,3)
    print('-dpng',[fname '.png'])
    disp(' PNG');
end
if bitget(plot_formats,4)
    fig = gcf;
    set(fig,'Units','inches');
    set(fig,'PaperUnits','inches');
    pos = get(fig, 'Position');
    set(fig,'PaperPosition',[0 0 pos(3:4)],'PaperSize',[pos(3:4)]);
    print(fig,'-dpdf','-vector',[fname '.pdf']); % or -painters instead of -vector
    % The first two lines measure the size of your figure (in inches). The
    % next line configures the print paper size to fit the figure size. The
    % last line uses the print command and exports a vector pdf document as
    % the output.)
    disp(' PDF');
end
if bitget(plot_formats,5)
    saveas(gcf,[fname '.m'],'m')
    disp(' M');
end
if bitget(plot_formats,6)
    saveas(gcf,fname,'mmat')
    disp(' MMAT');
end
% New, may not work with older MATLAB
if bitget(plot_formats,7)
    saveas(gcf,fname,'svg')
    disp(' SVG');
end

