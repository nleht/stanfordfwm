function res=sincmn(m,n,x,y)
%SINCMN Used in second term of Dyson expansion
% Defined as
%  (2*tau)^(m+n+2)*sincmn(m,n,x*tau,y*tau)=...
%     \int_{-\tau}^\tau t1^m dt1 exp(i*x*t1) ...
%        \int_{-\tau}^t1 t2^n exp(i*y*t2) dt2
% See also: SINCN, SINCN0
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.
if any(size(y)~=size(x))
    error('size')
end
iy0=(y==0);
yy=y(~iy0); xx=x(~iy0);
s1=sincn(m,xx+yy); s2=ones(size(yy)); t1=ones(size(yy)); t2=t1;
for k=1:n
    t1=t1.*(-2i*yy)/k;
    s1=s1+t1.*sincn(m+k,xx+yy);
    t2=t2.*(1i*yy)/k;
    s2=s2+t2;
end
res=zeros(size(x));
res(~iy0)=-prod(1:n)./(-2i*yy).^(n+1).*(s1-exp(-1i*yy).*sincn(m,xx).*s2);
res(iy0)=(sincn(m+n+1,x(iy0))-(-.5)^(n+1)*sincn(m,x(iy0)))/(n+1);
