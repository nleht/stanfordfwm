function chiu_wrapper(z,deg,needTEC)
% Wrapper for chiu_ionden
COMMON Chiu_Share,sst

IF(Keyword_Set(degrees))THEN s=!dtor ELSE s=1d

IF (N_Params() LT 6) THEN Message,'not enough arguments'
IF (NOT Keyword_Set(tecflag) AND Is_Undefined(zarr)) THEN $
  Message,'altitudes not given and "TEC" keyword is not set'
;;tmp=Where(zarr GT 500 OR zarr LT 90)
IF (NOT Keyword_Set(tecflag)) THEN BEGIN
    tmp=Where(zarr LT 0)
    IF(tmp[0] NE -1) THEN $
      ;;Message,'Z is out of 90--500km bounds'
    Message,'Z<0'
ENDIF

IF (Is_Undefined(sst)) THEN BEGIN
    ;; Load the table
    Message,'Loading sunspot table',/Informational
    sst=ReadMatrix('/nh/toaster/u/nleht/idl/const/sunspots.txt')
ENDIF

geo_to_mag,lat*s,lon*s,RLTM,RLGM

DIP=atan(2*sin(RLTM)/cos(RLTM))

TMO=month+day/(365./12.)-.5

;; Number of sunspots
ii=Where(sst[0,*] EQ 100.*year+month)
IF (ii[0] eq -1) THEN Message,'no sunspot data'
RZUR=sst[3,ii[0]]

RLT=lat*s
PHI=(ut/24.)*2.*!pi+lon*s

RETURN,ChiuIonden_Main(RZUR,PHI,TMO,RLT,RLTM,RLGM,DIP,TEC=tecflag,Z=zarr)

END

