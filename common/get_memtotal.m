function m=get_memtotal
% Memory in kB; Only Linux
[r,w] = unix('cat /proc/meminfo | grep MemTotal');
m=str2double(regexp(w, '[0-9]*', 'match'));