function jd=astro_jd(utc)
%ASTRO_JD Convert UTC to Julian day
% The Julian date (JD) is the interval of time in days and fractions of a
% day, since January 1, 4713 BC Greenwich noon, Julian proleptic calendar.
% See http://en.wikipedia.org/wiki/Julian_day
% WARNING: This function has not been debugged!
% See also: ASTRO_GMST, ASTRO_DOY, ASTRO_JDOY
year=utc(1);
month=utc(2);
day=utc(3);
hour=utc(4);
minute=utc(5);
second=utc(6);
if (month == 1)||(month == 2)
    year  = year - 1;
    month = month + 12;
end
a = floor(year/100);
b = 2 - a + floor(a/4);
c = floor(365.25*year);
d = floor(30.6001*(month + 1));
ut=hour*3600 + minute*60 + second;
jd=b + c + d + 1720994.5 + day + ut/86400;
