function box=get_box_sub(bbox,pos)
%GET_BOX_SUB "Multiplication" of boxing-in transformation
% A box is a rectangle [left bottom width height], same format as used for
% 'Position' property in MATLAB axes.
% Usage: box=get_box_sub(parentbox,position);
% See also: GET_BOX, GET_BOX_GRID
box=[bbox(1)+pos(1)*bbox(3) bbox(2)+pos(2)*bbox(4) pos(3)*bbox(3) pos(4)*bbox(4)];
