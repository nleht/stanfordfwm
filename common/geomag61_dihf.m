function [d,i,h,f]=geomag61_dihf(x,y,z)
%GEOMAG61_DIHF Computes the geomagnetic d, i, h, and f from x, y, and z.
% Input:
%    x  - northward component
%    y  - eastward component
%    z  - vertically-downward component
% Output:
%    d  - declination
%    i  - inclination
%    h  - horizontal intensity
%    f  - total intensity
% FORTRAN
%    A. Zunde
%    USGS, MS 964, box 25046 Federal Center, Denver, CO.  80225
% C
%    C. H. Shaffer
%    Lockheed Missiles and Space Company, Sunnyvale CA
%    August 22, 1988
% MATLAB
%    Nikolai G. Lehtinen
%    Stanford University, Stanford, CA
%    September 10, 2009

sn = 0.0001;

h2 = x*x + y*y;
h = sqrt(h2); % calculate horizontal intensity
f = sqrt(h2 + z*z); % calculate total intensity
if (f < sn)
    d = NaN; % If d and i cannot be determined,
    i = NaN; % set equal to NaN
else
    i = atan2(z,h);
    if (h < sn)
        d = NaN;
    else
        hpx = h + x;
        if (hpx < sn)
            d = pi;
        else
            d = 2.0 * atan2(y,hpx);
        end
    end
end
