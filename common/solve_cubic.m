function [x,err]=solve_cubic(a0,b0,c0,d0)
%SOLVE_CUBIC Solve a*x^3+b*x^2+c*x+d=0
% function [x,err]=solve_cubic(a,b,c,d)
% Can take 1D (column) arrays of a,b,c,d.
% See Wikipedia.
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.

% Extensive error checking in the beginning
if any(a0==0)
    error('a==0');
end
a0=a0(:); b0=b0(:); c0=c0(:); d0=d0(:);
lengths=[length(a0),length(b0),length(c0),length(d0)];
n=max(lengths);
if n>1
    if length(a0)==1
        a0=repmat(a0,n,1);
    end
    if length(b0)==1
        b0=repmat(b0,n,1);
    end
    if length(c0)==1
        c0=repmat(c0,n,1);
    end
    if length(d0)==1
        d0=repmat(d0,n,1);
    end
end
lengths=[length(a0),length(b0),length(c0),length(d0)];
if any(lengths~=n)
    error('wrong lengths');
end

% To form x^3+3*a*x^2+b*x+c=0:
a=b0./a0/3; b=c0./a0; c=d0./a0;
% Depressed cubic: t^3+3*p*t+2*q=0, x=t-a
p=b/3-a.^2; q=c/2+a.^3-a.*b/2;
[t,err]=solve_depressed_cubic(p,q);
x=t-repmat(a,1,3);
