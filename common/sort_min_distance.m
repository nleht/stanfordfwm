function [fsort,ii,failed]=sort_min_distance(f0,f)
%SORT_MIN_DISTANCE Note the output args order is the same as SORT
% Usage: [fsort,ii]=sort_min_distance(f0,f)
% See also: SMOOTH_SORT, APPLY_SORT, SIMPLE_EXTRAP
nf=length(f);
if length(f0)~=nf
    error('wrong size')
end
ii=zeros(size(f0));
for j=1:nf
    [dummy,ii(j)]=min(abs(f0(j)-f));
end
if length(unique(ii))~=nf
    %disp('SORT_MIN_DISTANCE: elements are not unique')
    failed=true; fsort=f;
else
    fsort=f(ii);
    failed=false;
end
