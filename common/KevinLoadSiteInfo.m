%Load site information

%Sets the following variables:
%   TxNames - cell array of transmitter abbreviations
%   TxData  - numerical array of transmitter data
%   RxNames - cell array of receiver abbreviations
%   RxData  - numerical array of receiver data
%Latitudes  will be returned in -90 to 90 geographic, decimal format
%Longitudes will be returned in -180 to 180 geographic, decimal format

%List compiled by Morris Cohen
%Script modified by Kevin Graf on 2008-01-10
%       -Changed format.  Setup as cell arrays.  Added some receivers.
%Updated by Kevin Graf on 2008-06-13
%       -Updated Transmitter List & Recalculated L-Shell, Conjugate Points
%       -L-Shell and Conjugate Points of Tx were calculated using
%           http://omniweb.gsfc.nasa.gov/cgi/vitmo/vitmo_model.cgi
%           for 2008 at height = 0 above Earth's surface.
%       -L-Shell and Conjugate Points of Rx were NOT recalculated.

%% Transmitters
Transmitters1 = {
%Lat     Lon         L-shell     ConjLat     ConjLon ?   %Abbrev   % Freq [Hz]   Name, Location                     MOD     Baud    kW
 55.760   84.450     2.61        -38.99       93.27  0   'RA1'    % 11904.76     Novosibirsk, Russia                CW      0       500
 45.403   38.158     1.75        -31.87       46.56  0   'RA2'    % 12648.91     Krasnodar, Russia                  CW      0       500
 55.070  135.600     2.33        -32.20      132.96  0   'RA3'    % 14880.95     Komsomolsk-na-Amur, Russia         CW      0       500
 25.030  111.670     1.11         -9.45      111.93  1   '3SA'    % 20600        Changde, China (Alternates 3SB)    MSK     225
 39.600  103.330     1.47        -23.07      105.29  1   '3SB'    % 20600        Datonge, China (Alternates 3SA)    MSK     225
  8.387   77.753     1            00.00       00.00  1   'VTX'    % 18200        Katabomman, India                  MSK     200
 32.040  130.810     1.23        -15.94      130.57  1   'JJI'    % 22200        Ebino, Japan                       MSK     225     200
-38.481  146.935     2.34         55.46      156.14  1   'NST'    % 18600        Woodside, Australia (USA)          MSK     100
-21.816  114.166     1.41         38.18      113.07  1   'NWC'    % 19800        North West Cape, Australia (USA)   MSK     200     1000
 63.851  337.541     5.40        -69.15       28.84  1   'NRK'    % 37500        Grindavik, Iceland (USA)           MSK     200
 59.910   10.520     2.69        -48.35       33.45  1   'JXN'    % 16400        Kolsas, Norway                     MSK     100     45
 52.911  356.720     2.39        -46.21       18.24  1   'GBZ'    % 19600        Anthorn, Great Britain (NATO)      MSK     100     100
 52.911  356.720     2.39        -46.21       18.24  1   'GQD'    % ?????        Anthorn, Great Britain (NATO)      MSK     100     100
 52.911  356.720     2.39        -46.21       18.24  1   'GQD1'   % 19600        Anthorn, Great Britain (NATO)      MSK     100     100
 52.911  356.720     2.39        -46.21       18.24  1   'GQD2'   % 22100        Anthorn, Great Britain (NATO)      MSK     100     200
 46.713    1.245     1.79        -33.76       13.78  1   'HWU'    % 22600        Le Blanc, France (NATO)            MSK     200     400
 48.544    2.576     1.93        -36.95       16.74  1   'HWV'    % 20900        St Assise, France                  MSK     200     400
 53.079    7.614     2.35        -44.35       26.71  1   'DHO'    % 23400        Rhauderfehn, Germany (NATO)        MSK     200     800
 40.923    9.731     1.46        -23.74       15.70  1   'ICV'    % 20270        Isola di Tavolara, Italy (NATO)    MSK     200     20
 38.000   13.500     1.35        -19.69       17.28  1   'NSC'    % 45900        Sicilty, Italy (USA)               MSK     200
 37.430   27.550     1.37        -20.72       30.12  1   'TBB'    % 26700        Bafa, Turkey                       MSK     225
 44.646  292.719     2.80        -68.04      301.67  1   'NAA'    % 24000        Cutler, Maine, USA                 MSK     200     1000
 46.366  261.665     3.21        -63.51      232.08  1   'NML'    % 25200        LaMoure, North Dakota, USA         MSK     200      
 48.203  238.083     2.85        -54.42      206.01  1   'NLK'    % 24800        Jim Creek, Washington, USA         MSK     200     192
 21.420  201.846     1.15        -19.82      194.08  1   'NPM'    % 21400        Lualualei, Hawaii, USA             MSK     200     424
 18.399  292.822     1            00.00       00.00  1   'NAU'    % 40750        Aguado, Puerto Rico, USA           MSK     200     100
 56      44          1            00.00      00.000  1   'UMS'    % 16200        Gorki, Russia            
 43 	 135         1            00.00       00.00  1   'RPS'    % 17100        Eastern Siberia, Russia            

 %-90.000   00.000     13.33        66.11      293.84  1   'NPX'    % 20000        South Pole, Antarctica
};
% Rearrange as a dictionary
[NXmtr,Ndat]=size(Transmitters1);
Xmtrs=cell(1,2*NXmtr);
Xmtrs(1:2:end)=Transmitters1(:,7);
Xmtrs(2:2:end)=mat2cell(cell2mat(Transmitters1(:,1:6)),ones(1,NXmtr),[Ndat-1]);
% Now we can use tmp=getvaluefromdict(Xmtrs,'NML',[]) etc.

TxData  = cell2mat(Transmitters1(:,1:6));
TxData(TxData>180) = (rem(TxData(TxData>180),180)-180);  %Puts all longitudes in -180 to 180 format
TxNames = Transmitters1(:,7);

%% Receivers
Receivers1 = {
%% HAIL AND SID SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
 37.62      -104.82      2.1125      -52.56       232.07   1    'WS'   %  Walsenburg, Colorado, USA
 39.9719    -105.1844    2.3116      -54.614      229.45   1    'BD'   %  Boulder, Colorado, USA
 35.6       -105.22      1.9579      -50.431      233.32   1    'LV'    %  Las Vegas, New Mexico, USA
 41.13      -104.8       2.4294      -55.855      228.91   1    'CH'   %  Cheyenne, Wyoming, USA
 39.52      -104.77      2.2737      -54.387      230.44   1    'PK'   %  Parker, Colorado, USA
 40.49      -85.5        2.5897      -63.651      257.41   1    'TA'    %  Taylor University, Indiana, USA
 37.4       -122.15      1.856       -44.765      215.75   1    'ST'   %  Stanford, California, USA
 42.3       -71.06       2.6749      -67.481      291.12   1    'BO'    %  Boston, Massachusetts, USA
 37.12      -122.21      1.8388      -44.487      215.93   1    'SC'    %  Santa Cruz (Wilcox), USA
 37.84      -75.48       2.2817      -63.306      281.15   0    'WI'    %  Wallops Island, Virginia
 30.80      -81.55       0.0000      -00.000      000.00   0    'KB'    %  Kings Bay, Florida
 29.60      -82.35       0.0000      -00.000      000.00   0    'GA'    %  Gainesville, Florida
 44.81      -106.98      2.8009      -58.056      223.17   0    'SH'    %  Sheridan, Wyoming
 47.52      -111.32      3.0717      -58.369      215.80   0    'GF'    %  Great Falls, Montana
%  32.40      -100.76      0.0000      -00.000      000.00   0    'U1'    %  Unknown Future 2
%  26.23      -97.721      0.0000      -00.000      000.00   0    'U2'    %  Unknown Future 1
 18.3472	-66.7542     0.0000      -00.000      000.00   0    'AO'    %  Arecibo, Puerto Rico

%% former HAIL sites
% 40.54      -105.04            0            0            0  0    'FC'    % Fort Collins, CO
% 40.30      -105.09            0            0            0  0    'BT'    % Berthoud, CO
% 39.69      -105.14            0            0            0  0    'LT'    % Littleton/Green Mountain, CO
% 38.97      -104.85            0            0            0  0    'CS'    % Colorado Springs, CO
% 38.72       -104.7            0            0            0  0    'FT'    % Fountain, CO
% 38.31      -104.58            0            0            0  0    'PB'    % Pueblo, CO
% 37.93      -104.93            0            0            0  0    'RY'    % Rye, CO
% 37.17      -104.51            0            0            0  0    'TR'    % Trinidad, CO

%% ALASKA SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
 62.613     -144.62      4.977       -56.677      173.92   0    'CHI'    %  Chistochina, Alaska, USA
 63.66      -144.06      5.4096      -57.613      172.8    0    'DLA'    %  Dot Lake, Alaska, USA
 62.39      -145.15      4.8646      -56.347      173.96   1    'HRP'    %  HAARP Array, Alaska, USA
 63.66      -148.81      5.066       -56.166      170.66   0    'HEA'    %  Healy, Alaska, USA
 59.68      -151.41      3.7838      -52.332      173.98   0    'HOM'    %  Homer, Alaska, USA
 58.59      -134.9       4.3495      -57.089      184.13   1    'JUN'    %  Juneau, Alaska, USA
 55.3       -131.53      3.7246      -55.918      190.32   0    'KET'    %  Ketchikan, Alaska, USA
 57.87      -152.88      3.3585      -50.447      175.02   1    'KOD'    %  Kodiak, Alaska, USA
 61.06      -146.02      4.3938      -55.08       175.18   0    'VAL'    %  Valdez, Alaska, USA
 59.55      -139.72      4.3311      -56.055      180.28   0    'YAK'    %  Yakutat, Alaska, USA
 62.362     -145.17           0            0           0   0    'GK'     % Gakona,
% Fort Yukon

%% PACIFIC/OCEANIA SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
%  21.96      -159.67      1.1595      -20.083      192.43   1    'WM  '    %  Waimea, Hawaii, USA
 8.7195      167.7184    1.0033      -15.084      186.25   1    'KA  '    %  Kwajalein Atoll
 28.21      -177.38      1.2134      -20.018      174.19   1    'MI  '    %  Midway Atol
%  23.87      -166.28      1.1738      -19.893      185.55   1    'TI  '    %  Tern Island, Pacific
-31.95       115.87      1.9501       49.259      113.99   1    'PAU '   %  Perth, Australia
-34.62       138.46      2.0642       52.129      142.56   1    'AAU '   %  Adelaide, Australia
-42.87       147.32      2.8909       59.906      159.98   1    'HOB '    %  Hobart, Tasmania
-53.18      -70.9        1.634        27.556      286.98   1    'USAP'  %  USAP Nathaniel B. Palmer, Pacific (docked at Punta Arenas)
-41.23       174.77      2.1836       50.148      196.55   1    'NIWA'  %  NIWA Tangaroa, Pacific (docked at Wellington, NZ)
 
%% PALMER RELATED SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
-64.77      -64.05       2.3907       39.764      290.53   1    'PA'    %  Palmer Station, Antarctica
-65.25      -64.27       2.4376       40.289      290.48   0    'FV'    %  Faraday/Vernadsky Station, Antarctica
-67.57      -68.13       2.6829       42.347      289.23   0    'RS'    %  Rothera Station, Antarctica
 32.33      -64.75       1.7688      -54.242      307.91   1    'BE'    %  Bermuda
-7.922      -15.425      1.1112       25.518      336.94   1    'AI'    %  Ascension Island
 
%% EUROPEAN/AFRICAN SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
 47.38       2.19        1.8677      -34.868     15.966    0    'NFR'   %  Nancay, France
 38.6815     39.203      1.4303      -23.148     43.254    1    'ETK'   %  Elazig, Turkey
 35.3069     25.0819     1.3122      -17.3       28.872    0    'ICR'   %  Iraklio, Crete
 30.86       34.78       1.2101      -13.725     35.5      1    'SBI'   %  Sde Boker, Israel
 32.1108     34.8065     0.0000      -00.000      000.00   0    'TEL'   %  Tel Aviv, Israel
 36.8342     10.1437     1.3374      -17.542     14.481    0    'TTU'   %  Tunis, Tunisia
 53.3436    -6.2521      2.4914      -48.045     17.915    1    'BIR'   %  Dublin, Ireland
 33.9993    -6.8515      1.257       -14.896     0.40043   1    'RMO'   %  Rabat, Morocco
 36.83       3.1         1.3369      -17.561     8.8681    0    'AAL'   %  Algiers, Algeria
 27.0343     14.4417     1.1146      -6.0521     15.45     1    'SLI'   %  Sebha, Libya
 32.11       20.03       1.2173      -12.631     21.444    1    'BLI'   %  Banghazi, Libya
 31.13       29.58       1.2075      -13.006     30.251    1    'AEG'   %  Alexandria, Egypt
 7.15        5.05        1            7.1499     5.0501    1    'ANI'   %  Akure, Nigeria
 52.1155     21.2376     2.2319      -41.195     37.406    1    'SPO'   %  Swider, Poland
 30.1		 31.2        0.0000      -00.000      000.00   0    'CAI'    %  Cairo, Egypt
 40.7823     48.5998     0.0000      -00.000      000.00   0    'AZB'    %  Baku, Azerbaijan
 9.0		 38.8        0.0000      -00.000      000.00   0    'AAE'    %  Addis Ababa, Ethopia
 NaN		 NaN         0.0000      -00.000      000.00   0    'AUS'    %  Gratz, Austria
 NaN		 NaN         0.0000      -00.000      000.00   0    'SA1'    %  Kwazulu-Natal, South Africa
 NaN		 NaN         0.0000      -00.000      000.00   0    'SA1'    %  Sharjah, UAE
 44.8553     20.3909     0.0000      -00.000      000.00   0    'BEL'    %  Belgrade, Serbia
 41.3257     69.2961     0.0000      -00.000      000.00   0    'TSK'    %  Tashkent, Uzbekistan


 
%% ASIA SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
 38.37       140.76      1.3741      -21.667      138.88   1    'SJA'   %  Sendai, Japan
 22.4634     88.3867     1.0801      -5.8356      89.34    1    'CIN'   %  Kolkata, India
 25.45       81.85       1.1202      -8.935       83.504   1    'AIN'   %  Allahabad, India
 29.5        78          1.1926      -13.181      80.498   1    'NIN'   %  Nainital, India
 25.2        83          1.1158      -8.6226      84.549   1    'VIN'   %  Varanasi, India
 
%% NEW SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
-49.3522     70.2564     3.70         62.38        47.48   0    'KI'    %  Kerguelen Island, TAAF
-37.7962     77.5504     2.34         53.02        66.33   0    'AM'    %  Amsterdam Island, TAAF
 
%% Possible China Sites:
22.01       100.80      0.00         00.00        0.000   0    'CH1'    %  Heihe, China
25.10       102.70      0.00         00.00        0.000   0    'CH2'    %  Kunming, China
29.55       106.55      0.00         00.00        0.000   0    'CH3'    %  Chongging, China
30.54       114.36      0.00         00.00        0.000   0    'CH4'    %  Wuhan, China
36.67       117.00      0.00         00.00        0.000   0    'CH5'    %  Jinan, China
41.83       123.42      0.00         00.00        0.000   0    'CH6'    %  Shenyang, China
45.97       126.65      0.00         00.00        0.000   0    'CH7'    %  Haerbin, China
50.25       127.50      0.00         00.00        0.000   0    'CH8'    %  Heihe, China

47.83        88.14      0.00         00.00        0.000   0    'CH9'    %  NW Site, China
43.60        93.01      0.00         00.00        0.000   0    'CH0'    %  NW-Central, China
40.76       107.43      0.00         00.00        0.000   0    'CHa'    %  Linhe, China
43.94       116.09      0.00         00.00        0.000   0    'CHb'    %  Xilinhot, China
52.97       122.54      0.00         00.00        0.000   0    'CHc'    %  NE Site, China


% 45.99       126.72      0.00         00.00        0.000   0    'CH1'    %  Haerbin, China
% 41.97       121.86      0.00         00.00        0.000   0    'CH2'    %  Shenyang, China
% 36.67       117.00      0.00         00.00        0.000   0    'CH3'    %  Jinan, China
% 31.39       110.13      0.00         00.00        0.000   0    'CH4'    %  Chongging, China
% 25.10       102.70      0.00         00.00        0.000   0    'CH5'    %  Kunming, China
% 
% 50.25       127.50      0.00         00.00        0.000   0    'CH6'    %  Heihe, China
% 22.01       100.80      0.00         00.00        0.000   0    'CH7'    %  Heihe, China
% 34.26       108.94      0.00         00.00        0.000   0    'CH8'    %  Heihe, China



%NONEXISTANT (HYPOTHETICAL) SITES
%Lat        Lon          L-shell     ConjLat     ConjLon   ?    Abbrev  %  Name, Location
%  24.6       -81.8        0.0000       00.000      000.00   0    'KWF'   %  Key West, Florida
%  42.72      -73.75       0.0000      -00.000      000.00   0    'SNA'   %  Siena College, New York
};

%%
RxData  = cell2mat(Receivers1(:,1:6));
RxData(RxData>180) = (rem(RxData(RxData>180),180)-180);  %Puts all longitudes in -180 to 180 format
RxNames = Receivers1(:,7);

%% Rearrange as a dictionary
[NRcvr,Ndat]=size(Receivers1);
Rcvrs=cell(1,2*NRcvr);
Rcvrs(1:2:end)=Receivers1(:,7);
Rcvrs(2:2:end)=mat2cell(cell2mat(Receivers1(:,1:Ndat-1)),ones(1,NRcvr),[Ndat-1]);
% Now we can use tmp=getvaluefromdict(Xmtrs,'NML',[]) etc.
