function [f,missing_arg]=gradual_profile(varargin)
%GRADUAL_PROFILE Sine-like gradual change profile
% Usage:
%    f=gradual_profile(z,options)
% Options:
%    'zmax' - where the profile flattens out
%    'dfdz' - slope at zero
%    'df'   - total change in f
% Exactly 2 out of these 3 are to be given. The third may be returned as
% the optional second output,e.g.:
%    [f,dfdz]=gradual_profile(z,'zmax',1,'df',1);
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.

[z,options]=parsearguments(varargin,1,{'zmax','dfdz','df'});
if length(options)~=4
    error('Exactly 2 out of 3 options are to be given.');
end
zmax=getvaluefromdict(options,'zmax',[]);
dfdz=getvaluefromdict(options,'dfdz',[]);
df=getvaluefromdict(options,'df',[]);
if isempty(dfdz)
    dfdz=df*pi/4/zmax;
    missing_arg=dfdz;
elseif isempty(df)
    df=4*dfdz*zmax/pi;
    missing_arg=df;
else
    zmax=df*pi/4/dfdz;
    missing_arg=zmax;
end
f=zeros(size(z));
f(abs(z)<=zmax)=(df/2)*sin(pi*z(abs(z)<=zmax)/2/zmax);
f(z>zmax)=df/2;
f(z<-zmax)=-df/2;
