function [x,err]=solve_quartic(a0,b0,c0,d0,e0)
%SOLVE_QUARTIC Solve a*x^4+b*x^3+c*x^2+d*x+e=0
% function [x,err]=solve_quartic(a,b,c,d,e)
% Can take 1D (column) arrays a,b,c,d,e.
% See Wikipedia.
%
% BSD License:
% Copyright (c) 2010, Nikolai G. Lehtinen (Email: nlehtinen at gmail.com)
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
% POSSIBILITY OF SUCH DAMAGE.

% Extensive error checking in the beginning
if any(a0==0)
    error('a==0');
end
a0=a0(:); b0=b0(:); c0=c0(:); d0=d0(:); e0=e0(:);
lengths=[length(a0),length(b0),length(c0),length(d0),length(e0)];
n=max(lengths);
if n>1
    if length(a0)==1
        a0=repmat(a0,n,1);
    end
    if length(b0)==1
        b0=repmat(b0,n,1);
    end
    if length(c0)==1
        c0=repmat(c0,n,1);
    end
    if length(d0)==1
        d0=repmat(d0,n,1);
    end
    if length(e0)==1
        e0=repmat(e0,n,1);
    end
end
lengths=[length(a0),length(b0),length(c0),length(d0),length(e0)];
if any(lengths~=n)
    error('wrong lengths');
end

% Form with a=1
b=b0./a0; c=c0./a0; d=d0./a0; e=e0./a0;

% Depressed form u^4+alfa*u^2+beta*u+gamma=0
% x=u-b/4
alpha = -3*b.^2/8 + c;
beta  =  b.^3/8 - b.*c/2 + d;
gamma = -3*b.^4/256 + c.*b.^2/16 - b.*d/4 + e;
[u,err]=solve_depressed_quartic(alpha,beta,gamma);
x=u-repmat(b/4,1,4);

if any(~isfinite(x))
    save solve_quartic_dump a0 b0 c0 d0 e0
    error('failed to calculate x')
end
