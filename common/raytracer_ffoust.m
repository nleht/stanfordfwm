function [rtraj,ntraj,ttraj,FFtraj]=raytracer_ffoust(varargin)
%RAYTRACER_FFOUST Simple 2nd order raytracer based on F. Foust's algorithm
% This function can only calculate the ray trajectory, not the energy flux
% or losses.
% Usage:
%    [rtraj,ntraj,ttraj,FFtraj]=raytracer_ffoust(@fun_perm,r0,n0[,options);
% Various conventions:
%    time is in length units (=c*t)
%    refractive index vector n=k/(w/c)
% Inputs:
%    fun_perm(dw/w,r) -- permittivity tensor at frequency w+dw and
%       position r, should be hermitian (lossless)
%    r0,n0 (3 x 1) - initial values of the position vector and refractive
%       index vector
% Options are give as key-value pairs, key is a string and the value is the
% argument right after it.
% Options:
%    'regularizer' -- a function of (dw/w,r), to be multiplied into the
%       permittivity to make it regular (e.g., 1-Y); default=1
%    'num_steps' -- number of time steps; default=15000
%    'dt' -- initial time step (negative if using adaptive time stepping),
%       default=-1e-2
%    'dr','dn','dw' -- absolute values of small variations in position
%       vector, refractive index vector and the relative variation in
%       frequency (i.e., dw=delta w/w), used to calculate derivatives
%       d/dr, d/dn, d/dw; defaults are = 1e-5. Note that if the typical
%       scale of medium variation is L, one should take approximately
%       dr=1e-5*L
%    'error_target' -- value which |dn/dt|/|n|*dt should not exceed, used
%       in adaptive time-stepping; default=1e-3
%    'step_multiplier' -- coefficient by which the time step dt should be
%       divided if it is too large, in adaptive time-stepping; default=2
%    'debug' -- set to 0 to suppress output; default=1
% Author: Nikolai G. Lehtinen, algorithm author: Forrest Foust.
% See also: raytracer_ffoust_example_XltY, raytracer_ffoust_example_XgtY

%% Parse arguments
keys={'regularizer','num_steps','dt','dr','dn','dw','error_target','step_multiplier','debug'};
[fun_perm,r0,n0,options]=parsearguments(varargin,3,keys);
% Regularizing function. Default is no regularization
fun_regularizer=getvaluefromdict(options,'regularizer',@(dw1,r1) ones(1,size(r1,2)));
debugflag=getvaluefromdict(options,'debug',1);
Ns=getvaluefromdict(options,'num_steps',15000);
dt=getvaluefromdict(options,'dt',-1e-2);
drperp=getvaluefromdict(options,'dr',1e-5);
dnperp=getvaluefromdict(options,'dn',1e-5);
dw=getvaluefromdict(options,'dw',1e-5);
if dt<0
    adaptive_dt=1;
    dt=-dt;
    dt0=dt;
else
    adaptive_dt=0;
end
if adaptive_dt
    errtarget=getvaluefromdict(options,'error_target',1e-3);
    coef=getvaluefromdict(options,'step_multiplier',2);
end

%% Check arguments
if Ns<1
    error('must have at least one step')
end
if adaptive_dt
    need_FF=(nargout>3);
else
    need_FF=(nargout>2);
end
small_values=[dt dw drperp dnperp];
if any(small_values<=0)
    error('dt dw drperp dnperp < 0');
end

%% Initial conditions
rcur=r0;
ncur=n0;
wu=1+dw/2; wd=1-dw/2;
ntraj=nan(3,Ns+1);
rtraj=ntraj;
if need_FF
    FFtraj=nan(8,Ns+1);
end
dr0=drperp/2*[0 -1 1 0 0 0 0;0 0 0 -1 1 0 0;0 0 0 0 0 -1 1];
ntraj(:,1)=n0; rtraj(:,1)=r0;
t=0;
if adaptive_dt
    ttraj=nan(1,Ns+1);
    ttraj(1)=0;
end
if need_FF
    FFtraj(1)=0;
end

%% The cycle
Ux=kron([0 -1;1 0],[0 0 0;0 0 -1;0 1 0]);
Uy=kron([0 -1;1 0],[0 0 1;0 0 0;-1 0 0]);
Uz=kron([0 -1;1 0],[0 -1 0;1 0 0;0 0 0]);
%F=det(n(1)*Ux+n(2)*Uy+n(3)*Uz-[perm zeros(3); zeros(3) eye(3)]);
% NOTE: det(a*Ux+b*Uy+c*Uz)==0 for any a,b,c!
extras={fun_perm,fun_regularizer,Ux,Uy,Uz,dr0,drperp,dnperp,dw,wu,wd};
for ks=1:Ns
    if debugflag>0 && 500*floor(ks/500)==ks
        disp(['Step ' num2str(ks) '/' num2str(Ns)]);
    end
    %% Step 1: Derivatives of Foust function F at the start of interval
    [FF0,dFFdr,dFFdk,dFFdw]=ffoust_raytracer_aux(ncur,rcur,extras);
    if isnan(FF0)
        if debugflag>0
            disp(['Out of the medium; exiting at time step ' num2str(ks)]);
        end
        break
    end
    
    %% Derivatives (estimates)
    dndt=real(dFFdr/dFFdw); drdt=real(-dFFdk/dFFdw);
    
    %% Find the adaptive time step
    if adaptive_dt
        while 1
            dndterr=sqrt(sum(abs(dndt).^2)/sum(abs(ncur).^2));
            err=dndterr*dt;
            if err<errtarget/coef^2 && dt*coef<=dt0
                dt=dt*coef;
                if debugflag>0
                    disp(['|dn/dt|/|n|=' num2str(dndterr) ', increasing time step to ' num2str(dt)]);
                end
            elseif err<errtarget
                break
            else
                dt=dt/coef;
                if debugflag>0
                    disp(['|dn/dt|/|n|=' num2str(dndterr) ', decreasing time step to ' num2str(dt)]);
                end
            end
        end
    end
    
    %% Correct for the change in FF
    % Must be done only ONCE, at the beginning of the interval
    tmp=real(FF0)/sum(real(dFFdk).^2);
    ncur=ncur-tmp*real(dFFdk);

    %% Step 1 Update
    ncur1=ncur+dt*dndt/2;
    rcur1=rcur+dt*drdt/2;
    
    %% Rinse-repeat for the intermediate point
    [FF0,dFFdr,dFFdk,dFFdw]=ffoust_raytracer_aux(ncur1,rcur1,extras);
    % Note that here we Do Not Correct for the change in FF
    
    %% The second order (more accurate) Derivatives
    dndt=real(dFFdr/dFFdw); drdt=real(-dFFdk/dFFdw);
    
    %% Step 2 Update
    ncur=ncur+dt*dndt;
    rcur=rcur+dt*drdt;
    t=t+dt;
    
    %% Save trajectory
    rtraj(:,ks+1)=rcur;
    ntraj(:,ks+1)=ncur;
    if adaptive_dt
        ttraj(:,ks+1)=t;
    end
    if need_FF
        FFtraj(1,ks+1)=FF0;
        FFtraj(2,ks+1)=dFFdw;
        FFtraj(3:5,ks+1)=dFFdr;
        FFtraj(6:8,ks+1)=dFFdk;
    end
end

if ~adaptive_dt
    ttraj=FFtraj;
end

return

%% Auxiliary function

function [FF0,dFFdr,dFFdk,dFFdw]=ffoust_raytracer_aux(nc,rc,extras)
% extras={fun_perm,fun_regularizer,Ux,Uy,Uz,dr0,drperp,dnperp,dw}
%%
[fun_perm,fun_regularizer,Ux,Uy,Uz,dr0,drperp,dnperp,dw,wu,wd]=deal(extras{:});
% Calculate the spatial derivatives
nU=nc(1)*Ux+nc(2)*Uy+nc(3)*Uz;
permvic=fun_perm(0,repmat(rc,[1 7])+dr0);
if any(isnan(permvic(:)))
    FF0=nan; dFFdr=nan; dFFdk=nan; dFFdw=nan;
    return
end
regvic=fun_regularizer(0,repmat(rc,[1 7])+dr0);
FFvic=zeros(1,6);
for k=1:7
    M=nU-[permvic(:,:,k) zeros(3); zeros(3) eye(3)];
    FF=det(M)*regvic(k);
    if k==1
        M0=M;
        FF0=FF;
    else
        FFvic(k-1)=FF;
    end
end
dFFdr=[FFvic(2)-FFvic(1); FFvic(4)-FFvic(3); FFvic(6)-FFvic(5)]/drperp;
% Calculate the k-derivatives, use w=1
FFvar=zeros(1,6);
FFvar(1)=det(M0-dnperp/2*Ux);
FFvar(2)=det(M0+dnperp/2*Ux);
FFvar(3)=det(M0-dnperp/2*Uy);
FFvar(4)=det(M0+dnperp/2*Uy);
FFvar(5)=det(M0-dnperp/2*Uz);
FFvar(6)=det(M0+dnperp/2*Uz);
FFvar=FFvar*regvic(1);
dFFdk=[FFvar(2)-FFvar(1); FFvar(4)-FFvar(3); FFvar(6)-FFvar(5)]/dnperp;
% Calculate the w-derivative
% Use n=k/(w/c)=k/w in our units
FFu=det(nU/wu-[fun_perm(dw/2,rc) zeros(3); zeros(3) eye(3)])*fun_regularizer(dw/2,rc);
FFd=det(nU/wd-[fun_perm(-dw/2,rc) zeros(3); zeros(3) eye(3)])*fun_regularizer(-dw/2,rc);
dFFdw=(FFu-FFd)/dw;



