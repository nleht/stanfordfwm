function progress_disp(flag,sfirst,done,sadd)
%PROGRESS_DISP Display the progress of a long cycle.
% Usage:
%    % Resetting:
%    progress_disp(0,output_interval,program_name)
%    for k=1:N
%        ...
%        % Output occurs every "output_interval" seconds
%        progress_disp(1,'This is displayed only once in the beginning',...
%           fraction_done,'some additional information on the next line');
%    end
persistent tstart toutput output_interval first_output program_name
if flag==0
    tstart=now*24*3600; toutput=tstart; first_output=1;
    output_interval=sfirst;
    program_name=done;
else
    timec=now*24*3600; ttot=timec-tstart;
    if timec-toutput>output_interval
        toutput=timec;
        if ~isempty(sfirst) && first_output
            disp([program_name ': ' sfirst]);
            first_output=0;
        end
        disp([program_name ': Done=' num2str(done*100) '%; Time=' hms(ttot) ', ETA=' hms(ttot*(1/done-1))]);
        if nargin<4
            sadd=[];
        end
        if ~isempty(sadd)
            disp([program_name ': ' sadd]);
        end
    end
end
