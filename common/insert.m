function b=insert(a,ins,pos)
%INSERT Insert elements into 1D array "a" after position "pos"
ni=numel(ins);
nnew=numel(a)+ni;
if size(a,1)==1
    b=zeros(1,nnew);
elseif size(a,2)==1
    b=zeros(nnew,1);
else
    error('a must be 1D array');
end
% now fill in your values
b(1:pos) = a(1:pos);
b(pos+1:pos+ni) = ins(:);
b(pos+ni+1:end) = a(pos+1:end);
