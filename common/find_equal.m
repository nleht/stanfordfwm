function [donei,done,notdone,isdone,index]=find_equal(xi,x,err)
%FIND_EQUAL Find indeces of equal values in two arrays
% Usage:
%    % Pre-calculate fi=F(xi)
%    % Now, calculate f=F(x) in the fastest way avoiding duplications
%    [donei,done,notdone,isdone]=find_equal(xi,x);
%    f(done)=fi(donei);
%    for k=notdone
%        % Calculate f(k)=F(x(k))
%        isdone(k)=1;
%    end
% A wish for a future version: xi={x1i,x2i} and x={x1,x2} (2D table expansion)
% Author: Nikolai G. Lehtinen
if nargin<3
    err=0;
end
if err<0
    err=abs(err);
    disp('FIND_EQUAL: WARNING: err changed to positive');
end
index=zeros(size(x));
for k=1:length(x)
    if err==0
        ii=find(x(k)==xi);
    else
        ii=find(abs(x(k)-xi)<=err);
    end
    if ~isempty(ii)
        index(k)=ii(1);
    end
end
isdone=(index~=0);
done=find(isdone);
donei=index(done);
notdone=find(~isdone);
