function res=newcoefspp(pp,xx,neworder)
% New coefficients at shifted points, also extend order (pad with zeros)

%  obtain the row vector xs equivalent to XX
sizexx = size(xx); lx = numel(xx); xs = reshape(xx,1,lx);
%  if XX is row vector, suppress its first dimension
if length(sizexx)==2&&sizexx(1)==1, sizexx(1) = []; end

[breaks,coefs,pieces,order,dim] = unmkpp(pp);
% coefs have size prod(dim)*pieces x order

if lx
    [ignored,index] = histc(xs,[-inf,breaks(2:pieces),inf]);
else
    index = ones(1,lx);
end

% now go to local coordinates ...
xs = xs-breaks(index);

d = prod(dim);
if d>1 % ... replicate xs and index in case PP is vector-valued ...
   xs = reshape(xs(ones(d,1),:),1,d*lx);
   index = d*index; temp = (-d:-1).';
   index = reshape(1+index(ones(d,1),:)+temp(:,ones(1,lx)), d*lx, 1 );
else
   if length(sizexx)>1, dd = []; else dd = 1; end
end

% ... and apply nested multiplication:
if nargin<2
    neworder=order;
end
res=zeros(prod(dim)*lx,neworder);
for nder=1:order
    v = coefs(index,1);
    for i=2:order-nder+1
        v = xs(:).*v + coefs(index,i);
    end
    res(:,neworder-nder+1)=v;
    coefs=repmat(order-nder:-1:1,prod(dim)*pieces,1).*coefs(:,1:order-nder)/nder;
end

