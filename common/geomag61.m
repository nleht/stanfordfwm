function [x,y,z,d,i,h,f,xdot,ydot,zdot,ddot,idot,hdot,fdot]=...
    geomag61(mdfile,modelinfo,sdate,alt,latitude,longitude)
%GEOMAG61 MATLAB implementation of Geomag 6.1 driver
% NGDC's Geomagnetic Field Modeling software for the IGRF and WMM
% http://www.ngdc.noaa.gov/IAGA/vmod/igrf.html
% Usage:
%    mdfile=['/home/nleht/Documents/MATLAB/common/inputs/' ...
%       'Geomag61_unix_Feb_20_2008/IGRF10.unx']
%    modelinfo=geomag61_read_modelinfo(mdfile);
%    [x,y,z,d,i,h,f,xdot,ydot,zdot,ddot,idot,hdot,fdot]=...
%        geomag61(mdfile,modelinfo,sdate,alt,latitude,longitude)
% Input:
%    sdate - decimal year (use geomag61_julday to convert from month/day)
%    alt   - altitude in km (usually -1 to 600 km)
%    latitude, longitude - in degrees, in intervals (-90,90), (-180,180)
% Output:
%    Geomagnetic field (intensities are in nT):
%    x  - northward component
%    y  - eastward component
%    z  - vertically-downward component
%    d  - declination (degrees)
%    i  - inclination (degrees)
%    h  - horizontal intensity
%    f  - total intensity
%    xdot,ydot,zdot,ddot,idot,hdot,fdot -
%       - geomagnetic field derivatives (in nT/yr and min/yr)
% Based on "main()" in geomag61.c
% MATLAB implementation
%    Nikolai G. Lehtinen, Stanford University, September 14, 2009


% Lines 996-1064 from geomag61.c

% This will compute everything needed for 1 point in time.
        
% Get modelI
nmodel=length(modelinfo);
minyr=min([modelinfo.yrmin]);
maxyr=max([modelinfo.yrmax]);
if sdate<minyr || sdate>maxyr+1
    fprintf(2,'\nError: The date %4.2f is out of range.\n', sdate)
    return
end
if ((sdate>maxyr)&&(sdate<=maxyr+1))
    fprintf(1,'\nWarning: The date %4.2f is out of range,\n', sdate);
    fprintf(1,'         but still within one year of model expiration date.\n');
    fprintf(1,'         An updated model file is available before 1.1.%4.0f\n', maxyr);
end

% Pick model
for modelI=1:nmodel
    if (sdate<modelinfo(modelI).yrmax)
        break;
    end
end
if modelI == nmodel + 1 
    modelI=modelI-1; % if beyond end of last model use last model
end
      
% Get altitude min and max for selected model.
minalt=modelinfo(modelI).altmin;
maxalt=modelinfo(modelI).altmax;

if (alt<minalt)||(alt>maxalt)
    fprintf(2,'\nError: Altitude %4.2f is out of range.\n', alt)
    return
end

if ((latitude<-90)||(latitude>90))
    fprintf(2,'\nError: Latitude %4.2f is out of range.\n', latitude)
    return
end
if ((longitude<-180)||(longitude>180))
    fprintf(2,'\nError: Longitude %4.2f is out of range.\n', longitude)
    return
end

if (modelinfo(modelI).max2 == 0)
    gh1=geomag61_getshc(mdfile, 1, modelinfo(modelI).irec_pos, modelinfo(modelI).max1);
    gh2=geomag61_getshc(mdfile, 1, modelinfo(modelI+1).irec_pos, modelinfo(modelI+1).max1);
    [gha,nmax] = geomag61_interpsh(sdate, modelinfo(modelI).yrmin, modelinfo(modelI).max1, gh1, ...
        modelinfo(modelI+1).yrmin, modelinfo(modelI+1).max1,gh2);
    [ghb,nmax] = geomag61_interpsh(sdate+1, modelinfo(modelI).yrmin, modelinfo(modelI).max1,gh1, ...
        modelinfo(modelI+1).yrmin, modelinfo(modelI+1).max1,gh2);
else
    gh1=geomag61_getshc(mdfile, 1, modelinfo(modelI).irec_pos, modelinfo(modelI).max1);
    gh2=geomag61_getshc(mdfile, 0, modelinfo(modelI).irec_pos, modelinfo(modelI).max2);
    [gha,nmax] = geomag61_extrapsh(sdate, modelinfo(modelI).epoch, modelinfo(modelI).max1, gh1, modelinfo(modelI).max2, gh2);
    [ghb,nmax] = geomag61_extrapsh(sdate+1, modelinfo(modelI).epoch, modelinfo(modelI).max1, gh1, modelinfo(modelI).max2, gh2);
end
        
        
% Do the first calculations
igdgc=1; % geodetic
[x,y,z]=geomag61_shval3(igdgc, latitude, longitude, alt, nmax,0,0,0,0,gha);
[d,i,h,f]=geomag61_dihf(x,y,z);
% New: L-shell


% At sdate + 1 year
[xtemp,ytemp,ztemp]=geomag61_shval3(igdgc, latitude, longitude, alt, nmax,0,0,0,0,ghb);
[dtemp,itemp,htemp,ftemp]=geomag61_dihf(xtemp,ytemp,ztemp);
        
% Time derivative (over 1 year)
ddot = ((dtemp - d)*57.29578);
if (ddot > 180.0)
    ddot = ddot - 360.0;
end
if (ddot <= -180.0)
    ddot = ddot + 360.0;
end
ddot = ddot * 60.0;
        
idot = ((itemp - i)*57.29578)*60;
d = d*(57.29578);   i = i*(57.29578);
hdot = htemp - h;   xdot = xtemp - x;
ydot = ytemp - y;   zdot = ztemp - z;
fdot = ftemp - f;
        
% deal with geographic and magnetic poles */
        
if (h < 100.0) % at magnetic poles */
    d = NaN;
    ddot = NaN;
    % while rest is ok */
end
        
if (h < 1000.0)
    warn_H = 0;
    warn_H_strong = 1;
    if (h<warn_H_strong_val) warn_H_strong_val = h;
    end
elseif (h < 5000.0 && ~warn_H_strong)
    warn_H = 1;
    if (h<warn_H_val) warn_H_val = h; end
end
        
if (90.0-abs(latitude) <= 0.001) % at geographic poles */
    x = NaN;
    y = NaN;
    d = NaN;
    xdot = NaN;
    ydot = NaN;
    ddot = NaN;
    warn_P = 1;
    warn_H = 0;
    warn_H_strong = 0;
    % while rest is ok */
end
        
% Above will compute everything for 1 point in time.
