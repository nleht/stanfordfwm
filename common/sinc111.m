function res=sinc111(x,y,z)
%SINC111 Used in third term of Dyson series for product of three terms ~t
% Definition is different from SINCMN, this integral is unscaled:
% sinc111(x,y,z)=...
%     \int_{-1}^1 t1 dt1 exp(i*x*t1) ...
%        \int_{-1}^t1 t2 dt2 exp(i*y*t2) \int_{-1}^t2 t3 dt3 exp(i*z*t3)
% Tour de force of writing and debugging this function is completed.
%
% See also: SINCN, SINCN0, SINCMN

%% Check sizes
if any(size(x)~=size(y)) || any(size(x)~=size(z))
    error('size');
end

%% Special cases
iz0=(z==0);
iy0=(y==0);
iyz0=(y+z==0);

%% Setup
res=zeros(size(x));

if 0
%% z==0 and y==0
xtmp=x(iz0 & iy0);
res(iz0 & iy0)=8*sincn(5,xtmp)-4*sincn(3,xtmp)+0.5*sincn(1,xtmp);

%% z==0 but y~=0
xtmp=x(iz0 & ~iy0);
ytmp=y(iz0 & ~iy0);
xy=xtmp+ytmp;
res(iz0 & ~iy0)=2*sincn(1,xtmp).*(sincn0(1,-ytmp,0)./ytmp.^2-sincn0(3,-ytmp,0)./ytmp.^4)+...
    (-2*sincn(1,xy)+4i*ytmp.*sincn(2,xy)).*(6./ytmp.^4+1./ytmp.^2)...
    +24*sincn(3,xy)./ytmp.^2-16i*sincn(4,xy)./ytmp;
else
    x0=x(iz0); y0=y(iz0);
    res(iz0)=32*sincmn(1,3,x0,y0)-8*sincmn(1,1,x0,y0);
end
%% z~=0
xs=x(~iz0); ys=y(~iz0); zs=z(~iz0);
A=32*sincmn(1,2,xs,ys+zs);
B=16*sincmn(1,1,xs,ys+zs);
C=16*sincmn(1,1,xs,ys);
% Collect them
res(~iz0)=-1i*A./zs+B./zs.^2-C.*sincn0(1,-zs,0)./zs.^2;
