function [nen,ene,den,deni,Ie,De,numome,Ael,Arot,Aie,Ase,Aatt,Aatt3,Aion,Div]=...
    enuseg_a_collisional(en,T0,species,composition,debugflag)
%ENUSEG_A_COLLISIONAL Collisional part of A matrix in ENUSEG
%
% This function is part of
% ENUSEG - ELENDIF with Non-Uniformly Spaced Energy Grid
%
% The electron distribution function is represented by a 1D array ne(nen,1)
% and is calculated at energies en(nen,1) (given in eV). The array "en" may
% me non-uniformly spaced, which is new compared to ELENDIF.
% The distribution function is normalized so that \int ne(en) d(en)=1.
%
% ENERGY GRID
%
% We make use of the following values which define the grid. Note that
% for the location of the grid points outside the interval under
% consideration, we assume en(0)==0 and en(nen+1)=en(nen)+den(nen).
%    ene(1,nen+1) - "extended" energy grid, at the center of each interval
%       [en(k),en(k+1)] where k=0..nen
%    den(1,nen+1) - length of each energy interval, i.e.
%       den(k)=en(k)-en(k-1), k=1..nen+1
%    deni(1,nen) - defined as deni(k)=ene(k+1)-ene(k) and is the length
%        energy interval around the value en(k). It is used, i.e., in
%        integration, so that sum(ne.*deni) represents \int ne(en) d(en)
%    enmax=en(nen)
%    enemax=ene(nen+1)
%
% FINITE DIFFERENCE SCHEME
%
% Various processes contributing to d(ne)/dt and linear in ne(en) are
% represented as operators Ax(nen,nen), i.e.
%    [d(ne)/dt]_x == Ax*ne at en(k)
% The processes that conserve the number of electrons are represented as
% divergences of fluxes Jxe(nen+1,nen), i.e.
%    Ax = -Div*Jxe = -diag(1./deni)*(Jxe(2:nen+1,:)-Jxe(1:nen,:)) 
% The fluxes are calculated at "extended" energies, i.e.
%    Jx = Jxe*ne at ene(k)
% We need Jxe(1,:)==0, Jxe(nen+1,:)==0, i.e. Jx(ene(1))==0, Jx(enemax)==0
% The most difficult to implement in this manner was the inelastic
% collision integral (see comments in the code). The conservation of the
% number of electrons can be checked by plotting (deni.'*Ax), which must be
% zero for such processes.
%
% USAGE:
%
%    weff=w-wH; % extraordinary wave
%    [nen,ene,den,deni,Ie,De,numome,Ael,Arot,Aie,Ase,Aatt,Aatt3,Aion] = ...
%        enuseg_a_collisional(en,T0,species,composition[,debugflag]);
%    [AE,shf]=enuseg_a_electric(nen,ene,den,deni,Ie,De,Nm*numome,weff);
%    A=Erms^2*AE+Nm*(Ael+Arot+Aie+Ase+Aatt+Aion+Aatt3*Nm);
%    % Electron distribution (thermal)
%    ne=sqrt(en).*exp(-en/T0)*sqrt(4/pi/T0^3);
%    ntot=sum(ne.*deni); % Must be 1, unless changed due to att/ion proc
%    sigma=Ne*sum(shf.*ne)/ntot;
%    % Attachment and ionization rate
%    nuatt=-Nm*sum((Aatt*ne).*deni)/ntot;
%    nuion=Nm*sum((Aion*ne).*deni)/ntot;
%    % Time step
%    ne1=inv(eye(nen)-dt*A)*ne;
%
% INPUTS AND OUTPUTS
%
% Inputs:
%    en - energy grid (a column vector)
%    T0 - ambient temperature in eV
% Optional inputs:
%    debugflag - default=0
%
% All physical quantities that are proportional to the atmosphere density
% are calculated at Nm == 1 m^{-3}.
% Outputs:
%    nen,ene,den,deni - various values for the energy grid, see above
%    Ie, De - extended identity and derivative operators (used in
%       ENUSEG_A_ELECTRIC)
%    numome - momentum loss rate
%    Acont, Aie, Ase - continuous energy loss, inelastic and superelastic
%       collision operators
%    Aatt, Aion - attachment and ionization operators
%
% Author: Nikolai G. Lehtinen
% See also: ENUSEG_A_ELECTRIC, ENUSEG_A

if nargin<5
    debugflag=0;
end

%% Inflexible settings (for debugging)
use_div=0; % default=0; for debugging only
use_ops=0; % default=0; for debugging only
int_method=2; % default=2;
nuiep_method=2; % default=2;

%% Correct the settings
if nargout==15
    use_div=1;
end
if debugflag<3
    use_ops=0; % Makes no sense to run extensive debugging with low debugflag
end

%% Setup
global ech me
if isempty(ech)
    loadconstants
end
nen=length(en);
enmax=en(nen);
den=[en(1) ; diff(en) ; en(nen)-en(nen-1)]; % den(k)=en(k)-en(k-1)
%deni=(den(1:nen)+den(2:nen+1))/2; % for integration, = diff(ene)
ene=([0 ; en]+[en ; enmax+den(nen+1)])/2; % extended energy grid
enemax=ene(nen+1);
deni=diff(ene);

%% Operators: derivative, identity, integration, divergence
% 1. Extended derivative (or gradient) matrix De(nen+1,nen)
% Argument f(k)=f(en(k)), assume f(0)=0 and f(nen+1)=0.
% De*f=d(f)/d(en) at ene(k)
De=eye(nen+1,nen);
De(2:nen+1,:)=De(2:nen+1,:)-eye(nen);
De=diag(1./den)*De;
% 2. Extended identity matrix (average of 2 points) Ie(nen+1,nen)
% Argument f(k)=f(en(k)), assume f(0)=0 and f(nen+1)=0.
% Ie*f=f at ene(k)
Ie=eye(nen+1,nen);
Ie(2:nen+1,:)=Ie(2:nen+1,:)+eye(nen);
Ie=Ie/2;
% 3. Extended integration matrix Int(nen+1,nen)
% Argument f(k)=f(en(k)), assume f(0)=0
% Intop*f = \int_0^ene f(enp) d(enp) at ene(k)
if use_ops
    Int=[zeros(1,nen);cumsum(diag(deni))];
end
% 4. Divergence matrix Div(nen,nen+1)
% Similar to derivative but only applied to fluxes
% Argument is J(k)=J(ene(k))
% Div*J=d(J)/d(en) at en(k)
if use_div
    % Use of this operator is inefficient, prefer not to use it.
    Div=-eye(nen,nen+1);
    Div(:,2:nen+1)=Div(:,2:nen+1)+eye(nen);
    Div=diag(1./deni)*Div;
end
% 5. Shift operators up*f=f(ene(k)+es) and down*f=f(ene(k)-es)
% where argument is taken at ene(k) are going to be introduced later, when
% the shift es is known

%% Velocity
% Remember that en is in eV!
veloc=sqrt(2*en*ech/me);

%% Collision (momentum xfer) rate, elastic and rotational energy loss rates
[numome,nuele,nurote]=getnumom(ene,species,composition);


%% Elastic and rotational continuous energy losses
% We use CAR (continuous approximation to rotation)
% Jen=Jene*ne=nuen*[(T0/2-en)*ne-T0*en*d(ne)/d(en)] at ene
tmp=(T0/2*eye(nen+1)-diag(ene))*Ie-T0*diag(ene)*De;
% Elastic
Jele=diag(nuele)*tmp;
Jele(1,:)=0; Jele(nen+1,:)=0; % flux at bounds
% Ael*ne=-d(Jel)/d(en) at en
Ael=-diag(1./deni)*(Jele(2:nen+1,:)-Jele(1:nen,:));
% Rotational
Jrote=diag(nurote)*tmp;
Jrote(1,:)=0; Jrote(nen+1,:)=0; % flux at bounds
% Ael*ne=-d(Jel)/d(en) at en
Arot=-diag(1./deni)*(Jrote(2:nen+1,:)-Jrote(1:nen,:));

%% Inelastic/superelastic, attachment, ionization
Aie=zeros(nen,nen);
Ase=zeros(nen,nen);
Aatt=zeros(nen,nen); Aatt3=zeros(nen,nen);
Aion=zeros(nen,nen);
if use_ops
    % Shift operators
    up=zeros(nen+1,nen+1); down=zeros(nen+1,nen+1);
    up1=zeros(nen+1,nen+1); down1=zeros(nen+1,nen+1); % for debugging
    Jiee=zeros(nen+1,nen); Jsee=zeros(nen+1,nen);
end
sigdep=zeros(nen,1);
dAie=zeros(nen,nen); dAse=zeros(nen,nen);
ttot1=0; ttot2=0;
%% Cycle
for isp=1:length(species)
    SP=species(isp);
    Nsp=composition(isp);
    %% Cycle
    for v=1:SP.numxsec
        pname=strtrim(SP.procname(v,:));
        % Skip continuous loss
        if SP.iscontinuous(v)
            if debugflag>1
                disp([SP.name ', process ' num2str(v) ': ' pname ' -- skipping']);
            end
            continue
        end
        %% Inelastic collision rate
        n=SP.numpoints(v);
        es=SP.eloss(v);
        if debugflag>1
            if debugflag>2
                disp(' ');
            end
            disp([SP.name ', process ' num2str(v) ': ' pname ', es=' num2str(es)]);
        end
        % Inelastic collision rate nuie(en) will be used in expressions
        % like
        %    \int nuie(en) ne(en) d(en)
        % We implemented 2 approaches to approximating this integral, using
        % array of values nuie(k) at en(k):
        % 1. Less accurate approach: integrand is a spiky function
        % This is the default approach used in the ionization treatment. In
        % the inelastic/superalastic treatment, use option int_method=1
        % In this approach, nuie(en) is approximated as
        %    sum nuie(k)*delta(en-en(k))
        % The integral is a piecewise-constant function over intervals
        % [en(k),en(k+1)].
        % 2. More accurate approach: integrand is piecewise constant
        % Use option int_method=2. In this approach, nuie(en) is piecewise
        % constant in each interval [ene(k);ene(k+1)].
        % The integral is a a piecewise-linear continuous function over
        % intervals [en(k),en(k+1)].
        sig=1e-20*interp1(SP.e(1:n,v),SP.q(1:n,v),en);
        sig(isnan(sig))=0;
        nuie=Nsp.*sig.*veloc; % inelastic collision rate at en
        % nuie is zero at en<es which corresponds to en(1:kes). Note
        % that "kes" is different for two integration methods:
        if int_method==1
            kes=find(en<es,1,'last');
        elseif int_method==2
            % Since nuie(en)==0 at en<es, we must have nuie(k)==0 if
            % ene(k)<es.
            kes=find(ene<es,1,'last');
            if kes>nen
                kes=nen;
            end
        else
            error('unknown interpolation method');
        end
        nuie(1:kes)=0;
        %% Branching
        if SP.statwt(v)==-1
            %% Attachment
            if debugflag>2
                disp(['ATTACHMENT: ' pname])
            end
            % Attachment collision term is given by
            %    Qatt[n](en) == -nuie(en)*ne(en)
            if SP.numbodies(v)==1
                Aatt=Aatt-diag(nuie);
            else
                % Three-body
                Aatt3=Aatt3-diag(nuie);
            end
        elseif SP.statwt(v)==2
            %% Ionization
            if debugflag>2
                disp(['IONIZATION: ' pname])
            end
            % Ionization collision term is given by
            %    Qion[n](en) == -nuie(en)*ne(ne)
            %       + \int_{en+es}^inf Nuie(enp,en) ne(enp) d(enp)
            % where, in the differential rate Nuie(enp,en), enp is the
            % primary and en is the secondary energy. We do not distinguish
            % between "new" and "old" electron after collision, thus
            % en==0..enp-es.
            % We use
            %    Nuie(enp,en)=c*nuie(enp)*sigdep(enp,en)
            % where [Opal et al, 1971, doi:10.1063/1.1676707]
            %    c == 1/(EBR*atan((enp-es)/(2*EBR)))
            %    sigdep == 1/(1+min(en,enp-es-en)^2/EBR^2)
            % The normalization is such that
            %    nuie(enp) == \int_0^{(enp-es)/2} Nuie(enp,en)d(en)
            %       == (1/2) \int_0^{enp-es} Nuie(enp,en)d(en)
            %
            % The number of electron is not conserved, therefore we use the
            % first integration method by default.
            %
            % We can afford to do this here
            % because the addition of particles occurs over a wide
            % interval of energies, instead of a single energy as in the
            % inelastic case. If we did the same in the inelastic case, we
            % would miss the addition of particles at some energies.
            dAion=-diag(nuie);
            for k=kes:nen
                enp=en(k);
                kend=find(en<enp-es,1,'last'); % Nu_s ~= 0 at en(1:kend)
                if isempty(kend)
                    continue
                end
                kmid=find(en<(enp-es)/2,1,'last');
                % 1:kmid => en<enp-en-es
                % kmid+1:kend => en>enp-en-es
                % Dependence of sigma on the secondary energy
                sigdep(1:kmid)=1./(1+(en(1:kmid)/SP.EBR).^2);
                sigdep(kmid+1:kend)=1./(1+((enp-en(kmid+1:kend)-es)/SP.EBR).^2);
                %sigdep(kend:nen)=0; % unnecessary
                c=2./sum(sigdep(1:kend).*deni(1:kend));
                if debugflag>3
                    c0=1/(EBR(isp)*atan((enp-es)/(2*EBR(isp))));
                    disp([num2str(k) ': enp=' num2str(enp) ', k=(1,' ...
                        num2str(kmid) '),(' num2str(kmid+1) ',' num2str(kend) ...
                        '), exact=' num2str(c0) ', approx=' num2str(c)]);
                end
                dAion(1:kend,k)=deni(k)*c.*nuie(k).*sigdep(1:kend);
            end
            Aion=Aion+dAion;
        else
            %% Inelastic stattering
            % Represent the inelastic loss term as divergence of flux Jie.
            %    Qie[n](en)=-d(Jie)/d(en)
            %    Jie(en) == \int_{-inf}^en d(enp)
            %       [nuie(enp)ne(enp)-nuie(enp+es)ne(enp+es)]
            %       == Fie(en)-Fie(en+es)
            %    Fie(en)== \int_{-inf}^en nuie(enp)ne(enp) d(enp)
            % In terms of operators applied to ne(k) and with output at
            % ene(k):
            %    Fie(ene(k))=Int*diag(nuie)*ne
            %    Fie(ene(k)+es)=up*Int*diag(nuie)*ne
            %    Jie(ene(k))=Jiee*ne where
            %    Jiee=(eye(nen+1)-up)*Int*diag(nuie)
            % Analogously for superelastic:
            %    Jse(en) == \int_{-inf}^en d(enp)
            %       [nuse(enp)ne(enp)-nuse(enp-es)ne(enp-es)]
            %       == Fse(en)-Fse(en-es)
            %    Fse(en)== \int_{-inf}^en nuse(enp)ne(enp) d(enp)
            % where the superelastic collision rate is
            %    nuse(en)=nuie(en+es)*sqrt((en+es)/en)*exp(-es/T)
            % In operator terms
            %    Jse(ene(k))=Jsee*ne where
            %    Jsee=(eye(nen+1)-down)*Int*diag(nuse)
            %% Inelastic collision rate at en+es
            % - used to calculate the superelastic collision rate at en.
            if nuiep_method==1
                sigp=1e-20*interp1(SP.e(1:n,v),SP.q(1:n,v),en+es);
                sigp(isnan(sigp))=0;
                velocp=sqrt(2*ech*(en+es)/me);
                nuiep=Nsp.*sigp.*velocp; % at en+es
            elseif nuiep_method==2
                nuiep=interp1(en,nuie,en+es);
                nuiep(isnan(nuiep))=0;
            elseif nuiep_method==3
                % The way to calculate it that preserves the thermal
                % distribution
                error('nuiep_method==3 not implemented')
            else
                error('unknown method for calculation of nuie(en+es)');
            end
            % We must have Jse(enemax)==0 for the
            % conservation of the number of particles, i.e.
            %    Fse(enemax)==Fse(enemax-es)
            % This can only be achieved if nuse(en)=0 for en>enemax-es,
            % which corresponds to en(kstop:nen). Again, kstop is different
            % for two interpolation methods:
            if int_method==1
                kstop=find(en>enemax-es,1,'first');
            elseif int_method==2
                kstop=find(ene>enemax-es,1,'first')-1;
                if kstop==0
                    kstop=1; % avoid zero index
                end
            else
                error('unknown interpolation method');
            end
            nuiep(kstop:nen)=0;
            % Super-elastic collision rate at en
            nuse=nuiep.*sqrt((en+es)./en)*exp(-es/T0);
            %% Interpolation operators at ene+-es, with extension to +-inf
            if debugflag>0
                tstart=now*24*3600;
            end
            % Operators up(nen+1,nen+1) and down(nen+1,nen+1) perform the
            % operations of up- and down-shifting, with interpolation.
            % Consider a column vector f(nen+1,1) which defines a
            % function f(en). We assume that this function is extended with
            % constant values to +-inf and is either (1) constant or (2)
            % piecewise linear on each interval
            % [ene(k);ene(k+1)] depending on option int_method.
            % The operators "up" and "down" are defined as
            %    up*f==f(ene+es)
            %    down*f==f(ene-es)
            % The integers kpa, kma are indicating in what interval do the
            % values ene+-es fall, i.e.
            if int_method==1
                %    en(kpa(k)) < ene(k)+es < en(kpa(k)+1)
                %    en(kma(k)) < ene(k)-es < en(kma(k)+1)
                [ignore,kpa] = histc(ene+es,en);
                kpa(ene+es>enmax)=nen;
                [ignore,kma] = histc(ene-es,en);
                kma(ene-es<en(1))=0;
                kma(ene-es>enmax)=nen;
            elseif int_method==2
                %    ene(kpa(k)) < ene(k)+es < ene(kpa(k)+1)
                %    ene(kma(k)) < ene(k)-es < ene(kma(k)+1)
                [ignore,kpa] = histc(ene+es,ene);
                kpa(ene+es>enemax)=nen;
                [ignore,kma] = histc(ene-es,ene);
                kma(ene-es<ene(1))=1;
            else
                error('unknown interpolation method');
            end
            % To optimize for speed/space, "up"/"down" operators are
            % represented with 1D arrays u1,u2 and d1,d2, so that
            %    up(k,kpa(k))==d1(k); up(k,kpa(k)-1)==1-du(k);
            %    down(k,kma(k))==dd(k); down(k,kma(k)+1)==1-dd(k);
            % and all other elements are zero.
            if int_method==1
                % Trivial, all elements are 1
            elseif int_method==2
                enpa=min(ene+es,enemax);
                u1=(ene(kpa+1)-enpa)./deni(kpa);
                u2=(enpa-ene(kpa))./deni(kpa);
                enma=max(ene-es,ene(1));
                d1=(ene(kma+1)-enma)./deni(kma);
                d2=(enma-ene(kma))./deni(kma);
            else
                error('unknown interpolation method');
            end
            if use_ops
                % The "up" and "down" operators
                up(:)=0; down(:)=0;
                if int_method==1
                    % Piecewise constant
                    for k=1:nen+1
                        up(k,kpa(k)+1)=1;
                        down(k,kma(k)+1)=1;
                    end
                elseif int_method==2
                    % Piecewise linear
                    % Note that we always have kpa(k)+1>=k and kma(k)<=k
                    % and kpa(k)+1==k only when k==nen+1 and kma(k)==k only when k==1
                    for k=1:nen+1
                        enp=ene(k)+es;
                        kp=kpa(k);
                        if enp>enemax
                            up(k,kp+1)=1;
                        else
                            up(k,kp:kp+1)=[ene(kp+1)-enp enp-ene(kp)]/deni(kp);
                        end
                        enm=ene(k)-es;
                        km=kma(k);
                        if enm<ene(1)
                            down(k,km)=1;
                        else
                            down(k,km:km+1)=[ene(km+1)-enm enm-ene(km)]/deni(km);
                        end
                    end
                    % Make sure they are the same
                    up1(:)=0; down1(:)=0;
                    for k=1:nen+1
                        up1(k,kpa(k))=u1(k);
                        up1(k,kpa(k)+1)=u2(k);
                        down1(k,kma(k))=d1(k);
                        down1(k,kma(k)+1)=d2(k);
                    end
                    tmp=up-up1;
                    uperror=max(abs(tmp(:)));
                    tmp=down-down1;
                    downerror=max(abs(tmp(:)));
                    disp(['Errors: up=' num2str(uperror) ',down=' num2str(downerror)]);
                else
                    error('unknown interpolation method');
                end
            end
            if debugflag>0
                tnow=now*24*3600;
                ttot1=ttot1+tnow-tstart;
                tstart=tnow;
            end
            %% Divergence of the flux - have fun debugging!
            denii=deni.*nuie;
            denis=deni.*nuse;
            dAie(:)=0; dAse(:)=0;
            if int_method==1
                for k=1:nen
                    % dAie
                    kp0=kpa(k);
                    kp1=kpa(k+1); % kp1>=kp0
                    dAie(k,kp0+1:kp1)=denii(kp0+1:kp1).'/deni(k);
                    dAie(k,k)=dAie(k,k)-nuie(k);
                    % dAse
                    km0=kma(k);
                    km1=kma(k+1); % km1>=km0
                    dAse(k,km0+1:km1)=denis(km0+1:km1).'/deni(k);
                    dAse(k,k)=dAse(k,k)-nuse(k);
                end
            elseif int_method==2
                for k=1:nen
                    % dAie
                    kp0=kpa(k);
                    kp1=kpa(k+1); % kp1>=kp0
                    dAie(k,kp0)=dAie(k,kp0)-u2(k)*denii(kp0)/deni(k);
                    dAie(k,kp1)=dAie(k,kp1)-u1(k+1)*denii(kp1)/deni(k);
                    dAie(k,kp0:kp1)=dAie(k,kp0:kp1)+denii(kp0:kp1).'/deni(k);
                    dAie(k,k)=dAie(k,k)-nuie(k);
                    % dAse
                    km0=kma(k);
                    km1=kma(k+1); % km1>=km0
                    dAse(k,km0)=dAse(k,km0)-d2(k)*denis(km0)/deni(k);
                    dAse(k,km1)=dAse(k,km1)-d1(k+1)*denis(km1)/deni(k);
                    dAse(k,km0:km1)=dAse(k,km0:km1)+denis(km0:km1).'/deni(k);
                    dAse(k,k)=dAse(k,k)-nuse(k);
                end
            else
                error('unknown interpolation method');
            end
            if use_ops
                % Save the previous
                dAie1=dAie;
                dAse1=dAse;
                % Slow because "Int" is not sparse:
                Jiee1=(-up+eye(nen+1))*Int*diag(nuie);
                Jsee1=(-down+eye(nen+1))*Int*diag(nuse);
                % Faster calculation but have fun debugging!
                Jiee(:)=0; Jsee(:)=0;
                if int_method==1
                    % Piecewise constant
                    for k=1:nen+1
                        % Index ranges might be empty
                        Jiee(k,k:kpa(k))=-denii(k:kpa(k));
                        Jsee(k,kma(k)+1:k-1)=denis(kma(k)+1:k-1);
                    end
                elseif int_method==2
                    % Piecewise linear
                    for k=1:nen+1
                        if k~=nen+1
                            kp=kpa(k);
                            Jiee(k,k:kp-1)=-denii(k:kp-1); % could be empty
                            Jiee(k,kp)=-u2(k)*denii(kp);
                        end
                        if k~=1
                            km=kma(k);
                            Jsee(k,km+1:k-1)=denis(km+1:k-1); % could be empty
                            Jsee(k,km)=d1(k)*denis(km);
                        end
                    end
                else
                    error('unknown interpolation method');
                end
                % Check if they are the same
                tmp=Jiee-Jiee1;
                errJie=max(abs(tmp(:)))/max(abs(Jiee1(:)));
                tmp=Jsee-Jsee1;
                if max(abs(Jsee(:)))==0
                    errJse=0;
                else
                    errJse=max(abs(tmp(:)))/max(abs(Jsee1(:)));
                end
                disp(['Errors in Jie=' num2str(errJie) ', in Jse=' num2str(errJse)]);
                % Flux at bounds Jiee([1 nen+1],:), Jsee([1 nen+1],:) == 0
                disp(['Boundary fluxes: Jie=(' num2str(max(abs(Jiee(1,:)))) ...
                    ',' num2str(max(abs(Jiee(nen+1,:)))) '); Jse=(' ...
                    num2str(max(abs(Jsee(1,:)))) ',' ...
                    num2str(max(abs(Jsee(nen+1,:)))) ')']);
                if use_div
                    dAie=-Div*Jiee;
                    dAse=-Div*Jsee;
                else
                    dAie=-diag(1./deni)*(Jiee(2:nen+1,:)-Jiee(1:nen,:));
                    dAse=-diag(1./deni)*(Jsee(2:nen+1,:)-Jsee(1:nen,:));
                end
                % Make sure they are the same
                tmp=dAie-dAie1;
                errAie=max(abs(tmp(:)))/max(abs(dAie(:)));
                tmp=dAse-dAse1;
                if max(abs(dAse(:)))==0
                    errAse=0;
                else
                    errAse=max(abs(tmp(:)))/max(abs(dAse(:)));
                end
                disp(['Errors in Aie=' num2str(errAie) ', in Ase=' num2str(errAse)]);
            end
            if debugflag>0
                ttot2=ttot2+now*24*3600-tstart;
            end
            Aie=Aie+dAie;
            Ase=Ase+dAse;
         end
    end
end
if debugflag>0
    disp(['Interpolation operators: ' hms(ttot1) '; other: ' hms(ttot2)]);
end

