function [lat,lon]=astro_substar(ra,dec,utc)
%ASTRO_SUBSTAR Sub-star point on the Earth
% Usage:
%    [lat,lon]=astro_substar(ra,dec,utc);
% Inputs:
%    ra,dec - right ascension and declination of the star (in radians)
%    utc - Coordinated Universal Time (an array of 1x6, e.g. output of
%          DATEVEC)
% Outputs:
%    lat,lon - latitude and longitude of the substar point (in radians)
% See also: ASTRO_GMST
lat=dec;
lon=ra-astro_gmst(utc);
