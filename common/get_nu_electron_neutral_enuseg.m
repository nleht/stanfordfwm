function nue=get_nu_electron_neutral_enuseg(h,Te)
global ech kB species composition
if isempty(ech)
    loadconstants
end
if nargin<2
    Te=kB*getTn(h)/ech; % in eV
end
en=0.005*sinh([0.05:0.05:10].');
nen=length(en);
enmax=en(nen);
den=[en(1) ; diff(en) ; en(nen)-en(nen-1)]; % den(k)=en(k)-en(k-1)
%deni=(den(1:nen)+den(2:nen+1))/2; % for integration, = diff(ene)
ene=([0 ; en]+[en ; enmax+den(nen+1)])/2; % extended energy grid
enemax=ene(nen+1);
deni=diff(ene);

nue=zeros(size(h));
numom=getnumom(en,species,composition);
Nm=getNm(h); % in m^{-3}
for iz=1:length(h)
    T0=Te(iz); % in eV
    neT=sqrt(en).*exp(-en/T0)*sqrt(4/pi/T0^3); % Maxwell's distribution
    nue(iz)=Nm(iz)*sum(numom.*neT.*deni);
end
