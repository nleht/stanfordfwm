function nu=get_nu_electron_neutral_schunk_nagy80(Te,spec)
%nu=get_nu_electron_neutral_schunk_nagy80(Te,spec)
% Ratio to the species density in m^3
% Table 3 in Schunk and Nagy [1980, doi:10.1029/RG018i004p00813]
% Usage: nu=get_nu_electron_neutral_schunk_nagy80(Te,spec)
% where Te is electron temperature in K
% spec is 'N2', 'O2', 'O', 'He', 'H', 'CO', 'CO2' or 'air'.
switch spec
    case 'N2'
        nu=2.33e-11*(1-1.21e-4*Te).*Te;
    case 'O2'
        sqrtTe=sqrt(Te);
        nu=1.81e-10*(1+3.6e-2*sqrtTe).*sqrtTe;
    case 'O'
        nu=8.9e-11*(1+5.7e-4*Te).*sqrt(Te);
    case 'He'
        nu=4.6e-10*sqrt(Te);
    case 'H'
        nu=4.5e-9*(1-1.35e-4*Te).*sqrt(Te);
    case 'CO'
        nu=2.34e-11*(Te+165);
    case 'CO2'
        nu=3.68e-8*(1+4.1e-11*abs(4500-Te).^2.93);
    case 'air'
        nu=(get_nu_electron_neutral_schunk_nagy80(Te,'N2')*.8+...
            get_nu_electron_neutral_schunk_nagy80(Te,'O2')*.2)*1e6;
end
nu=nu/1e6; % convert from nu/N with N in cm^{-3} to nu/N with N in m^{-3}
