function x=myrefinegrid(x0,dxm)
%MYREFINEGRID Generate a grid with a given maximum step
% Usage:
%    x=myrefinegrid(x0,dxm)
% Inputs:
%    x0  - initial grid
%    dxm - maximum step allowed in the new grid
% Ouput:
%    x   - the new grid which has all the old points plus the refinement
x0save=x0;
x0=x0(:).';
dx=diff(x0);
nins=ceil(dx./dxm);
x=zeros(1,sum(nins)+1);
ind=cumsum([1 nins]);
x(ind)=x0;
% Insert new values
ii=find(nins>1);
for k=ii
    n=nins(k);
    x(ind(k)+1:ind(k)+n-1)=[1:n-1]*dx(k)/n+x(ind(k));
end
if size(x0save,1)>1
    x=x.';
end
