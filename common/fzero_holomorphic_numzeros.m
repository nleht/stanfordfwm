function [nzin,nzon,zb,fb,phase]=fzero_holomorphic_numzeros(fun,params,zcp,dzmax,dztol,nref,debugflag,prompt)
%% The polygon edge lengths
zcp=zcp(:); % connected polygon - array of vertices
nvert=length(zcp); % number of vertices
zcpnext=circshift(zcp,-1);
zrange=abs(zcpnext-zcp);
%% Arguments
if nargin<8
    prompt='';
end
if nargin<7
    debugflag=[];
end
if nargin<6
    nref=[];
end
if nargin<5
    dztol=[];
end
if nargin<4
    dzmax=[];
end
% Default values
prompt=[prompt 'FZERO_HOLOMORPHIC_NUMZEROS: '];
if isempty(debugflag)
    debugflag=0;
end
if isempty(nref)
    nref=10;
end
dzmax0=min(zrange)/nref;
if isempty(dzmax)
    dzmax=dzmax0;
end
if isempty(dztol)
    dztol=1e-6;
end
dz=min(dzmax0,dzmax);
%% Refine to dz
nbp=ceil(zrange/dz)+1; % number of boundary points at a given edge
i2=cumsum(nbp-1);
nbpt=i2(end); % the total number of boundary points (taking care of double counting)
i1=[1;i2(1:nvert-1)+1];
zb=zeros(nbpt,1); % the boundary points
for k=1:nvert
    tmp=linspace(zcp(k),zcpnext(k),nbp(k));
    zb(i1(k):i2(k))=tmp(1:nbp(k)-1);
end
%% Refine according to phase changes
dzmin=dz;
while 1
    fb=fun(zb,params{:});
    phase=unwrap([angle(fb)-angle(fb(1)); 0])/(2*pi);
    if dzmin<dztol
        break
    end
    ii=find(abs(diff(phase))>=0.2); % change has to be small
    ni=length(ii);
    if ni==0
        break
    else
        % Refine the boundary - insert more points
        nins=0;
        zbsave=[zb;zb(1)];
        for k=1:ni
            % zb(ii(k):ii(k)+1) -> tmp(2:nref-1)
            tmp=linspace(zbsave(ii(k)),zbsave(ii(k)+1),nref+1);
            ins=tmp(2:nref);
            zb=insert(zb,ins,ii(k)+nins);
            %fb=insert(fb,fun(ins,params{:}),ii(k)+nins);
            nins=nins+nref-1;
        end
        dzmin=min(abs(diff([zb;zb(1)])));
        if debugflag>0
            disp([prompt 'Refined mesh to dz=' num2str(dzmin)]);
        end
    end
end
%% Zeros on boundary
if dzmin<=dztol
    nzon=1;
    % There were zeros exactly on the boundary
    if debugflag>0
        disp([prompt 'Found roots on boundary, result may be inaccurate!']);
    end
    %zbl=[0;cumsum(abs(diff([zb;zb(1)])))];
else
    nzon=0;
end
nzin=round(phase(end));
if ~nzon && abs(nzin-phase(end))>1e-3
    error([prompt 'Phase calculation failure (' num2str(phase(end)) ' roots)'])
end
